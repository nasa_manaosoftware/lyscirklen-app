package com.modules.config;

import android.util.Log;

import androidx.annotation.Nullable;

//import com.facebook.react.BuildConfig;
import com.lyscirklen.BuildConfig;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;


import java.util.HashMap;
import java.util.Map;

public class ReactNativeConfig extends ReactContextBaseJavaModule {
    public ReactNativeConfig(ReactApplicationContext reactApplicationContext) {
        super(reactApplicationContext);
    }

    @Override
    public String getName() {
        return "ReactNativeConfig";
    }

    @Nullable
    @Override
    public Map<String, Object> getConstants() {
        final Map<String, Object> constants = new HashMap<>();
        constants.put("buildEnvironment", BuildConfig.ENVIRONMENT);

        if ( BuildConfig.APPLICATION_ID.contains("com.businessdanmark") ){
            constants.put("backgroundColor", "#040236");
            constants.put("playBackgroundColor", "#0049E4");
            constants.put("infoBackgroundColor", "#393861");
            constants.put("categoryBackgroundColor", "#0049E4");
            constants.put("loginBackgroundColor", "#FF5634");
            constants.put("loginDisableBackgroundColor", "#949494");
            constants.put("trashBackgroundColor", "#852318");
            constants.put("inputBackgroundColor", "#393861");
            constants.put("inputTextColor", "#FFFFFF");

            constants.put("aboutBackgroundColor", "#FF5634");
            constants.put("loginBackgroundColor", "#FF5634");
            constants.put("drawBackgroundColor", "#393861");
            constants.put("messageDetailBackgroundColor", "#156D99");
            constants.put("networkBackgroundColor", "#852318");
            constants.put("pushNotificationBackgroundColor", "#FF190C");
            constants.put("headerBackgroundColor", "#FF5634");
            constants.put("secondaryBackgroundColor", "#F1F1FF");
            constants.put("messagesText", "#040236");
            constants.put("DOWNLOAD_BACKGROUND_COLOR", "#FF5634");
            constants.put("VIDEO_SEEK_BAR_COLOR", "#FF5634");
            constants.put("CANCEL_TEXT_COLOR", "#01010E");
            constants.put("NETWORK_AWARENESS", "#852318");
            constants.put("DELETE_BUTTON_COLOR", "#852318");

            /* icon */
            constants.put("SELECTED_ICON", "BD_SELECTED_ICON");
            constants.put("UN_SELECTED_ICON", "BD_UN_SELECTED_ICON");
            constants.put("CLOSE_MODAL_ICON", "BD_CLOSE_MODAL_ICON");
            constants.put("HOME_ACTIVE_ICON", "BD_HOME_ACTIVE_ICON");
            constants.put("SEARCH_ACTIVE_ICON", "BD_SEARCH_ACTIVE_ICON");
            constants.put("MY_LIBRARY_ACTIVE_ICON", "BD_MY_LIBRARY_ACTIVE_ICON");
            constants.put("ABOUT_US_ACTIVE_ICON", "BD_ABOUT_US_ACTIVE_ICON");
            constants.put("SEARCH_ICON", "BD_SEARCH_ICON");

            /* logo image */
            constants.put("HEADER_LOGO", "BD_LOGO");

       } else {
            constants.put("backgroundColor", "#121C21");
            constants.put("playBackgroundColor", "#0096DB");
            constants.put("infoBackgroundColor", "#156D99");
            constants.put("categoryBackgroundColor", "#156D99");
            constants.put("loginBackgroundColor", "#156D99");
            constants.put("loginDisableBackgroundColor", "#939393");
            constants.put("trashBackgroundColor", "#FF190C");
            constants.put("inputBackgroundColor", "#FFFFFF");
            constants.put("inputTextColor", "#272727");

            constants.put("aboutBackgroundColor", "#156D99");
            constants.put("drawBackgroundColor", "#156D99");
            constants.put("messageDetailBackgroundColor", "#FF5634");
            constants.put("networkBackgroundColor", "#FF190C");
            constants.put("pushNotificationBackgroundColor", "#FF190C");
            constants.put("headerBackgroundColor", "#156D99");
            constants.put("secondaryBackgroundColor", "#F7F8F9");
            constants.put("messagesText", "#272727");
            constants.put("DOWNLOAD_BACKGROUND_COLOR", "#156D99");
            constants.put("VIDEO_SEEK_BAR_COLOR", "#156D99");
            constants.put("CANCEL_TEXT_COLOR", "#121C21");
            constants.put("NETWORK_AWARENESS", "#FF190C");
            constants.put("DELETE_BUTTON_COLOR", "#D2271E");

             /* icon */
            constants.put("SELECTED_ICON", "ML_SELECTED_ICON");
            constants.put("UN_SELECTED_ICON", "ML_UN_SELECTED_ICON");
            constants.put("CLOSE_MODAL_ICON", "ML_CLOSE_MODAL_ICON");
            constants.put("HOME_ACTIVE_ICON", "ML_HOME_ACTIVE_ICON");
            constants.put("SEARCH_ACTIVE_ICON", "ML_SEARCH_ACTIVE_ICON");
            constants.put("MY_LIBRARY_ACTIVE_ICON", "ML_MY_LIBRARY_ACTIVE_ICON");
            constants.put("ABOUT_US_ACTIVE_ICON", "ML_ABOUT_US_ACTIVE_ICON");
            constants.put("SEARCH_ICON", "ML_SEARCH_ICON");

            /* logo image */
            constants.put("HEADER_LOGO", "ML_LOGO");
    }

        return constants;
    }
}
