import { IUser } from "../../models/user-store/user.types"
import { BaseServicesController } from "./base-request.controller"

class AuthServices extends BaseServicesController {
    public emptyTask = async (body: Partial<IUser>) => {
    }

}

export default new AuthServices()