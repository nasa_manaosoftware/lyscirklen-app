import { IUser } from "../../models/user-store/user.types"
import { ServiceType } from "../../constants/service.constant"
import { APIRequestParams } from "../api/config/api.router-config.types"
import { BaseServicesController } from "./base-request.controller"

class UserServices extends BaseServicesController {

    public updateDeviceToken = async (token: string) => {
        const requestParams: Partial<APIRequestParams> = {
            body: {
                token
            }
        }
        return await this.requestAPI(ServiceType.UpdateDeviceToken, requestParams)
    }
 
    public signOut = async (token: string) => {
        const params = {
            token
        }
        const requestParams: Partial<APIRequestParams> = {
            body: { ...params }
        }
        return await this.requestAPI(ServiceType.SignOut, requestParams)
    }

}

export default new UserServices()