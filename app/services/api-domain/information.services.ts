import { ServiceType } from "../../constants/service.constant"
import { BaseServicesController } from "./base-request.controller"

class InformationServices extends BaseServicesController {
    public fetchServerDateTime = async () => {
        const result = this.requestAPI(ServiceType.GetServerDateTime)
        return result
    }
}
export default new InformationServices()