import { APIRequestParams } from "../api/config/api.router-config.types"
import { ServiceType } from "../../constants/service.constant"
import { BaseServicesController } from "./base-request.controller"

class SearchAudioServices extends BaseServicesController {

    public searchAudioBook = async (params: any) => {
        const requestParams: Partial<APIRequestParams> = {
            params
        }
        return await this.requestAPI(ServiceType.SearchAudioBook, requestParams)
    }

}

export default new SearchAudioServices()