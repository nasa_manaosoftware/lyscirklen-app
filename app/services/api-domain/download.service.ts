import { Alert, Platform } from "react-native";
import { BaseServicesController } from "./base-request.controller";
import RNFS, { DownloadBeginCallbackResult, DownloadProgressCallbackResult, stopDownload } from 'react-native-fs'
import { RootStore } from "../../models/root-store/root.types";
import { DownloadStatus } from "../../constants/app.constant";

class DownloadServices extends BaseServicesController {
    
    public downloadedAudioBook = async (rootStore: RootStore, params) => {
        const url = params.video.download_url
        const id = params.uid
        const fileType = params.video.name.split('.')

        const downloadFailed = () => {
            const audioBooks = rootStore?.getAudioBooksStore?.getAudiobooksList
            let data = []
            audioBooks.map(item => {
                if (item.uid === params.uid) {
                    data.push({
                        ...item,
                        download_status: DownloadStatus.DO_NOT_DOWNLOAD,
                        is_downloading: false
                    })
                } else {
                    data.push({
                        ...item
                    })
                }
            })
            rootStore?.getAudioBooksStore.setAudiobooksList(data)

            const downloadedData = {
                download_status: DownloadStatus.FAILED
            }
            return downloadedData
        }

        try {
            if (url) {
                const options = {
                    fromUrl: url,
                    toFile: Platform.OS === 'ios' ? RNFS.DocumentDirectoryPath + "/" + id + "." + fileType[1] : RNFS.ExternalDirectoryPath + "/" + id + "." + fileType[1],
                    background: false,
                    progressInterval: 100,
                    progressDivider: 10,
                    begin: (res: DownloadBeginCallbackResult) => {
                    },
                    progress: (res: DownloadProgressCallbackResult) => {
                    }
                }

                const result = RNFS.downloadFile(options)
                    .promise.then((result) => {
                        if (result) {
                            let jobId
                            let filePath

                            jobId = result.jobId
                            if (Platform.OS === 'ios') {
                                filePath = RNFS.DocumentDirectoryPath + "/" + id + "." + fileType[1]
                            } else {
                                filePath = RNFS.ExternalDirectoryPath + "/" + id + "." + fileType[1]
                            }
                            const audioBooks = rootStore?.getAudioBooksStore?.getAudiobooksList
                            let data = []
                            audioBooks.map(item => {
                                    if (item.uid === params.uid) {
                                        data.push({
                                            ...item,
                                            download_status: DownloadStatus.SUCCESS,
                                            is_downloading: false, 
                                            file_path: Platform.OS === 'ios' ? RNFS.DocumentDirectoryPath + "/" + id + "." + fileType[1] : filePath = RNFS.ExternalDirectoryPath + "/" + id + "." + fileType[1]
                                        })
                                    } else {
                                        data.push({
                                            ...item
                                        })
                                    }
                            })
                            rootStore?.getAudioBooksStore?.setAudiobooksList(data)

                            const downloadedData = {
                                job_id: jobId,
                                download_status: DownloadStatus.SUCCESS,
                                file_path: filePath
                            }

                            return downloadedData
                        } else {
                            return downloadFailed()
                        }
                    })
                    .catch((err) => {
                        console.log(err);
                        return downloadFailed()
                    })
                return result
            } else {
                return downloadFailed()
            }
        } catch (e) {
            console.log(e)
            return downloadFailed()
        }
    }

    public deleteAudioBook = async (rootStore: RootStore, params) => {
        
        const result = RNFS.unlink(params.file_path)
            .then(() => {
                const audioBooks = rootStore?.getAudioBooksStore.getAudiobooksList
                let updateAudioBooks = []
                audioBooks.map(item =>
                    item.uid === params.uid ?
                        updateAudioBooks.push({
                            ...item,
                            download_status: DownloadStatus.DO_NOT_DOWNLOAD,
                            is_downloading: false
                        })
                        :
                        updateAudioBooks.push(item)
                )
                rootStore?.getAudioBooksStore.setAudiobooksList(updateAudioBooks)
                return 'Delete success'
            })
            .catch((err) => {
                console.log(err.message);
                return err.message
            })
        return result
    }
}

export default new DownloadServices()