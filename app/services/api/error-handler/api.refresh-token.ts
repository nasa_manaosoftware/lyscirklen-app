import { AxiosError, AxiosRequestConfig, AxiosPromise } from "axios"
import { api } from "../config/api.config"
// import { useStores } from "../../../models/root-store"
import { IAuthResponse } from "../../../models/auth-store/auth.types"
import { HeaderType } from "../../../constants/service.constant"
import FireBaseAuthServices from "../../../utils/firebase/authentication/auth"

let isRefreshing = false
let failedQueue = []
let originalRequest: AxiosRequestConfig

const processQueue = (error: string, token: string = null) => {
    for (const promise of failedQueue) {
        error ?
            promise.reject(error) :
            promise.resolve(token)
    }
    failedQueue = []
}

// expiredToken : TESTING purpose only
// const expiredToken =  'eyJhbGciOiJSUzI1NiIsImtpZCI6ImUwOGI0NzM0YjYxNmE0MWFhZmE5MmNlZTVjYzg3Yjc2MmRmNjRmYTIiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vYnVzaW5lc3MtZGFubWFyay1hdWRpb2Jvb2stZGV2IiwiYXVkIjoiYnVzaW5lc3MtZGFubWFyay1hdWRpb2Jvb2stZGV2IiwiYXV0aF90aW1lIjoxNjEwMDc3NjgzLCJ1c2VyX2lkIjoiMTAwMDA4Iiwic3ViIjoiMTAwMDA4IiwiaWF0IjoxNjEwMDc4MTAyLCJleHAiOjE2MTAwODE3MDIsImVtYWlsIjoiMTAwMDA4QGJ1c2luZXNzLWRhbm1hcmsuY29tIiwiZW1haWxfdmVyaWZpZWQiOmZhbHNlLCJmaXJlYmFzZSI6eyJpZGVudGl0aWVzIjp7ImVtYWlsIjpbIjEwMDAwOEBidXNpbmVzcy1kYW5tYXJrLmNvbSJdfSwic2lnbl9pbl9wcm92aWRlciI6InBhc3N3b3JkIn19.mug9H9eLJRPnwBLdzB6IOEY0_VbQP1FpkR4ePoaQZCeFKng91UA-Z0EiWVJtQrIBE6Ujf-DdYhN-fn328FbBGhsIwkeA5YVvIErXutdhPumwcBzXYEmIeubuHa7EHsalFSxMk8o7Lfw_KvF9o9y9Jn9Tgx7qK0aBK8MdMa6e9cJH5dpMWofku0vuSEA5WuhFtW4GysH9_J_mcIb5RhMPH3GDZ8TNSMBLFOvN1OOL6aMA1_W5hxb6uAE0pnryld4OkXY6DkAWG_jUP5MVsoW3dRnW5GHL0LYk7KPOlzvCX3MEQEB_1lI0mneEVkl5CD4xH6uyH0-GULfkdvpCmYuCMg'

const setRefreshedToken = (token: string) => originalRequest.headers[HeaderType.Authorization] = HeaderType.Bearer + ' ' + token

const pushToFailedQueue = async (): Promise<string> => await new Promise<string>((resolve, reject) => failedQueue.push({ resolve, reject }))

const refreshTokenAndExecuteQueue = async (): Promise<AxiosPromise<IAuthResponse>> => await new Promise<AxiosPromise<IAuthResponse>>(async (resolve, reject) => {

    // const rootStore = useStores()
    // const { getAuthStore } = rootStore

    //const localToken = getAuthStore?.getToken //Get Token from LocalStorage(old token) to send to api to refresh

    try {
        //Request API for Refreshing
        // const data: IAuthResponse = (await api.post<IAuthResponse>("refreshtokenAPI", { token: localToken })).data

        //Firebase Auth refresh token
        const token = await FireBaseAuthServices.getFirebaseToken()
        // const data = { token: token }

        //Save Token to LocalStorage
        // getAuthStore?.setToken(data)
        // setRefreshedToken(getAuthStore?.getToken)
        // processQueue(null, getAuthStore?.getToken)
        setRefreshedToken(token)
        processQueue(null, token)
        resolve(api(originalRequest))
    }
    catch (e) {
        processQueue(e, null)
        reject(e)
    }
    finally {
        isRefreshing = false
    }
})

export const refreshTokenManager = async (error: AxiosError<any>) => {
    originalRequest = error.config
    if (isRefreshing) {
        try {
            const token: string = await pushToFailedQueue()
            setRefreshedToken(token)
            return api(originalRequest)
        }
        catch (e) {
            return Promise.reject(e)
        }
    }
    originalRequest.headers._retry = true
    isRefreshing = true
    return refreshTokenAndExecuteQueue()
}