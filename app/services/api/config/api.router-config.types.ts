export type APIRequestParams = {
    body: any,
    params: any,
    queryParams: string
}