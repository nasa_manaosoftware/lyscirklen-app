import { ServiceType } from "../../../constants/service.constant"
import { AxiosRequestConfig } from "axios"
import { APIRequestParams } from "./api.router-config.types"

export const router = (type: ServiceType, params?: Partial<APIRequestParams>): AxiosRequestConfig => {
    let router: AxiosRequestConfig = {}
    const queryParams: string = params?.queryParams
    const paramsObject = params?.params
    const data = params?.body && JSON.stringify(params?.body)
    const version = 'v1.0/mobile'
    switch (type) {

        case ServiceType.UpdateDeviceToken:
            router.method = 'patch'
            router.url = version + '/users/device-token'
            router.data = data
            break
    
        case ServiceType.SignOut:
            router.method = 'patch'
            router.url = version + '/users/signout'
            router.data = data
            break
            
        case ServiceType.SearchAudioBook:
            router.method = 'get'
            router.url = version + '/audiobooks/searchAudiobooks'
            router.params = paramsObject
        break

        case ServiceType.GetServerDateTime:
            router.method = 'get'
            router.url = version + '/server/datetime'
            break
    
        default: break
    }
    return router
}