export type IResponse = {
    data?: any;
    errors?: string;
}