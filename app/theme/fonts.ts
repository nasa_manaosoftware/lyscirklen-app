import { RFPercentage } from "react-native-responsive-fontsize";
import * as metric from './metric';
import { RFValue } from "react-native-responsive-fontsize";

const size = {
    header: RFValue(22),
    extraLarge: RFValue(18),
    large: RFValue(16),
    medium: RFValue(2.7),
    small: RFValue(2.5),
    tiny: RFValue(1),
    errorMessage: RFValue(2),
}

const type = {
    base: 'Roboto-Regular',
    bold: 'Roboto-Bold',
    italic: 'Roboto-Italic',
    title: metric.isMicrolearn() ? 'Nunito-SemiBold' : 'Teko-SemiBold',
    titleBookname: metric.isMicrolearn() ? 'Nunito-Regular' : 'Teko-SemiBold',
    emphasis: metric.isMicrolearn() ? 'Nunito-SemiBold' : 'PTSans-Bold'
}

export {
    size,
    type
}