import { palette } from "./palette"
import { NativeModules } from 'react-native'
const { ReactNativeConfig } = NativeModules

/**
 * Roles for colors.  Prefer using these over the palette.  It makes it easier
 * to change things.
 *
 * The only roles we need to place in here are the ones that span through the app.
 *
 * If you have a specific use-case, like a spinner color.  It makes more sense to
 * put that in the <Spinner /> component.
 */
export const color = {
  /**
   * The palette is available to use, but prefer using the name.
   */
  palette,
  /**
   * A helper for making something see-thru. Use sparingly as many layers of transparency
   * can cause older Android devices to slow down due to the excessive compositing required
   * by their under-powered GPUs.
   */
  transparent: "rgba(0, 0, 0, 0)",
  /**
   * The screen background.
   */
  // background: palette.white,
  /**
   * The main tinting color.
   */
  primary: palette.orange,
  /**
   * The main tinting color, but darker.
   */
  primaryDarker: palette.orangeDarker,
  /**
   * A subtle color used for borders and lines.
   */
  line: palette.offWhite,
  /**
   * The default color of text in many components.
   */
  text: palette.white,
  /**
   * Secondary information.
   */
  dim: palette.lightGrey,
  /**
   * Error messages and icons.
   */
  error: palette.angry,

  /**
   * Storybook background for Text stories, or any stories where
   * the text color is color.text, which is white by default, and does not show
   * in Stories against the default white background
   */
  storybookDarkBg: palette.black,

  /**
   * Storybook text color for stories that display Text components against the
   * white background
   */
  storybookTextColor: palette.black,

  background: "#121C21",
  backgroundActive: ReactNativeConfig.drawBackgroundColor,
  redLine: ReactNativeConfig.headerBackgroundColor,
  redDot: '#CE3937',
  redBadge: '#FF190C',
  whiteText: '#FFFFFF',
  categoryButton: '#0049E4',
  messagesBackground: ReactNativeConfig.secondaryBackgroundColor,
  white: "#FFFFFF",
  lightPurple: ReactNativeConfig.secondaryBackgroundColor,
  medium: ReactNativeConfig.NETWORK_AWARENESS,
  textTerm: ReactNativeConfig.messagesText,
  green: '#22A540',
  darkPurple: '#393861',
  black: '#000000',
  lightRed: '#EC1A1A',
  buttonDisabled: '#949494',
  darkRed: ReactNativeConfig.DELETE_BUTTON_COLOR,
  fileUploadEnable: ReactNativeConfig.DOWNLOAD_BACKGROUND_COLOR,
  fileUploadDisable: '#B8B8B8',
  playBackground: ReactNativeConfig.playBackgroundColor,
  infoBackground: ReactNativeConfig.infoBackgroundColor,
  categoryBackground: ReactNativeConfig.categoryBackgroundColor,
  loginBackground: ReactNativeConfig.loginBackgroundColor,
  loginDisableBackground: ReactNativeConfig.loginDisableBackgroundColor,
  trashBackground: ReactNativeConfig.trashBackgroundColor,
  inputBackground: ReactNativeConfig.inputBackgroundColor,
  inputText: ReactNativeConfig.inputTextColor,
  aboutBackground: ReactNativeConfig.aboutBackgroundColor,
  drawBackground: ReactNativeConfig.drawBackgroundColor,
  messageDetailBackground: ReactNativeConfig.messageDetailBackgroundColor,
  networkBackground: "#C9252D",
  pushNotificationBackground: ReactNativeConfig.pushNotificationBackgroundColor,
  headerBackground: ReactNativeConfig.headerBackgroundColor,
  messagesText: ReactNativeConfig.messagesText,
  downloadBackgroundColor: ReactNativeConfig.DOWNLOAD_BACKGROUND_COLOR,
  seekBarColor: ReactNativeConfig.VIDEO_SEEK_BAR_COLOR,
  cancelText: ReactNativeConfig.CANCEL_TEXT_COLOR,
  messageHighlight: "#245167",
  buttonGradient: ['#156D99', '#138FCE'],
  whiteGRV: "#FFFFFF",
  textMessage: "#FFFFFF"
}
