import { StyleSheet } from 'react-native';
import { FULL } from "../../styles/styles"

export default StyleSheet.create({
  backdrop: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    opacity: 0,
    backgroundColor: 'black',
  },
  content: {
    ...FULL,
    justifyContent: 'center',
  },
  containerBox: {
    zIndex: 2,
    opacity: 1,
    backgroundColor: 'transparent',
  },
});
