import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    Animated,
    TouchableOpacity,
    Linking
} from 'react-native'
import * as Animatable from 'react-native-animatable'

import * as metric from "../../theme"
import { color } from "../../theme"
import { RFPercentage, RFValue } from "react-native-responsive-fontsize"
import { Icon, Button } from "../../components"
import { images } from '../../theme/images';
import { FULL, FONT_REGULAR, FONT_LIGHT } from "../../styles/styles"
import * as StringUtils from '../../utils/string.utils'
import HTML from 'react-native-render-html'
import { type } from '../../theme/fonts'

class MemberLabel extends Component {
    constructor(props) {
        super(props);

        let initialPadding = 0;
        let initialOpacity = 0;

        if (this.props.visible) {
            initialPadding = 0;
            initialOpacity = 1;
        }

        this.state = {
            paddingAnim: new Animated.Value(initialPadding),
            opacityAnim: new Animated.Value(initialOpacity)
        }
    }

    UNSAFE_componentWillReceiveProps(newProps) {
        const animatedOptions = {
            useNativeDriver: false,
            duration: 230
        }
        Animated.timing(this.state.paddingAnim, {
            toValue: newProps.visible ? 0 : 1,
            ...animatedOptions
        }).start();

        return Animated.timing(this.state.opacityAnim, {
            toValue: newProps.visible ? 1 : 0,
            ...animatedOptions
        }).start();
    }

    render() {
        return (
            <Animated.View shouldRasterizeIOS useNativeDriver style={[styles.floatingLabel, { paddingTop: this.state.paddingAnim, opacity: this.state.opacityAnim }]}>
                {this.props.children}
            </Animated.View>
        );
    }
}

class MemberLabelTextField extends Component {
    constructor(props) {
        super(props);
        this.state = {
            focused: false,
            text: this.props.value
        };
    }

    UNSAFE_componentWillReceiveProps(newProps) {
        if (newProps.hasOwnProperty('value') && newProps.value !== this.state.text) {
            this.setState({ text: newProps.value })
        }
    }

    leftPadding(isDisabled) {
        return {
            width: this.props.leftPadding || metric.ratioWidth(20),
            // backgroundColor: isDisabled ? color.disabledTextField : color.palette.white
            // backgroundColor: '#393861',
            backgroundColor: color.inputBackground,
        }
    }

    withBorder() {
        if (!this.props.noBorder) {
            return styles.withBorder;
        }
    }

    renderDropdownIconIfNeeded(onTouch) {
        return this.props.isDropDown &&
            <Button
                isAnimated={false}
                isSolid
                preset="none"
                imageSource={images.down}
                style={styles.iconModalContainer}
                imageStyle={styles.downArrow}
                onPress={onTouch} />
    }

    renderMultitpleSelectionIconIfNeeded(onTouch) {
        return this.props.isMultipleSelectionDropDown &&
            <Button
                isAnimated={false}
                isSolid
                preset="none"
                imageSource={images.plus}
                style={styles.iconModalContainer}
                imageStyle={styles.plus}
                onPress={onTouch} />
    }

    renderTouchableView(onTouch) {
        return (this.props.onTouchTextField || this.props.onTouchTextFieldXL)
            && <TouchableOpacity
                style={this.props.onTouchTextFieldXL ? styles.touchableViewXL : styles.touchableView}
                onPress={onTouch} activeOpacity={1} />
    }

    renderDescriptionViewIfNeeded() {
        return this.props.description &&
            <View shouldRasterizeIOS style={styles.descriptionContainer}>
                <Text {...this.props.testIDDescription} style={styles.descriptionText}>{this.props.description}</Text>
            </View> || null
    }

    renderErrorMessageIfNeeded() {

        const openUrl = (link) => {
            Linking.canOpenURL(link).then(supported => {
                if (!supported) {
                    //console.log('Can\'t handle url: ' + link);
                } else {
                    Linking.openURL(link)
                        .catch(err => {
                            console.warn('openURL error', err);
                        });
                }
            }).catch(err => console.warn('An unexpected error happened', err))
        }

        return this.props.errorMessage &&
            <Animatable.View shouldRasterizeIOS useNativeDriver animation="fadeIn" style={styles.errorContainer}
                {...this.props.testIDErrorMessage}
            >
                <Icon icon="warning" containerStyle={styles.warningContainer} style={styles.warning} />
                <HTML
                    containerStyle={styles.HTML_VIEW}
                    tagsStyles={{
                        p: {
                            paddingVertical: 0,
                            fontFamily: type.base,
                            fontSize: RFValue(14),
                            color: color.white
                        },
                        h1: {
                            paddingVertical: 0,
                            fontFamily: type.base,
                            fontWeight: Platform.OS === 'ios' ? 'bold' : 'normal'
                        },
                        h2: {
                            paddingVertical: 0,
                            fontFamily: type.base,
                            fontWeight: Platform.OS === 'ios' ? 'bold' : 'normal'
                        },
                        a: {
                            paddingVertical: 0,
                            fontFamily: type.base,
                            fontSize: RFValue(14),
                            color: color.white
                        }
                    }}
                    ignoredStyles={["font-family", "letter-spacing", "font-size", "color"]}
                    html={this.props.errorMessage}
                    onLinkPress={(event, href) => openUrl(href)} />
            </Animatable.View> || null
    }


    renderMultipleSelectionViewIfNeeded(onTouch) {
        return this.props.multipleSelectionItems && <Animatable.View shouldRasterizeIOS useNativeDriver animation="fadeIn" duration={400} style={{ ...styles.multipleSelectionContainer, ...this.props.containerStyle }}
            {...this.props.testIDMultipleSelectionItems}
        >
            <View shouldRasterizeIOS style={styles.SEPARATOR_VIEW} />
            <TouchableOpacity onPress={onTouch} activeOpacity={1} style={styles.multitpleSelectiontouchableView}>
                <Text style={styles.multipleSelectionText}>{this.props.multipleSelectionItems}</Text>
            </TouchableOpacity>
        </Animatable.View> || null
    }
    render() {
        const textfieldOptions = this.props.textfieldOptions
        const paddingStyle = this.props.multipleSelectionItems ? styles.multitpleSelectionPaddingView : styles.paddingView
        const containerStyle = [paddingStyle, this.leftPadding(this.props.disabled),
            this.props.errorMessage && styles.paddingViewError || styles.valueTextNormal]
        const valueTextStyle = this.props.multipleSelectionItems ? styles.multitpleSelectionValueText : styles.valueText
        const textfieldStyle = [valueTextStyle,
            this.props.disabled === true ? styles.textFieldDisabled : styles.valueTextNormal,
            this.props.errorMessage && styles.valueTextError || styles.valueTextNormal]
        const onTouch = () => this.props.onTouchTextField && this.props.onTouchTextField() ||
            this.props.onTouchTextFieldXL && this.props.onTouchTextFieldXL()
        return (
            <View shouldRasterizeIOS style={{ ...styles.container, ...this.props.containerStyle }}>
                <View shouldRasterizeIOS style={styles.viewContainer}>
                    <MemberLabel visible={this.state.text}>
                        <Text numberOfLines={1} style={[styles.fieldLabel, this.labelStyle()]}>{this.placeholderValue()}</Text>
                    </MemberLabel>
                    <View shouldRasterizeIOS style={containerStyle} />
                    <View shouldRasterizeIOS style={[styles.fieldContainer, this.withBorder()]}>
                        <TextInput {...this.props}
                            autoCompleteType="off"
                            autoCorrect={false}
                            ref='input'
                            underlineColorAndroid={color.palette.clear}
                            style={textfieldStyle}
                            defaultValue={this.props.defaultValue}
                            value={this.props.value}
                            maxLength={this.props.maxLength}
                            onFocus={() => this.setFocus()}
                            onBlur={() => this.unsetFocus()}
                            onChangeText={(value) => this.setText(value)}
                            selectionColor={color.inputText}
                            numberOfLines={this.props.maxLength || 1}
                            allowFontScaling
                            placeholderTextColor='#979797CF'
                        />
                        {/* {this.renderDropdownIconIfNeeded(onTouch)}
                        {this.renderMultitpleSelectionIconIfNeeded(onTouch)}
                        {this.renderTouchableView(onTouch)} */}
                    </View>
                </View>
                {/* {this.renderDescriptionViewIfNeeded()} */}
                {this.renderErrorMessageIfNeeded(onTouch)}
                {/* {this.renderMultipleSelectionViewIfNeeded(onTouch)} */}
            </View>
        );
    }

    clearErrorMessage() {
        // this.props.errorMessage = null
    }

    inputRef() {
        return this.refs.input;
    }

    focus() {
        this.inputRef().focus();
    }

    blur() {
        this.inputRef().blur();
    }

    isFocused() {
        return this.inputRef().isFocused();
    }

    clear() {
        this.inputRef().clear();
    }

    setFocus() {
        this.setState({
            focused: true
        });
        try {
            return this.props.onFocus();
        } catch (_error) { }
    }

    unsetFocus() {
        this.setState({
            focused: false
        });
        try {
            return this.props.onBlur();
        } catch (_error) { }
    }

    labelStyle() {
        if (this.state.focused) {
            return styles.focused;
        }
    }

    placeholderValue() {
        if (this.state.text) {
            return this.props.placeholder;
        }
    }

    setText(value) {
        this.setState({
            text: value
        });
        try {
            return this.props.onChangeTextValue(value);
        } catch (_error) { }
    }
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        width: metric.ratioWidth(320)
    },
    viewContainer: {
        flexDirection: 'row',
    },
    paddingView: {
        height: metric.ratioHeight(50),
        width: metric.ratioWidth(0),
        backgroundColor: 'white',
        borderTopLeftRadius: metric.ratioWidth(6),
        borderBottomLeftRadius: metric.ratioWidth(6),
        marginTop: metric.ratioHeight(30),
    },
    multitpleSelectionPaddingView: {
        height: metric.ratioHeight(50),
        width: metric.ratioWidth(0),
        backgroundColor: 'white',
        borderTopLeftRadius: metric.ratioWidth(6),
        marginTop: metric.ratioHeight(30),
    },
    paddingViewNormal: {
        borderLeftWidth: 0,
        borderTopWidth: 0,
        borderBottomWidth: 0,
    },
    paddingViewError: {
        //Error
        borderTopColor: color.palette.red,
        borderLeftColor: color.palette.red,
        borderBottomColor: color.palette.red,

        borderLeftWidth: metric.ratioWidth(2),
        borderTopWidth: metric.ratioWidth(2),
        borderBottomWidth: metric.ratioWidth(2),
    },
    floatingLabel: {
        position: 'absolute',
        top: metric.ratioHeight(6),
        left: metric.ratioWidth(6)
    },
    fieldLabel: {
        fontFamily: type.base,
        height: metric.ratioHeight(21),
        fontSize: RFValue(13),
        color: color.palette.white,
        marginLeft: metric.ratioWidth(-6)
    },
    fieldContainer: {
        ...FULL,
        justifyContent: 'center',
        position: 'relative',
        marginTop: metric.ratioHeight(30)
    },
    withBorder: {
        // borderBottomWidth: 1 / 2,
    },
    multitpleSelectionValueText: {
        ...FONT_REGULAR,
        marginTop: metric.ratioHeight(30),
        height: metric.ratioHeight(50),
        fontSize: RFValue(17),
        color: color.palette.darkGrey,
        paddingVertical: !metric.isIPhone ? metric.ratioHeight(11) : 0,
        backgroundColor: color.palette.white,
        borderTopRightRadius: metric.ratioWidth(6),
        paddingRight: metric.ratioWidth(40),
    },
    valueText: {
        ...FONT_REGULAR,
        height: metric.ratioHeight(50),
        fontSize: RFValue(18),
        // color: color.palette.white,
        color: color.inputText,
        paddingVertical: !metric.isIPhone ? metric.ratioHeight(11) : 0,
        backgroundColor: color.inputBackground,
        borderBottomRightRadius: metric.ratioWidth(6),
        borderTopRightRadius: metric.ratioWidth(6)
    },
    valueTextWithIcon: {
        ...FONT_REGULAR,
        marginTop: metric.ratioHeight(30),
        height: metric.ratioHeight(50),
        fontSize: RFValue(17),
        paddingVertical: !metric.isIPhone ? metric.ratioHeight(11) : 0,
        backgroundColor: color.palette.white,
        borderBottomRightRadius: metric.ratioWidth(6),
        borderTopRightRadius: metric.ratioWidth(6),
        marginRight: 100
    },
    valueTextError: {
        //Error
        borderTopColor: color.palette.red,
        borderRightColor: color.palette.red,
        borderBottomColor: color.palette.red,
        borderRightWidth: metric.ratioWidth(2),
        borderTopWidth: metric.ratioWidth(2),
        borderBottomWidth: metric.ratioWidth(2),
    },

    valueTextNormal: {
        borderLeftWidth: 0,
        borderTopWidth: 0,
        borderBottomWidth: 0,
    },
    focused: {
        color: color.palette.white
    },
    descriptionContainer: {
        marginTop: metric.ratioHeight(6),
        width: metric.ratioWidth(300),
        marginBottom: metric.ratioHeight(7),
        paddingLeft: metric.ratioWidth(3),
    },
    descriptionText: {
        ...FONT_REGULAR,
        fontSize: RFValue(11),
        color: color.palette.white,
        lineHeight: metric.ratioHeight(17),
    },
    errorContainer: {
        flexDirection: 'row',
        marginTop: metric.ratioHeight(20),
        backgroundColor: color.palette.red,
        borderRadius: metric.ratioHeight(30 / 2),
        width: metric.ratioWidth(320),
        minHeight: metric.ratioHeight(30),
        paddingLeft: metric.ratioWidth(10),
        paddingVertical: metric.ratioHeight(12),
        paddingRight: metric.ratioWidth(35),
    },
    errorText: {
        ...FONT_REGULAR,
        fontSize: RFValue(13),
        padding: 0,
        color: color.palette.white,
        marginLeft: metric.ratioWidth(9.23),
        width: '90%',
        marginVertical: metric.ratioHeight(5),
    },
    multipleSelectionContainer: {
        flexDirection: 'column',
        backgroundColor: color.palette.white,

        borderBottomRightRadius: metric.ratioHeight(30 / 2),
        borderBottomLeftRadius: metric.ratioHeight(30 / 2),
        width: metric.ratioWidth(300),
        minHeight: metric.ratioHeight(50),
        paddingHorizontal: metric.ratioWidth(21),
        justifyContent: 'center',
    },
    warningContainer: {

    },
    warning: {
        width: metric.ratioWidth(14.53),
        height: metric.ratioWidth(14.53),
        marginTop: 5
    }, iconModalContainer: {
        position: 'absolute',
        bottom: metric.ratioHeight(14),
        right: metric.ratioWidth(20),
        zIndex: 1,
        justifyContent: 'center'
    },
    downArrow: {
        width: metric.ratioWidth(16),
        height: metric.ratioHeight(18),
    },
    plus: {
        width: metric.ratioWidth(18),
        height: metric.ratioWidth(18),
    },
    touchableView: {
        position: 'absolute',
        left: metric.ratioWidth(-19.8),
        right: 0,
        bottom: 0,
        width: metric.ratioWidth(300),
        height: metric.ratioHeight(50),
        backgroundColor: color.clear,
        zIndex: 2
    },
    touchableViewXL: {
        position: 'absolute',
        left: metric.ratioWidth(-19.8),
        right: 0,
        bottom: 0,
        width: metric.ratioWidth(344),
        height: metric.ratioHeight(50),
        backgroundColor: color.clear,
        zIndex: 2,
    },
    multitpleSelectiontouchableView: {
        ...FULL,
        justifyContent: 'center'
    },
    textFieldDisabled: {
        ...FULL,
        backgroundColor: color.disabledTextField
    },
    multipleSelectionText: {
        ...FONT_REGULAR,
        fontSize: RFValue(12),
        color: color.palette.darkGrey,
    },
    SEPARATOR_VIEW: {
        height: metric.ratioHeight(1),
        backgroundColor: color.boder,
        width: metric.ratioWidth(344),
        marginLeft: -metric.ratioWidth(20)
    },
    HTML_VIEW: {
        paddingLeft: metric.ratioWidth(7),
        marginTop: metric.ratioHeight(3)
    }
});

export default MemberLabelTextField;