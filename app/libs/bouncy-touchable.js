import React, { Component } from 'react'
import { Animated, PanResponder, View } from 'react-native'
import PropTypes from 'prop-types'
import debounce from 'lodash.debounce'

const truty = () => true
const noop = () => { }

export class BouncyView extends Component {
  static propTypes = {
    onPress: PropTypes.func,
    scale: PropTypes.number,
    moveSlop: PropTypes.number,
    delay: PropTypes.number
  }

  static defaultProps = {
    onPress: noop,
    scale: 1.1, // Max scale of animation
    moveSlop: 15, // Slop area for press
    delay: 40 // Animation delay in miliseconds
  }

  state = {
    scale: new Animated.Value(1)
  }

  UNSAFE_componentWillMount() {
    const { buttonAnimatedOptions,
      onBeginAnimatedButtonTouch,
      onEndButtonAnimation,
      buttonSpringOptions,
      animatedButtonDebounceOptions } = this.props
    this.debouncedOnPress = (fn, evt) => fn && fn(evt)
    this.onEndButtonAnimationDebounce = debounce(this.debouncedOnPress, animatedButtonDebounceOptions.delay, { leading: true, trailing: false })

    this.panResponder = PanResponder.create({
      onStartShouldSetPanResponder: truty,
      onStartShouldSetPanResponderCapture: truty,
      onMoveShouldSetPanResponder: truty,
      onMoveShouldSetPanResponderCapture: truty,
      onPanResponderTerminationRequest: truty,
      onPanResponderTerminate: noop,
      onPanResponderGrant: () => {
        onBeginAnimatedButtonTouch()
        Animated.timing(
          this.state.scale,
          {
            toValue: buttonAnimatedOptions.scale,
            ...buttonSpringOptions
          }
        ).start()
      },

      onPanResponderRelease: (evt, gestureState) => {
        const moveSlop = buttonAnimatedOptions.moveSlop
        const isOutOfRange = gestureState.dy > moveSlop || gestureState.dy < (-moveSlop) || gestureState.dx > moveSlop || gestureState.dx < (-moveSlop)

        if (!isOutOfRange) {
          setTimeout(() => {
            Animated.spring(
              this.state.scale,
              {
                toValue: 1,
                ...buttonSpringOptions
              }
            ).start(() => {
              this.onEndButtonAnimationDebounce(onEndButtonAnimation, evt)
            })
          }, buttonAnimatedOptions.delay)
        } else {
          Animated.timing(
            this.state.scale,
            {
              toValue: 1,
              useNativeDriver: false
            }
          ).start()
        }
      }
    })
  }

  render() {
    const { scale } = this.state
    const { children, style, ...rest } = this.props

    return (
      <Animated.View
        style={[{
          transform: [
            {
              scale
            }
          ]
        }, style
        ]} {...rest}>

        <View{...this.panResponder.panHandlers}>
          {children}
        </View>
      </Animated.View>
    )
  }
}