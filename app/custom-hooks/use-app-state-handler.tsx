import { FunctionComponent, useEffect } from "react"
import { AppState, Keyboard } from "react-native"
import { observer } from "mobx-react-lite"
import { useStores } from "../models/root-store"

interface GlobalAppStateHandlerProps {
}

export const AppStateHandler: FunctionComponent<GlobalAppStateHandlerProps> = observer(props => {
    const rootStore = useStores()
    useEffect(() => {
        AppState.addEventListener('change', handleChange)
        return () => AppState.removeEventListener('change', handleChange)
    }, [])

    const handleChange = (newState: any) => {
        rootStore?.setAppState(newState)
        if (newState !== "active") {
            Keyboard.dismiss()
        } else if ( newState === "background") {
            console.log('newState ', newState);
            
            rootStore?.setIsFetchingResources(true)
        }
    }
    return (props as any).children
})
