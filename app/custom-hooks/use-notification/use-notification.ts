import { useEffect } from "react"
import { NotificationController } from "./notification.controller"
import { RootStore } from "../../models/root-store/root.types"
import PushNotification from "react-native-push-notification"
import NotificationHandler from "./notification.handler"
import PushNotificationIOS from "@react-native-community/push-notification-ios"

export interface NotificationCompletion {
    badge?: boolean
    alert?: boolean
    sound?: boolean
}

export const useNotification = (rootStore?: RootStore) => {
    const controller = new NotificationController(rootStore)
    useEffect(() => {
        console.log('useEffect useNotification');
        controller.requestNotification()
        controller.registerNotificationReceivedForeground()
        // controller.registerNotificationOpened()

        PushNotification.configure({
            // (required) Called when a remote is received or opened, or local notification is opened
            onNotification: function (notification) {
                console.log("NOTIFICATION @@:", notification);
              // process the notification
              // (required) Called when a remote is received or opened, or local notification is opened
              notification.finish(PushNotificationIOS.FetchResult.NoData);

              if (notification.userInteraction) {
                NotificationHandler.handleNotificationPayloadByUserActions(rootStore, notification)
                controller.registerNotificationOpened(notification)
              }
            }
        })

        controller.registerNotificationReceivedBackground()
        controller.getInitialNotification()

        

        return () => {
            NotificationController.forgroundMessagingDisposer && NotificationController.forgroundMessagingDisposer()
        }
    }, [])
}
