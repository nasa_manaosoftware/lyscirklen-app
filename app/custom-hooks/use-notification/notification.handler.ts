import { RootStore } from "../../models/root-store/root.types"
import { NavigationKey } from "../../constants/app.constant"
import { InfoNotification, INotification } from "../../models/notification-store/notification-store.types"
import * as DataUtils from "../../utils/data.utils"
import { FirebaseMessagingTypes } from "@react-native-firebase/messaging"
import PushNotification from "react-native-push-notification"

class NotificationHandler {

    public handleNotificationPayload = (rootStore?: RootStore, notification?: FirebaseMessagingTypes.RemoteMessage): boolean => {
        const data: INotification = DataUtils.transformNotificationData(notification)
        const currentScreen: string = rootStore?.getNavigationStore?.getCurrentPageName
        const previousScreen: string = rootStore?.getNavigationStore?.getPreviousPageName
        const notificationStore = rootStore?.getNotificationStore
        const notificationId = data?.notification_id
        let isInSamePage: boolean = false
        let shouldShowForeground: boolean = true
        return true
    }

    public handleNotificationPayloadByUserActions = (rootStore?: RootStore, remoteMessage?: FirebaseMessagingTypes.RemoteMessage) => {

        console.log('handleNotificationPayloadByUserActions=> ', remoteMessage);
        
        const data: InfoNotification = DataUtils.transformNotificationData(remoteMessage) 
        console.log('data test', JSON.stringify(data));
        this._notificationRouter(rootStore, data)
    }

    private _notificationRouter = (rootStore?: RootStore, params?: InfoNotification) => {
        const notificationStore = rootStore?.getNotificationStore
        const userStore = rootStore?.getUserStore
        const notificationId = params?.push_notification_id
        const navigationStore = rootStore?.getNavigationStore
        const navigation = navigationStore?.getNavigation
        let baseParams = params

        console.log('params: ', params)
        console.log('params: ', navigation)

        navigation.navigate(NavigationKey.MessageDetail, { 
                ...params,
                uid: params?.audiobook_id,
                heading: params?.heading,
                message: params?.message,
                notification_id: params?.push_notification_id,
                button_text: params?.button_text
            })

            notificationStore?.markReadNotificationById(notificationId)
    }

    public handleNotificationBadgeNumber = (notification?: FirebaseMessagingTypes.RemoteMessage) => {
        PushNotification.setApplicationIconBadgeNumber(notification.notification?.android?.count)
    }
}

export default new NotificationHandler()

