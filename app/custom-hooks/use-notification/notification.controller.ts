import NotificationServices from "../../utils/firebase/notification/notification"
import NotificationHandler from "./notification.handler"
import { RootStore } from "../../models/root-store/root.types"
import message, { FirebaseMessagingTypes } from '@react-native-firebase/messaging'
import { Platform } from "react-native"
import PushNotification from "react-native-push-notification"
import PushNotificationIOS from "@react-native-community/push-notification-ios"
// import MessagesController from "../../modules/messages/messages.controller"
import { useConfigurate } from "../use-configure-controller"

export class NotificationController {
    private _rootStore: RootStore
    public static forgroundMessagingDisposer: () => void
    private isIos = Platform.OS === "ios"
    // public controller = useConfigurate(MessagesController) as MessagesController

    constructor(rootStore: RootStore) {
        this._rootStore = rootStore
    }

    public getFCMToken = async () => {
        const token: string = await NotificationServices.getToken()
        this.setAndUpdateFCMToken(token)
    }

    public requestNotification = async () => {
        await this.registerDeviceForRemoteMessages()
        await this.getFCMToken()
        await NotificationServices.subscribeTopic()
        this.onTokenRefresh()
    }

    public registerDeviceForRemoteMessages = async () => {
        if (!NotificationServices.isDeviceRegisteredForRemoteMessages) {
            await NotificationServices.registerDeviceForRemoteMessages()
            await NotificationServices.requestPermission()
        }
    }
    public onTokenRefresh = () => {
        NotificationServices.onTokenRefresh(token => {
            console.log("FCM Token Received: ", token)
            this.setAndUpdateFCMToken(token)
        })
    }

    public setAndUpdateFCMToken = (token: string,) => {
        this._rootStore?.UserStore?.updateDeviceToken(token)
        this._rootStore?.UserStore?.setDeviceToken(token)
    }

    public registerNotificationReceivedForeground = () => {
        NotificationController.forgroundMessagingDisposer = NotificationServices.foregroundStateMessage(async (remoteMessage: FirebaseMessagingTypes.RemoteMessage) => {
            console.log("foreground notification", remoteMessage)
            const isShowForeground: boolean = NotificationHandler.handleNotificationPayload(this._rootStore, remoteMessage)
            console.log('isShowForeground', isShowForeground);

            if (isShowForeground) NotificationServices.createLocalNotification(remoteMessage)
            console.log('connn', remoteMessage.data.push_notification_id);
            // this.controller.onRefreshMessage()
            // this.controller.onSetUnReadMessage(remoteMessage.data.push_notification_id)
            if (!this.isIos) {
                NotificationHandler.handleNotificationBadgeNumber(remoteMessage)
            } else {
                const newBadgeCount = remoteMessage.data?.badge ? remoteMessage.data?.badge : this._rootStore.getUserStore.getBadgeNumber
                PushNotificationIOS.setApplicationIconBadgeNumber(newBadgeCount)
            }
            // const newBadgeCount = remoteMessage.data?.badge ? remoteMessage.data?.badge : this._rootStore.getUserStore.getBadgeNumber
            // if (!this.isIos) {
            //     // PushNotification.setApplicationIconBadgeNumber(newBadgeCount)
            //     // ShortcutBadge.getCount((count) => {
            //     ShortcutBadge.setCount(newBadgeCount);
            //     // });
            // } else {
            //     PushNotificationIOS.setApplicationIconBadgeNumber(newBadgeCount)
            // }
        })
    }

    public registerNotificationOpened = async (notification) => {
        // this.controller.onSetUnReadMessage(notification.data.push_notification_id)
    }

    public registerNotificationReceivedBackground = () => {
        NotificationServices.setBackgroundMessageHandler(async (remoteMessage: FirebaseMessagingTypes.RemoteMessage) => {
            console.log("background notification", remoteMessage)
            // this.controller.onRefreshMessage()
            const newBadgeCount = remoteMessage.data?.badge ? remoteMessage.data?.badge : this._rootStore.getUserStore.getBadgeNumber
            if (!this.isIos) {
                PushNotification.setApplicationIconBadgeNumber(newBadgeCount)
                // ShortcutBadge.setCount(newBadgeCount);
            } else {
                PushNotificationIOS.setApplicationIconBadgeNumber(newBadgeCount)
            }
        })
    }

    public getInitialNotification = () => {
        message().getInitialNotification()
            .then((remoteMessage: FirebaseMessagingTypes.RemoteMessage) => {
                console.log("Initial notification was:", (remoteMessage ? remoteMessage : 'N/A'))
                if (remoteMessage) {
                    NotificationHandler.handleNotificationPayloadByUserActions(this._rootStore, remoteMessage)
                }
            })
            .catch((err) => console.log("getInitialNotifiation() failed", err));
    }
}