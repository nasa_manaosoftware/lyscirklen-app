import { FunctionComponent, useEffect } from "react"
import { Alert, BackHandler, ToastAndroid } from "react-native"
import { observer } from "mobx-react-lite"
import { useStores } from "../models/root-store"
import { ListenerKey, NavigationKey } from "../constants/app.constant"
import { GeneralResources } from "../constants/firebase/remote-config"

interface GlobalBackButtonHandlerProps {
}

export const GlobalBackButtonHandler: FunctionComponent<GlobalBackButtonHandlerProps> = observer(props => {

  // MARK: Global Variables

  const rootStore = useStores()
  const locale = rootStore?.getLanguage

  const { SharedKeys } = GeneralResources

  useEffect(() => {

    const onBackPress = () => {

      const message = rootStore.getGeneralResourcesStore(locale, NavigationKey.ShardedKeys, SharedKeys.backHandleMessage, true)
      const ok = rootStore.getGeneralResourcesStore(locale, NavigationKey.ShardedKeys, SharedKeys.backHandleOK, true)
      const cancel = rootStore.getGeneralResourcesStore(locale, NavigationKey.ShardedKeys, SharedKeys.backHandleCancel, true)

      if (rootStore.getNavigationStore.canExit && !rootStore.getSharedStore.getIsEditing) {
        Alert.alert(
          "",
          message,
          [
            {
              text: ok,
              onPress: () => BackHandler.exitApp()
            },
            {
              text: cancel,
              onPress: () => console.log('Cancel Pressed'),
            }
          ],
          {
            cancelable: false
          }
        )
        // BackHandler.exitApp()
      } else {
        return false
      }
      return true
    }
    BackHandler.addEventListener(ListenerKey.HardwareBackPress, onBackPress)
    return () => BackHandler.removeEventListener(ListenerKey.HardwareBackPress, onBackPress)
  }, [])
  return (props as any).children
})
