import { useEffect, useCallback } from 'react'
import { useStores } from '../models/root-store'
import { useFocusEffect, ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"
import { IRoute } from '../models'
import { DrawerContentComponentProps, DrawerContentOptions } from '@react-navigation/drawer'
import { MaterialTopTabBarProps } from '@react-navigation/material-top-tabs'
import { BaseController } from '../modules/base.controller'
export type BaseNavigationProps =
    | NavigationContainerProps<ParamListBase>
    | DrawerContentComponentProps<DrawerContentOptions>
    | MaterialTopTabBarProps & string

export type NavigationProps = {
    navigation?: NavigationContainerProps<ParamListBase>,
    route?: IRoute
}

export const useConfigurate = (controller: typeof BaseController, props?: BaseNavigationProps) => {
    const rootStore = useStores()
    const ctrl = new controller(rootStore, props)
    const setupNotification = () => {
        const routeProps = props as NavigationProps
        
    }
    ctrl.viewWillAppearOnce && ctrl.viewWillAppearOnce()
    setupNotification()
    useEffect(() => {
        const viewDidAppearOnce = async () => await ctrl.viewDidAppearOnce()
        viewDidAppearOnce()
        return () => ctrl.deInit()
    }, [])
    try {
        useFocusEffect(
            useCallback(() => {
                const viewDidAppearAfterFocus = async () => {
                    await ctrl.viewDidAppearAfterFocus(props)
                }
                ctrl.viewDidAppearAfterFocus && viewDidAppearAfterFocus()
                return () => ctrl.viewWillDisappear && ctrl.viewWillDisappear()
            }, [props])
        )
    } catch (e) {
    }
    return ctrl
}