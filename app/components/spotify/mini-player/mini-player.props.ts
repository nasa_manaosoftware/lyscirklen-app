
export interface MiniPlayerProps {
  onShowPlayerPress: () => void
  onPlayPausePress: () => void
}