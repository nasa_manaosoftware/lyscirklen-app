import React from "react";
import { Text, TouchableWithoutFeedback, View } from "react-native"
import FastImage from "react-native-fast-image";
import * as Styles from "./mini-player.styles"
import { images } from "../../../theme/images"
import { TouchableOpacity } from "react-native-gesture-handler"
import { observer } from "mobx-react-lite"
import { MiniPlayerProps } from "./mini-player.props"
import TextTicker from 'react-native-text-ticker'
import { useStores } from "../../../models/root-store";
import * as metric from '../../../theme'

export const MiniPlayer: React.FunctionComponent<MiniPlayerProps> = observer(props => {
  const { onShowPlayerPress } = props
  const rootStore = useStores()
  const playbackStore = rootStore?.getPlaybackStore
  return (
    <View>
      <View style={Styles.CONTAINER}>

        <TouchableWithoutFeedback onPress={onShowPlayerPress}>
          <View style={Styles.BOOK_COVER_CONTAINER}>
            <FastImage source={ playbackStore.getAudiobook?.image?.url ? { uri: playbackStore.getAudiobook?.image?.url } : images.defaultImage } style={Styles.BOOK_COVER} resizeMode={'contain'} />
          </View>
        </TouchableWithoutFeedback>

        <TouchableWithoutFeedback onPress={onShowPlayerPress}>
          <View style={Styles.SUB_CONTAINER}>
            <TextTicker style={Styles.TITLE}
              duration={10000}
              loop
              bounce
              scroll={false}
              repeatSpacer={50}
              marqueeDelay={3000}
              numberOfLines={1}
            >{metric.isMicrolearn() ? playbackStore.getAudiobook?.name : playbackStore.getAudiobook?.name.toUpperCase()}</TextTicker>
            <TextTicker style={Styles.SUB_TITLE}
              duration={10000}
              loop
              bounce
              scroll={false}
              repeatSpacer={50}
              marqueeDelay={3000}
              numberOfLines={1}>
              {playbackStore.getAudiobook?.forfatter}</TextTicker>

          </View>
        </TouchableWithoutFeedback>
        <View style={Styles.CONTROL_PLAY_PAUSE_CONTAINER}>
          <TouchableOpacity onPress={props?.onShowPlayerPress}>
            <FastImage source={images.expand} style={Styles.CONTROL_PLAY_PAUSE} />
          </TouchableOpacity>
        </View>
      </View>
    </View>
  )
})
