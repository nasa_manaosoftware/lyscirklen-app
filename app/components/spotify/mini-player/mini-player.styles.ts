import { ViewStyle, TextStyle, ImageStyle } from "react-native"
import * as metric from "../../../theme"
import { RFValue } from "react-native-responsive-fontsize"
import { color } from "../../../theme"
import { FULL, FONT_REGULAR } from "../../../styles/styles"
import { type } from "../../../theme/fonts"


export const SAFE_AREA_VIEW: ViewStyle = {
  ...FULL
}

export const CONTAINER: ViewStyle = {
  height: metric.ratioHeight(104),
  backgroundColor: color.background,
  paddingHorizontal: metric.ratioWidth(23),
  paddingVertical: metric.ratioHeight(5),
  flexDirection: 'row',
}

export const BOOK_COVER_CONTAINER: ViewStyle = {
  width: metric.ratioWidth(82),
  alignContent: 'center',
  alignSelf: 'center'
}

export const BOOK_COVER: ImageStyle = {
  width: metric.ratioWidth(61),
  height: metric.ratioHeight(92)
}

export const TITLE: TextStyle = {
  fontFamily: type.bold,
  fontSize: RFValue(18),
  color: color.text,
}

export const SUB_TITLE: TextStyle = {
  fontFamily: type.base,
  marginTop: metric.ratioHeight(6),
  fontSize: RFValue(16),
  color: color.text,
}

export const SUB_CONTAINER: ViewStyle ={
  marginRight: metric.ratioWidth(17),
  flex: 1.0,
  justifyContent: 'center'
}

export const CONTROL_PLAY_PAUSE_CONTAINER: ViewStyle = {
  justifyContent: 'center',
  flex: 0.5,
  alignContent: 'center'
}

export const CONTROL_PLAY_PAUSE: ImageStyle = {
  justifyContent: 'center',
  alignSelf: 'flex-end',
  width: metric.ratioWidth(15),
  height: metric.ratioWidth(15)
}