'use strict'
import React, { useRef, useState } from "react"
import {
  ActivityIndicator,
  SafeAreaView,
  StyleSheet,
  View, Text
} from "react-native"
import * as Styles from "./video-player.styles"
import Video from "react-native-video"
import { PlayerProps } from "./video-player.props"
import { observer } from "mobx-react-lite"
import { DownloadStatus } from "../../../constants/app.constant"
import { useStores } from "../../../models/root-store"
import FirebaseAnalytics from "../../../utils/firebase/analytics/analytics";
import RNFS from 'react-native-fs'
import { color } from "../../../theme/color"
import { VideoControl } from "./views/video-control-view/video-control"

import YouTubePlayer from "react-native-youtube-sdk";


export const VideoPlayer: React.FunctionComponent<PlayerProps> = observer(props => {
  // const { onCollapse, onClose } = props
  const rootStore = useStores()
  const playbackStore = rootStore?.getPlaybackStore
  const audioBook = playbackStore?.getAudiobook
  const player = props?.player
  let isOnBuffer: boolean

  let yp = useRef(null)

  const queryCategoryName = (book_categoryId) => {
    const categoryStore = rootStore?.getAudioBookCategoriesStore?.getAudiobooksCetegories
    var categoryName = ""
    categoryStore.map(category => {
      if (category.uid === book_categoryId) {
        categoryName = category.name
      }
    })
    return categoryName
  }

  const playCompletedAnalytic = () => {
    var category_name = queryCategoryName(audioBook?.category_id)
    const params = {
      user_number: rootStore?.getUserStore?.getUserId,
      book_name: audioBook?.name,
      category_name: category_name,
    }

    FirebaseAnalytics.videoPlayCompleted(params)
  }

  const onLoad = (data) => {
    playbackStore?.onLoaded(data?.duration)
    player?.current?.seek(0)
  }

  const onEnded = (data) => {

    //firebase analytic
    !playbackStore?.getIsEnded && playCompletedAnalytic()

    // player?.current?.seek(0)
    playbackStore?.onEnded()

    playbackStore?.onBuffering(false)
  }

  const onBuffer = (data) => {
    playbackStore?.onBuffering(data.isBuffering)
  }

  const onPlaybackRateChange = (event) => {
    playbackStore?.setIsEnded(false)

    // if (!playbackStore?.getIsLoadDone) return
    if (event?.playbackRate === 0) {
      playbackStore?.setIsPause(true)
      playbackStore?.setIsSeeking(false)
      if (rootStore?.getAppState !== 'active')
        playbackStore?.setPlayPause(true)
    } else if (event?.playbackRate === 1) {
      // if(playbackStore?.getIsDone && playbackStore?.getIsPause) {
      //   player?.current?.seek(0)
      //   playbackStore?.playAfterEnd()
      // }
      playbackStore?.setIsPause(false)
      playbackStore?.setIsSeeking(false)
    }
  }

  const getSource = () => {
    // FileSystem.readFileByPath(audioBook?.file_path).then((result) => { console.log(result) })
    switch (audioBook?.download_status) {
      case DownloadStatus.SUCCESS:
        const filePath = 'file:///'.concat(audioBook?.file_path)
        if (RNFS.exists(filePath) && audioBook?.file_path) {
          return { uri: filePath }
        } else {
          return undefined
        }
      case DownloadStatus.DO_NOT_DOWNLOAD:
        return { uri: audioBook?.video?.download_url ? audioBook?.video?.download_url : audioBook?.video?.url }
      default:
        return undefined
    }
  }

  const parseVideoId = (url: any) => {
    let u: string = url
    if (u !== undefined) {
      url = u.split(/(vi\/|v=|\/v\/|vi=|\/vi\/|youtu\.be\/|\/embed\/|\/1\/|\/2\/)/);
      return (url[2] !== undefined) ? url[2].split(/[^0-9a-z_\-]/i)[0] : url[0];
    }
    return u
  }
  const testVideoId = (url: any) => {
    var urls = [
      '//www.youtube-nocookie.com/embed/up_lNV-yoK4?rel=0',
      'http://www.youtube.com/user/Scobleizer#p/u/1/1p3vcRhsYGo',
      'http://www.youtube.com/watch?v=cKZDdG9FTKY&feature=channel',
      'http://www.youtube.com/watch?v=yZ-K7nCVnBI&playnext_from=TL&videos=osPknwzXEas&feature=sub',
      'http://www.youtube.com/ytscreeningroom?v=NRHVzbJVx8I',
      'http://www.youtube.com/user/SilkRoadTheatre#p/a/u/2/6dwqZw0j_jY',
      'http://youtu.be/6dwqZw0j_jY',
      'http://www.youtube.com/watch?v=6dwqZw0j_jY&feature=youtu.be',
      'http://youtu.be/afa-5HQHiAs',
      'http://www.youtube.com/user/Scobleizer#p/u/1/1p3vcRhsYGo?rel=0',
      'http://www.youtube.com/watch?v=cKZDdG9FTKY&feature=channel',
      'http://www.youtube.com/watch?v=yZ-K7nCVnBI&playnext_from=TL&videos=osPknwzXEas&feature=sub',
      'http://www.youtube.com/ytscreeningroom?v=NRHVzbJVx8I',
      'http://www.youtube.com/embed/nas1rJpm7wY?rel=0',
      'http://www.youtube.com/watch?v=peFZbP64dsU',
      'http://youtube.com/v/dQw4w9WgXcQ?feature=youtube_gdata_player',
      'http://youtube.com/vi/dQw4w9WgXcQ?feature=youtube_gdata_player',
      'http://youtube.com/?v=dQw4w9WgXcQ&feature=youtube_gdata_player',
      'http://www.youtube.com/watch?v=dQw4w9WgXcQ&feature=youtube_gdata_player',
      'http://youtube.com/?vi=dQw4w9WgXcQ&feature=youtube_gdata_player',
      'http://youtube.com/watch?v=dQw4w9WgXcQ&feature=youtube_gdata_player',
      'http://youtube.com/watch?vi=dQw4w9WgXcQ&feature=youtube_gdata_player',
      'http://youtu.be/dQw4w9WgXcQ?feature=youtube_gdata_player',
      'abcdefg'
    ];
    console.log('--------------------');

    for (var i = 0; i < urls.length; ++i) {
      console.log(parseVideoId(urls[i]));
    }

    console.log('--------------------');
  }

  const checkState = (e: any) => {
    if (e.state === 'ENDED') {
      playCompletedAnalytic()
    } 
  }


  return (
    <SafeAreaView style={Styles.CONTAINER}>
      <View style={Styles.VDO_CONTAINER}>
        {(parseVideoId(audioBook?.video_url) == null) ? null :
          (<YouTubePlayer
            ref={ref => (yp = ref)}
            videoId={parseVideoId(audioBook?.video_url)}
            autoPlay={true}
            fullscreen={false}
            showFullScreenButton={true}
            showSeekBar={true}
            showPlayPauseButton={true}
            startTime={5}
            style={{ width: "100%", height: 200 }}
            onError={e => console.log(e)}
            onChangeState={e => checkState(e)}
            onChangeFullscreen={e => console.log(e)}
          />)}
      </View>
      <VideoControl player={player} onPlayPausePress={props?.onPlayPausePress} />
    </SafeAreaView>
  )
})