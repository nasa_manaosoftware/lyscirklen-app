'use strict'
import React from "react"
import {
  Text,
  TouchableOpacity,
  View,
  Platform
} from "react-native"
import * as Styles from "./video-control.styles"
import { VideoControlProps } from "./video-control.props"
import { observer } from "mobx-react-lite"
import FastImage from "react-native-fast-image"
import { images } from "../../../../../theme/images"
import { useStores } from "../../../../../models/root-store"
import moment from "moment"
import Slider from "react-native-slider"
import { color } from "../../../../../theme"
import * as metric from '../../../../../theme'

export const VideoControl: React.FunctionComponent<VideoControlProps> = observer(props => {
  // const { onCollapse, onClose } = props
  const rootStore = useStores()
  const playbackStore = rootStore?.getPlaybackStore
  const audioBook = playbackStore?.getAudiobook
  const { player } = props
  const isIos = Platform.OS === "ios"

  const secondsToMinutesSeconds = (second: number) => {
    return moment.utc((second) * 1000).format('mm:ss')
  }


  return (
    <View style={Styles.VDO_CONTROL_CONTAINER}>
      <Text style={Styles.TITLE}>{metric.isMicrolearn() ? audioBook?.name : audioBook?.name.toUpperCase()}</Text>
      <Text style={Styles.SUB_TITLE}>{audioBook?.forfatter}</Text>
      {/* <Text style={Styles.SUB_TITLE}>{audioBook?.video_url}</Text> */}
      {/* <TouchableOpacity onPress={() => {
        props?.onPlayPausePress()
      }}>
        <FastImage source={playbackStore?.getIsPause ? images.playWhite : images.pause} style={Styles.VDO_CONTROL_BUTTON} />
      </TouchableOpacity> */}
      {/* 
      <TouchableOpacity style={Styles.FULL_SCREEN} onPress={() => {
        player?.current?.presentFullscreenPlayer()
        // playbackStore?.setIsPause(!playbackStore?.getIsPause)
        // if (playbackStore?.getIsDone) {
        //   player?.current?.seek(0)
        //   playbackStore?.playAfterEnd()
        //   // playbackStore?.setIsDone(false)
        // } else {
        //   playbackStore?.setIsPause(!playbackStore?.getIsPause)
        // }
        // player?.current?.seek(playbackStore?.getCurrentDuration)
      }}>
         <Text style={Styles.TITLE}>{'Enter Fullscreen'}</Text>
        
      </TouchableOpacity> */}

      {/* <View style={Styles.VDO_SEEK_CONTAINER}>
        <Slider
          style={Styles.VDO_SEEK}
          minimumValue={0}
          maximumValue={playbackStore?.getVideoDuration ? isIos ? playbackStore?.getVideoDuration : playbackStore?.getVideoDuration + 1 : 0}
          minimumTrackTintColor={color.seekBarColor}
          maximumTrackTintColor="#FFFFFF"
          trackStyle={Styles.VDO_SEEK_TRACK}
          value={playbackStore?.getVideoDuration ? isIos ? playbackStore?.getCurrentDuration : playbackStore?.getCurrentDuration + 1 : 0}
          // disabled={playbackStore?.getIsDone}
          onSlidingStart={() => {
            playbackStore?.setIsSeeking(true)
          }}
          onSlidingComplete={(value) => {
              player?.current.seek(value)
              if(playbackStore?.getIsPause) {
                playbackStore?.setIsSeeking(false)
              }
          }}
          thumbTintColor={color.seekBarColor} />
      </View> */}
      {/* <View style={Styles.VDO_DURATION_CONTAINER}>
        <Text style={Styles.VDO_SEEK_TIME_START}>{
          secondsToMinutesSeconds(playbackStore?.getCurrentDuration)
        }</Text>
        <Text style={Styles.VDO_SEEK_TIME_END}>
          {
            playbackStore?.getVideoDuration ? isIos ? '-'.concat(secondsToMinutesSeconds(playbackStore?.getVideoDuration - playbackStore?.getCurrentDuration)) : secondsToMinutesSeconds(playbackStore?.getVideoDuration + 1) : "00:00"
          }
        </Text>
      </View> */}
    </View>
  )
})