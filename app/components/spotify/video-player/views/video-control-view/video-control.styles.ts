import { ViewStyle, TextStyle, ImageStyle, Platform } from "react-native"
import * as metric from "../../../../../theme"
import { RFValue } from "react-native-responsive-fontsize"
import { color, isIPhone, isNewIPhone } from "../../../../../theme"
import { FULL, FONT_REGULAR } from "../../../../../styles/styles"
import { type } from "../../../../../theme/fonts"

export const SAFE_AREA_VIEW: ViewStyle = {
  ...FULL
}

export const CONTAINER: ViewStyle = {
  ...FULL,
  backgroundColor: color.background,
  // height: metric.screenHeight - metric.ratioHeight(80),
  marginBottom: isIPhone ? metric.ratioHeight(83) : metric.ratioHeight(50),
}
export const HEADER_CONTAINER: ViewStyle = {
  justifyContent: 'flex-end',
  marginTop: metric.ratioHeight(20)
}

export const VDO_CONTAINER: ViewStyle = {
  marginTop: metric.ratioHeight(20),
  minHeight: metric.ratioHeight(280),
  backgroundColor: color.white
}

export const VDO_CONTROL_CONTAINER: ViewStyle = {
  ...FULL,
  marginTop: metric.ratioHeight(50),
  height: metric.ratioHeight(280),
  alignItems: 'center'
}


export const TITLE: TextStyle = {
  fontFamily: type.bold,
  fontSize: RFValue(18),
  color: color.text,
  lineHeight: metric.ratioHeight(31),
  textAlign: 'center'
}

export const SUB_TITLE: TextStyle = {
  marginTop: metric.ratioHeight(12),
  fontFamily: type.base,
  fontSize: RFValue(16),
  color: color.text,
  minHeight: metric.ratioHeight(24),
  textAlign: 'center'
}

export const VDO_CONTROL_BUTTON: ViewStyle = {
  marginTop: metric.ratioHeight(40),
  height: metric.ratioWidth(76),
  width: metric.ratioWidth(76)
}

export const VDO_SEEK: any = {
  marginTop: metric.ratioHeight(20),
  height: metric.ratioHeight(24),
  width: metric.ratioWidth(300)
}

export const VDO_SEEK_TRACK: ViewStyle = {
  height: metric.ratioHeight(2)
}

export const VDO_SEEK_TIME_START: TextStyle = {
  marginTop: metric.ratioHeight(11),
  fontFamily: type.base,
  fontSize: RFValue(16),
  color: color.text,
  height: metric.ratioHeight(24),
  flex: 0.5,
  textAlign: 'left'
}

export const VDO_SEEK_TIME_END: TextStyle | ViewStyle = {
  marginTop: metric.ratioHeight(11),
  fontFamily: type.base,
  fontSize: RFValue(16),
  color: color.text,
  height: metric.ratioHeight(24),
  flex: 0.5,
  textAlign: 'right'
}

export const VDO_SEEK_CONTAINER: ViewStyle = {
  marginTop: metric.ratioHeight(11),
  flexDirection: "column",
}

export const VDO_DURATION_CONTAINER: ViewStyle = {
  marginTop: metric.ratioHeight(11),
  flexDirection: 'row',
  alignItems: 'center',
  width: metric.ratioWidth(330),
  marginHorizontal: metric.ratioWidth(20),
  height: metric.ratioHeight(20)
}

export const DESCRIPTION_VIEW: ViewStyle = {
  backgroundColor: color.lightPurple,
  height: '100%',
  paddingHorizontal: metric.ratioWidth(27)
}

export const SUB_DESCRIPTION: ViewStyle = {
  flexDirection: 'row',
  marginTop: metric.ratioHeight(26.42),
  paddingBottom: metric.ratioHeight(25)
}

export const FULL_SCREEN: ViewStyle = {
  padding: 0,
  marginTop: metric.ratioHeight(20),
  height: metric.ratioHeight(25)
}

export const RED_LINE: ViewStyle = {
  width: metric.ratioWidth(2),
  minHeight: metric.ratioHeight(30),
  backgroundColor: color.redLine,
  marginBottom: metric.ratioHeight(1),
  marginTop: metric.ratioHeight(-1),
}

export const TITLE_VIEW: ViewStyle = {
  marginHorizontal: metric.ratioWidth(17)
}

export const TEXT_TITLE: TextStyle = {
  fontFamily: type.title,
  fontSize: RFValue(22),
  color: color.background,
  marginTop: metric.ratioHeight(-2),
  lineHeight: metric.ratioHeight(32)
}

export const TEXT_AUTHOR: TextStyle = {
  fontFamily: type.base,
  fontSize: RFValue(16),
  color: color.background,
  includeFontPadding: false,
  textAlignVertical: 'center',
}

export const SCROLL_VIEW: ViewStyle = {
  flex: 1,
}

export const SUB_DESCRIPTION_VIEW: ViewStyle = {
  paddingBottom: 18
  // marginTop: metric.ratioHeight(25)
}