// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import { Button } from "../"
import { IntroductionHeaderProps } from "./custom-header.props"
import * as Styles from "./custom-header.styles"
import { useStores } from "../../models/root-store"

export const HeaderComponent: React.FunctionComponent<IntroductionHeaderProps> = observer((props) => {
    const rootStore = useStores()
    return (<Button
        isAnimated={false}
        testID="btnBack"
        accessibilityLabel="btnBack"
        preset="topLeftMenu"
        imagePreset="back"
        onPress={props.onBackButtonPress}

        /* right now, this logic is a work around for overlapping margin while toggle internet conntection. */
        style={rootStore?.getSharedStore?.getIsConnected ? Styles.HEADER : Styles.HEADER_MARGIN_FOR_OFFLINE}
    />)
})