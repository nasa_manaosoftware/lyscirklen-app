import * as React from "react"
import Modal from '../../libs/react-native-modal'
import { LoadingIndicatorProps } from "./loading-indicator.props"
import { useStores } from "../../models/root-store"
import { observer } from "mobx-react-lite"
import { ActivityIndicator } from "react-native"
import { color } from "../../theme"

export const LoadingIndicator = observer((props: LoadingIndicatorProps) => {
  const { containerStyle } = props
  const rootStore = useStores()
  const { isLoading } = rootStore.getSharedStore

  return (
    <Modal presentationStyle="overFullScreen" isVisible={isLoading} animationIn='fadeIn' animationOut='fadeOut' style={containerStyle}>
      {/* <FastImage source={Images.images.splashLogo} style={Styles.INDICATOR_VIEW} resizeMode={FastImage.resizeMode.contain} /> */}
      <ActivityIndicator size="large" color={color.white} />
    </Modal>
  )
})
