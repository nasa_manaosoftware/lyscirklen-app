import { ImageStyle } from "react-native"
import * as metric from "../../theme"

const iconWidth = metric.ratioWidth(100)
const iconHeight = metric.ratioHeight(124)

export const INDICATOR_VIEW: ImageStyle = {
    position: 'absolute',
    top: (metric.screenHeight / 2) - (iconHeight / 2),
    left: (metric.screenWidth / 2) - metric.ratioWidth(70),
    width: iconWidth,
    height: iconHeight
}