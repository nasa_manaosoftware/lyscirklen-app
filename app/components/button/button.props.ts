import { ViewStyle, TextStyle, TouchableOpacityProps, ImageStyle, ImageSourcePropType } from "react-native"
import { ButtonPresetNames, ImageButtonPresetNames, TextPresetNames } from "./button.presets"

export interface ButtonProps extends TouchableOpacityProps {
  /**
   * Text which is looked up via i18n.
   */
  tx?: string

  /**
   * The text to display if not using `tx` or nested components.
   */
  text?: string

  imageSource?: ImageSourcePropType | string
  /**
   * An optional style override useful for padding & margin.
   */

  containerStyle?: ViewStyle | ViewStyle[]

  style?: ViewStyle | ViewStyle[]

  /**
   * An optional style override useful for the button text.
   */
  textStyle?: TextStyle | TextStyle[]

  imageStyle?: ImageStyle | ImageStyle[]

  /**
   * One of the different types of text presets.
   */
  preset?: ButtonPresetNames

  textPreset?: TextPresetNames

  imagePreset?: ImageButtonPresetNames

  isAnimated?: boolean

  isSolid?: boolean

  /**
   * One of the different types of text presets.
   */
  children?: React.ReactNode
}
