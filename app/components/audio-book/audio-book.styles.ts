import { ImageStyle, TextStyle, ViewStyle } from "react-native";
import { RFValue } from "react-native-responsive-fontsize";
import * as metric from '../../theme'
import { color } from "../../theme";
import { size, type } from "../../theme/fonts";

export const BOOK_NAME_CONTAINER: ViewStyle = {
    flex: 0,
    justifyContent: 'flex-start'
}

export const AUTHER_CONTAINER: ViewStyle = {
    flex: 0,
    justifyContent: 'center',
    paddingTop: metric.ratioHeight(10),
    paddingBottom: metric.ratioHeight(20)
}

export const ROW_ICON: ViewStyle = {
    flex: 0,
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: metric.ratioWidth(-2)
}

export const BOOK_COVER: ImageStyle = {
    width: metric.isTablet() ? metric.ratioWidth(86.5) : metric.ratioWidth(120),
    height: metric.isTablet() ? metric.ratioWidth(130) : metric.ratioHeight(179)
}

export const CONTAINER: ViewStyle = {
    flex: 1,
    flexDirection: 'row',
    width: '100%',
    minHeight: metric.ratioHeight(95)
}

export const ICON: ImageStyle = {
    width: metric.ratioWidth(15),
    height: metric.ratioHeight(18)
}

export const TITLE: TextStyle = {
    fontFamily: type.base,
    fontSize: size.header,
    color: color.text,
    lineHeight: metric.ratioHeight(32)
}

export const AUTHOR_NAME: TextStyle = {
    fontFamily: type.base,
    fontSize: size.large,
    color: color.text
}

export const SUB_CONTAINER: ViewStyle ={
    flex: 1,
    marginLeft: metric.ratioWidth(30)
}
export const BOOK_COVER_CONTAINER: ViewStyle = {
    flex: 0
}

export const CIRCLE: ViewStyle = {
    borderRadius: 100,
    width: metric.isTablet() ? metric.ratioWidth(35) :metric.ratioWidth(40),
    height: metric.isTablet() ? metric.ratioWidth(35) :metric.ratioWidth(40),
    justifyContent: 'center',
    marginRight : metric.ratioWidth(20)
}

export const LOADING: ViewStyle = { 
    position: "absolute", 
    zIndex: 99, 
    alignSelf: 'center', 
    justifyContent: 'center', 
    // backgroundColor: color.darkDim, 
    width: metric.isTablet() ? metric.ratioWidth(86.5) : metric.ratioWidth(120),
    height: metric.isTablet() ? metric.ratioWidth(130) : metric.ratioHeight(179),
    marginLeft: metric.ratioWidth(-30)
}
