
export interface AudioBookProps {
  title?: string
  author?: string
  bookCover?: string
  downloadStatus?: string 
  isMyLibrary?: boolean,
  onPressBookCover?(): void,
  onPressBookName?(): void,
  onPressInfoIcon?(): void,
  onPressAuthorName?(): void,
  onPressDelete?(): void,
  disabled?: boolean,
  screen?: string,
  onPressDownload?(): void,
  isDownloading?: boolean,
  onPressPlayBackButton?(): void,
  key?: number,
  audiobookId?: string,
  hasVideoUrl?: boolean
}
