import React from "react";
import { Image, Text, View, Alert, ActivityIndicator, TouchableHighlight } from "react-native";
import { observer } from "mobx-react-lite";
import { AudioBookProps } from "./audio-book.props";
import { images } from "../../theme/images";
import * as Styles from './audio-book.styles'
import FastImage from "react-native-fast-image";
import * as metric from '../../theme'
import { color } from "../../theme/color";
import { DownloadStatus, NavigationKey } from "../../constants/app.constant";
import { useStores } from "../../models/root-store";
import LinearGraient from 'react-native-linear-gradient'

export const AudioBook: React.FunctionComponent<AudioBookProps> = observer((props) => {

    const {
        title,
        author,
        bookCover,
        downloadStatus,
        isMyLibrary,
        onPressBookCover,
        onPressBookName,
        onPressInfoIcon,
        onPressAuthorName,
        onPressDelete,
        disabled,
        screen,
        onPressDownload,
        onPressPlayBackButton,
        isDownloading,
        audiobookId,
        hasVideoUrl
    } = props

    const downloadIcon = downloadStatus === DownloadStatus.SUCCESS ? images.downloaded : images.download
    const isDisabled = disabled !== undefined ? disabled : false

    const loadingColor = isDownloading ? color.fileUploadDisable : color.fileUploadEnable

    const rootStore = useStores()
    const audiobookPause = false
    const audiobookId_playback = rootStore?.getPlaybackStore?.getAudiobook?.uid

    const renderIcon = (icon, event, margin, touchMargin, disabled) => {

        return (
            <View>
                {
                    !event ? null :

                        <TouchableHighlight
                            activeOpacity={0.65}
                            // underlayColor={color.background}
                            disabled={disabled}
                            onPress={() => event()}
                            style={[Styles.CIRCLE, { marginLeft: metric.ratioWidth(touchMargin) }]}>
                            <LinearGraient colors={color.buttonGradient} style={Styles.CIRCLE}>
                                <Image source={icon} resizeMode={'contain'} style={[Styles.ICON, { alignSelf: 'center', marginLeft: metric.ratioWidth(margin) }]} />
                            </LinearGraient>
                        </TouchableHighlight>
                }
            </View>
        )
    }

    return (
        <View style={Styles.CONTAINER}>
            <View>
                {isDownloading && downloadStatus !== DownloadStatus.SUCCESS ?
                    <View style={Styles.LOADING}>
                        <ActivityIndicator size="large" color={color.green} />
                    </View>
                    :
                    null
                }
                <TouchableHighlight
                    activeOpacity={0.65}
                    underlayColor={color.background}
                    disabled={isDisabled}
                    onPress={() => onPressBookCover()}
                    style={Styles.BOOK_COVER_CONTAINER}
                >
                    <FastImage resizeMode="contain" source={bookCover ? { uri: bookCover } : images.defaultImage} style={Styles.BOOK_COVER} />
                </TouchableHighlight>
            </View>

            <View style={Styles.SUB_CONTAINER}>
                <View style={Styles.BOOK_NAME_CONTAINER}>
                    {title !== "" ?
                        <TouchableHighlight
                            activeOpacity={0.65}
                            underlayColor={color.background}
                            disabled={isDisabled}
                            onPress={() => onPressBookName()}
                        >
                            <Text style={Styles.TITLE} numberOfLines={2} ellipsizeMode={"tail"}>{title}</Text>
                        </TouchableHighlight>
                        :
                        null
                    }

                </View>
                <View style={Styles.AUTHER_CONTAINER}>
                    {author !== "" ?
                        <TouchableHighlight
                            activeOpacity={0.65}
                            underlayColor={color.background}
                            disabled={isDisabled}
                            onPress={() => onPressAuthorName()}
                        >
                            <Text style={Styles.AUTHOR_NAME} numberOfLines={1} ellipsizeMode={"tail"}>{author}</Text>
                        </TouchableHighlight>
                        :
                        <View />
                    }
                </View>
                <View style={Styles.ROW_ICON}>
                    {renderIcon(images.detail, onPressInfoIcon, 0, 0, isDisabled)}
                    {
                        hasVideoUrl ?
                            renderIcon(images.play, () => onPressPlayBackButton(), metric.isTablet() ? 2 : 3, 3, isDisabled)
                            :
                            null
                    }
                    {/* {isMyLibrary && (screen === NavigationKey.MyLibrary) ?
                        renderIcon(
                            audiobookId_playback === audiobookId && !audiobookPause ?
                                color.disabled
                                :
                                color.darkRed,
                                images.delete,
                            () => onPressDelete(),
                            0,
                            20,
                            audiobookId_playback === audiobookId && !audiobookPause ?
                                true
                                :
                                isDisabled
                        )
                        :
                        downloadStatus === DownloadStatus.SUCCESS ?
                            renderIcon(color.green, images.downloaded, () => Alert.alert('delete audiobook'), 0, 20, true)
                            :
                            <View>
                                {
                                    <View>
                                        {
                                            renderIcon(loadingColor, downloadIcon, () => { onPressDownload() }, 0, 20, isDownloading? true : isDisabled)
                                        }
                                    </View>
                                }
                            </View>
                    } */}
                </View>
            </View>
        </View>
    )
})