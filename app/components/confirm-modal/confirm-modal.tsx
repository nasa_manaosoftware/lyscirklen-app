import React from "react"
import Modal from '../../libs/react-native-modal'
import { ConfirmModalProps } from "./confirm-modal.props"
import * as Styles from "./confirm-modal.styles"
import { observer } from "mobx-react-lite"
import { View } from "react-native"
import { Text } from "../text/text"
import { Button } from "../button/button"

export const ConfirmModal = observer((props: ConfirmModalProps) => {
  const { isVisible, okButtonText, clearButtonText, onOK, onCancel, onClose, message, onModalClosed } = props

  const renderedMessage = !message ? null : (
    <Text style={Styles.MESSAGE_TEXT_STYLE}>{message}</Text>
  )

  const renderOKButton = () => {
    return (
      <Button
        onPress={onOK}
        text={okButtonText}
        {...Styles.OK_BUTTON_PROPS}
      />
    )
  }

  const renderCancelButton = () => {
    return (
      <Button
        onPress={onDismissModal}
        text={clearButtonText}
        {...Styles.CLEAR_BUTTON_PROPS}
      />
    )
  }

  const onDismissModal = () => {
    if (onClose) {
      return onClose()
    }
    return onCancel()
  }

  return (
    <Modal
      presentationStyle="overFullScreen"
      isVisible={isVisible}
      animationIn={props?.animationIn || 'fadeIn'}
      animationOut={props?.animationOut || 'fadeOut'}
      animationOutTiming={props?.animationOutTiming}
      supportedOrientations={['portrait']}
      onModalHide={onModalClosed && onModalClosed}
    >
      <View style={Styles.CONTAINER}>
        <View shouldRasterizeIOS style={Styles.CONTENT_CONTAINER}>{renderedMessage}</View>
        <View shouldRasterizeIOS style={Styles.FOOTER_CONTAINER}>
          {(renderCancelButton)()}
          {(renderOKButton)()}
        </View >
      </View>
    </Modal>
  )
})
