import React, { FunctionComponent } from "react"
import Animated from "react-native-reanimated"
import { ScrollViewProps, ScrollViewProperties } from "react-native"
import { NativeViewGestureHandlerProperties } from "react-native-gesture-handler"

interface AnimatedScrollViewProps extends ScrollViewProps {
  setRef?: () => any
}

export const AnimatedScrollView: FunctionComponent<NativeViewGestureHandlerProperties & ScrollViewProperties & AnimatedScrollViewProps> = props => {

  // MARK: Global Variables

  const yOffset = new Animated.Value(0)
  const onScroll = Animated.event(
    [{ nativeEvent: { contentOffset: { y: yOffset } } }],
    { useNativeDriver: true }
  )

  const { children, contentContainerStyle, setRef, ...rest } = props

  // MARK: Render

  return (<Animated.ScrollView
    ref={setRef && setRef}
    {...rest}
    contentContainerStyle={contentContainerStyle}
    shouldRasterizeIOS
    keyboardShouldPersistTaps='handled'
    scrollEventThrottle={1}
    onScroll={onScroll}
  >
    {children}
  </Animated.ScrollView>)

}
