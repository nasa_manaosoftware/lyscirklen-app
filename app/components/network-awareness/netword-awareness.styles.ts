import { ViewStyle, TextStyle } from "react-native"
import * as metric from "../../theme"
import { RFValue } from "react-native-responsive-fontsize"
import { color } from "../../theme"
import { type } from "../../theme/fonts";


export const containerStyle: ViewStyle = {
    backgroundColor:  color.background
}

export const deviceStyle: ViewStyle = {
    marginTop: metric.ratioHeight(30),
    height: metric.ratioHeight(47),
    paddingTop: metric.ratioHeight(0),
    backgroundColor: color.networkBackground,
    
}

export const defaultViewStyle: ViewStyle = {
    backgroundColor: color.networkBackground,
    justifyContent: 'center',
    alignContent : 'center',
    position: 'relative',
    paddingHorizontal: metric.ratioWidth(20)
}

export const baseTextStyle: TextStyle = {
    fontFamily: type.base,
    alignSelf: 'center',
    textAlign: 'center'
}
export const defaultTextStyle: TextStyle = {
    ...baseTextStyle,
    fontWeight: '200',
    fontSize: RFValue(18),

    //Android
    fontFamily: type.base
}