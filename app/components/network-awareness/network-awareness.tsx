import * as React from "react"
import { TouchableOpacity, View } from "react-native"
import { Text } from "../"
import { NetworkAwarenessProps } from "./network-awareness.props"
import { useStores } from "../../models/root-store"
import { GeneralResources } from "../../constants/firebase/remote-config"
import * as Styles from "./netword-awareness.styles"
import { observer } from "mobx-react-lite"
import * as metric from "../../theme"
import { NavigationKey } from "../../constants/app.constant"


export const NetworkAwarenessComponent = observer((props: NetworkAwarenessProps) => {

  const rootStore = useStores()
  const { SharedKeys } = GeneralResources
  const { getIsConnected } = rootStore.getSharedStore
  const locale = rootStore?.getLanguage
  // grab the props
  const {
    tx,
    text,
    style: styleOverride,
    textStyle: textStyleOverride,
    children,
    ...rest
  } = props

  const viewStyle = styleOverride
  const textStyle = textStyleOverride
  const content = children || <Text tx={tx} text={text || rootStore.getGeneralResourcesStore(locale, NavigationKey.ShardedKeys, SharedKeys.noInternetConnectionTitle)} style={[textStyle, Styles.defaultTextStyle]} />

  return (
    getIsConnected !== null && !getIsConnected ?
      <View style={Styles.containerStyle}>
        <TouchableOpacity disabled={true} style={[viewStyle, Styles.defaultViewStyle, Styles.deviceStyle, !metric.isIPhone && { position: 'relative' }]} {...rest}>
          {content}
        </TouchableOpacity>
      </View>

      : null
  )
})
