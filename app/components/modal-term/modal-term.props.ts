import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"

export interface ModalTermProps extends NavigationContainerProps<ParamListBase> {
    visible: boolean,
    description?: string,
    title?: string,
    onPressed?(): void,
    isConnected?: boolean
}