import { ImageStyle, Platform, TextStyle, ViewStyle } from "react-native";
import { RFValue } from "react-native-responsive-fontsize";
import { color } from "../../theme";
import { type } from "../../theme/fonts";
import * as metric from '../../theme/metric'

export const CONTAINER_VIEW: ViewStyle = {
    flex: 1,
    marginHorizontal: metric.ratioWidth(0),
    marginBottom: metric.ratioWidth(0),
    // ...Platform.select({
    //     ios: {
    //         marginTop: metric.isNewIPhone ? metric.ratioHeight(40) : metric.ratioHeight(20)
    //     },
    //     android: {
    //         marginTop: metric.ratioHeight(0),
    //     }
    // })
}

export const ICON_CLOSE: ImageStyle = {
    width: metric.ratioWidth(20),
    height: metric.ratioHeight(20),
    alignSelf: 'flex-end',
    marginHorizontal: metric.ratioWidth(20),
    marginVertical: metric.ratioHeight(20)
}

export const HEADER_VIEW: ViewStyle = {
    backgroundColor: color.background,
    paddingBottom: metric.ratioHeight(20)
}

export const TITLE_VIEW: ViewStyle = {
    flexDirection: 'row',
    marginLeft: metric.ratioWidth(25),
    alignItems: 'center'
}

export const VERTICAL_LINE: ViewStyle = {
    backgroundColor: color.redLine,
    width: metric.ratioWidth(2),
    height: metric.ratioHeight(30)
}

export const TITLE_TEXT: TextStyle = {
    fontFamily: type.title,
    fontSize: RFValue(22),
    color: color.white,
    marginLeft: metric.ratioWidth(18),
    marginBottom: metric.isMicrolearn() ? null : metric.ratioHeight(-5)
}

export const DESCRIPTION_VIEW: ViewStyle = {
    backgroundColor: color.lightPurple
}

export const HTML_VIEW: ViewStyle = {
    padding: metric.ratioWidth(25)
}

export const TAG_STYLE: TextStyle = {
    p: {
        paddingVertical: 0,
        lineHeight: metric.ratioHeight(28),
        fontFamily: type.base,
        fontSize: RFValue(16),
        color: color.textTerm
    },
    h1: {
        paddingVertical: 0,
        fontFamily: type.bold,
        color: color.textTerm,
        fontWeight: Platform.OS === 'ios' ? 'bold' : 'normal',
        fontSize: metric.isTablet() ? RFValue(29) : RFValue(29),
    },
    h2: {
        paddingVertical: 0,
        fontFamily: type.bold,
        color: color.textTerm,
        fontWeight: Platform.OS === 'ios' ? 'bold' : 'normal',
        fontSize: metric.isTablet() ? RFValue(22) : RFValue(22)
    },
    li: {
        paddingVertical: 0,
        lineHeight: metric.ratioHeight(28),
        fontFamily: type.base,
        fontSize: RFValue(16),
        color: color.textTerm
    },
    a: {
        paddingVertical: 0,
        lineHeight: metric.ratioHeight(28),
        fontFamily: type.base,
        fontSize: RFValue(16),
        color: color.textTerm,
        backgroundColor: color.lightPurple
    },
    strong: {
        paddingVertical: 0,
        lineHeight: metric.ratioHeight(28),
        fontFamily: type.bold,
        fontSize: RFValue(16),
        color: color.textTerm,
        fontWeight: Platform.OS === 'ios' ? 'bold' : 'normal'
    },
    em: {
        paddingVertical: 0,
        lineHeight: metric.ratioHeight(28),
        fontFamily: type.italic,
        fontSize: RFValue(16),
        fontStyle: Platform.OS === 'ios' ? 'italic' : 'normal',
        color: color.textTerm
    },
    u: {
        paddingVertical: 0,
        lineHeight: metric.ratioHeight(28),
        fontFamily: type.base,
        color: color.textTerm
    }
}