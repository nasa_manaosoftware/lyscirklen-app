import { observer } from "mobx-react-lite"
import { ModalTermProps } from "./modal-term.props";
import Modal from 'react-native-modal'
import React from "react";
import { Image, TouchableOpacity, ScrollView, Text, View, SafeAreaView, Linking, Platform } from "react-native";
import * as Styles from './modal-term.styles'
import { images } from "../../theme/images";
import HTML from 'react-native-render-html'
import * as metric from '../../theme'
// import { TouchableOpacity } from "react-native-gesture-handler";

export const ModalTermComponents: React.FunctionComponent<ModalTermProps> = observer((props) => {

    const { visible, description, title, onPressed, isConnected } = props
    
    const openUrl = (link) => {
        Linking.canOpenURL(link).then(supported => {
            if (!supported) {
                console.log('Can\'t handle url: ' + link);
            } else {
                Linking.openURL(link)
                    .catch(err => {
                        console.warn('openURL error', err);
                    });
            }
        }).catch(err => console.warn('An unexpected error happened', err))
    }

    return (
        <SafeAreaView style={{ flex: 1}}>
            <Modal
                isVisible={visible}
                hasBackdrop={false}
                backdropOpacity={1}
                style={[Styles.CONTAINER_VIEW, {
                    ...Platform.select({
                        ios: {
                            marginTop: metric.isNewIPhone ? (!isConnected ? metric.ratioHeight(77) : metric.ratioHeight(40)) : (!isConnected ? metric.ratioHeight(77) : metric.ratioHeight(20))
                        },
                        android: {
                            marginTop: !isConnected ? metric.ratioHeight( metric.isTablet() ? 56 : 42) : metric.ratioHeight(0),
                        }
                    })
                }]}
                animationIn={'slideInUp'}
                animationOut={'slideOutDown'}>
                <View style={{ flex: 1 }}>
                    <View style={Styles.HEADER_VIEW}>
                        <TouchableOpacity onPress={() => onPressed()}>
                            <Image source={images.closeModalIcon} resizeMode={'contain'} style={Styles.ICON_CLOSE} />
                        </TouchableOpacity>
                        <View style={Styles.TITLE_VIEW}>
                            <View style={Styles.VERTICAL_LINE} />
                            <Text style={Styles.TITLE_TEXT}>{metric.isMicrolearn() ? title : title.toUpperCase()}</Text>
                        </View>
                    </View>
                    <ScrollView style={Styles.DESCRIPTION_VIEW}>
                        <HTML
                            containerStyle={Styles.HTML_VIEW}
                            tagsStyles={Styles.TAG_STYLE}
                            ignoredStyles={["font-family", "letter-spacing", "font-size", "color", "background-color"]}
                            html={description}
                            onLinkPress={(event, href) => openUrl(href)}
                        />
                    </ScrollView>
                </View>
            </Modal>
        </SafeAreaView>
    )
})