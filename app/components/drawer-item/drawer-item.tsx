import { observer } from "mobx-react-lite"
import * as React from "react"
import { Image, Text, TouchableOpacity, View } from "react-native"
import * as Style from "../../navigation/drawers/custom-drawer/drawer-item/drawer-item.styles"
import { color } from "../../theme/color"
import { DrawerProps } from "./drawer-item.props"

export const DrawerItem = observer((props: DrawerProps) => {
    return (
        <TouchableOpacity
            activeOpacity={0.5}
            style={props.selected ? [Style.CONTAINNER_VIEW, { backgroundColor: color.drawBackground, width: '100%' }] : Style.CONTAINNER_VIEW}
            disabled={props.disabled ? true : false}
            onPress={() => props.onPress()}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Image source={props.iconSource} resizeMode={'contain'} style={Style.ICON_VIEW} />
                <Text style={Style.TEXT_VIEW}>{props.text}</Text>
            </View>
        </TouchableOpacity>
    )
})