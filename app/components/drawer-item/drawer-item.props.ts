import { ParamListBase } from '@react-navigation/native'
import { ImageSourcePropType } from 'react-native'
import { NavigationContainerProps } from "react-navigation"

export interface DrawerProps extends NavigationContainerProps<ParamListBase> {
    text?: string,
    onPress?(): void,
    selected?: boolean,
    disabled?: boolean,
    iconSource?: ImageSourcePropType | string
}