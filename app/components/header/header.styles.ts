import { ViewStyle, TextStyle, ImageStyle, Platform } from "react-native"
import * as metric from "../../theme"
import { FULL } from "../../styles/styles"
import { type } from "../../theme/fonts";
import { color } from "../../theme/color";
import { RFValue } from "react-native-responsive-fontsize";

export const CONTAINER_VIEW: ViewStyle = {
  flexDirection: "row",
  justifyContent: "flex-start"
}

export const HEADER: ViewStyle = {
  marginTop: metric.backButtonHeaderMarginTop,
  height: metric.ratioHeight(50)
}

export const HEADER_MARGIN_FOR_OFFLINE: ViewStyle = {
  marginTop: metric.actualHeaderMargin,
  height: metric.ratioHeight(50)
}

export const TITLE: TextStyle = { textAlign: "center" }
export const TITLE_MIDDLE: ViewStyle = { ...FULL, justifyContent: "center", alignItems: 'center', marginRight: metric.ratioWidth(50)}
export const LEFT: ViewStyle = { width: metric.ratioWidth(24) }
export const RIGHT: ViewStyle = { width: metric.ratioWidth(24) }

export const BD_LOGO: ImageStyle = {
  width: metric.ratioWidth(71.56),
  height: metric.ratioHeight(34)
 
}

export const ML_LOGO: ImageStyle = {
  width: metric.ratioWidth(100),
  height: metric.ratioHeight(50)
}

export const BADGE_CONTAINER: ViewStyle = {
  ...Platform.select({
    ios: {
      height : metric.widthPercentageToDP(8.5), 
      width : metric.widthPercentageToDP(8.5), 

      flexDirection: 'row', 
      justifyContent : 'flex-end', 
      marginLeft : metric.ratioWidth(40),
      marginRight : metric.ratioWidth(10),
      marginTop : -1, 
    },
    android: {
      height : metric.widthPercentageToDP(8.0), 
      width :  metric.widthPercentageToDP(8.0), 
      flexDirection: 'row', 
      justifyContent : 'flex-end', 
      marginLeft : metric.ratioWidth(40),
      marginRight : metric.ratioWidth(10),
      marginTop : 1 , 
    }
  })
}
export const BADGE_TOUNCHABLE: ViewStyle = {
  ...Platform.select({
    ios: {
      justifyContent: 'center', marginRight  : metric.ratioWidth(14), marginTop : metric.ratioHeight(-12)
    },
    android: {
      justifyContent: 'center', marginRight  : metric.ratioWidth(14), marginTop : metric.ratioHeight(-12)
    }
  })
}

export const RIGHT_TOUNCHABLE_IPHONE: ViewStyle = {
  flexDirection: 'row', 
  backgroundColor : 'transparent',
  justifyContent: 'flex-end',
}

export const BADGE_TOUNCHABLE_IPHONE: ViewStyle = {
  ...Platform.select({
    ios: {
      justifyContent: 'center', marginRight  : metric.ratioWidth(14), marginTop : metric.ratioHeight(0)
    },
    android: {
      justifyContent: 'center', marginRight  : metric.ratioWidth(14), marginTop : metric.ratioHeight(-12)
    }
  })
}


export const BADGE_BUTTON: ViewStyle = {
  height : '100%', 
  width : '100%', 
  flexDirection:'row', 
  backgroundColor : 'transparent', 
  justifyContent : 'flex-end',
}

export const BADGE_TEXT_CONTAINER: ViewStyle = {
    ...Platform.select({
      ios: {
        position : 'absolute', 
        zIndex : 99, 
        left: 0,
        backgroundColor : color.redBadge,
        minHeight : metric.ratioWidth(14),
        minWidth : metric.ratioWidth(14),
        borderRadius : 100,
        justifyContent : 'center', alignItems : 'center',
        marginLeft : -8  
      },
      android: {
        position : 'absolute', 
        zIndex : 99, 
        left: 0,
        backgroundColor : color.redBadge,
        minHeight : metric.ratioWidth(11),
        minWidth : metric.ratioWidth(11),
        borderRadius : 100,
        justifyContent : 'center', alignItems : 'center'  ,
        marginLeft : -5
      }
    })
  
 }

 export const BADGE_ICON: ImageStyle = {
  minHeight :  metric.ratioHeight(20),
  minWidth :  metric.ratioWidth(17.5),
}

export const BADGE_TEXT: TextStyle = {
  padding : metric.ratioWidth(1),
  fontSize : RFValue(7),
  fontFamily:type.emphasis
}

export const LEFT_VIEW: ViewStyle = {
  paddingLeft: metric.ratioWidth(14), 
  paddingRight: metric.ratioWidth(40)
}
