import * as React from "react"
import { View, ViewStyle, Image, TouchableOpacity, Platform } from "react-native"
import { HeaderProps } from "./header.props"
import { Button } from "../button/button"
import { Icon } from "../icon/icon"
import { Text } from "../text/text"
import { translate } from "../../i18n/"
import * as Styles from "./header.styles"
import { useStores } from "../../models/root-store"
import { observer } from "mobx-react-lite"
import * as Utils from "../../utils"
import { images } from "../../theme/images"

export const Header: React.FunctionComponent<HeaderProps> = observer(props => {
  const rootStore = useStores()
  const { getBadgeNumber } = rootStore.getUserStore
  const organizationId = rootStore?.getOrganizationId

  const {
    headerText,
    headerTx,
    containerStyle,
    titleStyle,
    style
  } = props
  const header = headerText || (headerTx && translate(headerTx)) || ""
  const workAroundStyle = rootStore?.getSharedStore?.getIsConnected ? Styles.HEADER : Styles.HEADER_MARGIN_FOR_OFFLINE

  const { leftIcon,
    leftIconSource,
    leftIconStyle,
    onLeftPress,
    isLeftIconAnimated } = props

  const { rightIcon,
    rightIconSource,
    rightText,
    isRightIconAnimated,
    onRightPress,
    rightIconBadgeNumber,
    rightIconStyle,
    rightTextStyle,
    hideBadgeNumber,
    hideCenter, 
    onCenterPress } = props

  const { customLeftView,
    customRightView } = props


  const calculatedContainer: ViewStyle = {
    ...containerStyle,
    ...workAroundStyle,
    marginTop: parseFloat((containerStyle?.marginTop || 0).toString()) + parseFloat((workAroundStyle?.marginTop || 0).toString())
  }

  const renderLeftMenuView = () => {
    if (customLeftView) return customLeftView
    return leftIcon || leftIconSource ? (
      <Button isAnimated={isLeftIconAnimated} preset="left" onPress={onLeftPress} style={Styles.LEFT_VIEW}>
        <Icon icon={leftIcon} source={leftIconSource} style={leftIconStyle} />
      </Button>
    ) : null
  }

  const renderRightMenuViewIphone = () => {
    return (
      <TouchableOpacity style={ Styles.RIGHT_TOUNCHABLE_IPHONE } onPress={onRightPress}>
      <View style={ Styles.BADGE_TOUNCHABLE_IPHONE } >
        {(getBadgeNumber > 0 && !hideBadgeNumber) ?
          (<View
            style={Styles.BADGE_TEXT_CONTAINER}
          >
            <Text text={Utils.getBadgeNumber(getBadgeNumber)} style={Styles.BADGE_TEXT} />
          </View>)
          : null}
        <Icon icon={rightIcon} source={rightIconSource} style={Styles.BADGE_ICON} />
      </View>
      </TouchableOpacity>
    )
  }

  const renderRightMenuView = () => {
    return ( (Platform.OS === "ios" ) ? renderRightMenuViewIphone()  : 
      <TouchableOpacity style={Styles.BADGE_TOUNCHABLE} onPress={onRightPress}>
        {(getBadgeNumber > 0 && !hideBadgeNumber) ?
          (<View
            style={Styles.BADGE_TEXT_CONTAINER}
          >
            <Text text={Utils.getBadgeNumber(getBadgeNumber)} style={Styles.BADGE_TEXT} />
          </View>)
          : null}
        <Icon icon={rightIcon} source={rightIconSource} style={Styles.BADGE_ICON} />
      </TouchableOpacity>
    )
    
  }

  const renderCenterView = () => {
    return (
      <TouchableOpacity onPress={onCenterPress}>
        <Image source={images.appLogo} style={Styles.BD_LOGO} resizeMode={'contain'} />
      </TouchableOpacity>
    )
  }

  return (
    <View shouldRasterizeIOS style={{ ...Styles.CONTAINER_VIEW, ...calculatedContainer }}>
      {
        renderLeftMenuView ? renderLeftMenuView() : (
          <View shouldRasterizeIOS style={Styles.LEFT} />
        )}
      <View shouldRasterizeIOS style={Styles.TITLE_MIDDLE}>
        {!hideCenter && renderCenterView()}
      </View>
      {
        renderRightMenuView ? renderRightMenuView() : (
          <View shouldRasterizeIOS style={Styles.RIGHT} />
        )}
    </View>
  )
})
