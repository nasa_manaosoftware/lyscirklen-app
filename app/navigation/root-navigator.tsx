import React from "react"
import { NavigationKey } from "../constants/app.constant"
import { Splashscreen } from "../modules/splash/splash.screen"
import { useStores } from "../models/root-store"
// import { AuthStackComponents } from "./stacks/auth-stack"
import { createStackNavigator } from "@react-navigation/stack"
import { useDynamicLinks, useInitialDynamicLinks } from "../utils/firebase/dynamic-links/use-dynamic-links"
import { observer } from "mobx-react-lite"
import { MainStackComponents } from "./stacks/main-stack"
import { Text, View } from "react-native"

export const IntroStack = createStackNavigator()

export const RootNavigator = observer((props) => {
  
  const rootStore: any = useStores()
  const { getIsFetchingResources } = rootStore
  // const { getLoggedIn } = rootStore.getAuthStore

  // console.log('getLoggedIn ', getLoggedIn);
  
  // MARK: Dynamic Links

  return (
    // getIsFetchingResources ?
    // <IntroStack.Navigator headerMode="none" screenOptions={{ gestureDirection: 'vertical' }}>
    //   <IntroStack.Screen name={NavigationKey.Splash} component={Splashscreen} />
    // </IntroStack.Navigator> :
    // getLoggedIn ?
    //   <MainStackComponents /> : null
    // <MainStackComponents />
    // <View>
    //   <Text>Root Navigator works!</Text>
    // </View>
    getIsFetchingResources ?
    <IntroStack.Navigator headerMode="none" screenOptions={{ gestureDirection: 'vertical' }}>
      <IntroStack.Screen name={NavigationKey.Splash} component={Splashscreen} />
    </IntroStack.Navigator> :
      <MainStackComponents />
)})