import React from "react"
import { createDrawerNavigator } from "@react-navigation/drawer"
import { NavigationKey } from "../../constants/app.constant"
import * as metric from "../../theme"
import { CustomDrawerContent } from "./custom-drawer/custom-drawer"
import { useDrawerInitialRender } from "../../custom-hooks/use-drawer-initial-render"
import { MainScreen } from "../../modules/main/main"
import { ImageStyle } from "react-native"
import { color } from "../../theme"
import { HeaderProps } from "../../components/header/header.props"
import { images } from "../../theme/images"

const Drawer = createDrawerNavigator()

export const DrawerComponents = ({ navigation }) => {

    const isFirstLoadDone = useDrawerInitialRender()

    return (
        <Drawer.Navigator
            drawerStyle={metric.DrawerStyleConfig(isFirstLoadDone)}
            drawerContent={props => <CustomDrawerContent {...props} />}
            {...metric.drawerOptions}
            initialRouteName={NavigationKey.MainScreen}
            // detachInactiveScreens={false}
        >
            <Drawer.Screen name={NavigationKey.MainScreen} component={MainScreen} />
        </Drawer.Navigator>
    )
}