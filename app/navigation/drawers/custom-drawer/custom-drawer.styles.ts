import { ImageStyle, Platform, TextStyle, ViewStyle } from "react-native"
import { color } from "../../../theme/color"
import * as metric from "../../../theme"
import { HeaderProps } from "../../../components/header/header.props"
import { images } from "../../../theme/images"
import { RFValue } from "react-native-responsive-fontsize"
import { type } from "../../../theme/fonts"

export const BACKGROUND_COLOR: ViewStyle = {
    backgroundColor: color.background
}

export const SCREEN_SIZE_VIEW: ViewStyle = {
    width: metric.screenWidth - metric.ratioWidth(60),
    height: metric.screenHeight + metric.ratioWidth(16),
    ...Platform.select({
        ios: {
            top: metric.isIPad() ? metric.ratioHeight(0) : metric.ratioHeight(5)
        },
        android: {
            top:  metric.isTablet() ? metric.ratioHeight(-15) : metric.ratioHeight(0)
        }
    })
}

export const CONTAINER_VIEW: ViewStyle = {
    ...BACKGROUND_COLOR,
    ...SCREEN_SIZE_VIEW,
    ...Platform.select({
        android: {   
            marginTop: metric.ratioHeight(27.5)
        }
    })
}

export const CONTAINER_OFFLINE_VIEW: ViewStyle = {
  ...CONTAINER_VIEW,
  ...Platform.select({
    android: {
        marginTop: metric.ratioHeight(0)
    }
}),
}

export const GRADIENT_VIEW: ViewStyle = {
    width: metric.ratioWidth(240),
    height: '100%',
    position: 'absolute'
}

export const SCROLL_VIEW: ViewStyle = {
    marginTop: metric.ratioHeight(48)
}

export const CLOSE_VIEW: ViewStyle = {
    width: metric.ratioWidth(22),
    height: metric.ratioWidth(22),
    borderRadius: metric.ratioWidth(67 / 2),
    position: 'relative',
    justifyContent: 'center',
}

export const BD_LOGO: ImageStyle = {
    width: metric.ratioWidth(132),
    height: metric.ratioHeight(50),
    left: metric.ratioWidth(16),
    position: 'absolute'
}

export const ML_LOGO: ImageStyle = {
    width: metric.ratioWidth(87),
    height: metric.ratioHeight(100),
    position: 'absolute'
}

export const HEADER_VIEW: ViewStyle = {
    width: '100%',
    height: metric.ratioHeight(150),
    justifyContent: 'center'
}

export const LINE_VIEW: ViewStyle = {
    width: '100%',
    height: metric.ratioHeight(1),
    backgroundColor: color.drawBackground
}


export const MENU_ICON_STYLE: ImageStyle = {
    width: metric.ratioWidth(18),
    height: metric.ratioHeight(18)
}

export const HEADER_PROPS: HeaderProps = {
    rightIconStyle: MENU_ICON_STYLE,
    rightIconSource: images.close,
}

export const LOG_OUT: ViewStyle = {
    flexDirection: "row",
    paddingLeft: metric.ratioWidth(22),
    width: metric.ratioWidth(332),
    height: metric.ratioHeight(65),
    alignItems: 'center'
}

export const TEXT_LOG_OUT: TextStyle = {
    marginLeft: metric.ratioWidth(27),
    color: color.text,
    fontFamily: type.base,
    fontSize: RFValue(18)
}

export const TOUCHABLE_STYLE: ViewStyle = {
    alignItems: 'flex-end',
    paddingRight: metric.ratioWidth(24),
    top: metric.ratioHeight(-28)
}