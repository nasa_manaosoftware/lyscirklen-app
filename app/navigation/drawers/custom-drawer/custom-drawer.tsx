import React from "react"
import { View } from "react-native-animatable"
import { observer } from "mobx-react-lite"
import * as Styles from "./custom-drawer.styles"
import { Text } from "../../../components"
import { images } from "../../../theme/images"
import { useConfigurate } from "../../../custom-hooks/use-configure-controller"
import CustomDrawerController from "./custom-drawer.controller"
import { CustomDrawerContentProps } from "./custom-drawer.props"
import { NavigationKey } from "../../../constants/app.constant"
import { Image, TouchableOpacity } from "react-native"
import { DrawerItem } from '../../../components/drawer-item/drawer-item'
import * as metric from "../../../theme"
import { useStores } from "../../../models/root-store"

export const CustomDrawerContent: React.FunctionComponent<CustomDrawerContentProps> = observer((props) => {

    const controller = useConfigurate(CustomDrawerController, props) as CustomDrawerController
    const rootStore = useStores()
    const { getIsConnected } = rootStore.getSharedStore
    const locale = CustomDrawerController.rootStore?.getLanguage
    
    return (
        <View shouldRasterizeIOS style={Styles.BACKGROUND_COLOR}>
            <View shouldRasterizeIOS style={getIsConnected? Styles.CONTAINER_VIEW : Styles.CONTAINER_OFFLINE_VIEW}>
                <View style={Styles.HEADER_VIEW}>
                    <TouchableOpacity onPress={controller.closeDrawer} activeOpacity={1} style={Styles.TOUCHABLE_STYLE}>
                        <Image source={images.close} resizeMode={'contain'} style={Styles.CLOSE_VIEW} />
                    </TouchableOpacity>
                    <Image source={images.headerLogo} resizeMode={'contain'} style={metric.isMicrolearn()? Styles.ML_LOGO : Styles.BD_LOGO} />
                </View>
                <DrawerItem
                    text={CustomDrawerController.resourcesViewModel?.getResourceFrontScreenMenuTitle(locale).toUpperCase()}
                    iconSource={images.home}
                    disabled={CustomDrawerController.viewModel?.getIsDisabled ? true : false}
                    selected={CustomDrawerController.rootStore?.getIsSelectedFront}
                    onPress={() => { controller.onDrawerItemPressed(NavigationKey.FrontScreen), controller.setUI(0) }}
                />
                {/* <DrawerItem
                    text={CustomDrawerController.resourcesViewModel?.getResourceSearchMenuTitle(locale).toUpperCase()}
                    iconSource={images.search}
                    disabled={CustomDrawerController.viewModel?.getIsDisabled ? true : false}
                    selected={CustomDrawerController.rootStore?.getIsSelectedSearch}
                    onPress={() => { controller.onDrawerItemPressed(NavigationKey.SearchAudioBook), controller.setUI(1) }}
                /> */}
                {/* <DrawerItem
                    text={CustomDrawerController.resourcesViewModel?.getResourceMyibraryMenuTitle().toUpperCase()}
                    iconSource={images.myLibrary}
                    disabled={CustomDrawerController.viewModel?.getIsDisabled ? true : false}
                    selected={CustomDrawerController.rootStore?.getIsSelectedMyLibrary}
                    onPress={() => { controller.onDrawerItemPressed(NavigationKey.MyLibrary), controller.setUI(2) }}
                /> */}
                <DrawerItem
                    text={CustomDrawerController.resourcesViewModel?.getResourceNotificationMenuTitle(locale).toUpperCase()}
                    iconSource={images.bell}
                    disabled={CustomDrawerController.viewModel?.getIsDisabled ? true : false}
                    selected={CustomDrawerController.rootStore?.getIsSelectedNotification}
                    onPress={() => { controller.onDrawerItemPressed(NavigationKey.Messages), controller.setUI(3) }}
                />
                <DrawerItem
                    text={CustomDrawerController.resourcesViewModel?.getResourceAboutUsMenuTitle(locale).toUpperCase()}
                    iconSource={images.aboutUsLyscirklen}
                    disabled={CustomDrawerController.viewModel?.getIsDisabled ? true : false}
                    selected={CustomDrawerController.rootStore?.getIsSelectedAboutUs}
                    onPress={() => { controller.onDrawerItemPressed(NavigationKey.AboutUs), controller.setUI(4) }}
                />
                {/* <View style={Styles.LINE_VIEW} /> */}
                <DrawerItem
                    text={CustomDrawerController.resourcesViewModel?.getResourceTermOfUseMenuTitle(locale).toUpperCase()}
                    iconSource={images.termOfUse}
                    disabled={CustomDrawerController.viewModel?.getIsDisabled ? true : false}
                    selected={CustomDrawerController.rootStore?.getIsSelectedTermOfUse}
                    onPress={() => { controller.onDrawerItemPressed(NavigationKey.TermOfUse), controller.setUI(5) }}
                />
                <DrawerItem
                    text={CustomDrawerController.resourcesViewModel?.getResourcePrivacyPolicyMenuTitle(locale).toUpperCase()}
                    iconSource={images.privacy}
                    disabled={CustomDrawerController.viewModel?.getIsDisabled ? true : false}
                    selected={CustomDrawerController.rootStore?.getIsSelectedPrivacy}
                    onPress={() => { controller.onDrawerItemPressed(NavigationKey.PrivacyPolicy), controller.setUI(6) }}
                />
                {/* <TouchableOpacity
                    style={Styles.LOG_OUT}
                    onPress={() => controller.onSignOutPressed()}
                    disabled={CustomDrawerController.viewModel?.getIsDisabled ? true : false}>
                    <Image source={images.logout} resizeMode={'contain'} style={Styles.MENU_ICON_STYLE} />
                    <Text style={Styles.TEXT_LOG_OUT}>{CustomDrawerController.resourcesViewModel?.getResourceLogoutMenuTitle()}</Text>
                </TouchableOpacity> */}
            </View>
        </View>

    )
})