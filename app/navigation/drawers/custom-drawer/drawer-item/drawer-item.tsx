import React from "react"
import { observer } from "mobx-react-lite"
import * as Styles from "./drawer-item.styles"
import { Icon, Text, Button } from "../../../../components"
import { color } from "../../../../theme"
import { DrawerItemProps } from "./drawer-item.props"


export const DrawerItemCompoenents: React.FunctionComponent<DrawerItemProps> = observer((props) => {

    return (
        // <Button isSolid preset='none' style={props.selected ? [Styles.CONTAINNER_VIEW,{backgroundColor: color.backgroundActive, width: '100%'}] : Styles.CONTAINNER_VIEW} onPress={props.onPress} disabled={props.disabled}>
        <Button isSolid preset='none' style={props.selected ? [Styles.CONTAINNER_VIEW, { backgroundColor: color.drawBackground, width: '100%' }] : Styles.CONTAINNER_VIEW} onPress={props.onPress} disabled={props.disabled}>
            <Icon source={props.iconSource} style={Styles.ICON_VIEW} />
            <Text text={props.text} style={Styles.TEXT_VIEW} />
        </Button>
    )
})
