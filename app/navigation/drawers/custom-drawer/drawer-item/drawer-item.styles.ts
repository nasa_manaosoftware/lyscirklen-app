import { ViewStyle, ImageStyle, TextStyle } from "react-native"
import * as metric from "../../../../theme"
import { color } from "../../../../theme"
import { RFValue } from "react-native-responsive-fontsize"
import { FONT_REGULAR, FONT_LIGHT, FONT_BOLD } from "../../../../styles/styles"
import { types } from "util"
import { type } from "../../../../theme/fonts"

export const CONTAINNER_VIEW: ViewStyle = {
    flexDirection: 'row',
    width: metric.ratioWidth(332),
    minHeight: metric.ratioHeight(65),
    paddingLeft: metric.ratioWidth(22),
    alignItems: 'center'
}

export const ICON_VIEW: ImageStyle = {
    width: metric.ratioWidth(18),
    height: metric.ratioWidth(18),
    resizeMode:'contain'
}

export const TITLE: TextStyle = {
    ...FONT_REGULAR,
    color: color.text,
    fontSize: RFValue(18)
}
export const TEXT_VIEW: TextStyle = {
    ...TITLE,
    marginLeft: metric.ratioWidth(27), 
    fontFamily: type.base
}

export const SEPARATOR_VIEW: ViewStyle = {
    position: 'absolute',
    left: 0,
    bottom: 0,
    right: 0,
    height: metric.ratioHeight(1),
    backgroundColor: color.palette.white
}

export const BADGE_TITLE: TextStyle = {
    ...FONT_LIGHT,
    fontSize: RFValue(11),
    color: color.palette.white
}

export const BADGE_VIEW: TextStyle = {
    marginLeft: metric.ratioWidth(10)
}