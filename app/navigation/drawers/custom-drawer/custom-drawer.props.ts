import { DrawerContentComponentProps, DrawerContentOptions } from "@react-navigation/drawer"

export interface CustomDrawerContentProps extends DrawerContentComponentProps<DrawerContentOptions> {
}