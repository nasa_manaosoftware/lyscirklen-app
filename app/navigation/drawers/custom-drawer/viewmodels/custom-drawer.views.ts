import { types } from "mobx-state-tree"
import { CustomDrawerPropsModel } from "./custom-drawer.models"
import { GeneralResources } from "../../../../constants/firebase/remote-config"
import { NavigationKey } from "../../../../constants/app.constant"
import { GeneralResourcesStoreModel } from "../../../../models/general-resources-store"

export const CustomDrawerViews = types.model(CustomDrawerPropsModel)
    .views(self => ({
        // get getIsSelected() {
        //     return self.isSelected
        // },
        get getIsDisabled() {
            return self.isDisabled
        }
    }))

export const CustomDrawerResourcesViews = GeneralResourcesStoreModel.views(self => {

    //MARK: Volatile State

    const { MainMenu } = GeneralResources
    const { 
        frontScreenMenu,
        searchMenu,
        myLibraryMenu,
        aboutUsMenu,
        logoutMenu,
        notification,
        termOfUseTitle,
        privacyPolicyTitle,
        logoutTitle,
        logoutMessage,
        confirmButton,
        cancelButton
    } = MainMenu

    //MARK: Views
    
    const getResources = (locale: string, key: string, childKeyOrShareKey: string | boolean = false, isChildDotNotation: boolean = false) => self.getValues(locale, childKeyOrShareKey ? key : NavigationKey.MainMenu, childKeyOrShareKey ? true : key, isChildDotNotation)
    const getResourceFrontScreenMenuTitle = (locale: string) => getResources(locale, frontScreenMenu)
    const getResourceSearchMenuTitle = (locale: string) => getResources(locale, searchMenu, false)
    const getResourceMyibraryMenuTitle = (locale: string) => getResources(locale, myLibraryMenu)
    const getResourceAboutUsMenuTitle = (locale: string) => getResources(locale, aboutUsMenu)
    const getResourceTermOfUseMenuTitle = (locale: string) => getResources(locale, termOfUseTitle, true)
    const getResourcePrivacyPolicyMenuTitle = (locale: string) => getResources(locale, privacyPolicyTitle, true)
    const getResourceLogoutMenuTitle = (locale: string) => getResources(locale, logoutMenu)
    const getResourceNotificationMenuTitle = (locale: string) => getResources(locale, notification)
    const getResourceLogoutTitle = (locale: string) => getResources(locale, logoutTitle)
    const getResourceLogoutMessage = (locale: string) => getResources(locale, logoutMessage)
    const getResourceConfirmButton = (locale: string) => getResources(locale, confirmButton)
    const getResourceCancelButton = (locale: string) => getResources(locale, cancelButton)

    return {
        getResourceFrontScreenMenuTitle,
        getResourceSearchMenuTitle,
        getResourceMyibraryMenuTitle,
        getResourceAboutUsMenuTitle,
        getResourceTermOfUseMenuTitle,
        getResourcePrivacyPolicyMenuTitle,
        getResourceLogoutMenuTitle,
        getResourceNotificationMenuTitle,
        getResourceLogoutTitle,
        getResourceLogoutMessage,
        getResourceConfirmButton,
        getResourceCancelButton
    }
})
