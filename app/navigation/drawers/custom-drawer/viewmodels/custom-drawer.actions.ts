import { types } from "mobx-state-tree"
import { CustomDrawerPropsModel } from "./custom-drawer.models"

export const CustomDrawerActions = types.model(CustomDrawerPropsModel).actions(self => {

    //MARK: Volatile State

    //MARK: Actions

    // const setIsSelected = (value: boolean) => {
    //     self.isSelected = value
    // }
    const setIsDisabled = (value: boolean) => {
        self.isDisabled = value
    }

    return {
        // setIsSelected,
        setIsDisabled
    }
})