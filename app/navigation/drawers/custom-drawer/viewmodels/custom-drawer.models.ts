import { types } from "mobx-state-tree"

export const CustomDrawerPropsModel = {
    // isSelected: types.optional(types.boolean, false),
    isDisabled: types.optional(types.boolean, false)
} 