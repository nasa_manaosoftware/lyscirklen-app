// MARK: Import
import { NavigationKey } from "../../../constants/app.constant"
import { CustomDrawerResourcesStoreModel, CustomDrawerStoreModel } from "./storemodels/custom-drawer.store"
import { CustomDrawerResourcesStore, CustomDrawerStore } from "./storemodels/custom-drawer.types"
import { INavigationRoute } from "../../../models"
import { RootStore } from "../../../models/root-store/root.types"
import { DrawerActions } from "@react-navigation/native"
import { CustomDrawerContentProps } from "./custom-drawer.props"
// import { HomeStore } from "../../../modules/home/storemodels/home.types"
// import { HomeStoreModel } from "../../../modules/home/storemodels/home.store"
import NotificationServices from "../../../utils/firebase/notification/notification"
import { BaseController } from "../../../modules/base.controller"
import { onSnapshot } from "mobx-state-tree"
import { Alert, Platform } from "react-native"
import FirebaseAnalytics from "../../../utils/firebase/analytics/analytics";
import FirebaseFireStore from "../../../utils/firebase/fire-store/fire-store";
import deviceInfoModule from "react-native-device-info"
import PushNotification from "react-native-push-notification";
import PushNotificationIOS from "@react-native-community/push-notification-ios";

class CustomDrawerController extends BaseController {

    /*
        Mark Injectable Variable & Declaration
    */

    /*
        Vulnerable Variable -- always will be cleared when deinit.
    */

    /*
        Stateful Variable
    */

    public static resourcesViewModel: CustomDrawerResourcesStore
    public static viewModel: CustomDrawerStore
    // public static homeViewModel: HomeStore
    /*
        Mark Constructor
    */

    constructor(rootStore?: RootStore, props?: CustomDrawerContentProps & Partial<INavigationRoute>) {
        super(rootStore, props)
        if (this._myProps?.route?.params?.isInitialized) return
        this._setupResourcesViewModel()
        this._setupProps(props)
        this._setupViewModel()
        this._setupSnapShot()
    }

    /*
       Mark Setup
   */

    private _setupResourcesViewModel = () => {
        CustomDrawerController.resourcesViewModel = CustomDrawerResourcesStoreModel.create({ ...this._rootStore?.getAllGeneralResourcesStore })
    }
    private _setupViewModel = () => {
        const data = this._rootStore?.getModuleStore(NavigationKey.Home)
        // if (data) CustomDrawerController.homeViewModel = HomeStoreModel.create({ ...data })
        CustomDrawerController.viewModel = CustomDrawerStoreModel.create({})
        this._setInitializedToPropsParams()
    }

    private _setupSnapShot = () => {
        onSnapshot(CustomDrawerController.viewModel, (snap: CustomDrawerStore) => {
        })
    }

    /*
       Mark Data
   */

    /*
       Mark Event
   */
    public onSignOutPressed = () => {
        // this.closeDrawer()
        Alert.alert(
            CustomDrawerController.resourcesViewModel?.getResourceLogoutTitle(CustomDrawerController.rootStore?.getLanguage),
            CustomDrawerController.resourcesViewModel?.getResourceLogoutMessage(CustomDrawerController.rootStore?.getLanguage),
            [
                {
                    text: CustomDrawerController.resourcesViewModel?.getResourceConfirmButton(CustomDrawerController.rootStore?.getLanguage),
                    onPress: async () => await this.signOut(),
                    style: 'default'
                },
                {
                    text: CustomDrawerController.resourcesViewModel?.getResourceCancelButton(CustomDrawerController.rootStore?.getLanguage),
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'default'
                }
            ],
            { cancelable: false }
        );
    }

    public signOut = async () => {
        this.removeDeviceToken()

        this.closeDrawer()
        //firebase analytic
        FirebaseAnalytics.userProperty(null)
        this._rootStore?.getAuthStore?.setIsLoggedIn(false)
        this._rootStore?.setIsSelectedFront(true)
        this._rootStore?.setIsSelectedSearch(false)
        this._rootStore?.setIsSelectedMyLibrary(false)
        this._rootStore?.setIsSelectedAboutUs(false)
        this._rootStore?.setIsSelectedNotofication(false)
        this._rootStore?.setIsSelectedTermOfUse(false)
        this._rootStore?.setIsSelectedPrivacy(false)
        this._rootStore?.setIndex(0)
        this._rootStore?.getUserStore?.forceLogoutWithoutAuth(this._rootStore)
        this._rootStore?.resetPlaybackStore()
        await NotificationServices.unSubScribeTopic()
        if (Platform.OS === "android") {
            PushNotification.setApplicationIconBadgeNumber(0)
        } else {
            PushNotificationIOS.setApplicationIconBadgeNumber(0)
        }
    }


    public removeDeviceToken = async () => {
        await FirebaseFireStore.removeDeviceToken(this._rootStore?.getUserStore.getUserId, deviceInfoModule.getUniqueId())
    }

    public setSelectedScreen = (front: boolean, search: boolean, myLibrary: boolean, noti: boolean, aboutUs: boolean, Terms: boolean, Privacy: boolean, index: number) => {
        CustomDrawerController.rootStore?.setIsSelectedFront(front),
            CustomDrawerController.rootStore?.setIsSelectedSearch(search),
            CustomDrawerController.rootStore?.setIsSelectedMyLibrary(myLibrary),
            CustomDrawerController.rootStore?.setIsSelectedNotofication(noti),
            CustomDrawerController.rootStore?.setIsSelectedAboutUs(aboutUs),
            CustomDrawerController.rootStore?.setIsSelectedTermOfUse(Terms),
            CustomDrawerController.rootStore?.setIsSelectedPrivacy(Privacy),
            CustomDrawerController.rootStore?.setIndex(index)
    }

    public setUI = (index) => {
        switch (index) {
            case 0: 
                this.setSelectedScreen(true, false, false, false, false, false, false, 0)
                break;
            case 1: 
                this.setSelectedScreen(false, true, false, false, false, false, false, 1)
                break;
            case 2: 
                this.setSelectedScreen(false, false, true, false, false, false, false, 2)
                break;
            case 3: 
                this.setSelectedScreen(false, false, false, true, false, false, false, 3)
                break;
            case 4: 
                this.setSelectedScreen(false, false, false, false, true, false, false, 4)
                break;
            case 5: 
                this.setSelectedScreen(false, false, false, false, false, true, false, 5)
                break;
            case 6: 
                this.setSelectedScreen(false, false, false, false, false, false, true, 6)
                break;
            default: 
                this.setSelectedScreen(true, false, false, false, false, false, false, 0)
                break;
        }
    }

    public onDrawerItemPressed = (identifier: any) => {
        if (CustomDrawerController.viewModel?.getIsDisabled) return

        CustomDrawerController.viewModel?.setIsDisabled(true)
        let actions: any[]
        switch (identifier) {
            default:
                actions = [DrawerActions.jumpTo(identifier, { isInitialized: false })]
                break
        }
        this.closeDrawer()
        Promise.all(actions.map(e => this._myProps?.navigation?.dispatch(e)));
    }

    public closeDrawer = () => {
        this._myProps?.navigation?.dispatch(DrawerActions.closeDrawer() as any)
    }

    /*
       Mark Life cycle
   */
}

export default CustomDrawerController