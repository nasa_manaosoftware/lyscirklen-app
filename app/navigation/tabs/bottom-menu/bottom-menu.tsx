import * as React from 'react'
import { NavigationKey } from '../../../constants/app.constant'
import { TabControllerProps } from '../tab.props'
import { CustomTabBar } from '../custom-tab/custom-tab'
import * as metric from '../../../theme/metric'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { FrontScreenStackComponents } from '../../stacks/front-screen-stack'
import { SearchStackComponents } from '../../stacks/search-stack'
import { LibraryStackComponents } from '../../stacks/library-stack'
import { AboutUsStackComponents } from '../../stacks/about-us-stack'
import { MessagesStackComponents } from '../../stacks/messages-stack'
import { PrivacyPolicyStackComponents } from '../../stacks/privacy-policy-stack'
import { TermOfUseStackComponents } from '../../stacks/term-of-use-stack'
import { observer } from 'mobx-react-lite'
import { useStores } from "../../../models/root-store"
import { color } from '../../../theme'

export const BottomMenuComponents = observer((mainProps: TabControllerProps) => {
    const Tab = createBottomTabNavigator()
    const rootStore = useStores()
    const { getIsConnected } = rootStore.getSharedStore

    return (
        <Tab.Navigator tabBar={props => CustomTabBar(mainProps?.tabController, NavigationKey.MainScreen, props as any, mainProps.rootStore)}
            {...metric.tabNavigationOptions}
            sceneContainerStyle={{
                paddingBottom: mainProps.rootStore?.getPlaybackStore?.getIsShowMiniPlayer ? metric.ratioHeight(80) : 0,
                marginTop: getIsConnected ? 0 : -20,
                backgroundColor: color.background
                // backgroundColor: color.redBadge
            }}
            tabBarOptions={{
                keyboardHidesTabBar: true
            }}>
            <Tab.Screen name={NavigationKey.FrontScreen} component={FrontScreenStackComponents} options={{ unmountOnBlur: true }} />
            
            <Tab.Screen name={NavigationKey.AboutUs} component={AboutUsStackComponents} initialParams={{ isInitialized: false }} options={{ unmountOnBlur: true }} />

            {/* ========= Load Stacks ========= */}
            <Tab.Screen
                name={NavigationKey.TermOfUse}
                component={TermOfUseStackComponents}
                options={{
                    tabBarButton: () => null,
                    tabBarLabel: () => null,
                    tabBarVisible: false, // if you don't want to see the tab bar
                    unmountOnBlur: true
                }} />

            <Tab.Screen
                name={NavigationKey.PrivacyPolicy}
                component={PrivacyPolicyStackComponents}
                options={{
                    tabBarButton: () => null,
                    tabBarLabel: () => null,
                    tabBarVisible: false, // if you don't want to see the tab bar
                    unmountOnBlur: true
                }} />

            <Tab.Screen
                name={NavigationKey.Messages}
                component={MessagesStackComponents}
                options={{
                    tabBarButton: () => null,
                    tabBarLabel: () => null,
                    tabBarVisible: false, // if you don't want to see the tab bar
                    unmountOnBlur: true
                }} />

            <Tab.Screen
                name={NavigationKey.SearchAudioBook}
                component={SearchStackComponents}
                options={{
                    tabBarButton: () => null,
                    tabBarLabel: () => null,
                    tabBarVisible: false, // if you don't want to see the tab bar
                    unmountOnBlur: true
                }} />

        </Tab.Navigator>
    )
    
})