import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"
// import { PlaybackStore } from '../../../models/playback-store'
import { TabController } from '../../../modules/tab.controller'

export interface BottomMenuComponentProps extends NavigationContainerProps<ParamListBase> {
    tabController: TabController,
    // playbackStore: PlaybackStore
}