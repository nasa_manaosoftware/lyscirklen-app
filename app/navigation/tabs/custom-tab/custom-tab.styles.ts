import { ViewStyle, ImageStyle } from "react-native"
import { images } from '../../../theme/images'
import * as metric from "../../../theme"
import { TabItemIconProps } from "./tab-item/tab-item.props"
import { NavigationKey } from "../../../constants/app.constant"
import { HeaderProps } from "../../../components/header/header.props"

export const CONTAINER: ViewStyle = {
    maxWidth: metric.screenWidth,
    minHeight: metric.ratioHeight(80)
}

export const TAB_BAR_CONTAINER: ViewStyle = {
    height: metric.ratioHeight(80),
    flexDirection: 'row',
    borderTopWidth: metric.ratioHeight(1),
    borderTopColor: '#FFFFFF33'
}

const CENTER: ImageStyle = {
    alignSelf: 'center'
}

export const getTabMenuIconProps = (index: number, key: string): TabItemIconProps => {

    if (key === NavigationKey.MainScreen) {
        switch (index) {
            case 0:
                return {
                    iconSource: images.home,
                    iconFocused: images.homeActive,
                    iconStyle: {
                        width: metric.ratioWidth(44),
                        height: metric.ratioHeight(24),
                        ...CENTER
                    }
                }
            case 1:
                return {
                    iconSource: images.aboutUs,
                    iconFocused: images.aboutUsActive,
                    iconStyle: {
                        width: metric.ratioWidth(44),
                        height: metric.ratioHeight(24),
                        ...CENTER
                    }
                }
            default:
                return null
        }
    }
    return null
}

export const HEADER_CONTAINER: ViewStyle = {
    height: metric.isNewIPhone ? metric.ratioHeight(200) : metric.isIPhone ? metric.ratioHeight(200) : metric.ratioHeight(80),
    justifyContent: 'flex-end'
}

export const MENU_ICON_STYLE: ImageStyle = {
    width: metric.ratioWidth(44),
    height: metric.ratioHeight(24)
  }

export const HEADER_PROPS: HeaderProps = {
    leftIconSource: images.collapse,
    leftIconStyle: MENU_ICON_STYLE,
    rightIconStyle: MENU_ICON_STYLE,
    rightIconSource: images.redClose,
    isLeftIconAnimated: false,
    isRightIconAnimated: false
  }