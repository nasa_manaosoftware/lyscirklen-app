import * as React from 'react'
import { MaterialTopTabBarProps } from "@react-navigation/material-top-tabs"
import { Platform, View } from "react-native"
import * as Styles from "./custom-tab.styles"
import { TabItem } from "./tab-item/tab-item"
import { TabItemProps } from "./tab-item/tab-item.props"
import { TabController } from '../../../modules/tab.controller'
import { RootStore } from '../../../models/root-store/root.types'

export const CustomTabBar = (tabController: TabController, tabBarKey: string, props: MaterialTopTabBarProps, rootStore: RootStore) => {
  tabController?.setupChildProps(props)
  const { state } = props
  const hidePlayer = () => {
    rootStore?.getPlaybackStore?.onHidePlayer()
  }
  const showIndexes = [0, 1]
  return (
    <View shouldRasterizeIOS style={Styles.TAB_BAR_CONTAINER}>
      {state?.routes?.map((route: any, index: number) => {
        if (showIndexes.includes(index)) {
          const isFocused = state.index === index
          const tabItemProps: TabItemProps = {
            //hidePlayer(), shouldShowMiniPlayer()
            onPress: () => [tabController.onPress(route, index), hidePlayer()],
            // onLongPress: () => tabController.onLongPress(route),
            iconProps: Styles.getTabMenuIconProps(index, tabBarKey),
            isFocused: isFocused
          }
          return <TabItem {...tabItemProps} key={index} />
        } else {
          return null
        }})}
    </View>
  )
}