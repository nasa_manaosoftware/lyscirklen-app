import { ImageStyle } from "react-native";

export type TabItemIconProps = {
    iconSource?: object,
    iconFocused?: object,
    iconStyle?: ImageStyle,
}

export interface TabItemProps {
    onPress?: () => void
    onLongPress?: () => void
    isFocused?: boolean
    iconProps?: TabItemIconProps
}