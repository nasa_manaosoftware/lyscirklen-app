import { ViewStyle } from "react-native";
import * as metric from "../../../../theme"
import { FULL } from "../../../../styles/styles";

export const CONTAINNER: ViewStyle = {
    ...FULL,
}

export const TAB_ITEM: ViewStyle = {
    ...CONTAINNER,
    height: metric.ratioHeight(80),
    justifyContent: 'center'
}

export const TINT_COLOR_FOCUSED: any = ['rgba(233,140,183,1)', 'rgba(228,143,187,1)']
export const TINT_COLOR_BLURRED: any = ['rgba(0,0,0,0)', 'rgba(0,0,0,0)']

