import * as React from 'react'
import { TabItemProps } from "./tab-item.props"
import * as Style from "./tab-item.styles"
import { TouchableHighlight, View } from 'react-native'
import FastImage from 'react-native-fast-image'
import { color } from '../../../../theme/color'

export const TabItem = (props: TabItemProps) => {
    return (
        <View shouldRasterizeIOS style={Style.CONTAINNER}>
           <TouchableHighlight
                onPress={props?.onPress}
                onLongPress={props?.onLongPress}
                activeOpacity={0.8}
                underlayColor={color.drawBackground}
                style={Style.TAB_ITEM}
                // disabled={props.isFocused}
                >
                <FastImage
                    source={props?.isFocused ? props?.iconProps?.iconFocused : props?.iconProps?.iconSource}
                    style={props?.iconProps?.iconStyle}
                    resizeMode={'contain'} />
            </TouchableHighlight>
        </View>)
}

