import React from "react"
import { NavigationKey } from "../../constants/app.constant"
import { createStackNavigator } from "@react-navigation/stack"
import * as metric from "../../theme/metric"
import { DrawerComponents } from "../drawers/main-drawer"
import { Splashscreen } from "../../modules/splash/splash.screen"

const MainStack = createStackNavigator()

export const MainStackComponents = () => {

    return (
        <MainStack.Navigator
            mode="modal"
            screenOptions={metric.baseStackNavigaitonOptions}
            initialRouteName={NavigationKey.Splash}
        >
            <MainStack.Screen name={NavigationKey.MainStack} component={DrawerComponents} options={{ headerShown: false }} />
        </MainStack.Navigator>)
}