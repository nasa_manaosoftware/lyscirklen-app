import React from "react"
import { NavigationKey } from "../../constants/app.constant"
import { createStackNavigator } from "@react-navigation/stack"
import * as metric from "../../theme/metric"
//import sreen
import { Header } from "../../components/header/header"
import { Image, ImageStyle, TouchableOpacity, View } from "react-native"
import { HeaderProps } from "../../components/header/header.props"
import { images } from "../../theme/images"
import { color } from "../../theme"
import { DrawerActions, CommonActions } from "@react-navigation/core"
import { TermOfUse } from "../../modules/term-of-use/term-of-use"
import { useStores } from "../../models/root-store"
import { MessageDetailScreen } from "../../modules/message-detail-screen/message-detail-screen"
import { BACK_HEADER, BACK_HEADER_OFFLINE, HEADER_CONTAINER, HEADER_CONTAINER_OFFLINE } from "./front-screen-stack"
import { AuidoBookScreen } from "../../modules/audio-book-screen/audio-book-screen"

const disableGesture = {
    gestureEnabled: false
}

const TermOfUseStack = createStackNavigator()

export const MENU_ICON_STYLE: ImageStyle = {
    width: metric.ratioWidth(24),
    height: metric.ratioHeight(24)
}

export const HEADER_PROPS: HeaderProps = {
    leftIconSource: images.hamMenu,
    leftIconStyle: MENU_ICON_STYLE,
    rightIconStyle: MENU_ICON_STYLE,
    rightIconSource: images.whiteBell,
    isLeftIconAnimated: false,
    isRightIconAnimated: false,
    containerStyle: { backgroundColor: color.background }
}


export const TermOfUseStackComponents = ({ navigation }) => {

    const rootStore = useStores()

    return (<TermOfUseStack.Navigator
        mode='card'
        screenOptions={metric.baseStackNavigaitonOptions}
        initialRouteName={NavigationKey.TermOfUseStack}
    >
        <TermOfUseStack.Screen
            name={NavigationKey.TermOfUse}
            component={TermOfUse}
            options={{
                header: props =>
                    <View style={rootStore?.getSharedStore?.getIsConnected ? HEADER_CONTAINER : HEADER_CONTAINER_OFFLINE}>
                        <Header
                            {...HEADER_PROPS}
                            onLeftPress={() => navigation?.dispatch(DrawerActions.openDrawer() as any)}
                            onRightPress={() => navigation?.dispatch(CommonActions.navigate(NavigationKey.Messages) as any,
                                rootStore?.setIsSelectedFront(false),
                                rootStore?.setIsSelectedSearch(false),
                                rootStore?.setIsSelectedMyLibrary(false),
                                rootStore?.setIsSelectedNotofication(true),
                                rootStore?.setIsSelectedAboutUs(false),
                                rootStore?.setIsSelectedTermOfUse(false),
                                rootStore?.setIsSelectedPrivacy(false),
                                rootStore?.setIndex(3)
                            )}
                            onCenterPress={() => navigation?.dispatch(CommonActions.navigate(NavigationKey.AboutUs) as any,
                                rootStore?.setIsSelectedFront(false),
                                rootStore?.setIsSelectedSearch(false),
                                rootStore?.setIsSelectedMyLibrary(false),
                                rootStore?.setIsSelectedNotofication(false),
                                rootStore?.setIsSelectedAboutUs(true),
                                rootStore?.setIsSelectedTermOfUse(false),
                                rootStore?.setIsSelectedPrivacy(false),
                                rootStore?.setIndex(4))}
                        />
                    </View>
            }}
        />

        <TermOfUseStack.Screen
            name={NavigationKey.MessageDetail}
            component={MessageDetailScreen}
            options={{
                header: (props) =>
                    <View style={{ width: '100%', backgroundColor: color.background }}>
                        <TouchableOpacity
                            style={BACK_HEADER}
                            onPress={() => {
                                props.previous.route.name === NavigationKey.Messages ?
                                    navigation?.pop()
                                    :
                                    navigation?.goBack()
                            }}
                            activeOpacity={1}
                        >
                            <Image source={images.back} resizeMode={'contain'} style={MENU_ICON_STYLE} />
                        </TouchableOpacity>
                    </View>
                ,
                ...metric.baseStackNavigaitonOptions
            }}
        />

        <TermOfUseStack.Screen
            name={NavigationKey.AudioBookScreen}
            component={AuidoBookScreen}
            options={{
                header: (props) =>
                    <View style={rootStore?.getSharedStore?.getIsConnected ? HEADER_CONTAINER : HEADER_CONTAINER_OFFLINE}>
                        <TouchableOpacity
                            style={rootStore?.getSharedStore?.getIsConnected ? BACK_HEADER : BACK_HEADER_OFFLINE}
                            onPress={() => {
                                navigation?.pop()
                            }}
                            activeOpacity={1}
                        >
                            <Image source={images.back} resizeMode={'contain'} style={MENU_ICON_STYLE} />
                        </TouchableOpacity>
                    </View>
                ,
                headerLeft: () => {
                    return null;
                },
                ...metric.baseStackNavigaitonOptions
            }}
        />

    </TermOfUseStack.Navigator>)
}