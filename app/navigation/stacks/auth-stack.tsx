import React from "react"
import { NavigationKey } from "../../constants/app.constant"
import { createStackNavigator } from "@react-navigation/stack"
import * as metric from "../../theme/metric"
import { SigninScreen } from "../../modules/signin/signin.screen"

const disableGesture = {
    gestureEnabled: false
}
export const AuthStack = createStackNavigator()

export const AuthStackComponents = () => {

    return (
        <AuthStack.Navigator initialRouteName={NavigationKey.Signin}
            screenOptions={metric.baseStackNavigaitonOptions}
            headerMode="none"
        >
            <AuthStack.Screen name={NavigationKey.Signin} component={SigninScreen} options={disableGesture} />
        </AuthStack.Navigator>)
}
