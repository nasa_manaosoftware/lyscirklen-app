import React from "react"
import { NavigationKey } from "../../constants/app.constant"
import { createStackNavigator } from "@react-navigation/stack"
import * as metric from "../../theme/metric"

//import sreen
import { Header } from "../../components/header/header"
import { ImageStyle, TouchableOpacity, View, ViewStyle } from "react-native"
import { HeaderProps } from "../../components/header/header.props"
import { images } from "../../theme/images"
import { color } from "../../theme"
import { Image } from "react-native-animatable"
import { DrawerActions, CommonActions } from "@react-navigation/core"
import { AboutUs } from "../../modules/about-us/about-us"
import { Messages } from "../../modules/messages/messages"
import { useStores } from "../../models/root-store"
import { MessageDetailScreen } from "../../modules/message-detail-screen/message-detail-screen"
import { BACK_HEADER_OFFLINE, HEADER_CONTAINER, HEADER_CONTAINER_OFFLINE } from "./front-screen-stack"
import { AuidoBookScreen } from "../../modules/audio-book-screen/audio-book-screen"

const disableGesture = {
    gestureEnabled: false
}

const AboutUsStack = createStackNavigator()

export const MENU_ICON_STYLE: ImageStyle = {
    width: metric.ratioWidth(24),
    height: metric.ratioHeight(24)
}

export const HEADER_PROPS: HeaderProps = {
    leftIconSource: images.hamMenu,
    leftIconStyle: MENU_ICON_STYLE,
    rightIconStyle: MENU_ICON_STYLE,
    rightIconSource: images.whiteBell,
    isLeftIconAnimated: false,
    isRightIconAnimated: false,
    containerStyle: { backgroundColor: color.background }
}

export const BACK_HEADER: ViewStyle = {
    backgroundColor: color.background,
    height: metric.ratioHeight(50),
    width: metric.ratioWidth(50),
    justifyContent: 'center',
    paddingLeft: metric.ratioWidth(10),
    marginTop: metric.backButtonHeaderMarginTop,
}

export const AboutUsStackComponents = ({ navigation }) => {

    const rootStore = useStores()

    return (<AboutUsStack.Navigator
        mode="card"
        screenOptions={metric.baseStackNavigaitonOptions}
        initialRouteName={NavigationKey.AboutUsStack}
    >
        <AboutUsStack.Screen
            name={NavigationKey.AboutUs}
            component={AboutUs}
            options={{
                header: props =>
                    <View style={rootStore?.getSharedStore?.getIsConnected ? HEADER_CONTAINER : HEADER_CONTAINER_OFFLINE}>
                        <Header
                            {...HEADER_PROPS}
                            onLeftPress={() => navigation?.dispatch(DrawerActions.openDrawer() as any)}
                            onRightPress={() =>
                                navigation?.dispatch(CommonActions.navigate(NavigationKey.Messages) as any,
                                    rootStore?.setIsSelectedFront(false),
                                    rootStore?.setIsSelectedSearch(false),
                                    rootStore?.setIsSelectedMyLibrary(false),
                                    rootStore?.setIsSelectedNotofication(true),
                                    rootStore?.setIsSelectedAboutUs(false),
                                    rootStore?.setIsSelectedTermOfUse(false),
                                    rootStore?.setIsSelectedPrivacy(false),
                                    rootStore?.setIndex(3)
                                )}
                        />
                    </View>
            }}
        />

        <AboutUsStack.Screen
            name={NavigationKey.Messages}
            component={Messages}
            options={{
                header: (props) =>
                    <View style={rootStore?.getSharedStore?.getIsConnected ? HEADER_CONTAINER : HEADER_CONTAINER_OFFLINE}>
                        <TouchableOpacity
                            style={rootStore?.getSharedStore?.getIsConnected ? BACK_HEADER : BACK_HEADER_OFFLINE}
                            onPress={() => {
                                navigation?.pop()
                            }}
                            activeOpacity={1}
                        >
                            <Image source={images.back} resizeMode={'contain'} style={MENU_ICON_STYLE} />
                        </TouchableOpacity>
                    </View>
                ,
                ...metric.baseStackNavigaitonOptions
            }}
        />

        <AboutUsStack.Screen
            name={NavigationKey.MessageDetail}
            component={MessageDetailScreen}
            options={{
                header: (props) =>
                    <View style={rootStore?.getSharedStore?.getIsConnected ? HEADER_CONTAINER : HEADER_CONTAINER_OFFLINE}>
                        <TouchableOpacity
                            style={rootStore?.getSharedStore?.getIsConnected ? BACK_HEADER : BACK_HEADER_OFFLINE}
                            onPress={() => {
                                props.previous.route.name === NavigationKey.Messages ?
                                    navigation?.pop()
                                    :
                                    navigation?.goBack()
                            }}
                            activeOpacity={1}
                        >
                            <Image source={images.back} resizeMode={'contain'} style={MENU_ICON_STYLE} />
                        </TouchableOpacity>
                    </View>
                ,
                ...metric.baseStackNavigaitonOptions
            }}
        />

        <AboutUsStack.Screen
            name={NavigationKey.AudioBookScreen}
            component={AuidoBookScreen}
            options={{
                header: (props) =>
                    <View style={rootStore?.getSharedStore?.getIsConnected ? HEADER_CONTAINER : HEADER_CONTAINER_OFFLINE}>
                        <TouchableOpacity
                            style={rootStore?.getSharedStore?.getIsConnected ? BACK_HEADER : BACK_HEADER_OFFLINE}
                            onPress={() => {
                                navigation?.pop()
                            }}
                            activeOpacity={1}
                        >
                            <Image source={images.back} resizeMode={'contain'} style={MENU_ICON_STYLE} />
                        </TouchableOpacity>
                    </View>
                ,
                headerLeft: () => {
                    return null;
                },
                ...metric.baseStackNavigaitonOptions
            }}
        />

    </AboutUsStack.Navigator>)
}