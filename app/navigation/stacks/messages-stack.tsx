import React from "react"
import { NavigationKey } from "../../constants/app.constant"
import { createStackNavigator } from "@react-navigation/stack"
import * as metric from "../../theme/metric"
//import sreen
import { Header } from "../../components/header/header"
import { Alert, ImageStyle, TouchableOpacity, View, ViewStyle, Platform } from "react-native"
import { HeaderProps } from "../../components/header/header.props"
import { images } from "../../theme/images"
import { color } from "../../theme"
import { DrawerActions, CommonActions } from "@react-navigation/core"
import { Messages } from "../../modules/messages/messages"
import { AuidoBookScreen } from "../../modules/audio-book-screen/audio-book-screen"
import { MessageDetailScreen } from "../../modules/message-detail-screen/message-detail-screen"
import { Image } from "react-native-animatable"
// import { HEADER_CONTAINER } from "../tabs/custom-tab/custom-tab.styles"
import { BACK_HEADER_OFFLINE, HEADER_CONTAINER_OFFLINE } from "./front-screen-stack"
import { useStores } from "../../models/root-store"

const disableGesture = {
    gestureEnabled: false
}

const MessagesStack = createStackNavigator()

export const MENU_ICON_STYLE: ImageStyle = {
    width: metric.ratioWidth(24),
    height: metric.ratioHeight(24)
}

export const LOGO_ICON_STYLE: ImageStyle = {
    width: metric.ratioWidth(71.56),
    height: metric.ratioHeight(34)
}

export const HEADER_PROPS: HeaderProps = {
    leftIconSource: images.hamMenu,
    leftIconStyle: MENU_ICON_STYLE,
    rightIconStyle: MENU_ICON_STYLE,
    rightIconSource: images.whiteBell,
    isLeftIconAnimated: false,
    isRightIconAnimated: false,
    containerStyle: { backgroundColor: color.background }
}

export const BACK_HEADER: ViewStyle = {
    backgroundColor: color.background,
    height: Platform.OS === 'android' ? metric.ratioHeight(50) : metric.ratioHeight(50),
    width: metric.ratioWidth(50),
    justifyContent: 'center',
    paddingLeft: metric.ratioWidth(10),
    marginTop: metric.backButtonHeaderMarginTop,
}

export const HEADER_CONTAINER: ViewStyle = {
    width: '100%',
    backgroundColor: color.background,
    marginTop: metric.ratioHeight(0)
}

export const MessagesStackComponents = ({ navigation }) => {

    const rootStore = useStores()

    return (<MessagesStack.Navigator
        mode='card'
        screenOptions={metric.baseStackNavigaitonOptions}
        initialRouteName={NavigationKey.MessagesStack}
    >
        <MessagesStack.Screen
            name={NavigationKey.Messages}
            component={Messages}
            options={{
                header: props =>
                    <View style={rootStore?.getSharedStore?.getIsConnected ? HEADER_CONTAINER : HEADER_CONTAINER_OFFLINE}>
                        <Header
                            {...HEADER_PROPS}
                            onLeftPress={() => navigation?.dispatch(DrawerActions.openDrawer() as any)}
                            onRightPress={() => navigation?.dispatch(CommonActions.navigate(NavigationKey.Messages) as any)}
                            onCenterPress={() => navigation?.dispatch(CommonActions.navigate(NavigationKey.AboutUs) as any,
                                rootStore?.setIsSelectedFront(false),
                                rootStore?.setIsSelectedSearch(false),
                                rootStore?.setIsSelectedMyLibrary(false),
                                rootStore?.setIsSelectedNotofication(false),
                                rootStore?.setIsSelectedAboutUs(true),
                                rootStore?.setIsSelectedTermOfUse(false),
                                rootStore?.setIsSelectedPrivacy(false),
                                rootStore?.setIndex(4))}
                        />
                    </View>
            }}
        />

        <MessagesStack.Screen
            name={NavigationKey.MessageDetail}
            component={MessageDetailScreen}
            options={{
                header: (props) =>
                    <View style={rootStore?.getSharedStore?.getIsConnected ? HEADER_CONTAINER : HEADER_CONTAINER_OFFLINE}>
                        <View style={{flexDirection: 'row'}}>
                        <TouchableOpacity
                            style={rootStore?.getSharedStore?.getIsConnected ? BACK_HEADER : BACK_HEADER_OFFLINE}
                            onPress={() => {
                                props.previous.route.name === NavigationKey.Messages ?
                                    navigation?.pop()
                                    :
                                    navigation?.goBack()
                            }}
                            activeOpacity={1}
                        >
                            <Image source={images.back} resizeMode={'contain'} style={MENU_ICON_STYLE} />
                        </TouchableOpacity>
                        <TouchableOpacity style={rootStore?.getSharedStore?.getIsConnected ? [BACK_HEADER, {width: '75%', alignItems: 'center'}] : [BACK_HEADER_OFFLINE, {width: '75%', alignItems: 'center'}] }
                            onPress={() => navigation?.dispatch(CommonActions.navigate(NavigationKey.AboutUs) as any)}
                            activeOpacity={1}
                        >
                            <Image source={images.appLogo} resizeMode={'contain'} style={LOGO_ICON_STYLE} />
                        </TouchableOpacity>
                        </View>
                    </View>
                ,
                ...metric.baseStackNavigaitonOptions
            }}
        />

        <MessagesStack.Screen
            name={NavigationKey.AudioBookScreen}
            component={AuidoBookScreen}
            options={{
                header: (props) =>
                    <View style={rootStore?.getSharedStore?.getIsConnected ? HEADER_CONTAINER : HEADER_CONTAINER_OFFLINE}>
                        <TouchableOpacity
                            style={rootStore?.getSharedStore?.getIsConnected ? BACK_HEADER : BACK_HEADER_OFFLINE}
                            onPress={() => {
                                navigation?.pop()
                            }}
                            activeOpacity={1}
                        >
                            <Image source={images.back} resizeMode={'contain'} style={MENU_ICON_STYLE} />
                        </TouchableOpacity>
                    </View>
                ,
                ...metric.baseStackNavigaitonOptions
            }}
        />


    </MessagesStack.Navigator>)
}