import React from "react"
import { NavigationKey } from "../../constants/app.constant"
import { createStackNavigator } from "@react-navigation/stack"
import * as metric from "../../theme/metric"

//import sreen
import { FrontScreen } from '../../modules/front-screen/front-screen'
import { AuidoBookScreen } from "../../modules/audio-book-screen/audio-book-screen"
import { MessageDetailScreen } from "../../modules/message-detail-screen/message-detail-screen"
import { Messages } from "../../modules/messages/messages"
import { Header } from "../../components/header/header"
import { ImageStyle, TouchableOpacity, View, ViewStyle, Platform } from "react-native"
import { HeaderProps } from "../../components/header/header.props"
import { images } from "../../theme/images"
import { color } from "../../theme"
import { Image } from "react-native-animatable"
import { DrawerActions, CommonActions } from "@react-navigation/core"
import { AudiobooksCategoryScreen } from "../../modules/audiobooks-category-screen/audiobooks-category-screen"
import { PlayBackScreen } from "../../modules/playback_screen/playback-screen"
import { useStores } from "../../models/root-store"
import { Splashscreen } from "../../modules/splash/splash.screen"

const disableGesture = {
    gestureEnabled: false
}

const FrontScreenStack = createStackNavigator()

export const MENU_ICON_STYLE: ImageStyle = {
    width: metric.ratioWidth(24),
    height: metric.ratioHeight(24)
}

export const HEADER_PROPS: HeaderProps = {
    leftIconSource: images.hamMenu,
    leftIconStyle: MENU_ICON_STYLE,
    rightIconStyle: MENU_ICON_STYLE,
    rightIconSource: images.whiteBell,
    isLeftIconAnimated: false,
    isRightIconAnimated: false,
    containerStyle: { backgroundColor: color.background }
}

export const calculatedContainer: ViewStyle = {
    marginTop: parseFloat((15 || 0).toString()) + parseFloat((15 || 0).toString())
}

export const BACK_HEADER: ViewStyle = {
    backgroundColor: color.background,
    height: metric.ratioHeight(50),
    width: metric.ratioWidth(50),
    justifyContent: 'center',
    paddingLeft: metric.ratioWidth(10),
    marginTop: metric.backButtonHeaderMarginTop,
}

export const BACK_HEADER_OFFLINE: ViewStyle = {
    backgroundColor: color.background,
    height: metric.ratioHeight(50),
    width: metric.ratioWidth(50),
    justifyContent: 'center',
    paddingLeft: metric.ratioWidth(10),
    marginTop: metric.ratioHeight(50),
}

export const HEADER_CONTAINER: ViewStyle = {
    width: '100%',
    backgroundColor: color.background,
    marginTop: metric.ratioHeight(0)
}

export const HEADER_CONTAINER_OFFLINE: ViewStyle = {
    width: '100%',
    backgroundColor: color.background,
    marginTop: metric.ratioHeight(-20)
}


export const FrontScreenStackComponents = ({ navigation }) => {

    const rootStore = useStores()

    return (<FrontScreenStack.Navigator
        mode='card'
        initialRouteName={NavigationKey.FrontScreenStack}
    >

        <FrontScreenStack.Screen
            name={NavigationKey.FrontScreen}
            component={FrontScreen}
            options={{
                header: props =>
                    <View style={rootStore?.getSharedStore?.getIsConnected ? HEADER_CONTAINER : HEADER_CONTAINER_OFFLINE}>
                        <Header
                            {...HEADER_PROPS}
                            onRightPress={() =>
                                navigation?.dispatch(CommonActions.navigate(NavigationKey.Messages) as any,
                                    rootStore?.setIsSelectedFront(false),
                                    rootStore?.setIsSelectedSearch(false),
                                    rootStore?.setIsSelectedMyLibrary(false),
                                    rootStore?.setIsSelectedNotofication(true),
                                    rootStore?.setIsSelectedAboutUs(false),
                                    rootStore?.setIsSelectedTermOfUse(false),
                                    rootStore?.setIsSelectedPrivacy(false),
                                    rootStore?.setIndex(3)
                                )}
                            onLeftPress={() => navigation?.dispatch(DrawerActions.openDrawer() as any)}
                            onCenterPress={() => navigation?.dispatch(CommonActions.navigate(NavigationKey.AboutUs) as any,
                                rootStore?.setIsSelectedFront(false),
                                rootStore?.setIsSelectedSearch(false),
                                rootStore?.setIsSelectedMyLibrary(false),
                                rootStore?.setIsSelectedNotofication(false),
                                rootStore?.setIsSelectedAboutUs(true),
                                rootStore?.setIsSelectedTermOfUse(false),
                                rootStore?.setIsSelectedPrivacy(false),
                                rootStore?.setIndex(4))}
                        />
                    </View>
                ,
                headerLeft: () => {
                    return null;
                },
                ...metric.baseStackNavigaitonOptions
            }}
        />
        <FrontScreenStack.Screen
            name={NavigationKey.AudioBookScreen}
            component={AuidoBookScreen}
            options={{
                header: (props) =>
                    <View style={rootStore?.getSharedStore?.getIsConnected ? HEADER_CONTAINER : HEADER_CONTAINER_OFFLINE}>
                        <TouchableOpacity
                            style={rootStore?.getSharedStore?.getIsConnected ? BACK_HEADER : BACK_HEADER_OFFLINE}
                            //style={ (rootStore?.getSharedStore?.getIsConnected || (Platform.OS === 'android')) ? BACK_HEADER : BACK_HEADER_OFFLINE }
                            onPress={() => {
                                navigation?.pop()
                            }}
                            activeOpacity={1}
                        >
                            <Image source={images.back} resizeMode={'contain'} style={MENU_ICON_STYLE} />
                        </TouchableOpacity>
                    </View>
                ,
                headerLeft: () => {
                    return null;
                },
                ...metric.baseStackNavigaitonOptions
            }}
        />
        <FrontScreenStack.Screen
            name={NavigationKey.MessageDetail}
            component={MessageDetailScreen}
            options={{
                header: (props) =>
                    <View style={rootStore?.getSharedStore?.getIsConnected ? HEADER_CONTAINER : HEADER_CONTAINER_OFFLINE}>
                        <TouchableOpacity
                            // style={BACK_HEADER}
                            style={rootStore?.getSharedStore?.getIsConnected ? BACK_HEADER : BACK_HEADER_OFFLINE}
                            onPress={() => {
                                props.previous.route.name === NavigationKey.Messages ?
                                    navigation?.pop()
                                    :
                                    navigation?.goBack()
                            }}
                            activeOpacity={1}
                        >
                            <Image source={images.back} resizeMode={'contain'} style={MENU_ICON_STYLE} />
                        </TouchableOpacity>
                    </View>
                ,
                headerLeft: () => {
                    return null;
                },
                ...metric.baseStackNavigaitonOptions
            }}
        />

        <FrontScreenStack.Screen
            name={NavigationKey.Messages}
            component={Messages}
            options={{
                header: (props) =>
                    <View style={rootStore?.getSharedStore?.getIsConnected ? HEADER_CONTAINER : HEADER_CONTAINER_OFFLINE}>
                        <TouchableOpacity
                            // style={BACK_HEADER}
                            style={rootStore?.getSharedStore?.getIsConnected ? BACK_HEADER : BACK_HEADER_OFFLINE}
                            onPress={() => {
                                navigation?.pop()

                            }}
                            activeOpacity={1}
                        >
                            <Image source={images.back} resizeMode={'contain'} style={MENU_ICON_STYLE} />
                        </TouchableOpacity>
                    </View>
                ,
                headerLeft: () => {
                    return null;
                },
                ...metric.baseStackNavigaitonOptions
            }}
        />

        <FrontScreenStack.Screen
            name={NavigationKey.AudiobooksCategory}
            component={AudiobooksCategoryScreen}
            options={{
                header: props =>
                    <View style={rootStore?.getSharedStore?.getIsConnected ? HEADER_CONTAINER : HEADER_CONTAINER_OFFLINE}>
                        <TouchableOpacity
                            // style={BACK_HEADER} 
                            style={rootStore?.getSharedStore?.getIsConnected ? BACK_HEADER : BACK_HEADER_OFFLINE}
                            onPress={() => navigation?.pop()}>
                            <Image source={images.back} resizeMode={'contain'} style={MENU_ICON_STYLE} />
                        </TouchableOpacity>
                    </View>
                ,
                headerLeft: () => {
                    return null;
                },
                ...metric.baseStackNavigaitonOptions
            }}
        />

        <FrontScreenStack.Screen
            name={NavigationKey.PlayBackScreen}
            component={PlayBackScreen}
            options={{
                header: props =>
                    <View style={{ width: '100%', backgroundColor: color.background }}>
                        <TouchableOpacity
                            // style={BACK_HEADER} 
                            style={rootStore?.getSharedStore?.getIsConnected ? BACK_HEADER : BACK_HEADER_OFFLINE}
                            onPress={() => navigation?.pop()}>
                            <Image source={images.back} resizeMode={'contain'} style={MENU_ICON_STYLE} />
                        </TouchableOpacity>
                    </View>
                ,
                headerLeft: () => {
                    return null;
                },
                ...metric.baseStackNavigaitonOptions
            }}
        />

    </FrontScreenStack.Navigator>)
}