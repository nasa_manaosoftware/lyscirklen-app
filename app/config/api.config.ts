import { NativeModules } from 'react-native'
const { ReactNativeConfig } = NativeModules
export const environment = ReactNativeConfig.buildEnvironment


export const fetchBaseURL = (env: string) => {
    switch (env) {
        // Production
        case "microlearnProduction":
        case "LyscirklenProduction":
        case "Production":
            return 'https://us-central1-business-danmark-1e889.cloudfunctions.net/api/'

        // Development
        case "microlearnDevelopment":
        case "LyscirklenDevelopment":
        case "Development":
            return 'https://us-central1-business-danmark-audiobook-dev.cloudfunctions.net/api/'

        // Staging
        case "microlearnStaging":
        case "LyscirklenStaging":
        case "Staging":
            return 'https://us-central1-business-danmark-book-staging.cloudfunctions.net/api/'

        default:
            return 'https://us-central1-business-danmark-audiobook-dev.cloudfunctions.net/api/'
    }
}

export const BaseURL: string = fetchBaseURL(environment)

export const ContentType: { [key: string]: any } = {
    URL_ENCODED: 'application/x-www-form-urlencoded',
    JSON: "application/json"
}
export const Timeout: number = 7000