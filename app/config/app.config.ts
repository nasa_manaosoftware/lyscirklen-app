import { NativeModules } from 'react-native'

import { NavigationKey } from "../constants/app.constant"

export const screenExitList = [NavigationKey.FrontScreen, NavigationKey.SearchAudioBook, NavigationKey.MyLibrary, NavigationKey.AboutUs, NavigationKey.TermOfUse, NavigationKey.PrivacyPolicy]

const { ReactNativeConfig } = NativeModules
export const environment = ReactNativeConfig.buildEnvironment
export const isDevelopment = (environment === "Development" || environment === "Alpha")

export const packageName = () => {
    switch (environment) {
        case "Development":
        case "LyscirklenDevelopment":
            //return 'com.lyscirklen.dev'
            return 'com.businessdanmark.dev'
        case "Staging":
        case "LyscirklenStaging":
            return 'com.lyscirklen.staging'
            //return 'com.businessdanmark.staging'
        default:
            //return 'com.lyscirklen.dev'
            return 'com.businessdanmark.dev'
        }
}