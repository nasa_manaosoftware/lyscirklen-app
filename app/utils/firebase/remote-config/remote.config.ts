import remoteConfig, { FirebaseRemoteConfigTypes } from '@react-native-firebase/remote-config'
import * as FirebaseConfig from "../../../config/firebase.config"

export const getRemoteConfigValues = async () => {
  const config = remoteConfig()
  let remoteConfigObject: any = {}
  try {
    await config.setDefaults({
      experiment: false,
    })
    await config.setConfigSettings({
      fetchTimeMillis: FirebaseConfig.remoteConfigCacheTime,
      minimumFetchIntervalMillis: FirebaseConfig.remoteConfigCacheTime
    })
    await config.fetch(FirebaseConfig.remoteConfigCacheTime)
    const activated = await config.activate()
    const configObjs = config.getAll()
    Object.entries(configObjs).forEach(([key, value]: [string, FirebaseRemoteConfigTypes.ConfigValue]) => {
      remoteConfigObject[key] = JSON.parse(value.asString())
    })

    return remoteConfigObject
  } catch (e) {
    return null
  }
}