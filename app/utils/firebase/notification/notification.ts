import PushNotificationIOS from '@react-native-community/push-notification-ios';
import messaging, { FirebaseMessagingTypes } from '@react-native-firebase/messaging'
import PushNotification from 'react-native-push-notification';

class NotificationServices {
    public getToken = async () => await messaging().getToken()
    public requestPermission = async () => await messaging().requestPermission()
    public setBackgroundMessageHandler = (message: (message: FirebaseMessagingTypes.RemoteMessage) => Promise<any>) => messaging().setBackgroundMessageHandler(message)
    public isDeviceRegisteredForRemoteMessages = messaging().isDeviceRegisteredForRemoteMessages
    public registerDeviceForRemoteMessages = async () => await messaging().registerDeviceForRemoteMessages()
    public deleteToken = async () => await messaging().deleteToken()
    public foregroundStateMessage = (listener: (message: FirebaseMessagingTypes.RemoteMessage) => Promise<any>) => messaging().onMessage(listener)
    public onNotificationOpenedTheApp = (message: (message: FirebaseMessagingTypes.RemoteMessage) => Promise<any>) =>{
        console.log('555');
        
        return messaging().onNotificationOpenedApp(message)
    }


    public onTokenRefresh = (callback: (token: string) => any) => messaging().onTokenRefresh(callback)
    public subscribeTopic = async () => await messaging().subscribeToTopic("all")
    public unSubScribeTopic = async () => await messaging().unsubscribeFromTopic("all")

    public createLocalNotification = (remoteMessage: FirebaseMessagingTypes.RemoteMessage) => {
        console.log('createLocalNotification', remoteMessage);

        PushNotification.createChannel(
            {
                channelId: "dmd", // (required)
                channelName: "Date Min Dude", // (required)
                channelDescription: "A channel to Date Min Dude your notifications", // (optional) default: undefined.
                soundName: "default", // (optional) See `soundName` parameter of `localNotification` function
                importance: 4, // (optional) default: 4. Int value of the Android notification importance
                vibrate: true, // (optional) default: true. Creates the default vibration patten if true.
            },
            (created) => console.log(`createChannel returned '${created}'`) // (optional) callback returns whether the channel was created, false means it already existed.
        );

        PushNotification.getChannels(function (channel_ids) {
            console.log(channel_ids); // ['channel_id_1']
        });

        PushNotification.localNotification({
            vibration: 300, // vibration length in milliseconds, ignored if vibrate=false, default: 1000

            /* iOS only properties */
            category: "", // (optional) default: empty string
            userInfo: remoteMessage?.data, // (optional) default: {} (using null throws a JSON value '<null>' error)

            // /* iOS and Android properties */
            title: remoteMessage?.notification?.title, // (optional)
            message: remoteMessage?.notification?.body, // (required)
        })
    }
}

export default new NotificationServices()
