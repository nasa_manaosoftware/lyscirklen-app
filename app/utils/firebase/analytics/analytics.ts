import analytics from '@react-native-firebase/analytics'

class FirebaseAnalytics {

    public videoPlayCompleted = (params: any) => {  
        console.log('videoPlayCompleted');     
        analytics().logEvent('play_video_completed', {
            user_id: params.user_id,
            video_name: params.book_name,
            category_name: params.category_name,
        })
    }

    public videoInitialPlayed = (params: any) => { 
        console.log('videoInitialPlayed');
        analytics().logEvent('initial_play_video', {
            video_name: params.book_name,
            category_name: params.category_name,
        })
    }

    public openVideoDetail = (params: any) => {
        console.log('openVideoDetail');
        analytics().logEvent('open_video_detail', {
            user_id: params.user_id,
            video_name: params.book_name,
            category_name: params.category_name,
        })
    }

    public userProperty = (userId: any) => {
        analytics().setUserProperty('member_id', userId)
    }

    public userLaunchApp = (params: any) => { 
        console.log('userLaunchApp');
        analytics().logEvent('launch_app', {
            user_id: params.user_id,
        })
    }

    public amountVideoPlayedPerUser = (params: any) => {
        console.log('amountVideoPlayedPerUser');
        analytics().logEvent('play_video_per_user', {
            user_id: params.user_id,
            video_name: params.book_name,
            category_name: params.category_name,
        })
    }

    public amountVideoAttached = (params: any) => {
        console.log('amountVideoAttached');
        analytics().logEvent('without_attached_video', {
            user_id: params.user_id,
            video_name: params.book_name,
            category_name: params.category_name,
            video_url: params.video_url
        })
        
    }
}

export default new FirebaseAnalytics()