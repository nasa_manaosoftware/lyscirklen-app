export type DynamicLink = {
    url: string
    minimumAppVersion: number | string | null
}