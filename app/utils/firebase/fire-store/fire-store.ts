import * as FireStoreConstant from "../../../constants/firebase/fire-store.constant"
import firestore, { FirebaseFirestoreTypes } from '@react-native-firebase/firestore'
import { INotificationListListener, IFirestoreListener, IUserListener } from "./fire-store.types"
import { Collection, Property } from "../../../constants/firebase/fire-store.constant"
import { DownloadStatus } from "../../../constants/app.constant"
import { FirebaseAuthErrorResponse } from "../../../constants/firebase/auth/auth-error-response.constant"

class FirebaseFireStore {
    private onFirestoreListener = (params: IFirestoreListener): () => void => {
        const { onNext, onError, onComplete } = params
        return (params.query as any)
            .onSnapshot(
                {
                    next: onNext && onNext,
                    error: onError && onError,
                    complete: onComplete && onComplete
                }
            )
    }

    //BD 
    public onNotificationListener = (params: INotificationListListener): () => void => {
        const { userId, ...callbacks } = params
        const query = firestore().collection(Collection.USERS)
            .doc(userId)
            .collection(Collection.MESSAGES)
        const firestoreParams: IFirestoreListener = {
            query,
            ...callbacks
        }
        return this.onFirestoreListener(firestoreParams)
    }

    //end

    //BD - fetch audiobooks
    public fetchAudiobooks = async (organization_id: string) => {
        const result = await firestore()
            .collection(FireStoreConstant.Collection.AUDIOBOOKS)
            .where("organization_id", "==", organization_id)
            .get()

        let data = []
        result?.docs?.map((docRef) => {
            data.push({
                uid: docRef?.id,
                created_date: docRef?.data()?.created_date.toDate(),
                category_id: docRef?.data()?.category_id,
                description: docRef?.data()?.description,
                forfatter: docRef?.data()?.forfatter,
                image: {
                    name: docRef?.data()?.image?.file_name,
                    // storage_path: docRef?.data()?.image?.storage_path,
                    // thumbnail_url: docRef?.data().image?.thumbnail_url,
                    url: docRef?.data()?.image?.url,
                },
                name: docRef?.data()?.name,
                organization_id: docRef?.data()?.organization_id,
                // video: {
                //     name: docRef?.data()?.video?.name,
                //     url: docRef?.data()?.video?.url,
                //     download_url: docRef?.data()?.video?.download_url,
                //     streaming_url: docRef?.data()?.video?.streaming_url
                // },
                video_url: docRef?.data()?.video_url,
                // download_status: DownloadStatus.DO_NOT_DOWNLOAD,
                // is_downloading: false,
                // file_path: ''
            })
        })
        return data
    }

    //BD - fetch audiobooks categories in firestore
    public fetchAudiobooksCategoriesFromFireStore = async (organization_id: string) => {
        const result = await firestore()
            .collection(FireStoreConstant.Collection.CATEGORIES)
            .where("organization_id", "==", organization_id)
            .get()

        let allCategory = []
        result?.docs?.map((docRef) => {
            allCategory.push({
                uid: docRef?.id,
                audio_book_count: docRef?.data()?.audio_book_count,
                name: docRef?.data()?.name,
                order: docRef?.data()?.order,
                organization_id: docRef?.data()?.organization_id
            })
        })

        //sort order
        let emptyOrder = allCategory.filter((category: any) => {
            if (category.order === 9007199254740991 || !category.order || category.order >= 1000) {
                return category
            }
        })
        let notEmptyOrder = allCategory.filter((category: any) => {
            if (category.order && category.order < 1000 && category.order !== 9007199254740991) {
                return category
            }
        })

        notEmptyOrder = notEmptyOrder.sort((firstCategory: any, secondCategory: any) => {
            if (firstCategory.order === secondCategory.order) {
                if (firstCategory.name.toUpperCase() < secondCategory.name.toUpperCase()) {
                    return -1
                } else {
                    return 1
                }
            } else {
                return firstCategory.order - secondCategory.order
            }
        })

        emptyOrder.sort((firstCategory, secondCategory) => firstCategory.name.toUpperCase() < secondCategory.name.toUpperCase() ? -1 : 1)
        allCategory = [...notEmptyOrder, ...emptyOrder]

        return allCategory

    }

    public fetchAudiobooksCategoriesFromFireStoreDidOrder = async () => {
        const result = await firestore()
            .collection(FireStoreConstant.Collection.CATEGORIES)
            .get()

        return result?.docs?.map((docRef) => docRef?.data())

    }

    public getPrivacyPolicy = async (appId: string) => {
        const result = await firestore().collection(Collection.PRIVACY_POLICY)
            .doc(appId)
            .get()
        return result?.data()
    }

    public getTermOfUse = async (appId: string) => {
        const result = await firestore().collection(Collection.TERM_OF_USE)
            .doc(appId)
            .get()
        return result?.data()
    }

    public getAboutUS = async (appId: string) => {
        const result = await firestore().collection(Collection.ABOUT_US)
            .doc(appId)
            .get()
        return result?.data()
    }
    
    //BD - fetch messages in firestore
    public fetchMessage = async (appId: string) => {
        const result = await firestore()
            .collection(FireStoreConstant.Collection.HOMESCREEN)
            .doc(appId)
            .get()

        return result?.data()
    }

    //BD - fetch audiobooks by categoryID
    public fetchAudiobooksByCategoryId = async (categoryId: string, organization_id: string) => {
        let lastAudiobook_id = ""
        const result = await firestore()
            .collection(Collection.AUDIOBOOKS)
            .where(`${FireStoreConstant.Property.CATEGORY_ID}`, "==", categoryId)
            .where(`${FireStoreConstant.Property.ORGANIZATION_ID}`, '==', organization_id)
            .orderBy(`${FireStoreConstant.Property.NAME}`, "asc")
            .limit(10)
            .get()

        let audiobooksList = []
        result?.docs?.map((docRef) => {
            audiobooksList.push({
                uid: docRef?.id,
                category_id: docRef?.data()?.category_id,
                description: docRef?.data()?.description,
                forfatter: docRef?.data()?.forfatter,
                image: {
                    file_name: docRef?.data()?.image?.file_name,
                    storage_path: docRef?.data()?.image?.storage_path,
                    thumbnail_url: docRef?.data().image?.thumbnail_url,
                    url: docRef?.data()?.image?.url,
                },
                name: docRef?.data()?.name,
                organization_id: docRef?.data()?.organization_id,
                video: {
                    name: docRef?.data()?.video?.name,
                    url: docRef?.data()?.video?.url,
                    download_url: docRef?.data()?.video?.download_url,
                    streaming_url: docRef?.data()?.video?.streaming_url
                },
                download_status: DownloadStatus.DO_NOT_DOWNLOAD,
                is_downloading: false,
                file_path: ''
            })
        })

        if (audiobooksList.length !== 0) {
            lastAudiobook_id = result.docs[result.docs.length - 1].id
        } else {
            lastAudiobook_id = ""
        }

        const audiobooksResult = {
            audiobooksList,
            lastAudiobook_id
        }

        return audiobooksResult
    }

    //BD - fetch audiobooks by categoryID
    public fetchAudiobooksWhenLoadMore = async (categoryId: string, audiobook_id: string, organization_id: string) => {
        let lastAudiobook_id = ""

        const lastDocData = await firestore()
            .collection(Collection.AUDIOBOOKS)
            .doc(audiobook_id)
            .get()

        const audiobooksList = await firestore()
            .collection(Collection.AUDIOBOOKS)
            .where(`${FireStoreConstant.Property.CATEGORY_ID}`, "==", categoryId)
            .where(`${FireStoreConstant.Property.ORGANIZATION_ID}`, "==", organization_id)
            .orderBy(`${FireStoreConstant.Property.NAME}`, "asc")
            .startAfter(lastDocData)
            .limit(10)
            .get()

        let resultAudiobooksList = []
        audiobooksList?.docs?.map((docRef) => {
            resultAudiobooksList.push({
                uid: docRef?.id,
                category_id: docRef?.data()?.category_id,
                description: docRef?.data()?.description,
                forfatter: docRef?.data()?.forfatter,
                image: {
                    file_name: docRef?.data()?.image?.file_name,
                    storage_path: docRef?.data()?.image?.storage_path,
                    thumbnail_url: docRef?.data().image?.thumbnail_url,
                    url: docRef?.data()?.image?.url,
                },
                name: docRef?.data()?.name,
                organization_id: docRef?.data()?.organization_id,
                video: {
                    name: docRef?.data()?.video?.name,
                    url: docRef?.data()?.video?.url,
                    download_url: docRef?.data()?.video?.download_url,
                    streaming_url: docRef?.data()?.video?.streaming_url
                },
                download_status: DownloadStatus.DO_NOT_DOWNLOAD,
                is_downloading: false,
                file_path: ''
            })
        })

        if (resultAudiobooksList.length !== 0) {
            lastAudiobook_id = audiobooksList.docs[audiobooksList.docs.length - 1].id
        } else {
            lastAudiobook_id = audiobook_id
        }

        const result = {
            resultAudiobooksList,
            lastAudiobook_id
        }
        return result
    }

    public fetchAudibookId = async (bookName: string) => {
        const result = await firestore()
            .collection(Collection.AUDIOBOOKS)
            .where('name', '==', bookName)
            .limit(1)
            .get()


        return result?.docs?.pop().id
    }

    public updateDownloadedAudioBook = async (params) => {
        try {
            const result = await firestore()
                .collection(Collection.DEVICES)
                .doc(params.deviceId)
                .collection(Collection.AUDIOBOOK)
                .doc(params.audiobook_id)
                .set({
                    job_id: params.job_id,
                    file_path: params.file_path,
                    category_id: params.category_id,
                    description: params.description,
                    forfatter: params.forfatter,
                    image: {
                        file_name: params.image.file_name,
                        storage_path: params.image.storage_path,
                        thumbnail_url: params.image.thumbnail_url,
                        url: params.image.url
                    },
                    name: params.name,
                    organization_id: params.organization_id,
                    video: {
                        name: params.video?.name,
                        url: params.video?.url,
                        download_url: params.video?.download_url,
                        streaming_url: params.video?.streaming_url
                    },
                    download_status: params.download_status
                })

            return result

        } catch (err) {
            console.log(err);
            return err

        }
    }

    public fetchMyDownloaded = async (deviceId: string, organization_id: string) => {
        try {
            const result = await firestore()
                .collection(Collection.DEVICES)
                .doc(deviceId)
                .collection(Collection.AUDIOBOOK)
                .where(`${FireStoreConstant.Property.ORGANIZATION_ID}`, "==", organization_id)
                .get()

            let downloaded = []

            result?.docs?.map((docRef) => {
                // if (docRef?.data()?.organization_id === organization_id) {
                downloaded.push({
                    uid: docRef?.id,
                    job_id: docRef?.data()?.job_id,
                    file_path: docRef?.data()?.file_path,
                    category_id: docRef?.data()?.category_id,
                    description: docRef?.data()?.description,
                    forfatter: docRef?.data()?.forfatter,
                    image: {
                        file_name: docRef?.data()?.image?.file_name,
                        storage_path: docRef?.data()?.image?.storage_path,
                        thumbnail_url: docRef?.data()?.image?.thumbnail_url,
                        url: docRef?.data()?.image?.url
                    },
                    name: docRef?.data()?.name,
                    organization_id: docRef?.data()?.organization_id,
                    video: {
                        name: docRef?.data()?.video?.name,
                        url: docRef?.data()?.video?.url,
                        download_url: docRef?.data()?.video?.download_url,
                        streaming_url: docRef?.data()?.video?.streaming_url
                    },
                    download_status: docRef?.data()?.download_status
                })
                // }
            })


            return downloaded
        } catch (err) {
            console.log(err);
            return err

        }
    }

    public setFormatMyAudiobookData = (data: any) => {
        let myDownloaded = []
        data?.docs?.map((docRef) => {
            myDownloaded.push({
                uid: docRef?.id,
                job_id: docRef?.data()?.job_id,
                file_path: docRef?.data()?.file_path,
                category_id: docRef?.data()?.category_id,
                description: docRef?.data()?.description,
                forfatter: docRef?.data()?.forfatter,
                image: {
                    file_name: docRef?.data()?.image?.file_name,
                    storage_path: docRef?.data()?.image?.storage_path,
                    thumbnail_url: docRef?.data()?.image?.thumbnail_url,
                    url: docRef?.data()?.image?.url
                },
                name: docRef?.data()?.name,
                organization_id: docRef?.data()?.organization_id,
                video: {
                    name: docRef?.data()?.video?.name,
                    url: docRef?.data()?.video?.url,
                    download_url: docRef?.data()?.video?.download_url,
                    streaming_url: docRef?.data()?.video?.streaming_url
                },
                download_status: docRef?.data()?.download_status,
                is_downloading: false
            })
        })
        return myDownloaded
    }

    public findLastAudiobook = (myDownloaded: any, previousLastId: string) => {
        let lastAudiobookId = ""
        if (myDownloaded.docs.length !== 0) {
            lastAudiobookId = myDownloaded.docs[myDownloaded.docs.length - 1].id
        } else {
            lastAudiobookId = previousLastId
        }
        return lastAudiobookId
    }

    public findAudiobook = async (id: string) => {
        try {
            const audiobook = await firestore()
                .collection(Collection.AUDIOBOOKS)
                .doc(id)
                .get()
            return audiobook?.data()
        } catch (err) {
            console.log(err);
            return {
                messages: err
            }
        }

    }

    public fetchMyAudiobooks = async (deviceId: string, organization_id: string, size: number) => {
        try {
            const result = await firestore()
                .collection(Collection.DEVICES)
                .doc(deviceId)
                .collection(Collection.AUDIOBOOK)
                .where(`${FireStoreConstant.Property.ORGANIZATION_ID}`, "==", organization_id)
                .orderBy(`${FireStoreConstant.Property.NAME}`, "asc")
                .limit(size)
                .get()

            const myDownloaded = this.setFormatMyAudiobookData(result)
            const lastAudiobook_id = this.findLastAudiobook(result, "")
            const myAudiobooksResult = {
                myAudiobooks: myDownloaded,
                lastAudibookId: lastAudiobook_id
            }
            return myAudiobooksResult
        } catch (err) {
            console.log(err);
            return err

        }
    }

    public loadMoreMyAudiobooks = async (deviceId: string, organization_id: string, size: number, lastId: string) => {
        try {
            const audiobookDoc = await firestore()
                .collection(Collection.DEVICES)
                .doc(deviceId)
                .collection(Collection.AUDIOBOOK)
                .doc(lastId)
                .get()

            const myAudiobooks = await firestore()
                .collection(Collection.DEVICES)
                .doc(deviceId)
                .collection(Collection.AUDIOBOOK)
                .where(`${FireStoreConstant.Property.ORGANIZATION_ID}`, "==", organization_id)
                .orderBy(`${FireStoreConstant.Property.NAME}`, "asc")
                .startAfter(audiobookDoc.data()?.name)
                .limit(size)
                .get()

            const myDownloaded = this.setFormatMyAudiobookData(myAudiobooks)
            const lastAudiobook_id = this.findLastAudiobook(myAudiobooks, lastId)
            const myAudiobooksResult = {
                myAudiobooks: myDownloaded,
                lastAudibookId: lastAudiobook_id
            }
            return myAudiobooksResult
        } catch (error) {
            console.log('error: ', error);
            return error

        }
    }

    public fetchAboutUsDetail = async (organization_id: string) => {
        try {

            const result = await firestore()
                .collection(Collection.ABOUT_US)
                .doc(organization_id)
                .get()

            return result.data()
        } catch (err) {
            console.log(err);
            return err
        }
    }

    public fetchMessages = async (organization_id: string) => {
        const result = await firestore()
            .collection(FireStoreConstant.Collection.PUSH_NOTIFICATIONS)
            .where("organization_id", "==", organization_id)
            .get()

        let data = []
        result?.docs?.map((docRef) => {
            data.push({
                message_id: docRef?.data()?.uid,
                button_text: docRef?.data()?.button_text ? docRef?.data()?.button_text : "GO TO AUDIOBOOK",
                heading: docRef?.data()?.heading,
                message: docRef?.data()?.message,
                organization_id: docRef?.data()?.organization_id,
                description: docRef?.data()?.description,
                sent_at: docRef?.data()?.sent_at ? docRef?.data()?.sent_at.toDate() : new Date(),
                read_message_status: false
            })
        })

        return data
    }

    public setReadMessage = async (membership_id: string, message_id: string) => {

        const result = await firestore()
            .collection(FireStoreConstant.Collection.USERS).doc(membership_id)
            .collection(FireStoreConstant.Collection.MESSAGES).doc(message_id)
            .update({
                read_message_status: true
            })
    }

    public setLoginDate = async (membership_id: string) => {
        const result = await firestore()
            .collection(FireStoreConstant.Collection.USERS).doc(membership_id)
            .update({
                login_date: new Date(),
            })
        console.log('result', result);
    }


    public setDeviceToken = async (userId: string, uuid: string, device_tokens: string) => {
        const result = await firestore()
            .collection(FireStoreConstant.Collection.USERS).doc(userId)
            .collection(FireStoreConstant.Collection.DEVICE_TOKENS).doc(uuid)
            .set({
                token: device_tokens
            })
    }

    public removeDeviceToken = async (membership_id: string, uuid: string) => {
        const result = await firestore()
            .collection(FireStoreConstant.Collection.USERS).doc(membership_id)
            .collection(FireStoreConstant.Collection.DEVICE_TOKENS).doc(uuid)
            .delete()

    }

    public updateDeviceTokenFlag = async (membership_id: string) => {
        const result = await firestore()
            .collection(FireStoreConstant.Collection.USERS).doc(membership_id)
            .update({
                has_device_token: true
            })
    }


    public fetchReadMessage = async (membership_id: string) => {
        
        const result = await firestore()
            .collection(FireStoreConstant.Collection.USERS).doc(membership_id)
            .collection(FireStoreConstant.Collection.MESSAGES)
            .get()

        let data = []
        result?.docs?.map((docRef) => {
            if (docRef?.data()?.heading !== undefined && docRef?.data()?.message !== undefined && docRef?.data()?.sent_at !== undefined) {
                data.push({
                    message_id: docRef?.id,
                    uid: docRef?.data()?.uid,
                    read_message_status: docRef?.data()?.read_message_status,
                    button_text: docRef?.data()?.button_text ? docRef?.data()?.button_text : "GO TO AUDIOBOOK",
                    description: docRef?.data()?.description,
                    organization_id: docRef?.data()?.organization_id,
                    heading: docRef?.data()?.heading,
                    message: docRef?.data()?.message,
                    sent_at: docRef?.data()?.sent_at ? docRef?.data()?.sent_at.toDate() : new Date(),
                })
            }

        })
        
        return data
    }

    public fetchUnReadMessages = async (membership_id: string) => {
        const messageList = await this.fetchReadMessage(membership_id)

        console.log('messageList ', messageList.length);
        

        let data = []
        messageList.map(item => {
            if (item.heading !== undefined && item.message !== undefined && item.sent_at !== undefined) {
                if (item.read_message_status === false) {
                    data.push(item)
                }
            }
        })
        return data
    }

    //Delete audiobook
    public deleteAudiobookInDevice = async (deviceId: string, audiobook_id: string): Promise<boolean> => {
        try {
            const result = await firestore()
                .collection(Collection.DEVICES)
                .doc(deviceId)
                .collection(Collection.AUDIOBOOK)
                .doc(audiobook_id)
                .delete()

            return true
        } catch (err) {
            console.log(err);
            return false
        }
    }

    public deleteAudiobookInDeviceOffline = async (deviceId: string, audiobook_id: string): Promise<boolean> => {
        try {
            const result = firestore()
                .collection(Collection.DEVICES)
                .doc(deviceId)
                .collection(Collection.AUDIOBOOK)
                .doc(audiobook_id)
                .delete()

            return true
        } catch (err) {
            console.log(err);
            return false
        }
    }

    //clear audiobooks downloaded
    public clearAudiobooksInDevice = async (deviceId: string, organization_id: string): Promise<boolean> => {
        try {
            await firestore()
                .collection(Collection.DEVICES)
                .doc(deviceId)
                .collection(Collection.AUDIOBOOK)
                .where(`${FireStoreConstant.Property.ORGANIZATION_ID}`, '==', organization_id)
                .get()
                .then(res => {
                    res.forEach(element => {
                        element.ref.delete()
                    });
                })

            return true
        } catch (err) {
            console.log(err);
            return false
        }
    }

    public addUserRole = async (uid: string) => {
        try {
            console.log('uid ', uid);
            
            await firestore()
                .collection(Collection.USERS)
                .doc(uid)
                .set({
                    role: "user"
                })
        } catch (err) {
            console.log(err);
        }
    } 

}

export default new FirebaseFireStore()