import auth from '@react-native-firebase/auth'

class FireBaseAuthServices {
    public fetchCurrentUser = () => {
        return auth().currentUser
    }
    
    public signInEmailAndPassword = async (email: string, password: string) => {
        return await auth().signInWithEmailAndPassword(email, password)
    }

    public sendPasswordResetEmail = async (email: string) => {
        return await auth().sendPasswordResetEmail(email)
    }

    public getFirebaseToken = async (): Promise<string> => {
        return await auth().currentUser?.getIdToken(true)
    }

    public signOut = async (): Promise<any> => {
        return await auth().signOut()
    }
}
export default new FireBaseAuthServices()
