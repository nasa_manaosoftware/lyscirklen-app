import { FileUploadType } from "../../../constants/app.constant"

export type IUploadFileData = {
    type?: FileUploadType,
    storage_path?: string,
    url?: string,
    file_path?: string, // Can be url from device and url from cloud storage.
    file_name?: string
}

export type IUploadFileParams = {
    fileList: IUploadFileData[],
    uid: string
}