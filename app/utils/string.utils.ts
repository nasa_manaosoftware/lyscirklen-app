export const newLineToBr = (inputString: string) => {
    return inputString?.replace(/\n/g, '<br/>').replace(/\s/g, '&nbsp;')
}

export const brToNewLine= (inputString: string) => {
    return inputString?.replace(/(<br>|<\/br>|<\/ br>|<br\/>|<br \/>)/mgi, '\n').replace(/&nbsp;/g, ' ')
}

export const htmlToString = (inputString: string) => {
    return inputString?.replace(/&nbsp;/g, ' ').replace(/(<br>|<\/br>|<\/ br>|<br\/>|<br \/>)/mgi, '')
}