import { ISelect, IUser } from "../models/user-store/user.types"
import { RootStore } from "../models/root-store/root.types"
import { toJS } from "mobx"
import { InfoNotification, INotification } from "../models/notification-store"
import { FirebaseMessagingTypes } from '@react-native-firebase/messaging'
import moment, { Moment } from "moment"
import 'moment/locale/da'
import { GeneralResources, RemoteConfigGroupedKey } from "../constants/firebase/remote-config"
import { GeneralResourcesStore } from "../models"
import { ISharedTimer } from "../models/shared-store"
import { FileUploadType } from "../constants/app.constant"
import { IUploadFileData } from "./firebase/fire-storage/fire-storage.types"
import { Property, MobileProperty } from "../constants/firebase/fire-store.constant"
import { IMessage } from "../libs/gifted-chat"
import * as LocaleUtils from "./locale"
import { UserModel } from "../models/user-store/user.store"
import { ExtractCSTWithSTN } from "mobx-state-tree/dist/core/type/type"



export const getSelectObject = (objects: any[]): ISelect[] => {
    let options: ISelect
    return objects?.map((v: {}) => {
        options = {
            key: Object.keys(v)[0],
            value: Object.values(v)[0] as any
        }
        return options
    })
}


export const getModalContentList = (data: any[], isSelectType: boolean = false) => {
    const objects = data as any[]
    return objects?.map(v => {
        let options = {
            key: isSelectType ? v.key : Object.keys(v)[0],
            label: isSelectType ? v.value : Object.values(v)[0]
        }
        return options
    })
}

export const getNotificationObject = (
    notificationData?: Partial<INotification>,
    convertDate?: boolean): Partial<INotification & ISharedTimer> => {
    let data: INotification & ISharedTimer = notificationData
    const responseDate = data?.misc?.date
    const date = convertDate ? moment(responseDate.toDate()) : moment(responseDate)
    data = {
        ...data,

        //to cast to shareStore timer.
        [MobileProperty.DATE_TIME]: date,
    }
    return data
}


export const getMessageObject = (rootStore: RootStore, messageData?: Partial<IMessage>, convertDate?: boolean): Partial<IMessage> => {
    const responseCreatedDate = messageData?.created_date
    const createdDate = convertDate ? moment(responseCreatedDate?.toDate()) : moment(responseCreatedDate)
    return {
        ...messageData,
        [Property.SENDER]: getUserProfileObject(rootStore, null, messageData?.sender),
        [Property.CREATED_DATE]: createdDate
    }
}

export const getUserProfileObject = (rootStore: RootStore, data?: any, userData?: Partial<IUser> | ExtractCSTWithSTN<typeof UserModel>) => {
    // const cities = rootStore?.getAllGeneralResourcesStore?.getCities
    // const seekings = getSelectObject(rootStore?.getAllGeneralResourcesStore?.getHavdSogerHans)
    const interests = rootStore?.getAllGeneralResourcesStore?.getInterests
    const tags = rootStore?.getAllGeneralResourcesStore?.getTags
    const existingData = data && toJS(data)
    const transformPrimativeType = (value?: any, referenceArray?: ISelect[]) => {
        if (!value) return
        const isPrimative: boolean = typeof value === "number" || typeof value === "string"
        if (!isPrimative) return value
        if (referenceArray) {
            const mappedValue = referenceArray?.find(e => e.key === value)?.value
            if (mappedValue?.length === 0) return referenceArray
            return {
                key: value,
                value: mappedValue
            }
        }
        return {
            key: value.toString(),
            value: value.toString()
        } as ISelect
    }

    const profileImage = (): IUploadFileData[] => {
        const profileImage = userData?.profile_image as IUploadFileData
        if (!profileImage) return []
        const obj: any = profileImage
        const url = profileImage?.url || obj && obj[0]?.file_path
        const baseProfileImage = {
            [MobileProperty.FILE_PATH]: url,
            [MobileProperty.TYPE]: FileUploadType.ProfileImage
        }
        if (Array.isArray(obj)) return [{ ...(obj[0]) }]
        return [{
            ...profileImage,
            ...baseProfileImage
        }]
    }
    // const galleryImageList = userData?.gallery_image_list as IUploadFileData[]
    // const galleryImages: IUploadFileData[] = galleryImageList &&
    //     galleryImageList?.map((obj) => {
    //         const url = obj?.url || (obj as any)?.file_path
    //         return {
    //             ...obj,
    //             [MobileProperty.FILE_PATH]: url,
    //             [MobileProperty.TYPE]: FileUploadType.GalleryImage,
    //         }
    //     })

    const transformObject = (primaryArray: ISelect[], secondArray: any): ISelect[] => {
        if (!secondArray || secondArray?.length === 0) return []
        if (secondArray[0]?.key) return secondArray
        return primaryArray?.filter((array: { key: string }) =>
            secondArray?.some((filter: any) => {
                return filter?.key ? filter?.key?.toLocaleLowerCase() : filter.toLocaleLowerCase() === array.key.toLocaleLowerCase()
            })
        )
    }
    const params = {
        ...userData,
         ...{
            [Property.PROFILE_IMAGE]: profileImage()
        },
        ...{
            [MobileProperty.COMPARE_PROFILE_IMAGE]: profileImage()
        }
    }
    if (existingData) return { ...existingData, ...params }
    return params
}

export const getDateTimeFormatted = (date?: any): string => {
    let dateMoment: Moment
    let dateTime = new Date(date)
    if (dateTime instanceof Date && !isNaN(dateTime.valueOf())) {
        dateMoment = moment(dateTime)
    } else {
        const firebaseTime = date?._seconds * 1000
        dateMoment = moment(new Date(firebaseTime))
    }
    return dateMoment?.locale(LocaleUtils.getLocaleDevice().toLowerCase())?.format("MMMM, D") || null
}


export const getNotificationByLocaleKey = (store: GeneralResourcesStore, value: string, type: RemoteConfigGroupedKey): string | undefined => {
    const keys = Object.values(GeneralResources.Notification)
    const translatedValue: string[] = keys.filter(e => e.includes(value))
    return translatedValue?.length > 0 && store.getNotification(type + "." + translatedValue[0])
}

export var isEqual = function (value, other) {

    // Get the value type
    var type = Object.prototype.toString.call(value);

    // If the two objects are not the same type, return false
    if (type !== Object.prototype.toString.call(other)) return false;

    // If items are not an object or array, return false
    if (['[object Array]', '[object Object]'].indexOf(type) < 0) return false;

    // Compare the length of the length of the two items
    var valueLen = type === '[object Array]' ? value.length : Object.keys(value).length;
    var otherLen = type === '[object Array]' ? other.length : Object.keys(other).length;
    if (valueLen !== otherLen) return false;

    // Compare two items
    var compare = function (item1, item2) {

        // Get the object type
        var itemType = Object.prototype.toString.call(item1);

        // If an object or array, compare recursively
        if (['[object Array]', '[object Object]'].indexOf(itemType) >= 0) {
            if (!isEqual(item1, item2)) return false;
        }

        // Otherwise, do a simple comparison
        else {

            // If the two items are not the same type, return false
            if (itemType !== Object.prototype.toString.call(item2)) return false;

            // Else if it's a function, convert to a string and compare
            // Otherwise, just compare
            if (itemType === '[object Function]') {
                if (item1.toString() !== item2.toString()) return false;
            } else {
                if (item1 !== item2) return false;
            }

        }
    };

    // Compare properties
    if (type === '[object Array]') {
        for (var i = 0; i < valueLen; i++) {
            if (compare(value[i], other[i]) === false) return false;
        }
    } else {
        for (var key in value) {
            if (value.hasOwnProperty(key)) {
                if (compare(value[key], other[key]) === false) return false;
            }
        }
    }

    // If nothing failed, return true
    return true;

};

//BD project
export const transformNotificationData = (notification: FirebaseMessagingTypes.RemoteMessage): InfoNotification => {
    const payload = notification?.data

    return {
        [Property.HEADING]: payload?.heading,
        [Property.MESSAGE]: payload?.message,
        [Property.PUSH_NOTIFICATION_ID]: payload?.push_notification_id,
        [Property.AUDIOBOOK_ID]: payload?.audiobook_id,
        [Property.BUTTON_TEXT]: payload?.button_text,
        ...notification?.notification,
    }
}