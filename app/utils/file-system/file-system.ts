export const writeFile = async (fileName: string, data: any) => {
    var RNFS = require('react-native-fs')
    var path = RNFS.DocumentDirectoryPath + '/' + fileName
    try {
        await RNFS.writeFile(path, data, 'utf8')
    } catch (error) {
        
    }
}

export const readFile = async (fileName: string): Promise<any> => {
    var RNFS = require('react-native-fs')
    var path = RNFS.DocumentDirectoryPath + '/' + fileName

    try {
        const result = RNFS.readFile(path, 'utf8')
        return result
    } catch (error) {
        return null
    }
}

export const readFileByPath = async (filePath: string): Promise<any> => {
    var RNFS = require('react-native-fs')
    try {
        const result = RNFS.readFile(filePath, 'utf8')
        return result
    } catch (error) {
        return null
    }
}