import { NativeModules } from "react-native"
import * as AppConstant from "../../constants/app.constant"
import * as RMCFConstant from "../../constants/firebase/remote-config"
import * as metric from "../../theme"

export const getLocaleDevice = (): string => {
    let defaultLocale = AppConstant.Locale.Default
    let locale = metric.isIPhone
        ? NativeModules.SettingsManager.settings.AppleLocale
        : NativeModules.I18nManager.localeIdentifier
    locale = locale?.slice(0,2)
    if (!locale) locale = NativeModules.SettingsManager.settings.AppleLanguages[0]
    return locale || defaultLocale
}

export const getLocalizedGeneralResources = async () => {
    //Refactor later >> can't use require('./' + locale + '/general-resources.json')
    const { GeneralResources} = RMCFConstant.RemoteConfigRootKey
    return {
        ...{ [GeneralResources]: await require('./da/general-resources.json') }
    }
}