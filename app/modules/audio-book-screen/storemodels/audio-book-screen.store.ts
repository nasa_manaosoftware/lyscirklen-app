import { types } from "mobx-state-tree"
import { StoreName } from "../../../constants/app.constant"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store/general-resources.store"
import { AudioBookScreenResourcesViews, AudioBookScreenViews } from "../viewmodels/audio-book-screen.views"
import { AudioBookPropsModel, AudioBookScreenActions } from "../viewmodels"
import { AudioBookService } from "../services/audio-book-screen.services"
import { DownloadedStoreModel } from "../../../models/downloaded-store/downloaded.store"
import { PlaybackStoreModel } from "../../../models/playback-store"

const AudioBookScreenModel = types.model(StoreName.Audiobook, AudioBookPropsModel)

export const AudioBookScreenStoreModel = types.compose(
    AudioBookScreenModel,
    AudioBookScreenActions,
    AudioBookScreenViews,
    AudioBookService,
    DownloadedStoreModel,
    PlaybackStoreModel
)

export const AudioBookScreenResourcesStoreModel = types.compose(GeneralResourcesStoreModel, AudioBookScreenResourcesViews)

