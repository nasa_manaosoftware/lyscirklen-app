import { Instance, SnapshotOut } from "mobx-state-tree"
import { AudioBookScreenResourcesStoreModel, AudioBookScreenStoreModel } from "./audio-book-screen.store"

export type AudioBookScreenStore = Instance<typeof AudioBookScreenStoreModel>
export type AudioBookScreenStoreSnapshot = SnapshotOut<typeof AudioBookScreenStoreModel>

export type AudioBookScreenResourcesStore = Instance<typeof AudioBookScreenResourcesStoreModel>
export type AudioBookScreenResourcesStoreSnapshot = SnapshotOut<typeof AudioBookScreenResourcesStoreModel>