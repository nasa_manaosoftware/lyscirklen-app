
import { ImageStyle, Platform, TextStyle, ViewStyle } from "react-native";
import { RFValue } from "react-native-responsive-fontsize";
import { color } from "../../theme";
import { size, type } from "../../theme/fonts";
import * as metric from '../../theme/metric'

export const ICON: ImageStyle = {
    width: metric.ratioWidth(15),
    height: metric.ratioHeight(18)
}

export const IMAGE: ViewStyle = {
    width: metric.isTablet() ? metric.ratioWidth(86.5) : metric.ratioWidth(138),
    height: metric.isTablet() ? metric.ratioWidth(130) : metric.ratioHeight(207.58),
    // backgroundColor: 'yellow'
}

export const HEADER_VIEW: ViewStyle = {
    flexDirection: 'row',
    paddingLeft: metric.ratioWidth(27),
    paddingTop: metric.ratioHeight(26),
    position: 'absolute',
    zIndex: 99,
    height: metric.ratioHeight(185),
    width: '100%',
    backgroundColor: color.background,
    // backgroundColor: 'green'
}

export const TEXT_ICON: ViewStyle = {
    flexDirection: 'row',
    alignItems: 'center',
    paddingBottom: metric.ratioHeight(50)
}

export const TEXT_ACTION: TextStyle = {
    color: color.white,
    fontFamily: type.base,
    fontSize: RFValue(20),
    paddingLeft: metric.ratioWidth(18),
    marginRight: metric.ratioWidth(20),
    marginTop: metric.ratioHeight(-5)
}

export const RED_LINE: ViewStyle = {
    width: metric.ratioWidth(2),
    minHeight: metric.ratioHeight(30),
    backgroundColor: color.redLine,
    marginBottom: metric.ratioHeight(1),
    marginTop: metric.ratioHeight(-1),
}

export const TEXT_TITLE: TextStyle = {
    fontFamily: type.base,
    fontSize: size.header,
    color: color.messagesText,
    marginTop: metric.ratioHeight(-2),
    lineHeight: metric.ratioHeight(32),
}

export const TEXT_AUTHOR: TextStyle = {
    fontFamily: type.base,
    fontSize: size.large,
    color: color.messagesText,
    includeFontPadding: false,
    textAlignVertical: 'center',
    marginTop: metric.ratioHeight(10)
}

export const DESCRIPTION_VIEW: ViewStyle = {
    backgroundColor: color.lightPurple,
    height: '100%',
    paddingHorizontal: metric.ratioWidth(27),
    // paddingTop: metric.ratioHeight(70),
    // marginTop: metric.ratioHeight(185),
}

export const SUB_DESCRIPTION: ViewStyle = {
    flexDirection: 'row',
    marginTop: metric.ratioHeight(26.42),
    paddingBottom: metric.ratioHeight(25)
}

export const TITLE_VIEW: ViewStyle = {
    marginHorizontal: metric.ratioWidth(17)
}

export const TEXT_DESCRIPTION: TextStyle = {
    fontFamily: type.base,
    fontSize: RFValue(16),
    color: color.background,
    lineHeight: metric.ratioHeight(28)
}

export const SUB_DESCRIPTION_VIEW: ViewStyle = {
    backgroundColor: color.lightPurple,
    paddingBottom: metric.ratioHeight(50)
}

export const CONTAINER_BUTTON: ViewStyle = {
    flex: 1, 
    height: '76.5%',
    // paddingVertical: metric.isTablet() ? metric.ratioHeight(5) : metric.ratioHeight(15),
    paddingHorizontal: metric.ratioWidth(34),
    justifyContent: 'space-around',
    // backgroundColor: 'red'
}

export const SCROLL_VIEW: ViewStyle = {
    flex: 1,
    backgroundColor: color.lightPurple,
    // paddingBottom: metric.ratioHeight(100)
}

export const CIRCLE: ViewStyle = {
    borderRadius: 100,
    width:  metric.isTablet() ? metric.ratioWidth(35) : metric.ratioWidth(40),
    height: metric.isTablet() ? metric.ratioWidth(35) :metric.ratioWidth(40),
    alignItems: 'center',
    justifyContent: 'center'
}

export const TAG_STYLE: TextStyle = {
    p: {
        // paddingVertical: 0,
        lineHeight: metric.ratioHeight(28),
        fontFamily: type.base,
        fontSize: RFValue(16),
        color: color.textTerm,
        paddingBottom: metric.ratioHeight(30)
    },
    h1: {
        // paddingVertical: 0,
        fontFamily: type.bold,
        color: color.textTerm,
        fontWeight: 'bold',
        fontSize: metric.isTablet() ? RFValue(29) : RFValue(29),
        paddingBottom: metric.ratioHeight(30)
    },
    h2: {
        // paddingVertical: 0,
        fontFamily: type.bold,
        fontWeight: 'bold',
        color: color.textTerm,
        fontSize: metric.isTablet() ? RFValue(22) : RFValue(22),
        paddingBottom: metric.ratioHeight(30)
    },
    li: {
        // paddingVertical: 0,
        lineHeight: metric.ratioHeight(28),
        fontFamily: type.base,
        fontSize: RFValue(16),
        color: color.textTerm
    },
    a: {
        // paddingVertical: 0,
        lineHeight: metric.ratioHeight(28),
        fontFamily: type.base,
        fontSize: RFValue(16),
        color: color.textTerm
    }
    // ,
    // strong: {
    //     // paddingVertical: 0,
    //     lineHeight: metric.ratioHeight(28),
    //     fontFamily: type.bold,
    //     fontSize: RFValue(16),
    //     color: color.textTerm,
    //     fontWeight: 'bold'
    // },
    // em: {
    //     // paddingVertical: 0,
    //     lineHeight: metric.ratioHeight(28),
    //     fontFamily: type.italic,
    //     fontSize: RFValue(16),
    //     fontStyle: 'italic',
    //     color: color.textTerm
    // },
    // u: {
    //     // paddingVertical: 0,
    //     lineHeight: metric.ratioHeight(28),
    //     fontFamily: type.base,
    //     color: color.textTerm
    // }
}

export const circles: ViewStyle = {
    flexDirection: 'row',
    alignItems: 'center',
  }
 export const progress: ViewStyle = {
    margin: 10,
  }

  export const LOADING: ViewStyle = { 
    position: "absolute", 
    zIndex: 99, 
    alignSelf: 'center', 
    justifyContent: 'center', 
    backgroundColor: color.darkDim, 
    width: metric.isTablet() ? metric.ratioWidth(86.5) : metric.ratioWidth(138),
    height: metric.isTablet() ? metric.ratioWidth(130) : metric.ratioHeight(207.58),
    marginLeft: metric.ratioWidth(-30)
}
