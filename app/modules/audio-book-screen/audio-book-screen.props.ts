import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"

export interface AudioBookScreenProps extends NavigationContainerProps<ParamListBase> {
    cover: string,
    author: string,
    title: string,
    description: string
}