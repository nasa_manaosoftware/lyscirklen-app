import { types } from "mobx-state-tree"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store/general-resources.store"
import { GeneralResources } from "../../../constants/firebase/remote-config"
import { NavigationKey } from "../../../constants/app.constant"
import { AudioBookPropsModel } from "."

export const AudioBookScreenViews = types.model(AudioBookPropsModel)
    .views(self => ({
        get getIsDisabled(): boolean {
            return self.isDisabled
        }

    }))

export const AudioBookScreenResourcesViews = GeneralResourcesStoreModel.views(self => {

    //MARK: Volatile State
    const { AudioBookScreen } = GeneralResources
    const {
        playButton,
        downloadButton,
        downloadedTitle,
        no_internet_unable_to_download,
        no_internet_unable_to_play
    } = AudioBookScreen

    //MARK: Views
    const getResources = (locale: string, key: string, childKeyOrShareKey: string | boolean = false, isChildDotNotation: boolean = false) => self.getValues(locale, childKeyOrShareKey ? key : NavigationKey.AudioBookScreen, childKeyOrShareKey ? true : key, isChildDotNotation)
    const getResourcePlayButtonTitle = (locale: string) => getResources(locale, playButton, true)
    const getResourceDownloadButtonTitle = (locale: string) => getResources(locale, downloadButton, true)
    const getResourceDownloadedTitle = (locale: string) => getResources(locale, downloadedTitle, true)
    const getResourceNoInternetUnableToDownload = (locale: string) => getResources(locale, no_internet_unable_to_download, true)
    const getResourceNoInternetUnableToPlay = (locale: string) => getResources(locale, no_internet_unable_to_play, true)

    return {
        getResourcePlayButtonTitle,
        getResourceDownloadButtonTitle,
        getResourceDownloadedTitle,
        getResourceNoInternetUnableToDownload,
        getResourceNoInternetUnableToPlay
    }
})
