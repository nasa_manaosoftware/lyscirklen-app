
import { types } from "mobx-state-tree"
import { AudioBookPropsModel } from "./audio-book-screen.model"

export const AudioBookScreenActions = types.model(AudioBookPropsModel).actions(self => {
    const setIsDisabled = (value: boolean) => {
        self.isDisabled = value
    }

    return {
        setIsDisabled
    }
})