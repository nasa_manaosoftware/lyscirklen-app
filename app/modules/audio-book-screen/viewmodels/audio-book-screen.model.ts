import { types } from "mobx-state-tree";

export const AudioBookPropsModel = {
    isDisabled: types.optional(types.boolean, false)
} 