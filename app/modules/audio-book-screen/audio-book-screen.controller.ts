import { onSnapshot } from "mobx-state-tree";
import { DownloadStatus, NavigationKey } from "../../constants/app.constant";
import { INavigationRoute } from "../../models";
import { RootStore } from "../../models/root-store/root.types";
import { BaseController } from "../base.controller";
import { AudioBookScreenProps } from "./audio-book-screen.props";
import { AudioBookScreenResourcesStoreModel, AudioBookScreenStoreModel } from "./storemodels/audio-book-screen.store";
import { AudioBookScreenResourcesStore, AudioBookScreenStore } from "./storemodels/audio-book-screen.types";
import FirebaseFireStore from '../../utils/firebase/fire-store/fire-store'
import { Alert, PermissionsAndroid, Platform } from "react-native";
import { DOWNLOADEDBOOK } from "../../models/downloaded-store/downloaded.types";
import FirebaseAnalytics from "../../utils/firebase/analytics/analytics";

class AudioBookScreenController extends BaseController {

    /*
       Stateful Variable
   */

    public static viewModel: AudioBookScreenStore
    public static resourcesViewModel: AudioBookScreenResourcesStore
    public static myProps: AudioBookScreenProps & Partial<INavigationRoute>
    public static category_name: string

    /*
        Mark Constructor
    */

    constructor(rootStore?: RootStore, props?: AudioBookScreenProps & Partial<INavigationRoute>) {
        super(rootStore, props)
        if (this._myProps?.route?.params?.isInitialized) return
        this._setupResourcesViewModel()
        this._setupNavigation()
        this._setupProps(props)
        this._setupViewModel()
        this._setupSnapShot()
    }

    /*
       Mark Setup
    */

    private _setupResourcesViewModel = () => {
        AudioBookScreenController.resourcesViewModel = AudioBookScreenResourcesStoreModel.create({ ...this._rootStore?.getAllGeneralResourcesStore })
    }
    private _setupViewModel = () => {
        const localStore = this._rootStore?.getModuleStore(NavigationKey.AudioBookScreen)
        AudioBookScreenController.viewModel = localStore && AudioBookScreenStoreModel.create({ ...localStore }) || AudioBookScreenStoreModel.create({})
        this._setInitializedToPropsParams()
    }

    private _setupSnapShot = () => {
        onSnapshot(AudioBookScreenController.viewModel, (snap: AudioBookScreenStore) => {
            this._rootStore?.setModuleStoreValue(NavigationKey.AudioBookScreen, snap)
        })
    }

    public onDownloaded = async (book, result) => {
        const params = {
            deviceId: this._rootStore?.getDeviceId,
            file_path: result.file_path,
            download_status: result.download_status,
            job_id: result.job_id,
            audiobook_id: book.uid,
            category_id: book.category_id,
            description: book.description,
            forfatter: book.forfatter,
            image: {
                file_name: book.image.file_name,
                storage_path: book.image.storage_path,
                thumbnail_url: book.image.thumbnail_url,
                url: book.image.url
            },
            name: book.name,
            organization_id: book.organization_id,
            video: {
                name: book.video?.name,
                url: book.video?.url,
                download_url: book.video?.download_url,
                streaming_url: book.video?.streaming_url
            }
        }
        const resultSuccess = await FirebaseFireStore.updateDownloadedAudioBook(params)
        this.onFetchDownloaded()
    }

    public checkAudiobookDownloaded = (audiobooks: any, downloadedList: any) => {
        return audiobooks.map(item1 => {
            downloadedList.map(item2 => {
                if (item1.uid === item2.uid) {
                    item1.download_status = DownloadStatus.SUCCESS
                }
            })
            return item1
        })
    }

    public onFetchDownloaded = async () => {
        const organizationId = AudioBookScreenController.rootStore?.getOrganizationId
        const downloaded = await FirebaseFireStore.fetchMyDownloaded(AudioBookScreenController.rootStore?.getDeviceId, organizationId)
        AudioBookScreenController.rootStore?.getDownloadedStore?.setDownloadedList(downloaded)
        this.onFetchAudiobooksList(downloaded)
    }

    public onFetchAudiobooksList = (downloaded) => {
        const audiobooks = AudioBookScreenController.rootStore?.getAudioBooksStore?.getAudiobooksList
        const result = this.checkAudiobookDownloaded(audiobooks, downloaded)
        AudioBookScreenController.rootStore?.getAudioBooksStore?.setAudiobooksList(result)
    }

    public onUpdateStatusDownloading = (granted) => {
        const book = this._myProps?.route?.params
        const audioBooks = AudioBookScreenController.rootStore?.getAudioBooksStore.getAudiobooksList
        let data = []
        audioBooks.map(item => {
            if (item.uid === book.id && granted === PermissionsAndroid.RESULTS.GRANTED) {
                data.push({
                    ...item,
                    is_downloading: true
                })
            } else {
                data.push({
                    ...item
                })
            }
        })
        AudioBookScreenController.rootStore?.getAudioBooksStore.setAudiobooksList(data)
    }

    public downloadProgress = async (book) => {
        AudioBookScreenController.viewModel?.setIsDisabled(true)
        const result = await AudioBookScreenController.viewModel?.downloadBook(this._rootStore, book)

        if (result.download_status === DownloadStatus.SUCCESS) {
            AudioBookScreenController.viewModel?.setIsDisabled(false)
            this.onDownloaded(book, result)
        } else if (result.download_status === DownloadStatus.FAILED) {
            const locale = AudioBookScreenController.rootStore?.getLanguage
            AudioBookScreenController.viewModel?.setIsDisabled(false)
            Alert.alert("", AudioBookScreenController.resourcesViewModel?.getResourceNoInternetUnableToDownload(locale))
        }
        this._delayExecutor(() => AudioBookScreenController.viewModel?.setIsDisabled(false), 1200)
    }

    public onPressDownload = async (book) => {

        if (this._rootStore?.getSharedStore?.getIsConnected) {
            if (Platform.OS === 'ios') {
                this.onUpdateStatusDownloading('granted')
                this.downloadProgress(book)
            } else {
                const result = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE)

                if (result === PermissionsAndroid.RESULTS.GRANTED) {
                    this.onUpdateStatusDownloading(result)
                    this.downloadProgress(book)
                } else {
                    Alert.alert('The permission is denied but requestable')
                }
            }
        } else {
            const locale = AudioBookScreenController.rootStore?.getLanguage
            Alert.alert("", AudioBookScreenController.resourcesViewModel?.getResourceNoInternetUnableToDownload(locale))
        }
    }

    public onAudioBookDetail = () => {
        const book = this._myProps?.route?.params
        const audioBooks = AudioBookScreenController.rootStore?.getAudioBooksStore.getAudiobooksList

        let data = []
        audioBooks.map(item => {
            if (item.uid === book.id) {
                data.push({
                    ...item
                })
            }
        })
        return data
    }

    viewDidAppearAfterFocus = async () => {
        AudioBookScreenController.viewModel?.setIsDisabled(false)
    }

    public queryCategoryName = (book) => {
        const categoryStore = AudioBookScreenController.rootStore?.getAudioBookCategoriesStore?.getAudiobooksCetegories

        categoryStore.map(item => {
            if (item.uid === book[0].category_id) {
                AudioBookScreenController.category_name = item.name
            }
        })
    }

    public playAudio = (book: any) => {

        if (this._rootStore?.getSharedStore?.getIsConnected) {
            this.queryCategoryName(book)
            const audioBooks: Partial<DOWNLOADEDBOOK>[] = AudioBookScreenController.rootStore?.getDownloadedStore?.getDownloadedList


            book = book?.slice()
            const audioData = audioBooks?.slice().filter(data => {
                return book[0]?.uid === data?.uid
            })

            this._rootStore?.resetPlaybackStore()
            setTimeout(() => {
                this._rootStore?.getPlaybackStore?.play((audioData?.length > 0 ? audioData[0] : book[0]) as DOWNLOADEDBOOK)
            }, 300)

             //firebase analytic
             FirebaseAnalytics.videoInitialPlayed({
                book_name: book[0].name,
                category_name: AudioBookScreenController.category_name,
            })

            FirebaseAnalytics.amountVideoPlayedPerUser({
                user_number: this._rootStore?.getUserStore?.getUserId,
                book_name: book[0].name,
                category_name: AudioBookScreenController.category_name,
            })

        } else {
            const locale = AudioBookScreenController.rootStore?.getLanguage
            Alert.alert("", AudioBookScreenController.resourcesViewModel?.getResourceNoInternetUnableToPlay(locale))
        }
    }
}

export default AudioBookScreenController
