import React from "react"
import { observer } from "mobx-react-lite"
import { useConfigurate } from "../../custom-hooks/use-configure-controller";
import { ActivityIndicator, Linking, SafeAreaView, ScrollView, Text, View } from "react-native"
import { color } from "../../theme";
import { AudioBookScreenProps } from "./audio-book-screen.props";
import AudioBookScreenController from "./audio-book-screen.controller";
import FastImage from "react-native-fast-image";
import { Image } from "react-native-animatable";
import { images } from "../../theme/images";
import * as Styles from './audio-book-screen.styles'
import { TouchableOpacity } from "react-native-gesture-handler"
import { DownloadStatus } from "../../constants/app.constant";
import HTML from 'react-native-render-html'
import { useStores } from "../../models/root-store";
import LinearGraient from 'react-native-linear-gradient'
import * as metric from '../../theme/metric'
import * as StringUtils from '../../utils/string.utils'

export const AuidoBookScreen: React.FunctionComponent<AudioBookScreenProps> = observer((props) => {

    const controller = useConfigurate(AudioBookScreenController, props) as AudioBookScreenController

    let rootStore = useStores()
    let book = controller.onAudioBookDetail()

    const isDisabled = AudioBookScreenController.viewModel?.getIsDisabled

    const locale = AudioBookScreenController.rootStore?.getLanguage

    const splitString = (link) => {
        const url = link.split(':')
        console.log('url ', url);
        
        if(url[0] === 'https' || url[0] === 'http' || url[0] === 'mailto') {
            return url
        } else {
            const urlLink = link.split('.')
            console.log('urlLink ', urlLink);
            
            return urlLink
        }
    }

    const openUrl = (link) => {
        const url = splitString(link)
        console.log("url ==> ", url);
        let openLink
        if (url[0] === 'https') {
            openLink = link
        } else if (url[0] === 'mailto') {
            openLink = link
        } else if (url[0] === 'http') {
            openLink = 'https://' + url[1]
        } else {
            openLink = 'https://' + link
        }

        Linking.canOpenURL(openLink).then(supported => {
            if (!supported) {
                console.log('Can\'t handle url: ' + openLink);
            } else {
                Linking.openURL(openLink)
                    .catch(err => {
                        console.warn('openURL error', err);
                    });
            }
        }).catch(err => console.warn('An unexpected error happened', err))
    }


    return (
        <SafeAreaView style={{ backgroundColor: color.background, flex: 1 }}>
            <ScrollView style={Styles.SCROLL_VIEW} bounces={false}>
                {book[0].image.url || book[0].video_url ?
                    <View style={Styles.HEADER_VIEW}>
                        <View>
                            <FastImage source={book[0].image.url ? { uri: book[0].image.url } : images.defaultImage} style={Styles.IMAGE} resizeMode={'contain'} />
                        </View>
                        {
                            book[0].video_url ?
                                <View style={Styles.CONTAINER_BUTTON}>
                                    <View style={Styles.TEXT_ICON}>
                                        <TouchableOpacity
                                            onPress={() => {
                                                controller.playAudio(book)
                                            }}
                                            activeOpacity={0.2}>
                                            <LinearGraient colors={color.buttonGradient} style={Styles.CIRCLE}>
                                                <Image
                                                    source={images.play}
                                                    resizeMode={'contain'}
                                                    style={[Styles.ICON, { marginLeft: 4 }]} />
                                            </LinearGraient>
                                        </TouchableOpacity>
                                        <Text
                                            style={Styles.TEXT_ACTION}>{AudioBookScreenController.resourcesViewModel?.getResourcePlayButtonTitle(locale).toUpperCase()}</Text>

                                    </View>
                                </View>
                                :
                                null
                        }
                    </View>
                    :
                    null
                }
                <View
                    style={
                        book[0].image.url || book[0].video_url ?
                            [Styles.DESCRIPTION_VIEW, { paddingTop: metric.ratioHeight(70), marginTop: metric.ratioHeight(185) }]
                            :
                            [Styles.DESCRIPTION_VIEW]}
                >
                    <View style={Styles.SUB_DESCRIPTION}>
                        <View style={Styles.RED_LINE} />
                        <View style={Styles.TITLE_VIEW}>
                            <Text style={Styles.TEXT_TITLE}>{book[0].name}</Text>
                            {book[0].forfatter ?
                                <Text style={Styles.TEXT_AUTHOR}>{book[0].forfatter}</Text>
                                :
                                null
                            }
                        </View>
                    </View>
                    <View style={Styles.SUB_DESCRIPTION_VIEW}>
                        <HTML
                            html={StringUtils.brToNewLine(book[0].description)}
                            tagsStyles={Styles.TAG_STYLE}
                            ignoredStyles={["font-family", "letter-spacing", "font-size", "color", "font-style"]}
                            onLinkPress={(event, href) => openUrl(href)}
                            listsPrefixesRenderers={{
                                ul: (_htmlAttribs, _children, _convertedCSSStyles, passProps) => {
                                    return <View style={{
                                        marginRight: 10,
                                        width: 10 / 2.8,
                                        height: 10 / 2.8,
                                        marginTop: metric.ratioHeight(12),
                                        borderRadius: 10 / 2.8,
                                        backgroundColor: color.textTerm,
                                    }} />
                                }
                            }}
                        />

                    </View>
                </View>
            </ScrollView>
        </SafeAreaView>
    )
})