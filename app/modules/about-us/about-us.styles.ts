import { ImageStyle, Platform, TextStyle, ViewStyle } from "react-native";
import { HeaderProps } from "../../components/header/header.props";
import { images } from "../../theme/images";
import * as metric from "../../theme"
import { color } from "../../theme";
import { type } from "../../theme/fonts";
import { RFValue } from "react-native-responsive-fontsize";

export const MENU_ICON_STYLE: ImageStyle = {
    width: metric.ratioWidth(24),
    height: metric.ratioHeight(24)
}

export const HEADER_PROPS: HeaderProps = {
    leftIconSource: images.hamMenu,
    leftIconStyle: MENU_ICON_STYLE,
    rightIconStyle: MENU_ICON_STYLE,
    rightIconSource: images.bell,
    isLeftIconAnimated: false,
    isRightIconAnimated: false
}

export const HEADER_VIEW: ViewStyle = {
    backgroundColor: color.background,
    paddingVertical: metric.ratioHeight(20)
}

export const TITLE_VIEW: ViewStyle = {
    flexDirection: 'row',
    marginLeft: metric.ratioWidth(48),
    alignItems: 'center'
}

export const VERTICAL_LINE: ViewStyle = {
    backgroundColor: color.redLine,
    width: metric.ratioWidth(2),
    height: metric.ratioHeight(30)
}

export const TITLE_TEXT: TextStyle = {
    fontFamily: type.base,
    fontSize: RFValue(32),
    color: color.white,
    marginLeft: metric.ratioWidth(18),
    marginTop: metric.ratioHeight(-3)
    // marginBottom: metric.isMicrolearn() ? null : metric.ratioHeight(-5)
}

export const DESCRIPTION_VIEW: ViewStyle = {
    backgroundColor: color.lightPurple,
}

export const HTML_VIEW: ViewStyle = {
    padding: metric.ratioWidth(25),
    marginLeft: metric.ratioWidth(23),
}

export const TAG_STYLE: TextStyle = {
    p: {
        // paddingVertical: 0,
        lineHeight: metric.ratioHeight(28),
        fontFamily: type.base,
        fontSize: RFValue(16),
        color: color.whiteText, 
        paddingBottom: metric.ratioHeight(30)
    },
    h1: {
        // paddingVertical: 0,
        fontFamily: type.bold,
        color: color.whiteText,
        fontWeight: 'bold',
        fontSize: metric.isTablet() ? RFValue(29) : RFValue(29),
        paddingBottom: metric.ratioHeight(30)
    },
    h2: {
        // paddingVertical: 0,
        fontFamily: type.bold,
        color: color.whiteText,
        fontWeight: 'bold',
        fontSize: metric.isTablet() ? RFValue(22) : RFValue(22),
        paddingBottom: metric.ratioHeight(30)
    },
    h4: {
        // paddingVertical: 0,
        fontFamily: type.bold,
        color: color.whiteText,
        fontWeight: 'bold',
        fontSize: metric.isTablet() ? RFValue(20) : RFValue(20),
        paddingBottom: metric.ratioHeight(30),
        textAlign: 'center'
    },
    li: {
        // paddingVertical: 0,
        lineHeight: metric.ratioHeight(28),
        fontFamily: type.base,
        fontSize: RFValue(16),
        color: color.whiteText
    },
    a: {
        // paddingVertical: 0,
        lineHeight: metric.ratioHeight(28),
        fontFamily: type.base,
        fontSize: RFValue(16),
        color: color.whiteText,
        // backgroundColor: color.lightPurple
    }
    // ,
    // strong: {
    //     // paddingVertical: 0,
    //     lineHeight: metric.ratioHeight(28),
    //     fontFamily: type.bold,
    //     fontSize: RFValue(16),
    //     color: color.whiteText,
    //     fontWeight: 'bold'
    // },
    // em: {
    //     // paddingVertical: 0,
    //     lineHeight: metric.ratioHeight(28),
    //     fontFamily: type.italic,
    //     fontSize: RFValue(16),
    //     fontStyle: 'italic',
    //     color: color.whiteText
    // },
    // u: {
    //     // paddingVertical: 0,
    //     lineHeight: metric.ratioHeight(28),
    //     fontFamily: type.base,
    //     color: color.whiteText
    // }
}

export const BUTTON: ViewStyle = {
    height: metric.ratioHeight(50),
    backgroundColor: color.loginBackground,
    borderRadius: 10,
    marginHorizontal: metric.ratioWidth(48),
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: metric.ratioHeight(30)
}

export const TEXT_BUTTON: TextStyle = {
    fontFamily: type.title,
    fontSize: RFValue(22),
    color: color.white
}