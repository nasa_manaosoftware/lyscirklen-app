import { types } from "mobx-state-tree"
import { AboutUsPropsModel } from "./about-us.models"

export const AboutUsActions = types.model(AboutUsPropsModel).actions(self => {

    const setAboutUsDetail = (value: string) => {
        self.aboutUsDetail = value
    }

    const setIsDisabled = (value: boolean) => {
        self.isDisabled = value
    }
    
    return {
        setAboutUsDetail,
        setIsDisabled
    }
})