import { types } from "mobx-state-tree"
import { NavigationKey } from "../../../constants/app.constant"
import { GeneralResources } from "../../../constants/firebase/remote-config"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store/general-resources.store"
import { AboutUsPropsModel } from "./about-us.models"

export const AboutUsViews = types.model(AboutUsPropsModel)
    .views(self => ({

        get getAboutUsDetail(): string {
            return self.aboutUsDetail
        },
        
        get getIsDisabled(): boolean {
            return self.isDisabled
        }
        
    }))

export const AboutUsResourcesViews = GeneralResourcesStoreModel.views(self => {

    //MARK: Volatile State

    const { AboutUs } = GeneralResources

    const { 
        callUs, 
        readMore,
        telephoneNumber,
        readMoreLink,
        aboutUsTitle
     } = AboutUs

    // //MARK: Views
    const getResources = (locale: string, key: string, childKeyOrShareKey: string | boolean = false, isChildDotNotation: boolean = false) => self.getValues(locale, childKeyOrShareKey ? key : NavigationKey.AboutUs, childKeyOrShareKey ? true : key, isChildDotNotation)
    const getResourceCallUsTitle = (locale: string) => getResources(locale, callUs)
    const getResourceReadMoreTitle = (locale: string) => getResources(locale, readMore)
    const getResourceTelephoneNumber = (locale: string) => getResources(locale, telephoneNumber)
    const getResourceReadMoreLink = (locale: string) => getResources(locale, readMoreLink)
    const getResourceAboutUsTitle = (locale: string) => getResources(locale, aboutUsTitle)

    return {
        getResourceCallUsTitle,
        getResourceReadMoreTitle,
        getResourceTelephoneNumber,
        getResourceReadMoreLink,
        getResourceAboutUsTitle
    }
})
