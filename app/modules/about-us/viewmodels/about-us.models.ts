import { types } from "mobx-state-tree";

export const AboutUsPropsModel = {
    aboutUsDetail: types.optional(types.string, ''),
    isDisabled: types.optional(types.boolean, false)
} 