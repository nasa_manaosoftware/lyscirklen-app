import { DrawerActions } from "@react-navigation/core";
import { onSnapshot } from "mobx-state-tree";
import { Alert, Linking, Platform } from "react-native";
import { NavigationKey } from "../../constants/app.constant";
import { INavigationRoute } from "../../models";
import { RootStore } from "../../models/root-store/root.types";
import { BaseController } from "../base.controller";
import { AboutUsProps } from "./about-us.props";
import { AboutUsResourcesStore, AboutUsResourcesStoreModel, AboutUsStore, AboutUsStoreModel } from "./storemodels";
import FirebaseFireStore from '../../utils/firebase/fire-store/fire-store'

class AboutUsController extends BaseController {

    /*
       Stateful Variable
   */

    public static viewModel: AboutUsStore
    public static resourcesViewModel: AboutUsResourcesStore

    /*
        Mark Constructor
    */

    constructor(rootStore?: RootStore, props?: AboutUsProps & Partial<INavigationRoute>) {
        super(rootStore, props)
        if (this._myProps?.route?.params?.isInitialized) return
        this._setupResourcesViewModel()
        this._setupNavigation()
        this._setupProps(props)
        this._setupViewModel()
        this._setupSnapShot()
    }

    /*
       Mark Setup
    */

    private _setupResourcesViewModel = () => {
        AboutUsController.resourcesViewModel = AboutUsResourcesStoreModel.create({ ...this._rootStore?.getAllGeneralResourcesStore })
    }
    private _setupViewModel = () => {
        const localStore = this._rootStore?.getModuleStore(NavigationKey.AboutUs)
        AboutUsController.viewModel = localStore && AboutUsStoreModel.create({ ...localStore }) || AboutUsStoreModel.create({})
        this._setInitializedToPropsParams()
    }

    private _setupSnapShot = () => {
        onSnapshot(AboutUsController.viewModel, (snap: AboutUsStore) => {
            this._rootStore?.setModuleStoreValue(NavigationKey.AboutUs, snap)
        })
    }

    public onSideMenuPress = () => {
        this._myProps?.navigation?.dispatch(DrawerActions.openDrawer() as any)
    }

    public onNotificationPress = () => {
        Alert.alert('Notifications about')
    }

    // public onPressTelephoneNumber = () => {
    //     if(AboutUsController.viewModel?.getIsDisabled) return
    //     AboutUsController.viewModel?.setIsDisabled(true)

    //     let phone = AboutUsController.resourcesViewModel?.getResourceTelephoneNumber();
    //     let phoneNumber
    //     if (Platform.OS !== 'android') {
    //         phoneNumber = `telprompt:${phone}`;
    //     }
    //     else {
    //         phoneNumber = `tel:${phone}`;
    //     }

    //     Linking.canOpenURL(phoneNumber).then(supported => {
    //         if (!supported) {
    //             console.log('Can\'t handle url: ' + phoneNumber);
    //         } else {
    //             Linking.openURL(phoneNumber)
    //                 .catch(err => {
    //                     console.warn('openURL error', err);
    //                 });
    //         }
    //     }).catch(err => console.warn('An unexpected error happened', err))
    //     this._delayExecutor(() => AboutUsController.viewModel?.setIsDisabled(false), 1200)
    // }

    // public onPressReadMore = () => {
    //     if(AboutUsController.viewModel?.getIsDisabled) return
    //     AboutUsController.viewModel?.setIsDisabled(true)

    //     const link = AboutUsController.resourcesViewModel?.getResourceReadMoreLink()
    //     Linking.canOpenURL(link).then(supported => {
    //         if (!supported) {
    //             console.log('Can\'t handle url: ' + link);
    //         } else {
    //             Linking.openURL(link)
    //                 .catch(err => {
    //                     console.warn('openURL error', err);
    //                 });
    //         }
    //     }).catch(err => console.warn('An unexpected error happened', err))
    //     this._delayExecutor(() => AboutUsController.viewModel?.setIsDisabled(false), 1200)
    // }

    viewWillAppearOnce = async () => {
        const organization_id = AboutUsController.rootStore?.getOrganizationId
        const aboutUsDetail = await FirebaseFireStore.fetchAboutUsDetail(organization_id)
        AboutUsController.viewModel?.setAboutUsDetail(aboutUsDetail.content)

    }

}

export default AboutUsController
