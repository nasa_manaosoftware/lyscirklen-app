import { Instance, SnapshotOut } from "mobx-state-tree"
import { AboutUsResourcesStoreModel, AboutUsStoreModel } from "./about-us.store"

export type AboutUsStore = Instance<typeof AboutUsStoreModel>
export type AboutUsStoreSnapshot = SnapshotOut<typeof AboutUsStoreModel>

export type AboutUsResourcesStore = Instance<typeof AboutUsResourcesStoreModel>
export type AboutUsResourcesStoreSnapshot = SnapshotOut<typeof AboutUsResourcesStoreModel>