import { types } from "mobx-state-tree"
import { AboutUsViews, AboutUsActions, AboutUsResourcesViews, AboutUsPropsModel } from "../viewmodels"
import { StoreName } from "../../../constants/app.constant"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store/general-resources.store"

const AboutUsModel = types.model(StoreName.AboutUs, AboutUsPropsModel)

export const AboutUsStoreModel = types.compose(
    AboutUsModel,
    AboutUsViews,
    AboutUsActions)

export const AboutUsResourcesStoreModel = types.compose(GeneralResourcesStoreModel, AboutUsResourcesViews)

