import { types } from "mobx-state-tree"
import { MainPropsModel, MainActions, MainViews, MainResourcesViews } from "../viewmodels"
import { StoreName } from "../../../constants/app.constant"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store/general-resources.store"

const MainModel = types.model(StoreName.Main, MainPropsModel)

export const MainStoreModel = types.compose(
    MainModel,
    MainActions,
    MainViews,
    GeneralResourcesStoreModel
)

export const MainResourcesStoreModel = types.compose(GeneralResourcesStoreModel, MainResourcesViews)

