import { Instance, SnapshotOut } from "mobx-state-tree"
import { MainResourcesStoreModel, MainStoreModel } from "./main.store"

export type MainStore = Instance<typeof MainStoreModel>
export type MainStoreSnapshot = SnapshotOut<typeof MainStoreModel>

export type MainResourcesStore = Instance<typeof MainResourcesStoreModel>
export type MainResourcesStoreSnapshot = SnapshotOut<typeof MainResourcesStoreModel>