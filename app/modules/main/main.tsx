import React, { useEffect, useRef, useState } from "react"
import { observer } from "mobx-react-lite"
import { useConfigurate } from "../../custom-hooks/use-configure-controller";
import MainController from "./main.controller";
import { MainProps } from "./main.props";
import { BottomMenuComponents } from "../../navigation/tabs/bottom-menu/bottom-menu";
import { Header, Wallpaper } from "../../components";
import Orientation from "react-native-orientation"
import { SafeAreaView } from "react-navigation";
import * as Styles from "./main.styles"
import { Button, Dimensions, Platform, ScrollView, Text, View } from "react-native";
import { MiniPlayer } from "../../components/spotify/mini-player/mini-player";
import SlidingUpPanel from 'rn-sliding-up-panel'
import * as metric from "../../theme"
import { useNotification } from "../../custom-hooks/use-notification";
import { useSafeAreaInsets } from "react-native-safe-area-context";
import SplashScreen from 'react-native-splash-screen'
import { FlatList } from "react-native-gesture-handler";
import { VideoPlayer } from "../../components/spotify/video-player/video-player";

export const MainScreen: React.FunctionComponent<MainProps> = observer((props) => {
  

  const controller = useConfigurate(MainController, props) as MainController
  controller.hideSplashScreen()
  Orientation.lockToPortrait()
  let player = useRef(null)
  const rootStore = MainController.rootStore
  const playbackStore = MainController.rootStore.getPlaybackStore
  const [audioBooks, setAudiobooks] = useState([])
  const { getIsConnected } = rootStore.getSharedStore
  useNotification(rootStore)
  const _panel = useRef()
  const height = metric.screenHeight
  const isIos = Platform.OS === "ios"
  const insets = useSafeAreaInsets();

  useEffect(() => {
    controller.setupPanelRef(_panel)
    // const init = async () => {
    //   let data = await controller.testImplementFirebase()
    //   let _audiobooks = []
    //   for(let item of data) {
    //     _audiobooks.push({
    //       ...item,
    //       isShowPlayer: false,
    //       isPause: true,
    //       isPlay: false,
    //       isEnd: false
    //     })
    //   }
    //   setAudiobooks(_audiobooks)
    //   await controller.testFirebaseAuthAnonymous()
    // }
    // init()
  }, [])

  const onPlayPausePress = () => {
    if (playbackStore?.getIsDone) {
      player?.current?.seek(0)
      setTimeout(() => {
        playbackStore?.playAfterEnd()
      }, 100)
    } else {
      if (playbackStore?.getPlayPause != playbackStore?.getIsPause) {
        playbackStore?.setPlayPause(playbackStore?.getIsPause)
      }
      setTimeout(() => {
        playbackStore?.setPlayPause(!playbackStore?.getPlayPause)
      }, 100)
    }

  }


  const renderMiniPlayer = () => {
    return (
      <View shouldRasterizeIOS style={[Styles.MINI_PLAYER, { bottom: metric.ratioHeight(80) + insets.bottom }]}>
        <MiniPlayer
          onShowPlayerPress={() => { controller?.showPlayer() }} onPlayPausePress={onPlayPausePress} />
      </View>)
  }
  if (!playbackStore?.getIsCollapse) {
    controller?.showPlayer()
  } else {
    (_panel?.current as SlidingUpPanel)?.hide()
  }

  return (
    <Wallpaper>
      <View style={getIsConnected ?
        [Styles.CONTAINER, { paddingTop: metric.isIPhone ? insets.top : 0, paddingBottom: metric.isIPhone ? insets.bottom : 0 }]
        :
        [Styles.CONTAINER_OFFLINE, { paddingTop: metric.isIPhone ? insets.top : 0, paddingBottom: metric.isIPhone ? insets.bottom : 0 }]}>
        {
          playbackStore?.getIsShowMiniPlayer && renderMiniPlayer()
          // renderMiniPlayer()
        }
        <BottomMenuComponents tabController={controller} rootStore={rootStore} />
        {/* - metric.ratioHeight(metric.isNewIPhone ? 120 : 80) */}
        <SlidingUpPanel
          ref={_panel}
          height={metric.screenHeight - (metric.ratioHeight(80) + insets.bottom)}
          draggableRange={{ top: height / 1.0, bottom: 0 }}
          allowDragging={false}
          allowMomentum={false}
          friction={0.001}
          containerStyle={{ position: 'absolute', zIndex: 99 }}
          showBackdrop={false}>
          <View style={{ flex: 1 }}>
            <View style={getIsConnected ? Styles.HEADER_CONTAINER : Styles.HEADER_CONTAINER_OFFLINE}>
              <Header hideCenter={true} hideBadgeNumber={true}
                {...Styles.HEADER_PROPS}
                onLeftPress={controller?.hidePlayer}
                onRightPress={controller?.closePlayer} />
            </View>
            <VideoPlayer player={player} onPlayPausePress={onPlayPausePress} />
          </View>
        </SlidingUpPanel>
        {/* <View style={{flex: 1, justifyContent: 'center', alignItems: 'flex-start', overflow: 'scroll', width: '100%', height: Dimensions.get('screen').height}}>
          <ScrollView 
            scrollEnabled={true}
            nestedScrollEnabled={true}
            contentContainerStyle={{ flexGrow: 1 }}
            style={{ flex: 1 }}
          >
            <View style={{ flex: 1 }}>
              { audioBooks &&
                <FlatList 
                  data={audioBooks}
                  extraData={audioBooks.slice()}
                  keyExtractor={(e, i) => "main-audio-book-item-" + i.toString()}
                  ItemSeparatorComponent={() => (
                    <View style={{ flex: 1, backgroundColor: 'white', height: 2, width: '100%', marginVertical: 5, paddingHorizontal: 3 }} />
                  )}
                  style={{ flex: 1 }}
                  renderItem={(item) => {
                    // console.log("item.item =>", item.item)
                    // console.log("=========================")
                    let book = item.item

                    return (
                      <View style={{ width: '100%', minHeight: 40, justifyContent: 'center', alignItems: 'flex-start', padding: 6, flex: 1, flexDirection: 'column' }}>
                        <View style={{ flex: 1, flexDirection: 'row', paddingVertical: 3 }}>
                          <Text style={{ fontSize: 16, color: 'white' }}>Name: </Text>
                          <Text style={{ fontSize: 16, color: 'white' }}>{book?.name ? book?.name : '฿฿฿'}</Text>
                        </View>
                        <View style={{ flex: 1, flexDirection: 'row', paddingVertical: 3 }}>
                          <Text style={{ fontSize: 16, color: 'white' }}>Author: </Text>
                          <Text style={{ fontSize: 16, color: 'white' }}>{book?.forfatter ? book?.forfatter : '฿฿฿'}</Text>
                        </View>
                        <View style={{ flex: 1, flexDirection: 'row', paddingVertical: 3 }}>
                          <Text style={{ fontSize: 16, color: 'white' }}>Description: </Text>
                          <Text style={{ fontSize: 16, color: 'white' }}>{book?.description ? book?.description : '฿฿฿'}</Text>
                        </View>
                        <Button title="Play" onPress={() => {console.log("play button #" + item.index + " touched!"), book.isShowPlayer = !book.isShowPlayer, console.log("#" + item.index + " book.isShowPlayer =>", book.isShowPlayer)}} />
                        { book.isShowPlayer &&
                          <VideoPlayer player={player} onPlayPausePress={onPlayPausePress} />
                        }
                      </View>
                    )
                  }}
                />
              }
            </View>
          </ScrollView>
        </View> */}
      </View>
    </Wallpaper>
  )
  
})