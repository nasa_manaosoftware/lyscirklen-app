import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"

export interface MainProps extends NavigationContainerProps<ParamListBase> {
}