import { types } from "mobx-state-tree";


export const MainPropsModel = {
    currentDuration: types.optional(types.frozen<number>(), 0),
}