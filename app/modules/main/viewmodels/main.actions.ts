import { types } from "mobx-state-tree"
import { MainPropsModel } from "./main.models"

export const MainActions = types.model(MainPropsModel).actions(self => {
    
    const setCurrentDuration = (value: number) => {
        self.currentDuration = value
    }
    
    return {
        setCurrentDuration 
    }
})