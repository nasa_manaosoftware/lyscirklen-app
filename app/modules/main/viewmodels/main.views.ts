import { types } from "mobx-state-tree"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store/general-resources.store"
import { MainPropsModel } from "./main.models"

export const MainViews = types.model(MainPropsModel)
    .views(self => ({
        get getCurrentDuration() {
            return self.currentDuration
        },
    }))

export const MainResourcesViews = GeneralResourcesStoreModel.views(self => {

    //MARK: Volatile State

    //MARK: Views

    return {

    }
})
