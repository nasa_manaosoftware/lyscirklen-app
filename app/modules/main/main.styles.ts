import { ImageStyle, Platform, PlatformStatic, ViewStyle } from "react-native";
import { HeaderProps } from "../../components/header/header.props";
import { images } from "../../theme/images";
import * as metric from "../../theme"
import { color, isIPhone } from "../../theme";
import { TabItemIconProps } from "../../navigation/tabs/custom-tab/tab-item/tab-item.props";
import { NavigationKey } from "../../constants/app.constant";

export const FULL: ViewStyle = {
    flex: 1
}

export const CONTAINER: ViewStyle = {
    ...FULL,
    position: 'absolute',
    width: metric.screenWidth,
    minHeight: metric.screenHeight,
    backgroundColor: color.background,
    ...Platform.select({
        android: {
            bottom: metric.ratioHeight(0),
            top: metric.ratioHeight(0)
        }
    })
}

export const CONTAINER_OFFLINE: ViewStyle = {
    ...FULL,
    position: 'absolute',
    width: metric.screenWidth,
    minHeight: metric.screenHeight - metric.ratioHeight(80),
    backgroundColor: color.background, 
    ...Platform.select({
        android: {
            bottom: metric.ratioHeight(0),
            top: metric.ratioHeight(0)
        }
    })
}

export const MENU_ICON_STYLE: ImageStyle = {
    width: metric.ratioWidth(24),
    height: metric.ratioHeight(24)
}

export const HEADER_PROPS: HeaderProps = {
    leftIconSource: images.collapse,
    leftIconStyle: MENU_ICON_STYLE,
    rightIconStyle: MENU_ICON_STYLE,
    rightIconSource: images.closeModalIcon,
    isLeftIconAnimated: false,
    isRightIconAnimated: false
}

export const ICON: ImageStyle = {
    width: metric.ratioWidth(24),
    height: metric.ratioHeight(24)
}

export const MINI_PLAYER: ViewStyle = {
    position: 'absolute',
    backgroundColor: color.background,
    minHeight: metric.ratioHeight(80),
    width: '100%',
    zIndex: 20,
    justifyContent: 'center'
}

export const MINI_PLAYER_OFFLINE: ViewStyle = {
    position: 'absolute',
    backgroundColor: color.background,
    minHeight: metric.ratioHeight(80),
    width: '100%',
    bottom: metric.ratioHeight(158),
    zIndex: 20,
    justifyContent: 'center'
}


export const TAB_BAR_CONTAINER: ViewStyle = {
    flexDirection: 'row',
    // flex: 0.18,
    backgroundColor: color.background
}

const CENTER: ImageStyle = {
    alignSelf: 'center'
}
const LEFT: ImageStyle = {
    alignSelf: 'flex-start'
}
const RIGHT: ImageStyle = {
    alignSelf: 'flex-end'
}

export const getTabMenuIconProps = (index: number, key: string): TabItemIconProps => {

    if (key === NavigationKey.MainScreen) {
        switch (index) {
            case 0:
                return {
                    iconSource: images.home,
                    iconFocused: images.home,
                    iconStyle: {
                        width: metric.ratioWidth(44),
                        height: metric.ratioHeight(24),
                        ...LEFT
                    }
                }
             case 1:
                return {
                    iconSource: images.aboutUs,
                    iconFocused: images.aboutUs,
                    iconStyle: {
                        width: metric.ratioWidth(44),
                        height: metric.ratioHeight(24),
                        ...RIGHT
                    }
                }
            default:
                return null
        }
    }
    return null
}

export const TAB_CONTAINER: ViewStyle = {
    maxWidth: metric.screenWidth,
    minHeight: metric.ratioHeight(80)
}

export const HEADER_CONTAINER: ViewStyle = {
    justifyContent: 'flex-end',
    backgroundColor: color.background,
    //backgroundColor: color.black,
    // marginTop : metric.isIPhone ? metric.ratioHeight(30) : metric.ratioHeight(metric.isTablet() ? 40 : 0)
    marginTop : metric.isIPhone ? metric.ratioHeight( metric.adjustIphone6() ) : metric.ratioHeight(metric.isTablet() ? 40 : 0)
}

export const HEADER_CONTAINER_OFFLINE: ViewStyle = {
    justifyContent: 'flex-end',
    backgroundColor: color.background,
    marginTop : metric.isIPhone ? metric.ratioHeight(30) : metric.ratioHeight(metric.isTablet() ? 40 : 30)
}
