import { DrawerActions, StackActions } from "@react-navigation/core";
import { onSnapshot } from "mobx-state-tree";
import { Alert, Platform } from "react-native";
import { DownloadStatus, NavigationKey } from "../../constants/app.constant";
import { INavigationRoute } from "../../models";
import { RootStore } from "../../models/root-store/root.types";
import { MainProps } from "./main.props";
import { MainResourcesStore, MainResourcesStoreModel, MainStore, MainStoreModel } from "./storemodel";
import FirebaseFireStore from "../../utils/firebase/fire-store/fire-store";
import { TabController } from "../tab.controller";
import { IMessages } from "../../models/messages-store"
import PushNotification from "react-native-push-notification";
import PushNotificationIOS from "@react-native-community/push-notification-ios";
import SlidingUpPanel from "rn-sliding-up-panel";
import NotificationServices from "../../utils/firebase/notification/notification"
import deviceInfoModule from "react-native-device-info"
import auth from '@react-native-firebase/auth'
import SplashScreen from "react-native-splash-screen";

class MainController extends TabController {

    /*
       Stateful Variable
   */

    public static viewModel: MainStore
    public static resourcesViewModel: MainResourcesStore
    public static myProps: MainProps & Partial<INavigationRoute>
    private static _panel: React.MutableRefObject<any>
    /*
        Mark Constructor
    */

    constructor(rootStore?: RootStore, props?: MainProps & Partial<INavigationRoute>) {
        super(rootStore, props)
        if (this._myProps?.route?.params?.isInitialized) return
        this._setupResourcesViewModel()
        this._setupProps(props)
        this._setupViewModel()
        this._setupSnapShot()
        this.updateDeviceToken()
    }

    /*
       Mark Setup
    */

    private _setupResourcesViewModel = () => {
        MainController.resourcesViewModel = MainResourcesStoreModel.create({ ...this._rootStore?.getAllGeneralResourcesStore })
    }
    private _setupViewModel = () => {
        const localStore = this._rootStore?.getModuleStore(NavigationKey.MainScreen)
        MainController.viewModel = localStore && MainStoreModel.create({ ...localStore }) || MainStoreModel.create({})
        this._setInitializedToPropsParams()
    }

    private _setupSnapShot = () => {
        onSnapshot(MainController.viewModel, (snap: MainStore) => {
        })
    }

    _setupProps = (myProps: MainProps & Partial<INavigationRoute>) => {
        super._setupProps && super._setupProps(myProps)
        MainController.myProps = {
            ...myProps,
            ...myProps?.route?.params
        }
    }

    /*
       Mark Event
    */

    private _getGeneralResources = async () => {
        await MainController.viewModel?.fetchRemoteConfig()
    }

    public setupPanelRef = (ref: React.MutableRefObject<any>) => MainController._panel = ref


    public showPlayer = () => {
        (MainController?._panel?.current as SlidingUpPanel)?.show()
        MainController.rootStore?.getPlaybackStore?.onShowPlayer()
    }

    public hidePlayer = () => {
        (MainController?._panel?.current as SlidingUpPanel)?.hide()
        MainController?.rootStore?.getPlaybackStore?.onHidePlayer()
    }

    public closePlayer = () => {
        (MainController?._panel?.current as SlidingUpPanel)?.hide()
        // playbackStore.onClosePlayer()
        MainController?.rootStore?.resetPlaybackStore()
    }

    public onCurrent = (duration: number) => {
        MainController?.viewModel?.setCurrentDuration(duration)
    }


    public onSideMenuPress = () => {
        this._myProps?.navigation?.dispatch(DrawerActions.openDrawer() as any)
    }

    public onNotificationPress = () => {
        // this._myProps?.navigation?.dispatch(StackActions.push(NavigationKey.NotificationList) as any)
        Alert.alert('Notifications main')
    }

    // public updateDeviceToken = async () => {
    //     const token: string = await NotificationServices.getToken()
    //     FirebaseFireStore.setDeviceToken(this._rootStore?.getUserStore?.getUserId, deviceInfoModule.getUniqueId(), token)
    // }

    public hideSplashScreen = () => {
        this._delayExecutor(() => {
            SplashScreen.hide()
        }, 1500)
    }


    public checkUnreadMessages = async () => {
        const organizationId = MainController.rootStore?.getOrganizationId
        const membershipNumber = this._rootStore?.getUserStore?.getUserId
        if ((organizationId) && (membershipNumber)) {
            const unReadmessageList: IMessages[] = await FirebaseFireStore.fetchUnReadMessages(membershipNumber)
            // console.log('unReadmessageList ', unReadmessageList);
            this.fectAudioBookList()
            
            this._rootStore?.UserStore?.setBadgeNumber(unReadmessageList.length)

            if (Platform.OS === 'android') {
                PushNotification.setApplicationIconBadgeNumber(unReadmessageList.length)
                // ShortcutBadge.setCount(unReadmessageList.length);
            } else {
                PushNotificationIOS.setApplicationIconBadgeNumber(unReadmessageList.length)
            }
        }
    }

    public getPrivacyPolicy = async () => {
        const privacyPolicy = await MainController.viewModel?.getPrivacyPolicy(this._rootStore?.getOrganizationId)

        if ((privacyPolicy) && (privacyPolicy.content)) {
            this._rootStore?.setPrivacyPolicy(privacyPolicy.content)
        }
    }

    public getTermOfUse = async () => {
        const termOfUse = await MainController.viewModel?.getTermOfUse(this._rootStore?.getOrganizationId)
        if ((termOfUse) && (termOfUse.content)) {
            this._rootStore?.setTermOfUse(termOfUse.content)
        }

    }
    public getAboutUS = async () => {
        const aboutUS = await MainController.viewModel?.getAboutUS(this._rootStore?.getOrganizationId)
        if ((aboutUS) && (aboutUS.content)) {
            this._rootStore?.setAboutUS(aboutUS.content)
        }

    }

    public updateDeviceToken = async () => {
        const token: string = await NotificationServices.getToken()
        FirebaseFireStore.setDeviceToken(this._rootStore?.getUserStore?.getUserId, deviceInfoModule.getUniqueId(), token)
    }

    public updateUserRole = async () => {
        await FirebaseFireStore.addUserRole(this._rootStore?.getUserStore?.getUserId)
    }

    private loginAnonymous = async () => {
        const navigation = this._myProps?.navigation
        auth()
            .signInAnonymously()
            .then(() => {
                console.log('User signed in anonymously');
                MainController.rootStore?.getAuthStore?.setIsLoggedIn(true)
                this.updateUserRole()
                navigation?.navigate(NavigationKey.MainStack)
                this._isGlobalLoading(false)
                this.updateDeviceToken()
                // this.onAuthStateChanged()
            })
            .catch(error => {
                if (error.code === 'auth/operation-not-allowed') {
                    console.log('Enable anonymous in your firebase console.');
                }
                console.error(error);
            })
    }

    private displayNewestVideo = () => {
        const video = MainController.rootStore?.getAudioBooksStore?.getAudiobooksList
        const newestVideo = []
        newestVideo.push(video[0], video[1], video[2])
        MainController.rootStore?.getAudioBooksStore?.setAudiobooksRandom(newestVideo)
    }

    private fectAudioBookList = async () => {
        //fetch audiobook list
        const organizationId = MainController.rootStore?.getOrganizationId
        const audiobooksList = await FirebaseFireStore.fetchAudiobooks(organizationId)

        audiobooksList.sort((firstBook, secondBook) => firstBook.created_date > secondBook.created_date ? -1 : 1)
        MainController.rootStore?.getAudioBooksStore?.setAudiobooksList(audiobooksList)

        if (audiobooksList.length !== 0) {
            this.displayNewestVideo()
        }
    }

    public testImplementFirebase = async () => {
        const organizationId = MainController.rootStore?.getOrganizationId
        const audiobooksList = await FirebaseFireStore.fetchAudiobooks(organizationId)

        audiobooksList.sort((firstBook, secondBook) => firstBook.created_date > secondBook.created_date ? -1 : 1)
        // MainController.rootStore?.getAudioBooksStore?.setAudiobooksList(audiobooksList)

        return audiobooksList
        // console.log("testImplementFirebase: audiobooksList =>", audiobooksList)
    }

    public testFirebaseAuthAnonymous = async () => {
        auth().signInAnonymously().then((user) => {
            MainController.rootStore?.getAuthStore?.setIsLoggedIn(true)
            this.updateUserRole()
            console.log("Firebase: user =>", user)
            this.updateDeviceToken()
        })
    }

    viewDidAppearOnce = async () => {
       // await this._getGeneralResources()
        this.getPrivacyPolicy()
        this.getTermOfUse()
        this.getAboutUS()
    }

    viewDidAppearAfterFocus = async () => {
       await this._getGeneralResources()
        this.loginAnonymous()
    }

    viewWillAppearOnce = async () => {

        // console.log('viewWillAppearOnce main');
        this._rootStore?.setIsFetchingResources(false)
        await this._getGeneralResources()
        this._rootStore.setLanguage('da')
        this.checkUnreadMessages()
        this._isGlobalLoading(false)

        if (MainController.rootStore?.getIsSelectedFront === true) {
            const organizationId = MainController.rootStore?.getOrganizationId
            const audiobookCetegories = await FirebaseFireStore.fetchAudiobooksCategoriesFromFireStore(organizationId)
            const messages = await FirebaseFireStore.fetchMessage(MainController.rootStore?.getOrganizationId)

            MainController.rootStore?.getAudioBookCategoriesStore?.setAudiobooksCetegories(audiobookCetegories)

            if (messages) {
                if (messages.heading) {
                    MainController.rootStore?.getAudioBooksStore?.setMessagesTitle(messages.heading)
                }
                if (messages.paragraph) {
                    MainController.rootStore?.getAudioBooksStore?.setMessagesDetail(messages.paragraph)
                }
            }
        }
    }

    //@override
    public deInit() {
        super.deInit()
        MainController?.rootStore?.getPlaybackStore?.onClosePlayer()
    }

    public backProcess = () => {
        MainController?.rootStore?.getPlaybackStore?.onHidePlayer()
        return true
    }
}

export default MainController
