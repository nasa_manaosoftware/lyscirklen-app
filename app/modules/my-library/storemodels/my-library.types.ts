import { Instance, SnapshotOut } from "mobx-state-tree"
import { MyLibraryResourcesStoreModel, MyLibraryStoreModel } from "./my-library.store"

export type MyLibraryStore = Instance<typeof MyLibraryStoreModel>
export type MyLibraryStoreSnapshot = SnapshotOut<typeof MyLibraryStoreModel>

export type MyLibraryResourcesStore = Instance<typeof MyLibraryResourcesStoreModel>
export type MyLibraryResourcesStoreSnapshot = SnapshotOut<typeof MyLibraryResourcesStoreModel>