import { types } from "mobx-state-tree"
import { MyLibraryActions, MyLibraryPropsModel, MyLibraryResourcesViews, MyLibraryViews } from "../viewmodels"
import { StoreName } from "../../../constants/app.constant"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store/general-resources.store"
import { MyLibraryService } from "../services/my-library-service"
import { NavigationStoreModel } from "../../../models/navigation-store/navigation.store"

const MyLibraryModel = types.model(StoreName.MyLibrary, MyLibraryPropsModel)

export const MyLibraryStoreModel = types.compose(
    MyLibraryModel,
    MyLibraryViews,
    MyLibraryActions,
    MyLibraryService,
    NavigationStoreModel
    )

export const MyLibraryResourcesStoreModel = types.compose(GeneralResourcesStoreModel, MyLibraryResourcesViews)

