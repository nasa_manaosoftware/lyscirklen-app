import { types, flow } from "mobx-state-tree"
import DownloadServices from '../../../services/api-domain/download.service'
import { RootStore } from "../../../models/root-store/root.types"
import { MyLibraryPropsModel } from "../viewmodels/my-library.models"

export const MyLibraryService = types.model(MyLibraryPropsModel).actions(self => {
    const deleteBook = flow(function* (rootStore: RootStore, book) {
        try {
            const result = DownloadServices.deleteAudioBook(rootStore, book)
            return result
        } catch (err) {
            console.log('err: ', err);
            return err
        }
    })
    return {
        deleteBook
    }
})