import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"

export interface MyLibraryProps extends NavigationContainerProps<ParamListBase> {
}