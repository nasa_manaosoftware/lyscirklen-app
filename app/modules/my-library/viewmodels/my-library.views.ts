import { types } from "mobx-state-tree"
import { NavigationKey } from "../../../constants/app.constant"
import { GeneralResources } from "../../../constants/firebase/remote-config/remote-config.constant"
import { DOWNLOADEDBOOK } from "../../../models/downloaded-store/downloaded.types"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store/general-resources.store"
import { MyLibraryPropsModel } from "./my-library.models"
export const MyLibraryViews = types.model(MyLibraryPropsModel)
    .views(self => ({
        get getIsShowModal(): boolean {
            return self.isShowModal
        },
        get getIsDisabled(): boolean {
            return self.isDisabled
        },
        get getmyAudiobooksList(): (Partial<DOWNLOADEDBOOK>[]) {
            return self.myAudiobooksList
        },
        get getLastMyAudiobookId(): string {
            return self.lastMyAudiobookId
        },
        get getIsRefresh(): boolean {
            return self.isRefresh
        },
        get getIsLoadMore(): boolean {
            return self.isLoadMore
        },
        get getIsLoading(): boolean {
            return self.isLoading
        },
        get getAudiobookSelected(): (Partial<DOWNLOADEDBOOK>[]) {
            return self.audiobookSelected
        },
        get getModalButtonsDisabled(): object {
            const buttons = {
                isCancelDisabled: self.isCancelDisabled,
                isConfirmDisabled: self.isConfirmDisabled,
                isCancelAnimated: self.isCancelAnimated,
                isConfirmAnimated: self.isConfirmAnimated
            }

            return buttons
        },
        get getMyLibraryLength(): number {
            return self.myLibraryLength
        }

    }))

export const MyLibraryResourcesViews = GeneralResourcesStoreModel.views(self => {
    //MARK: Volatile State
    const { MyLibrary } = GeneralResources
    const {
        myDownloadTitle,
        noDataTitle,
        deleteButtonTitle,
        cancelButtonTitle,
        modalMessage
    } = MyLibrary

    //MARK: Views
    const getResources = (locale: string, key: string, childKeyOrShareKey: string | boolean = false, isChildDotNotation: boolean = false) => self.getValues(locale, childKeyOrShareKey ? key : NavigationKey.MyLibrary, childKeyOrShareKey ? true : key, isChildDotNotation)
    const getResourceMyDownloadTitle = (locale: string) => getResources(locale, myDownloadTitle)
    const getResourceNoDataTitle = (locale: string) => getResources(locale, noDataTitle)
    const getResourceDeleteButtonTitle = (locale: string) => getResources(locale, deleteButtonTitle)
    const getResourceCancelButtonTitle = (locale: string) => getResources(locale, cancelButtonTitle)
    const getResourceModalMessage = (locale: string) => getResources(locale, modalMessage)

    return {
        getResourceMyDownloadTitle,
        getResourceNoDataTitle,
        getResourceDeleteButtonTitle,
        getResourceCancelButtonTitle,
        getResourceModalMessage
    }


    //MARK: Views

    return {

    }
})
