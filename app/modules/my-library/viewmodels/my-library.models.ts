import { types } from "mobx-state-tree";
import { DOWNLOADEDBOOK } from "../../../models/downloaded-store/downloaded.types";

export const MyLibraryPropsModel = {
    isShowModal: types.optional(types.boolean, false),
    isDisabled: types.optional(types.boolean, false),
    myAudiobooksList: types.optional(types.frozen<(Partial<DOWNLOADEDBOOK>)[]>(), []),
    lastMyAudiobookId: types.optional(types.string, ""),
    isRefresh: types.optional(types.boolean, false),
    isLoadMore: types.optional(types.boolean, false),
    isLoading: types.optional(types.boolean, false),
    audiobookSelected:  types.optional(types.frozen<(Partial<DOWNLOADEDBOOK>)[]>(), []),
    isConfirmDisabled: types.optional(types.boolean, false),
    isCancelDisabled: types.optional(types.boolean, false),
    isConfirmAnimated: types.optional(types.boolean, true),
    isCancelAnimated: types.optional(types.boolean, true),
    myLibraryLength: types.optional(types.number, 0)
}