import { types } from "mobx-state-tree"
import { DOWNLOADEDBOOK } from "../../../models/downloaded-store/downloaded.types"
import { MyLibraryPropsModel } from "./my-library.models"

export const MyLibraryActions = types.model(MyLibraryPropsModel).actions(self => {
    const setIsShowModal = (value: boolean) => {
        self.isShowModal = value
    }

    const setIsDisabled = (value: boolean) => {
        self.isDisabled = value
    }

    const setmyAudiobooksList = (value: (Partial<DOWNLOADEDBOOK>[])) => {
        self.myAudiobooksList = value
    }

    const setLastMyAudiobookId = (value: string) => {
        self.lastMyAudiobookId = value
    }

    const setIsRefresh = (value: boolean) => {   
        self.isRefresh = value     
    }

    const setIsLoadMore = (value: boolean) => {   
        self.isLoadMore = value     
    }

    const setIsLoading = (value: boolean) => { 
        self.isLoading = value  
    }

    const setAudiobookSelected = (value: (Partial<DOWNLOADEDBOOK>[])) => { 
        self.audiobookSelected = value  
    }

    const setModalButtonsDisabled = (disableValue: boolean, amimateValue: boolean) => {
        self.isCancelDisabled = disableValue
        self.isConfirmDisabled = disableValue
        self.isCancelAnimated = amimateValue
        self.isConfirmAnimated = amimateValue
    }

    const removeBook = (uid: string) => { 
        var data = [];

        for ( var i = 0; i < self.myAudiobooksList.length; i++ ){
            if ( uid != self.myAudiobooksList[i].uid ){            
                data.push(self.myAudiobooksList[i]);
            }
        }

        self.myAudiobooksList = data


    }

    const setMyLibraryLength = (value: number) => {
        self.myLibraryLength = value
    }

    return {
        setIsShowModal,
        setIsDisabled,
        setmyAudiobooksList,
        setLastMyAudiobookId,
        setIsRefresh,
        setIsLoadMore,
        setIsLoading,
        setAudiobookSelected,
        setModalButtonsDisabled,
        removeBook,
        setMyLibraryLength
    }
})