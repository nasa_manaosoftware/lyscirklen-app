import { ImageStyle, TextStyle, View, ViewStyle } from "react-native";
import { HeaderProps } from "../../components/header/header.props";
import { images } from "../../theme/images";
import * as metric from "../../theme"
import { type } from "../../theme/fonts";
import { RFValue } from "react-native-responsive-fontsize";
import { color } from "../../theme";

export const CONTAINER: ViewStyle = {
    flex: 1
}

export const SAFE_AREA_CONTAINER: ViewStyle = {
    ...CONTAINER,
    backgroundColor: color.background
}

export const BUTTON_TEXT: TextStyle = {
    textAlign: 'center',
    fontSize: RFValue(22),
    fontFamily: type.title,
  
}

export const MENU_ICON_STYLE: ImageStyle = {
    width: metric.ratioWidth(24),
    height: metric.ratioHeight(24)
}

export const HEADER_PROPS: HeaderProps = {
    leftIconSource: images.hamMenu,
    leftIconStyle: MENU_ICON_STYLE,
    rightIconStyle: MENU_ICON_STYLE,
    rightIconSource: images.bell,
    isLeftIconAnimated: false,
    isRightIconAnimated: false
}

export const TOPIC_VIEW_CATEGORY: ViewStyle = {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingHorizontal: metric.ratioWidth(16),
    marginTop: metric.ratioHeight(21),
    marginBottom: metric.ratioHeight(15)
}

export const URGENT_TITLE_BORDER: ViewStyle = {
    backgroundColor: color.redLine,
    width: metric.ratioWidth(2),
    height: metric.ratioHeight(22),
    marginRight: metric.ratioWidth(11)
}

export const TOPIC_TITLE_TEXT: TextStyle = {
    paddingTop: metric.isMicrolearn() ? null : metric.ratioHeight(5),
    paddingBottom: metric.isMicrolearn() ? metric.ratioHeight(2) : null,
    fontFamily: type.title,
    fontSize: RFValue(22),
    color: color.whiteText
}

export const ICON_SIZE: ViewStyle = {
    width: metric.isTablet()? metric.ratioWidth(50) : metric.ratioWidth(70),
    height: metric.isTablet()? metric.ratioWidth(30) : metric.ratioHeight(50)
}

export const EMPTY_TEXT_CONTAINER: ViewStyle = {
    marginVertical: metric.ratioHeight(30)
}

export const EMPTY_TEXT: TextStyle = {
    fontFamily: type.title,
    fontSize: RFValue(22),
    color: color.whiteText,
    textAlign: 'center'
}

export const NO_DATA_CONTAINER: ViewStyle = {
    ...CONTAINER,
    alignItems: 'center',
    justifyContent: 'center'
}

export const FLATLIST_CONTAINER: ViewStyle = {
    marginLeft: metric.isTablet() ? metric.ratioWidth(13) : metric.ratioWidth(14),
    paddingTop: metric.ratioHeight(10),
    paddingBottom: metric.ratioHeight(50),
    paddingHorizontal: metric.ratioWidth(17)
}

export const ITEMSEPERATOR: ViewStyle = {
    marginBottom: metric.ratioHeight(30)
}

export const SUCCESSED_MESSAGE_VIEW: ViewStyle = {
    justifyContent: 'center',
    backgroundColor: color.white,
    marginHorizontal: metric.ratioWidth(16),
    paddingHorizontal: metric.ratioWidth(35),
    paddingVertical: metric.ratioHeight(35)
}

export const TEXT_SUCCESSED: TextStyle = {
    fontFamily: type.base,
    fontSize: RFValue(18),
    color: color.black,
    marginTop: metric.ratioHeight(24),
    marginBottom: metric.ratioHeight(30),
    textAlign: 'center'
}

export const IMAGE_MESSAGE_SENT: ViewStyle = {
    width: metric.ratioWidth(90),
    height: metric.ratioHeight(90),
    alignSelf: 'center'
}

export const CONFIRM_BUTTON: ViewStyle = {
    backgroundColor: color.lightRed,
    width: metric.ratioWidth(200),
    borderRadius: 15,
    paddingTop: metric.isMicrolearn() ? null : metric.ratioHeight(12),
    paddingBottom: metric.isMicrolearn() ? metric.ratioHeight(12) : null 
}

export const CANCEL_BUTTON: ViewStyle = {
    backgroundColor: color.white,
    width: metric.ratioWidth(200),
    borderRadius: 15,
    borderWidth: metric.ratioWidth(1),
    borderColor: color.cancelText,
    paddingTop: metric.isMicrolearn() ? null : metric.ratioHeight(12),
    paddingBottom: metric.isMicrolearn() ? metric.ratioHeight(12) : null
}

export const CONFIRM_BUTTON_TEXT: TextStyle = {
    ...BUTTON_TEXT,
    color: color.white
}

export const CANCEL_BUTTON_TEXT: TextStyle = {
    ...BUTTON_TEXT,
    color: color.cancelText
}

export const BUTTOM_COMFIRM_CONTAINER: ViewStyle = {
    marginBottom: metric.ratioHeight(13),
    alignItems: 'center'
}
export const BUTTOM_CANCEL_CONTAINER: ViewStyle = {
    marginTop: metric.ratioHeight(13),
    alignItems: 'center'
}

export const BUTTON_DISABLE: ViewStyle = {
    backgroundColor: color.buttonDisabled,
    width: metric.ratioWidth(200),
    borderRadius: 15,
    paddingTop: metric.isMicrolearn() ? null : metric.ratioHeight(12),
    paddingBottom: metric.isMicrolearn() ? metric.ratioHeight(12) : null
}

export const CANCEL_BUTTON_DISABLE: ViewStyle = {
    backgroundColor: color.white,
    width: metric.ratioWidth(200),
    borderRadius: 15,
    borderWidth: metric.ratioWidth(1),
    borderColor: color.buttonDisabled,
    paddingTop: metric.isMicrolearn() ? null : metric.ratioHeight(12),
    paddingBottom: metric.isMicrolearn() ? metric.ratioHeight(12) : null
}

export const CANCEL_BUTTON_DISABLE_TEXT: TextStyle = {
    ...BUTTON_TEXT,
    color: color.buttonDisabled
}
