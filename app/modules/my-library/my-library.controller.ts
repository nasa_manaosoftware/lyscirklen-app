import { DrawerActions, StackActions } from "@react-navigation/core";
import { onSnapshot } from "mobx-state-tree";
import { Alert, Platform } from "react-native";
import { DownloadStatus, NavigationKey } from "../../constants/app.constant";
import { INavigationRoute } from "../../models";
import { RootStore } from "../../models/root-store/root.types";
import { BaseController } from "../base.controller";
import { MyLibraryProps } from "./my-library.props";
import { MyLibraryResourcesStore, MyLibraryResourcesStoreModel, MyLibraryStore, MyLibraryStoreModel } from "./storemodels";
import FirebaseFireStore from "../../utils/firebase/fire-store/fire-store";
import { DOWNLOADEDBOOK } from "../../models/downloaded-store/downloaded.types";
import FirebaseAnalytics from "../../utils/firebase/analytics/analytics";

class MyLibraryController extends BaseController {

    /*
       Stateful Variable
   */

    public static viewModel: MyLibraryStore
    public static resourcesViewModel: MyLibraryResourcesStore
    public static myProps: MyLibraryProps & Partial<INavigationRoute>

    public static category_name: string

    /*
        Mark Constructor
    */

    constructor(rootStore?: RootStore, props?: MyLibraryProps & Partial<INavigationRoute>) {
        super(rootStore, props)
        if (this._myProps?.route?.params?.isInitialized) return
        this._setupResourcesViewModel()
        this._setupNavigation()
        this._setupProps(props)
        this._setupViewModel()
        this._setupSnapShot()
    }

    /*
       Mark Setup
    */

    private _setupResourcesViewModel = () => {
        MyLibraryController.resourcesViewModel = MyLibraryResourcesStoreModel.create({ ...this._rootStore?.getAllGeneralResourcesStore })
    }
    private _setupViewModel = () => {
        const localStore = this._rootStore?.getModuleStore(NavigationKey.MyLibrary)
        MyLibraryController.viewModel = localStore && MyLibraryStoreModel.create({ ...localStore }) || MyLibraryStoreModel.create({})
        this._setInitializedToPropsParams()
    }

    private _setupSnapShot = () => {
        onSnapshot(MyLibraryController.viewModel, (snap: MyLibraryStore) => {
            this._rootStore?.setModuleStoreValue(NavigationKey.MyLibrary, snap)
        })
    }

    public onSideMenuPress = () => {
        this._myProps?.navigation?.dispatch(DrawerActions.openDrawer() as any)
    }

    public onNotificationPress = () => {
        // this._myProps?.navigation?.dispatch(StackActions.push(NavigationKey.NotificationList) as any)
        Alert.alert('Notifications')
    }

    public onPressAudiobookCoverAndNameAndInfoIcon = (item: object) => {
        MyLibraryController.viewModel?.setIsDisabled(true)
        this._rootStore?.getNavigationStore?.setNavigationTo(NavigationKey.AudioBookScreen)

        const navigation = this._myProps?.navigation
        const params = {
            id: item.uid
        }

        navigation?.navigate(NavigationKey.AudioBookScreen, params)
        this._delayExecutor(() => MyLibraryController.viewModel?.setIsDisabled(false), 1200)

        //firebase analytic
        FirebaseAnalytics.openAudiobooksDetail({
            userId: this._rootStore?.getUserStore.getUserId,
            book_name: item.name,
            organization_id: this._rootStore?.getOrganizationId
        })
    }

    public onDeleteAudiobook = (audiobook: any) => {
        MyLibraryController.viewModel?.setIsShowModal(true)
        MyLibraryController.viewModel?.setAudiobookSelected(audiobook)
    }

    public deleteAudiobookInFirestore = async (deviceId: string, audiobookId: string) => {
        const organizationId = MyLibraryController.rootStore?.getOrganizationId

        //delete audiobook in firestore
        const deleteInFirestore = await FirebaseFireStore.deleteAudiobookInDevice(deviceId, audiobookId)
        if (deleteInFirestore) {
            //update audiobook downloaded in download store
            const downloaded = await FirebaseFireStore.fetchMyDownloaded(deviceId, organizationId)
            MyLibraryController.rootStore?.getDownloadedStore?.setDownloadedList(downloaded)
            await this.fetchMyLibrary()
            MyLibraryController.viewModel?.setIsDisabled(false)
            MyLibraryController.viewModel?.setModalButtonsDisabled(false, true)
        }
        else {
            this._delayExecutor(() => this._isGlobalLoading(false), 1500)
            MyLibraryController.viewModel?.setIsDisabled(false)
            MyLibraryController.viewModel?.setModalButtonsDisabled(false, true)
            Alert.alert('Delete is not success, Please try again')
        }
    }

    // public updateAudiobookInRootStore = (audiobookId: string) => {
    //     const audioBooks = this._rootStore?.getAudioBooksStore.getAudiobooksList
    //     let updateAudioBooks = []
    //     audioBooks.map(item =>
    //         item.uid === audiobookId ?
    //             updateAudioBooks.push({
    //                 ...item,
    //                 download_status: DownloadStatus.DO_NOT_DOWNLOAD,
    //                 is_downloading: false
    //             })
    //             :
    //             updateAudioBooks.push(item)
    //     )

    //     this._rootStore?.getAudioBooksStore.setAudiobooksList(updateAudioBooks)
    // }

    public onPressConfirm = async () => {
        const audiobookSelected = MyLibraryController.viewModel?.getAudiobookSelected
        const deviceId = MyLibraryController.rootStore?.getDeviceId

        if (this._rootStore?.getSharedStore?.getIsConnected) {
            MyLibraryController.viewModel?.setModalButtonsDisabled(true, false)
            MyLibraryController.viewModel?.setIsDisabled(true)
            this._delayExecutor(() => MyLibraryController.viewModel?.setIsShowModal(false), 100)
            this._delayExecutor(() => this._isGlobalLoading(true), Platform.OS === 'ios' ? 700 : 500)
        } else {
            this._delayExecutor(() => MyLibraryController.viewModel?.setIsShowModal(false), 100)
            MyLibraryController.viewModel.removeBook(audiobookSelected.uid)
        }

        /* flow: delete file in device --> in firestore */
        //delete audiobook file in device
        const result = await MyLibraryController.viewModel?.deleteBook(this._rootStore, audiobookSelected)
        if (result === "Delete success") {
            //delete audiobook in firestore
            await this.deleteAudiobookInFirestore(deviceId, audiobookSelected.uid)
        } else {
            console.log('not found');
            await this.deleteAudiobookInFirestore(deviceId, audiobookSelected.uid)
        }
    }

    public onPressCancel = () => {
        //close modal
        MyLibraryController.viewModel?.setIsShowModal(false)
    }

    public queryCategoryName = (book) => {
        const categoryStore = MyLibraryController.rootStore?.getAudioBookCategoriesStore?.getAudiobooksCetegories

        categoryStore.map(item => {
            if (item.uid === book.category_id) {
                MyLibraryController.category_name = item.name
            }
        })
    }

    public onPressPlaybackAudio = (item: any) => {
        this.queryCategoryName(item)
        
        // this._rootStore?.getPlaybackStore?.setAudiobook((item) as DOWNLOADEDBOOK)
        this._rootStore?.resetPlaybackStore()
        setTimeout(() => {
            this._rootStore?.getPlaybackStore?.play(({...item}) as DOWNLOADEDBOOK)
        }, 300)

        MyLibraryController.viewModel?.setIsDisabled(true)
        this._delayExecutor(() => MyLibraryController.viewModel?.setIsDisabled(false), 1200)

        //firebbase analytic
        FirebaseAnalytics.audiobookInitialPlayed({
            book_name: item.name,
            category_name: MyLibraryController.category_name,
            organization_id: this._rootStore?.getOrganizationId
        })

        FirebaseAnalytics.amountAudiobookPlayedPerUser({
            user_number: this._rootStore?.getUserStore?.getUserId,
            book_name: item.name,
            category_name: MyLibraryController.category_name,
            organization_id: this._rootStore?.getOrganizationId
        })
    }

    public onRefresh = () => {
        MyLibraryController.viewModel?.setIsRefresh(true)
        this.fetchMyLibrary()
        this._delayExecutor(() => MyLibraryController.viewModel?.setIsRefresh(false), 1000)
    }

    public checkAudiobookData = async (audiobooks: any) => {
        let updateAudiobooksDownloadedInfo = []
        if (audiobooks.length > 0) {
            await Promise.all(audiobooks.map(async (data) => {
                const audiobook = await FirebaseFireStore.findAudiobook(data.uid)
                if (audiobook !== undefined) {
                    updateAudiobooksDownloadedInfo.push({
                        ...data,
                        category_id: audiobook?.category_id,
                        description: audiobook?.description,
                        forfatter: audiobook?.forfatter,
                        image: {
                            file_name: audiobook?.image?.file_name,
                            storage_path: audiobook?.image?.storage_path,
                            thumbnail_url: audiobook?.image?.thumbnail_url,
                            url: audiobook?.image?.url
                        },
                        name: audiobook?.name,
                        organization_id: audiobook?.organization_id,
                        video_url: audiobook?.video_url
                    })
                } else {
                    updateAudiobooksDownloadedInfo.push(data)
                }
            }))

        }
        updateAudiobooksDownloadedInfo.sort((firstCategory: any, secondCategory: any) => firstCategory.name.toUpperCase() < secondCategory.name.toUpperCase() ? -1 : 1)
        return updateAudiobooksDownloadedInfo

    }

    public onHandleLoadmore = async () => {
        const lastId = MyLibraryController.viewModel?.getLastMyAudiobookId
        const deviceId = MyLibraryController.rootStore?.getDeviceId
        const organizationId = MyLibraryController.rootStore?.getOrganizationId
        const size = 10

        if (MyLibraryController.viewModel?.getIsLoadMore === true || (lastId === "" || lastId === undefined)) return
        MyLibraryController.viewModel?.setIsLoadMore(true)

        const loadmoreMyAudiobooks = await FirebaseFireStore.loadMoreMyAudiobooks(deviceId, organizationId, size, lastId)
        const result = await this.checkAudiobookData(loadmoreMyAudiobooks.myAudiobooks)


        if (MyLibraryController.viewModel?.getmyAudiobooksList.length === 0) {
            MyLibraryController.viewModel?.setmyAudiobooksList(result)
        } else {
            const newAudiobook = [
                ...MyLibraryController.viewModel?.getmyAudiobooksList,
                ...result
            ]
            newAudiobook.sort((firstCategory: any, secondCategory: any) => firstCategory.name.toUpperCase() < secondCategory.name.toUpperCase() ? -1 : 1)
            MyLibraryController.viewModel?.setmyAudiobooksList(newAudiobook)
        }
        MyLibraryController.viewModel?.setLastMyAudiobookId(loadmoreMyAudiobooks.lastAudibookId)
        MyLibraryController.viewModel?.setIsLoadMore(false)
    }

    public sortingBookName = (mylibrary) => {
        const audiobooks = mylibrary.slice().sort(function (a, b) {
            return a.name.localeCompare(b.name);
        })

        MyLibraryController.viewModel?.setmyAudiobooksList(audiobooks)
        this._delayExecutor(() => this._isGlobalLoading(false), 1000)

    }

    public fetchMyLibrary = async () => {

        // fetch data from local
        const audiobookList = MyLibraryController.rootStore?.getDownloadedStore?.getDownloadedList

        const libraryList = []

        audiobookList.map(item => {
            if(item.organization_id === this._rootStore?.getOrganizationId) {
                libraryList.push({
                    ...item
                })
            }
        })

        // const mylibrary = []

        // audiobookList.map(item1 => {
        //     if (item1.download_status === DownloadStatus.SUCCESS) {
        //         mylibrary.push(item1)
        //     }
        // })
        // console.log('mylibrary ===> ', mylibrary);

        MyLibraryController.viewModel?.setMyLibraryLength(libraryList.length)
        this.sortingBookName(libraryList)

        // const organizationId = MyLibraryController.rootStore?.getOrganizationId
        // const deviceId = MyLibraryController.rootStore?.getDeviceId
        // const size = 10

        // //if (!((!this._rootStore?.getSharedStore?.getIsConnected) && (Platform.OS === 'ios'))) {
        // if (this._rootStore?.getSharedStore?.getIsConnected) {
        //     const myAudiobooksList = await FirebaseFireStore.fetchMyAudiobooks(deviceId, organizationId, size)
        //     var myLibrary = MyLibraryController.viewModel?.getmyAudiobooksList

        //     if (myLibrary.length !== 0 || myAudiobooksList.myAudiobooks.length !== 0) {
        //         const result = await this.checkAudiobookData(myAudiobooksList.myAudiobooks)
        //         var newAudiobooksUpdated = false

        //         //check current data and new data
        //         if (JSON.stringify(myLibrary) !== JSON.stringify(result)) {
        //             newAudiobooksUpdated = true
        //         }

        //         if (newAudiobooksUpdated) {
        //             this._isGlobalLoading(true)
        //             MyLibraryController.viewModel?.setmyAudiobooksList(result)
        //             MyLibraryController.viewModel?.setLastMyAudiobookId(myAudiobooksList.lastAudibookId)
        //         } else {
        //             MyLibraryController.viewModel?.setLastMyAudiobookId(myAudiobooksList.lastAudibookId)
        //         }
        //     } else {
        //         MyLibraryController.viewModel?.setmyAudiobooksList([])
        //         MyLibraryController.viewModel?.setLastMyAudiobookId("")
        //     }
        //     this._delayExecutor(() => this._isGlobalLoading(false), 1000)
        // }
    }


    viewDidAppearAfterFocus = async () => {
        MyLibraryController.viewModel?.setModalButtonsDisabled(false, true)
        MyLibraryController.viewModel?.setIsShowModal(false)
        MyLibraryController.viewModel?.setAudiobookSelected([])
        MyLibraryController.viewModel?.setIsDisabled(false)

        //check previous navigate
        if (this._rootStore?.getNavigationStore?.getNavigationTo !== NavigationKey.MyLibrary) {
            if (MyLibraryController.viewModel?.getMyLibraryLength !== MyLibraryController.rootStore?.getDownloadedStore?.getDownloadedList.length) {
                this._delayExecutor(() => this._isGlobalLoading(true), 100)
            } else {
                this._isGlobalLoading(false)
            }
            await this.fetchMyLibrary()
        }
    }

    viewWillDisappear = async () => {
        this._rootStore?.getNavigationStore?.setNavigationTo(null)
        MyLibraryController.viewModel?.setIsShowModal(false)
        MyLibraryController.viewModel?.setAudiobookSelected([])
    }

    deInit = async () => {
        MyLibraryController.viewModel?.setModalButtonsDisabled(false, true)
        MyLibraryController.viewModel?.setLastMyAudiobookId("")
        MyLibraryController.viewModel?.setIsLoadMore(false)
        MyLibraryController.viewModel?.setIsRefresh(false)
        MyLibraryController.viewModel?.setIsShowModal(false)
        MyLibraryController.viewModel?.setAudiobookSelected([])
    }
}

export default MyLibraryController
