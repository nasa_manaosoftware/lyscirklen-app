import React from "react"
import { observer } from "mobx-react-lite"
import { useConfigurate } from "../../custom-hooks/use-configure-controller";
import { Image, SafeAreaView, Text, View, FlatList, RefreshControl, ActivityIndicator, Platform } from "react-native"
import * as Styles from "./my-library.styles"
import MyLibraryController from "./my-library.controller";
import { MyLibraryProps } from "./my-library.props";
import { AudioBook } from "../../components/audio-book/audio-book";
import FastImage from "react-native-fast-image";
import { images } from "../../theme/images";
import Modal from 'react-native-modal';
import { color } from "../../theme";
import { Button } from "../../components/button/button";
import * as metric from '../../theme' 

export const MyLibrary: React.FunctionComponent<MyLibraryProps> = observer((props) => {

    const controller = useConfigurate(MyLibraryController, props) as MyLibraryController
    const isDisabled = MyLibraryController.viewModel?.getIsDisabled
    const myLibraryList = MyLibraryController.viewModel?.getmyAudiobooksList
    const confirmButtonIsDisabled = MyLibraryController.viewModel?.getModalButtonsDisabled?.isConfirmDisabled
    const confirmButtonIsAnimeted = MyLibraryController.viewModel?.getModalButtonsDisabled?.isConfirmAnimated
    const cancelButtonIsDisabled = MyLibraryController.viewModel?.getModalButtonsDisabled?.isCancelDisabled
    const cancelButtonIsAnimated = MyLibraryController.viewModel?.getModalButtonsDisabled?.isCancelAnimated
    const audiobookNameSeleted = MyLibraryController.viewModel?.getAudiobookSelected?.name
    
    const modal = () => {
        return (
            <Modal
                isVisible={MyLibraryController.viewModel?.getIsShowModal}
                presentationStyle="overFullScreen"
                animationIn={'fadeIn'}
                animationOut={'fadeOut'}
                backdropTransitionOutTiming={0}
                backdropColor={color.background}
            >
                <View style={Styles.SUCCESSED_MESSAGE_VIEW}>
                    <Image
                        source={images.deleteModal}
                        resizeMode={'contain'}
                        style={Styles.IMAGE_MESSAGE_SENT}
                    />
                    <Text style={Styles.TEXT_SUCCESSED}>
                        {MyLibraryController.resourcesViewModel?.getResourceModalMessage()} "{metric.isMicrolearn() ? audiobookNameSeleted : audiobookNameSeleted?.toUpperCase()}" ?
                    </Text>
                    <View style={Styles.BUTTOM_COMFIRM_CONTAINER}>
                        <Button    
                            disabled={confirmButtonIsDisabled}
                            isAnimated={confirmButtonIsAnimeted}
                            text={metric.isMicrolearn() ? MyLibraryController.resourcesViewModel?.getResourceDeleteButtonTitle() : MyLibraryController.resourcesViewModel?.getResourceDeleteButtonTitle().toUpperCase()}
                            textStyle={Styles.CONFIRM_BUTTON_TEXT}
                            style={!(confirmButtonIsDisabled && !confirmButtonIsAnimeted)? Styles.CONFIRM_BUTTON : Styles.BUTTON_DISABLE}
                            onPress={() => controller.onPressConfirm()}
                        />
                    </View>
                    <View style={Styles.BUTTOM_CANCEL_CONTAINER}>
                        <Button
                            disabled={cancelButtonIsDisabled}
                            isAnimated={cancelButtonIsAnimated}
                            text={metric.isMicrolearn() ? MyLibraryController.resourcesViewModel?.getResourceCancelButtonTitle() : MyLibraryController.resourcesViewModel?.getResourceCancelButtonTitle().toUpperCase()}
                            style={!(cancelButtonIsDisabled && !cancelButtonIsAnimated)? Styles.CANCEL_BUTTON: Styles.CANCEL_BUTTON_DISABLE}
                            textStyle={!(cancelButtonIsDisabled && !cancelButtonIsAnimated)? Styles.CANCEL_BUTTON_TEXT : Styles.CANCEL_BUTTON_DISABLE_TEXT }
                            onPress={() => controller.onPressCancel()}
                        />
                    </View>
                </View>
            </Modal>
        )
    }

    return (
        <SafeAreaView style={Styles.SAFE_AREA_CONTAINER}>
            <>
                {modal()}
            </>
            <View shouldRasterizeIOS style={Styles.TOPIC_VIEW_CATEGORY}>
                <View style={Styles.URGENT_TITLE_BORDER} />
                <Text style={Styles.TOPIC_TITLE_TEXT}>{metric.isMicrolearn() ? MyLibraryController.resourcesViewModel?.getResourceMyDownloadTitle() : MyLibraryController.resourcesViewModel?.getResourceMyDownloadTitle().toUpperCase()}</Text>
            </View>
            <View style={Styles.CONTAINER}>
                {MyLibraryController.viewModel?.getIsRefresh && Platform.OS === 'ios'?
                    <View >
                        <ActivityIndicator size="large" color={color.white} />
                    </View>
                    :
                    (myLibraryList?.length === 0) ?
                        <View style={Styles.NO_DATA_CONTAINER}>
                            <FastImage resizeMode={"contain"} source={images.myLibrary} style={Styles.ICON_SIZE} />
                            <View style={Styles.EMPTY_TEXT_CONTAINER}>
                                <Text style={Styles.EMPTY_TEXT}>{metric.isMicrolearn() ? MyLibraryController.resourcesViewModel?.getResourceNoDataTitle() : MyLibraryController.resourcesViewModel?.getResourceNoDataTitle().toUpperCase()}</Text>
                            </View>
                        </View>
                        :
                        <FlatList
                            contentContainerStyle={Styles.FLATLIST_CONTAINER}
                            extraData={myLibraryList?.slice()}
                            data={myLibraryList}
                            keyExtractor={(item, index) => index.toString()}
                            ItemSeparatorComponent={() =>
                                (<View shouldRasterizeIOS style={Styles.ITEMSEPERATOR} />)
                            }
                            refreshControl={
                                <RefreshControl
                                    refreshing={MyLibraryController.viewModel?.getIsRefresh}
                                    onRefresh={() => controller.onRefresh()}
                                />
                            }
                            initialNumToRender={10}
                            onEndReachedThreshold={0.1}
                            // onEndReached={controller.onHandleLoadmore}
                            renderItem={({ item, index }) =>
                                <AudioBook
                                    key={index}
                                    audiobookId={item.uid}
                                    screen={props.route?.name}
                                    title={item.name}
                                    author={item.forfatter}
                                    bookCover={item.image.url}
                                    downloadStatus={item.download_status}
                                    isMyLibrary={true}
                                    disabled={isDisabled}
                                    onPressBookCover={() => controller.onPressAudiobookCoverAndNameAndInfoIcon(item)}
                                    onPressBookName={() => controller.onPressAudiobookCoverAndNameAndInfoIcon(item)}
                                    onPressInfoIcon={() => controller.onPressAudiobookCoverAndNameAndInfoIcon(item)}
                                    onPressAuthorName={() => controller.onPressAudiobookCoverAndNameAndInfoIcon(item)}
                                    onPressDelete={() => controller.onDeleteAudiobook(item)}
                                    onPressPlayBackButton={() => controller.onPressPlaybackAudio(item)}
                                />
                            }

                        />
                }
            </View>
        </SafeAreaView>
    )
})