import { types, flow } from "mobx-state-tree"
import { SearchPropsModel } from "../viewmodels"
import { APIResponse, APISuccessResponse } from "../../../constants/service.types"
import { RootStore } from "../../../models/root-store/root.types"
import SearchAudioServices from "../../../services/api-domain/search-audio.services"
import { ISEARCH_AUDIO_BOOK } from "../../../models/user-store"
import DownloadServices from '../../../services/api-domain/download.service'
import { DownloadStatus } from "../../../constants/app.constant"

export const SearchServiceActions = types.model(SearchPropsModel).actions(self => {

    const fetchSearchAudioBook = flow(function* (rootStore?: RootStore, searchText?: string) {

        const params = {
            start_at: 0,
            page_size: 100,
            searchText: searchText
        }

        try {
            const result: APIResponse = yield SearchAudioServices.searchAudioBook(params)
            let success = (result as APISuccessResponse)?.data

            let listData = []
            let list: Partial<ISEARCH_AUDIO_BOOK[]> = success?.data

            if ( list ){
                list.map((item: any) => {
                    listData.push({
                        uid: item.uid,
                        category_id: item.category_id,
                        description: item.description,
                        forfatter: item.forfatter,
                        image: {
                            file_name: item.file_name,
                            storage_path: item.storage_path,
                            thumbnail_url: item.thumbnail_url,
                            url: item.image?.url,
                        },
                        name: item.name,
                        organization_id: item.organization_id,
                        video: {
                            name: item.video?.name,
                            url: item.video?.url,
                            download_url: item.video?.download_url,
                            streaming_url: item.video?.streaming_url
                        },
                        download_status: DownloadStatus.DO_NOT_DOWNLOAD,
                        is_downloading: false

                    })
                })
            }
            
            return listData
        } catch (e) {
            console.log(e)
            return e
        } finally {
            // self.isPullRefreshing = false
            // self.isLoadMore = false
            // self.isFirstLoadDone = true
        }

    })

    const downloadBook = flow(function* (rootStore: RootStore, book) {
        try {
            const result = DownloadServices.downloadedAudioBook(rootStore, book)
            return result
        } catch (err) {
            console.log(err);
            return err
        }
    })


    return {
        fetchSearchAudioBook,
        downloadBook
    }
})