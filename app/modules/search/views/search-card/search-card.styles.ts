import { TextStyle, ViewStyle, ImageStyle } from "react-native"
import * as metric from "../../../../theme"
import { color } from "../../../../theme"
import { RFValue } from "react-native-responsive-fontsize"

// const FULL: ViewStyle = {
//     flex: 1
// }
const TEXT: TextStyle = {
    color: color.palette.black,
    fontFamily: 'Verdana',
    fontSize: RFValue(16),
}

export const ROOT_CONTAINER: ViewStyle = {
    flexDirection: 'column', 
    backgroundColor: '#ffffff',  
    paddingRight : metric.ratioHeight(20),
    paddingVertical: metric.ratioHeight(22)
}

//******************* Message Container *******************
export const MESSAGE_CONTAINER: ViewStyle = {
    backgroundColor: '#ffffff',  
    flexDirection: 'row', 
    paddingBottom : 10
}


export const SOS_CONTAINER: ViewStyle = {
    backgroundColor: '#ffffff',  
    width: '5%', 
    height: 'auto', 
    flex: 0, 
    paddingRight : 5 ,
    marginTop: metric.ratioHeight(3)
}

export const MESSAGE_DETAIL_CONTAINER: ViewStyle = {
    backgroundColor: '#ffffff',  
    width: '85%', 
    height: 'auto', 
    flex: 0
}

export const SOS_CELL: ViewStyle = {
    backgroundColor: '#C8102E',  
    width: '100%', 
    height: metric.ratioHeight(22), 
    flex: 0 
}

export const HAMBURGER_CONTAINER: ViewStyle = {
    backgroundColor: '#ffffff',  
    width: '10%', 
    height: 'auto', 
    flex: 0,  
    padding : 0, 
    alignItems: 'center'
}

export const HAMBURGER_TOUCHABLE: ViewStyle = {
    backgroundColor: '#ffffff',  
    width: '100%', 
    height: metric.ratioHeight(50), 
    flex: 0,
    alignItems: 'flex-end', 
    // justifyContent: 'flex-end'
}

export const HAMBURGER_IMAGE: ImageStyle = {
    backgroundColor: '#ffffff',  
    // width: 10, 
    // height: 20, 
    width: metric.ratioHeight(10), 
    height: metric.ratioHeight(25), 
    padding : 0
}

export const TEXT_MESSAGE: TextStyle = {
    ...TEXT,
    color : 'black', 
}

export const BLANK_MESSAGE: TextStyle = {
    ...TEXT,
    color : color.palette.lightGrey, 
}


export const TEXT_SOS: TextStyle = {
    ...TEXT,
    color : '#C8102E',
    fontWeight: 'bold'
}

//******************* Status Container *******************
export const STATUS_CONTAINER: ViewStyle = {
    backgroundColor: color.whiteGRV,  
    flexDirection: 'row', 
    height: metric.ratioHeight(40), 
    padding : 0,  
    justifyContent: 'center'
}

export const STATUS_BLANK_CONTAINER: ViewStyle = {
    backgroundColor: '#ffffff',  
    width: '5%', 
    flex: 0,  
    justifyContent: 'center'
}

export const DATE_CELL: ViewStyle = {
    backgroundColor: color.whiteGRV,  
    width: '47%', 
    flex: 0,  
    justifyContent: 'center',
}

export const STATUS_CELL: ViewStyle = {
    backgroundColor: '#ffffff',  
    width: '48%', 
    flex: 0,  
    flexDirection: 'row', 
    alignItems: 'flex-end', 
    justifyContent: 'flex-end'
}

export const SENT_CONTAINER: ViewStyle = {
    backgroundColor: '#B1B1B1',  
    width: 'auto', 
    flexDirection: 'row', 
    borderRadius: 5, 
    paddingHorizontal: metric.ratioWidth(4),
    paddingVertical: metric.ratioHeight(4),
    alignItems: 'center' 
}

export const PENDING_CONTAINER: ViewStyle = {
    backgroundColor: '#597E50',  
    width: 'auto',  
    flexDirection: 'row', 
    borderRadius: 5, 
    paddingHorizontal: metric.ratioWidth(4),
    paddingVertical: metric.ratioHeight(4),  
    alignItems: 'center' 
}

export const DATE_VIEW: ViewStyle = {
    width: '100%',
    backgroundColor : color.whiteGRV,
    justifyContent: 'center',
    marginTop: metric.ratioHeight(12)
}

export const DATE_TEXT: TextStyle = {
    ...TEXT,
    color: color.palette.lightGrey,
    fontSize: RFValue(14)
}

export const SENT_TOUCHABLE: ViewStyle = {
    flexDirection: 'row',
    backgroundColor: '#597E50',
    justifyContent: 'center',
    alignItems: 'center',
    paddingRight : 5,
    height : '100%',
    width : '100%',
    borderRadius: 5
}

export const PENDING_TOUCHABLE: ViewStyle = {
    flexDirection: 'row',
    backgroundColor: color.textMessage,
    justifyContent: 'center',
    alignItems: 'center',
    height : '100%',
    width : '100%',
    borderRadius: 5,

}

export const ICON_STYLE: ImageStyle = {
    width: metric.ratioWidth(24),
    height: metric.ratioHeight(24)
}

export const STATUS_BUTTON: TextStyle = {
    ...TEXT,
    fontSize: RFValue(14),
    color: color.palette.white,
    marginHorizontal: metric.ratioWidth(4)
}
