import React, { useEffect, useState } from "react"
import { observer } from "mobx-react-lite"
import { useConfigurate } from "../../custom-hooks/use-configure-controller";
import { SafeAreaView, Text, View, Image, TouchableOpacity, Keyboard, Alert, TextInput } from "react-native"
import * as Styles from "./search.styles"
import { SearchProps } from "./search.props";
import SearchController from "./search.controller";
// import { TextInput } from "react-native-gesture-handler"
import { images } from "../../theme/images"
import { FlatList } from "react-native-gesture-handler";
import { AudioBook } from "../../components/audio-book/audio-book"
import { useIsFocused } from "@react-navigation/native";
import * as metric from "../../theme"

export const Search: React.FunctionComponent<SearchProps> = observer((props) => {

    const controller = useConfigurate(SearchController, props) as SearchController
    const isDisabled = SearchController.viewModel?.getIsDisabled
    const title = SearchController.resourcesViewModel?.getResourceSearchResults()

    const [searchStr, setSearchStr] = useState(SearchController.viewModel?.getStringToSearch)

    useEffect(() => {
        console.log('useEffect search');
        
        controller.onFetchSearchList()

    }, [SearchController.rootStore?.getAudioBooksStore?.getAudiobooksList])

    useEffect(() => {
        setSearchStr(SearchController.viewModel?.getStringToSearch)
    }, [SearchController.viewModel?.getStringToSearch, useIsFocused()])

    const onChangeText = (text) => {
        setSearchStr(text)
        // controller.onSearchContentChangeText(text)
    }

    const renderSearchView = () => {
        return (

            <View shouldRasterizeIOS
                style={Styles.SEARCH_VIEW}
            >

                <TouchableOpacity
                    style={Styles.SEARCH_TOUCHABLE}
                    onPress={() => { controller.onSearchAction(searchStr) }}
                    activeOpacity={1}
                >
                    {/* <Image source={images.search} resizeMode={'contain'} */}
                        <Image source={images.searchIcon} resizeMode={'contain'}
                            style={Styles.SEARCH_IMAGE}
                        />

                </TouchableOpacity>

                <TextInput
                    style={Styles.SEARCH_TEXT_VIEW}
                    // onChangeText={controller.onSearchContentChangeText}
                    autoCapitalize='none'
                    onChangeText={onChangeText}
                    // value={SearchController.viewModel?.getStringToSearch}
                    value={searchStr}
                    placeholder={SearchController.resourcesViewModel?.getResourceSearchPlaceHolder()}
                    placeholderTextColor="#ACACAC"
                    onSubmitEditing={() => { controller.onSearchAction(searchStr) }}
                >
                </TextInput>
            </View>
        )
    }

    const renderSearchCountView = () => {
        return (
            <View shouldRasterizeIOS style={Styles.SEARCH_COUNT_MAIN_VIEW}>
                <View style={Styles.SEARCH_COUNT_RED_VIEW} />

                <Text style={Styles.SEARCH_COUNT_RESULT_VIEW} >
                    {metric.isMicrolearn() ? title : title?.toUpperCase()}
                </Text>

                <Text style={Styles.SEARCH_COUNT_COUNT_VIEW} >
                    {(SearchController.viewModel?.getSearchResultsCount)}
            &nbsp;
            {SearchController.resourcesViewModel?.getResourceSearchItems()?.toLowerCase()}
                </Text>
            </View>
        )
    }

    const renderSearchListView = () => {

        return (
            <View shouldRasterizeIOS style={Styles.SEARCH_FRAME}>
                <FlatList
                    keyExtractor={(item, index) => item.uid + item.download_status}
                    data={SearchController.viewModel?.getSearchList}
                    // extraData={SearchController.viewModel?.getSearchList.slice()}
                    style={Styles.SEARCH_FLAT_LIST}
                    contentContainerStyle={{paddingBottom: metric.ratioHeight(50)}}
                    renderItem={({ item, index }) => {
                        return (<AudioBook
                            key={index}
                            title={item.name}
                            author={item.forfatter}
                            bookCover={item.image.url}
                            downloadStatus={item.download_status}
                            isMyLibrary={false}
                            disabled={isDisabled}
                            onPressPlayBackButton={() => controller.onClickAudiobookPlayBack(item)}
                            onPressBookCover={() => controller.onClickAudiobook(item)}
                            onPressBookName={() => controller.onClickAudiobook(item)}
                            onPressInfoIcon={() => controller.onClickAudiobook(item)}
                            onPressAuthorName={() => controller.onClickAudiobook(item)}
                            onPressDownload={() => controller.onClickAudiobookDownload(item)}
                            isDownloading={item.is_downloading}
                        />)
                    }
                    }
                    ItemSeparatorComponent={({ highlighted }) => (
                        <View shouldRasterizeIOS style={{ height: 30 }} />
                    )}
                    scrollEnabled={true}
                />
            </View>
        )
    }

    const renderNoSearchView = () => {
        return (
            <View shouldRasterizeIOS
                style={Styles.EMPTY_SEARCH_MAIN_VIEW}>
                <TouchableOpacity
                    onPress={() => { Keyboard.dismiss() }}
                    activeOpacity={1}
                >

                    <View shouldRasterizeIOS
                        style={Styles.EMPTY_SEARCH_VIEW}
                    >

                        <Image source={images.search} resizeMode={'contain'} style={Styles.EMPTY_SEARCH_IMAGE_VIEW} />
                        <Text style={Styles.EMPTY_SEARCH_TEXT_VIEW} >
                            {metric.isMicrolearn() ? SearchController.resourcesViewModel?.getResourceEnterSearchWord() : SearchController.resourcesViewModel?.getResourceEnterSearchWord().toUpperCase()}
                        </Text>
                    </View>

                </TouchableOpacity>

            </View>
        )
    }

    return (
        <SafeAreaView style={Styles.SAFE_CONTAINER}>
            <View shouldRasterizeIOS
                style={Styles.MAIN_CONTAINER}>
                {renderSearchView()}


                {(SearchController.viewModel?.getEmptySearch) ?
                    null
                    : (renderSearchCountView())
                }

                {(SearchController.viewModel?.getEmptySearch) ?
                    <View style={{ flex: 1, justifyContent: 'center' }}>{
                        renderNoSearchView()
                    }
                    </View>
                    : (renderSearchListView())
                }

            </View>

        </SafeAreaView>

    )
})