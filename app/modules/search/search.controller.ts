import { DrawerActions } from "@react-navigation/core";
import { onSnapshot } from "mobx-state-tree";
import { Alert, Keyboard, PermissionsAndroid, Platform } from "react-native";
import { DownloadStatus, NavigationKey } from "../../constants/app.constant";
import { INavigationRoute } from "../../models";
import { RootStore } from "../../models/root-store/root.types";
import { BaseController } from "../base.controller";
import { SearchProps } from "./search.props";
import { SearchResourcesStore, SearchResourcesStoreModel, SearchStore, SearchStoreModel } from "./storemodels";
import { CommonActions } from "@react-navigation/core"
import FirebaseFireStore from '../../utils/firebase/fire-store/fire-store'
import { DOWNLOADEDBOOK } from "../../models/downloaded-store/downloaded.types";
import FirebaseAnalytics from "../../utils/firebase/analytics/analytics";

class SearchController extends BaseController {
    /*
       Stateful Variable
   */
    public static viewModel: SearchStore
    public static resourcesViewModel: SearchResourcesStore

    public static category_name: string
    public static isGoToAudiobookDetail: boolean
    /*
        Mark Constructor
    */

    constructor(rootStore?: RootStore, props?: SearchProps & Partial<INavigationRoute>) {
        super(rootStore, props)
        if (this._myProps?.route?.params?.isInitialized) return
        this._setupResourcesViewModel()
        this._setupNavigation()
        this._setupProps(props)
        this._setupViewModel()
        this._setupSnapShot()
    }

    /*
       Mark Setup
    */

    private _setupResourcesViewModel = () => {
        SearchController.resourcesViewModel = SearchResourcesStoreModel.create({ ...this._rootStore?.getAllGeneralResourcesStore })
    }
    private _setupViewModel = () => {
        const localStore = this._rootStore?.getModuleStore(NavigationKey.SearchAudioBook)
        SearchController.viewModel = localStore && SearchStoreModel.create({ ...localStore }) || SearchStoreModel.create({})
        this._setInitializedToPropsParams()
    }

    private _setupSnapShot = () => {
        onSnapshot(SearchController.viewModel, (snap: SearchStore) => {
            // console.log(snap)
            this._rootStore?.setModuleStoreValue(NavigationKey.SearchAudioBook, snap)
        })
    }

    public onSideMenuPress = () => {
        this._myProps?.navigation?.dispatch(DrawerActions.openDrawer() as any)
    }

    public onNotificationPress = () => {
        this.gotoNotificationList();
    }


    public onSearchContentChangeText = (strSearch: string): void => {
        SearchController.viewModel?.setStringToSearch(strSearch)
    }

    public onClickAudiobook = (book) => {
        this.gotoAudiobook(book)
    }

    public updateDownloading = (searchList, audiobooksList) => {
        // const searchListData = [...searchList]

        // searchListData.map(item1 => {
        //     audiobooksList.map(item2 => {
        //         // if (item1.uid === item2.uid) {
        //         //     if (item2.is_downloading) {
        //         //         item1.is_downloading = item2.is_downloading
        //         //     }
        //         // } else {
        //             item1.download_status = item2.download_status
        //             item1.is_downloading = item2.is_downloading
        //         // }
        //     })
        //     return item1
        // })

        // return searchListData

        return searchList.map(item1 => {
            audiobooksList.map(item2 => {
                if (item1.uid === item2.uid) {
                    if (item2.is_downloading) {
                        item1.is_downloading = item2.is_downloading
                    } else {
                        item1.download_status = item2.download_status
                        item1.is_downloading = item2.is_downloading
                    }
                }
            })
            return item1
        })
    }


    public onUpdateStatusDownloading = (book) => {
        const audioBooks = SearchController.rootStore?.getAudioBooksStore.getAudiobooksList

        let data = []
        audioBooks.map(item => {
            if (item.uid === book.uid) {
                data.push({
                    ...item,
                    is_downloading: true
                })
            } else {
                data.push({
                    ...item
                })
            }
        })

        SearchController.rootStore?.getAudioBooksStore.setAudiobooksList(data)
        const result = this.updateDownloading(SearchController.viewModel?.getSearchList, SearchController.rootStore?.getAudioBooksStore?.getAudiobooksList)
        SearchController.viewModel?.setSearchList(result)
        this.downloadProgress(book)
    }

    public onUpdateDownloadFailed = (book) => {
        const searchList = SearchController.viewModel?.getSearchList
        let data = []
        searchList.map(item => {
            if (item.uid === book.uid) {
                data.push({
                    ...item,
                    is_downloading: false
                })
            }
            else {
                data.push({
                    ...item
                })
            }
        })

        SearchController.viewModel?.setSearchList(data)
    }

    public downloadProgress = async (book) => {
        const result = await SearchController.viewModel?.downloadBook(this._rootStore, book)
        if (result.download_status === DownloadStatus.SUCCESS) {
            this.onDownloaded(book, result)
        } else if (result.download_status === DownloadStatus.FAILED) {
            Alert.alert(SearchController.resourcesViewModel?.getResourceNoInternetUnableToDownload())
            this.onUpdateDownloadFailed(book)
        }
        this._delayExecutor(() => SearchController.viewModel?.setIsDisabled(false), 1200)
    }

    public onClickAudiobookDownload = async (book) => {

        if (Platform.OS === 'ios') {
            this.onUpdateStatusDownloading(book)
            // this.downloadProgress(book)
        } else {
            const result = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE)

            if (result === PermissionsAndroid.RESULTS.GRANTED) {
                this.onUpdateStatusDownloading(book)
                // this.downloadProgress(book)
            } else {
                Alert.alert('The permission is denied but requestable')
            }
        }
    }

    public onDownloaded = async (book, result) => {
        const params = {
            deviceId: SearchController.rootStore?.getDeviceId,
            file_path: result.file_path,
            download_status: result.download_status,
            job_id: result.job_id,
            audiobook_id: book.uid,
            category_id: book.category_id,
            description: book.description,
            forfatter: book.forfatter,
            image: {
                file_name: book.image.file_name,
                storage_path: book.image.storage_path,
                thumbnail_url: book.image.thumbnail_url,
                url: book.image.url
            },
            name: book.name,
            organization_id: book.organization_id,
            video: {
                name: book.video?.name,
                url: book.video?.url,
                download_url: book.video?.download_url,
                streaming_url: book.video?.streaming_url
            }
        }
        const resultSuccess = await FirebaseFireStore.updateDownloadedAudioBook(params)
        await this.onFetchDownloaded()
        await this.onFetchAudioBookDownloaded()
    }

    public onFetchDownloaded = async () => {
        const organizationId = SearchController.rootStore?.getOrganizationId
        const searchList = SearchController.viewModel?.getSearchList
        
        const downloaded = await FirebaseFireStore.fetchMyDownloaded(SearchController.rootStore?.getDeviceId, organizationId)
        SearchController.rootStore?.getDownloadedStore?.setDownloadedList(downloaded)

        searchList.map((item1: any) => {
            downloaded.map(item2 => {
                if (item1.uid === item2.uid) {
                    item1.download_status = DownloadStatus.SUCCESS
                    item1.file_path = item2.file_path
                }
            })
            return item1
        })
        if (searchList !== []) {
            SearchController.viewModel?.setEmptySearch(false)
        } else {
            SearchController.viewModel?.setEmptySearch(true)
        }

        SearchController.viewModel?.setSearchList(searchList)
    }

    public onFetchAudioBookDownloaded = async () => {
        const organizationId = SearchController.rootStore?.getOrganizationId
        const audiobooksList = SearchController.rootStore?.getAudioBooksStore?.getAudiobooksList
        const downloaded = await FirebaseFireStore.fetchMyDownloaded(SearchController.rootStore?.getDeviceId, organizationId)
        SearchController.rootStore?.getDownloadedStore?.setDownloadedList(downloaded)

        audiobooksList.map(item1 => {
            downloaded.map(item2 => {
                if (item1.uid === item2.uid) {
                    item1.download_status = DownloadStatus.SUCCESS
                }
            })
            return item1
        })

        SearchController.rootStore?.getAudioBooksStore?.setAudiobooksList(audiobooksList)
    }

    public queryCategoryName = (book) => {
        const categoryStore = SearchController.rootStore?.getAudioBookCategoriesStore?.getAudiobooksCetegories

        categoryStore.map(item => {
            if (item.uid === book.category_id) {
                SearchController.category_name = item.name
            }
        })
    }

    public onClickAudiobookPlayBack = (book) => {
        book = { ...book }
        if (((book as DOWNLOADEDBOOK).download_status == DownloadStatus.DO_NOT_DOWNLOAD) && (this._rootStore?.getSharedStore?.getIsConnected == false)) {
            Alert.alert(SearchController.resourcesViewModel?.getResourceNoInternetUnableToPlay())
            return
        }


        this.queryCategoryName(book)
        const audioBooks: Partial<DOWNLOADEDBOOK>[] = SearchController.rootStore?.getDownloadedStore?.getDownloadedList?.slice()
        let audioData = audioBooks.filter(data => {
            return book?.uid === data?.uid
        })

        audioData = audioData?.slice()
        // this._rootStore?.getPlaybackStore?.play((audioData?.length > 0 ? audioData[0] : book[0]) as DOWNLOADEDBOOK)
        this._rootStore?.resetPlaybackStore()
        setTimeout(() => {
            this._rootStore?.getPlaybackStore?.play((audioData?.length > 0 ? audioData[0] : book) as DOWNLOADEDBOOK)
        }, 300)

        SearchController.viewModel?.setIsDisabled(true)

        // const navigate = this._myProps?.navigation
        // navigate.dispatch(StackActions.push(NavigationKey.PlayBackScreen) as any)
        this._delayExecutor(() => SearchController.viewModel?.setIsDisabled(false), 1200)

        //firebbase analytic
        FirebaseAnalytics.audiobookInitialPlayed({
            book_name: book.name,
            category_name: SearchController.category_name,
            organization_id: this._rootStore?.getOrganizationId
        })

        FirebaseAnalytics.amountAudiobookPlayedPerUser({
            user_number: this._rootStore?.getUserStore?.getUserId,
            book_name: book.name,
            category_name: SearchController.category_name,
            organization_id: this._rootStore?.getOrganizationId
        })

    }

    public onSearchAction = (searchStr: string): void => {
        if (this._isConnected()) {
            this.onSearchText(searchStr)
            this.fectAudioBookList()
        } else {
            Alert.alert(SearchController.resourcesViewModel?.getResourceSearchNoInternet())
        }
    }

    private fectAudioBookList = async () => {
        //fetch audiobook list
        const organizationId = SearchController.rootStore?.getOrganizationId
        const audiobooksList = await FirebaseFireStore.fetchAudiobooks(organizationId)
        this.checkDownloadingAudiobook(audiobooksList, SearchController.rootStore?.getAudioBooksStore?.getAudiobooksList)

        //fetch downloaded list
        const device_id = SearchController.rootStore?.getDeviceId
        const downloaded_audiobooks = await FirebaseFireStore.fetchMyDownloaded(device_id, organizationId)
        SearchController.rootStore?.getDownloadedStore?.setDownloadedList(downloaded_audiobooks)
    }

    public checkDownloadingAudiobook = (audiobooksList, audiobooksListStore) => {

        audiobooksList.map(item1 => {
            audiobooksListStore.map(item2 => {
                if (item1.uid === item2.uid) {
                    if (item2.is_downloading === true) {
                        item1.is_downloading = item2.is_downloading
                    }
                }
            })
            return item1
        })
        this.checkDownloadedAudioBookList(SearchController.rootStore?.getDownloadedStore?.getDownloadedList, audiobooksList)
    }

    private checkDownloadedAudioBookList = (downloadedList, audiobooks) => {
        audiobooks.map(item1 => {
            downloadedList.map(item2 => {
                if (item1.uid === item2.uid) {
                    item1.download_status = DownloadStatus.SUCCESS
                }
            })
            return item1
        })

        SearchController.rootStore?.getAudioBooksStore?.setAudiobooksList(audiobooks)
    }

    public gotoNotificationList = () => {
        const navigation = this._myProps?.navigation
        const params = {
            description: "",
        }

        this._isGlobalLoading(true)
        Keyboard.dismiss()
        navigation?.dispatch(CommonActions.navigate(NavigationKey.NotificationList, params) as any)
        this._isGlobalLoading(false)
    }

    public gotoAudiobook = (book) => {
        if (SearchController.viewModel?.getIsDisabled) return
        SearchController.viewModel?.setIsDisabled(true)

        //firebase analytic
        FirebaseAnalytics.openAudiobooksDetail({
            userId: this._rootStore?.getUserStore.getUserId,
            book_name: book.name,
            organization_id: this._rootStore?.getOrganizationId
        })

        if (book) {
            SearchController.isGoToAudiobookDetail = true
            const navigation = this._myProps?.navigation
            const params = {
                id: book.uid
            }

            this._isGlobalLoading(true)
            Keyboard.dismiss()
            navigation?.dispatch(CommonActions.navigate(NavigationKey.AudioBookScreen, params) as any)
            this._isGlobalLoading(false)
        } else {
            Alert.alert('Invalid UID')
        }
        this._delayExecutor(() => SearchController.viewModel?.setIsDisabled(false), 1200)
    }

    public onFetchSearchList = () => {

        if (this._isConnected()) {
            const audiobooksList = SearchController.rootStore?.getAudioBooksStore?.getAudiobooksList
            const searchList = SearchController.viewModel?.getSearchList

            if (typeof searchList.map !== 'undefined') {
                audiobooksList.map(item1 => {
                    searchList.map((item2: any) => {
                        if (item1.is_downloading) {
                            if (item1.uid === item2.uid) {
                                item2.is_downloading = item1.is_downloading
                            }
                        } else {
                            if (item1.uid === item2.uid) {
                                item2.download_status = item1.download_status
                            }
                        }
                        return item2
                    })
                })
            } else {
                if (audiobooksList != null) {
                    audiobooksList.map((item1: any) => {
                        searchList.push(item1)
                    })
                }
            }

            SearchController.viewModel?.setSearchList(searchList)
        }
    }

    private onSearchText = async (searchStr: string) => {
        this._isGlobalLoading(true)
        Keyboard.dismiss()
        // let strSearch = SearchController.viewModel?.getStringToSearch
        let strSearch = searchStr
        const result = await SearchController.viewModel?.fetchSearchAudioBook(this._rootStore, strSearch)

        const audiobooks = SearchController.rootStore?.getAudioBooksStore?.getAudiobooksList

        result.map(item1 => {
            audiobooks.map(item2 => {
                if (item1.uid === item2.uid) {
                    item1.download_status = item2.download_status
                    item1.is_downloading = item2.is_downloading
                }
            })
            return item1
        })

        if (result) {
            let list = result

            if (SearchController.viewModel?.getStringToSearch === "") {
                list.sort((firstBook, secondBook) => firstBook.name < secondBook.name ? -1 : 1)
            }

            if (list !== []) {
                SearchController.viewModel?.setEmptySearch(false)
            } else {
                SearchController.viewModel?.setEmptySearch(true)
            }

            SearchController.viewModel?.setSearchList(list)
        }
        this._isGlobalLoading(false)
        this.onSearchContentChangeText(searchStr)
    }

    public checkDownloadedAudiobook = () => {
        const audiobook = SearchController.rootStore?.getAudioBooksStore?.getAudiobooksList
        const searchList = SearchController.viewModel?.getSearchList
        audiobook.map(item1 => {
            searchList.map((item2: any) => {
                if (item1.uid === item2.uid) {
                    if (item1.download_status === DownloadStatus.DO_NOT_DOWNLOAD) {
                        item2.download_status = DownloadStatus.DO_NOT_DOWNLOAD
                        item2.is_downloading = item1.is_downloading
                    }
                }
                return item2
            })
        })
        SearchController.viewModel?.setSearchList(searchList)

    }

    viewDidAppearAfterFocus = async () => {
        if (SearchController.isGoToAudiobookDetail) {
            SearchController.isGoToAudiobookDetail = false
            const result = this.updateDownloading(SearchController.viewModel?.getSearchList, SearchController.rootStore?.getAudioBooksStore.getAudiobooksList)
            SearchController.viewModel?.setSearchList(result)
        } else {
            this.checkDownloadedAudiobook()
        }

    }

}

export default SearchController
