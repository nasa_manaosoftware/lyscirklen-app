import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"
// import { SearchListType } from '../../constants/app.constant'

export interface SearchProps extends NavigationContainerProps<ParamListBase> {
    // type: SearchListType,
    name?: any,
}