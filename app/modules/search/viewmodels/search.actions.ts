import { types, unprotect } from "mobx-state-tree"
import { SearchPropsModel } from "./search.models"
import { SearchStoreModel } from "../../../models/search-store"

export const SearchActions = types.model(SearchPropsModel).actions(self => {
    const setEmptySearch = (value: boolean) => {
        self.emptySearch = value
    }

    const setStringToSearch = (value: string) => {
        self.stringToSearch = value
    }

    const clearSearchResult = () => {
        self.searchResult$.clear()
    }

    const setSearchResult = (list) => {
        self.searchResult$.push(list)
    }

    const setSearchResultAll = (list) => {
        console.log(list);

        list.forEach(e => {
            const childViewModel = SearchStoreModel.create(e)
            // unprotect(childViewModel)
            self.searchResult$.push(childViewModel)
        })
    }

    const setSearchList = (value) => {
        self.searchList = [].concat.apply(value)
    }

    const setIsDisabled = (value: boolean) => {
        self.isDisabled = value
    }

    return {
        setEmptySearch,
        setSearchResult,
        clearSearchResult,
        setSearchResultAll,
        setStringToSearch,
        setSearchList,
        setIsDisabled
    }
})
