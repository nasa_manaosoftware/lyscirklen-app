import { types } from "mobx-state-tree"
import { SearchPropsModel } from "./search.models"
import { GeneralResources } from "../../../constants/firebase/remote-config"
import { NavigationKey } from "../../../constants/app.constant"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store"

export const SearchViews = types.model(SearchPropsModel).views(self => ({
    get getEmptySearch() {
        return self.emptySearch
    },

    get getSearchResultsCount() {
        return self.searchList.length
    },
 
    get getSearchResults() {
        return self.searchResult$
    },

    get getStringToSearch() {
        return self.stringToSearch
    },
    get getSearchList() {
        return self.searchList
    },
    get getIsDisabled(): boolean {
        return self.isDisabled
    }
}))

export const SearchResourcesViews = GeneralResourcesStoreModel.views(self => {

        const { SearchAudioBookScreen } = GeneralResources
        const { enter_search_word, 
                search_results,
                search_items,
                search_place_holder,
                no_internet_unable_to_search,
                no_internet_unable_to_download,
                no_internet_unable_to_play
         } = SearchAudioBookScreen
        const { SearchAudioBook } = NavigationKey
        // Search result

        //MARK: Views
        const getResources = (locale: string, key: string, childKeyOrShareKey: string | boolean = false, isChildDotNotation: boolean = false) => self.getValues(locale, childKeyOrShareKey ? key : SearchAudioBook, childKeyOrShareKey ? true : key, isChildDotNotation)
        const getResourceEnterSearchWord = (locale: string) => getResources(locale, enter_search_word)
        const getResourceSearchResults = (locale: string) => getResources(locale, search_results)
        const getResourceSearchItems = (locale: string) => getResources(locale, search_items)
        const getResourceSearchPlaceHolder = (locale: string) => getResources(locale, search_place_holder)
        const getResourceSearchNoInternet = (locale: string) => getResources(locale, no_internet_unable_to_search)
        const getResourceNoInternetUnableToDownload = (locale: string) => getResources(locale, no_internet_unable_to_download)
        const getResourceNoInternetUnableToPlay = (locale: string) => getResources(locale, no_internet_unable_to_play, true)

        return {
            getResourceEnterSearchWord,
            getResourceSearchResults,
            getResourceSearchItems,
            getResourceSearchPlaceHolder,
            getResourceSearchNoInternet,
            getResourceNoInternetUnableToDownload,
            getResourceNoInternetUnableToPlay
        }
    })