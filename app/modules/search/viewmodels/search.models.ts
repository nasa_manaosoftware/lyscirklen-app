import { types } from "mobx-state-tree"
import { AUDIOBOOK } from "../../../models/audiobook-store/audiobook.types"
import { SearchStoreModel } from "../../../models/search-store/search-store.store"


export const SearchPropsModel = {
    emptySearch: types.optional(types.boolean,true),
    stringToSearch: types.optional(types.string,''),
    searchResult$: types.optional(types.array(SearchStoreModel), []),
    searchList:  types.optional(types.frozen<(Partial<AUDIOBOOK>)[]>(), []),
    isDisabled: types.optional(types.boolean, false)

} 