import { Instance, SnapshotOut } from "mobx-state-tree"
import { SearchResourcesStoreModel, SearchStoreModel } from "./search.store"

export type SearchStore = Instance<typeof SearchStoreModel>
export type SearchStoreSnapshot = SnapshotOut<typeof SearchStoreModel>

export type SearchResourcesStore = Instance<typeof SearchResourcesStoreModel>
export type SearchResourcesStoreSnapshot = SnapshotOut<typeof SearchResourcesStoreModel>