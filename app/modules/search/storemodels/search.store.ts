import { types } from "mobx-state-tree"
import { StoreName } from "../../../constants/app.constant"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store/general-resources.store"
import { SearchViews, SearchActions, SearchPropsModel, SearchResourcesViews } from "../viewmodels"
import { SearchServiceActions } from "../services/search.services"
import { DownloadedStoreModel } from "../../../models/downloaded-store/downloaded.store"
import { AudiobookStoreModel } from "../../../models/audiobook-store/audiobook.store"

const SearchModel = types.model(StoreName.Search, SearchPropsModel)

export const SearchStoreModel = types.compose(
    SearchModel,
    SearchViews,
    SearchActions,
    SearchServiceActions,
    DownloadedStoreModel,
    AudiobookStoreModel
)

export const SearchResourcesStoreModel = types.compose(GeneralResourcesStoreModel, SearchResourcesViews)

