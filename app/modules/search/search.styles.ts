import { ViewStyle, TextStyle, ImageStyle, Platform } from "react-native"
import { HeaderProps } from "../../components/header/header.props";
import { images } from "../../theme/images";
import * as metric from "../../theme"
import { RFValue } from "react-native-responsive-fontsize"
import { color, maxDescriptionChar } from "../../theme"
import { type } from "../../theme/fonts";

export const TITLE: TextStyle = {
    fontFamily: type.base,
    lineHeight: metric.ratioHeight(26),
    fontSize: RFValue(18),
    color: color.palette.white
}

export const TEXT_LARGE: TextStyle = {
    fontFamily: type.base,
    lineHeight: metric.ratioHeight(26),
    fontSize: RFValue(18),
    color: color.palette.white
}

export const TEXT_MEDIUM: TextStyle = {
    fontFamily: type.base,
    lineHeight: metric.ratioHeight(26),
    fontSize: RFValue(18),
    color: color.palette.white
}

export const TEXT_SMALL: TextStyle = {
    fontFamily: type.base,
    lineHeight: metric.ratioHeight(20),
    fontSize: RFValue(16),
    color: color.palette.white
}

export const MENU_ICON_STYLE: ImageStyle = {
    width: metric.ratioWidth(24),
    height: metric.ratioHeight(24),
    // backgroundColor: 'red'
}

/*****************************************/
export const SAFE_CONTAINER: ViewStyle = {
    backgroundColor: color.background,
    flex: 1,

}

export const MAIN_CONTAINER: ViewStyle = {
    backgroundColor: color.background,
    flex : 1,  
    alignItems: 'center', 
    marginHorizontal:  metric.ratioWidth(20),
    marginTop : metric.ratioHeight(20)
}

export const HEADER_PROPS: HeaderProps = {
    leftIconSource: images.hamMenu,
    leftIconStyle: MENU_ICON_STYLE,
    rightIconStyle: MENU_ICON_STYLE,
    rightIconSource: images.bell,
    isLeftIconAnimated: false,
    isRightIconAnimated: false
}

export const SEARCH_VIEW: ViewStyle = {
    // backgroundColor : color.backgroundActive, 
    backgroundColor : color.inputBackground,
    width : '100%',  
    flexDirection: 'row', 
    borderRadius : 10,
}

export const SEARCH_TOUCHABLE: ViewStyle = {
    flexDirection:"row",
    alignItems:'center',
    justifyContent:'center',
    paddingLeft: metric.ratioWidth(15)
}

export const SEARCH_IMAGE: ImageStyle = {
    width: metric.ratioWidth(22),
    height: metric.ratioHeight(22),
    marginTop: metric.isTablet() ? metric.ratioHeight(-1) : metric.ratioHeight(-3)
}

export const SEARCH_BUTTON_VIEW: ImageStyle = {
    // height : '80%', 
    // width : '100%', 
}


export const SEARCH_TEXT_VIEW: TextStyle = {
     fontFamily: type.base,
     fontSize: RFValue(18),
     color: color.inputText,
     paddingLeft: metric.ratioWidth(18),
     marginTop: metric.isTablet() ? metric.ratioHeight(-1) : metric.ratioHeight(-3),
     width: '100%',
     paddingVertical: Platform.OS === 'ios' ? metric.ratioHeight(10) : metric.ratioHeight(10)
}


/*****************************************/
export const EMPTY_SEARCH_MAIN_VIEW: ViewStyle  = {
    alignItems: 'center', 
    justifyContent: 'center', 
    flexDirection: 'row'
}

export const EMPTY_SEARCH_VIEW: ViewStyle  = {
    alignItems: 'center', 
    flexDirection: 'column'
}

export const EMPTY_SEARCH_TEXT_VIEW: TextStyle   = {
    marginTop : metric.ratioHeight(24),
    fontFamily: type.title,
    fontSize: RFValue(22),
    color: color.palette.white,
    textAlign: 'center'
}

export const EMPTY_SEARCH_IMAGE_VIEW: ImageStyle   = {
    height: metric.ratioHeight(54), 
    width: metric.ratioWidth(54)
}
/*****************************************/
export const SEARCH_COUNT_MAIN_VIEW: ViewStyle  = {
    width : '100%',  
    alignItems: 'center', 
    justifyContent: 'center', 
    flexDirection: 'row',
    marginTop: metric.ratioHeight(22),
    marginBottom: metric.ratioHeight(18)
}

export const SEARCH_COUNT_RED_VIEW: ViewStyle = {
    backgroundColor : color.redLine, 
    width : metric.ratioWidth(2),  
    height : metric.isTablet() ? metric.ratioHeight(23) : metric.ratioHeight(18)
}

export const SEARCH_COUNT_RESULT_VIEW: TextStyle  = {
    marginLeft : metric.ratioWidth(11), 
    fontFamily: type.title,
    fontSize: RFValue(22),
    color: color.palette.white,  
    marginTop: metric.isMicrolearn() ? null : metric.ratioHeight(5),
    marginBottom: metric.isMicrolearn() ? metric.ratioHeight(2) : null
}

export const SEARCH_COUNT_COUNT_VIEW: ViewStyle & TextStyle   = {
    flex : 1,  
    textAlign: 'right', 
    fontFamily: type.base,
    fontSize: RFValue(16),
    color: color.palette.white, 

}

/*****************************************/

export const SEARCH_FRAME: ViewStyle    = {
    flex: 1,
    alignItems: 'center', 
    justifyContent: 'center', 
    flexDirection: 'row', 
    // paddingBottom: metric.ratioHeight(20)
}

export const SEARCH_FLAT_LIST: ViewStyle   = {
    flex: 1, 
    width : '100%',  
    height : '100%', 
    marginHorizontal : metric.ratioWidth(5),
    paddingHorizontal : metric.ratioWidth(5), 
    paddingBottom: metric.ratioHeight(20)
    
}