import { strict } from "assert"
import { types } from "mobx-state-tree"
import { MessagesStoreModel } from "../../../models/messages-store/messages-store.store"


export const MessagesPropsModel = {
    messagesResult$: types.optional(types.array(MessagesStoreModel), []),
    messagesRead$: types.optional(types.array(MessagesStoreModel), []),
    isDisabled: types.optional(types.boolean, false),
    isRefreshing: types.optional(types.boolean, false)
} 