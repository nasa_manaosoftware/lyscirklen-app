import { types, unprotect } from "mobx-state-tree"
import { MessagesPropsModel } from "./messages.models"
import { IMessages, MessagesStoreModel } from "../../../models/messages-store"
import { RootStore } from "../../../models/root-store/root.types"

export const MessagesActions = types.model(MessagesPropsModel).actions(self => {

    const setMessagesResult = (list: IMessages[]) => {
        self.messagesResult$.clear()
        list.forEach(e => {
            const childViewModel = MessagesStoreModel.create(e)
            unprotect(childViewModel)
            self.messagesResult$.push(childViewModel)
        })
    }

    const setMessagesRead = (list: IMessages[]) => {
        self.messagesRead$.clear()
        list.forEach(e => {
            const childViewModel = MessagesStoreModel.create(e)
            unprotect(childViewModel)
            self.messagesRead$.push(childViewModel)
        })
    }

    const setIsDisabled = (value: boolean) => {
        self.isDisabled = value
    }

    const setIsRefreshing = (value: boolean) => {
        self.isRefreshing = value
    }

    const findExistingItemIndex = (notification): number => {
        const tempChatList = self.messagesRead$
        const existingList = tempChatList?.slice() //Slice to disobserve property
        const foundItemIndex = existingList?.findIndex(v => v.message_id === notification.message_id)
        return foundItemIndex
    }
    const addNotificationIntoList = (notification) => {
        const foundItemIndex = findExistingItemIndex(notification)
        if (foundItemIndex !== -1) return
        const tempList = self.messagesRead$
        const childViewModel = MessagesStoreModel.create(notification)
        unprotect(childViewModel)
        self.messagesRead$ = [].concat.apply(childViewModel, tempList)
    }

    const updateNotificationIntoList = (notification) => {
        const foundItemIndex = findExistingItemIndex(notification)
        if (foundItemIndex === -1) return
        const childViewModel = MessagesStoreModel.create(notification)
        console.log('childViewModel === .', childViewModel);
        
        unprotect(childViewModel)
        self.messagesRead$[foundItemIndex] = childViewModel
    }

    return {
        setMessagesResult,
        setMessagesRead,
        setIsDisabled,
        setIsRefreshing,
        addNotificationIntoList,
        updateNotificationIntoList
    }
})
