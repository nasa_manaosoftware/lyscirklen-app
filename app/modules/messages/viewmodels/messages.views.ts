import { types } from "mobx-state-tree"
import { MessagesPropsModel } from "./messages.models"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store"
import { GeneralResources } from "../../../constants/firebase/remote-config/remote-config.constant"
import { NavigationKey } from "../../../constants/app.constant"

export const MessagesViews = types.model(MessagesPropsModel).views(self => ({
 
    get getMessageResults() {
        return self.messagesResult$
    },

    get getMessageRead() {
        return self.messagesRead$
    },

    get getIsPullRefreshing(): boolean {
        return self.isRefreshing
    },

    get getIsDisabled() {
        return self.isDisabled
    },

    // get getIsDisabled() {
    //     return self.isDisabled
    // },

}))

export const MessagesResourcesViews = GeneralResourcesStoreModel.views(self => {
        // //MARK: Volatile State
    const { Messages } = GeneralResources

    const { noNotificationTitle, noNotificationSubTitle } = Messages

    // //MARK: Views
    const getResources = (locale: string, key: string, childKeyOrShareKey: string | boolean = false, isChildDotNotation: boolean = false) => self.getValues(locale, childKeyOrShareKey ? key : NavigationKey.Messages , childKeyOrShareKey ? true : key, isChildDotNotation)
    
    const getResourcesNoNotificationTitle = (locale: string) => getResources(locale, noNotificationTitle, true)
    const getResourcesNoNotificationSubTitle = (locale: string) => getResources(locale, noNotificationSubTitle, true)

    return {
        getResourcesNoNotificationTitle,
        getResourcesNoNotificationSubTitle
    }
})