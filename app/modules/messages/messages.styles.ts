import { ViewStyle, TextStyle, ImageStyle } from "react-native"
import { HeaderProps } from "../../components/header/header.props";
import { images } from "../../theme/images";
import * as metric from "../../theme"
import { RFValue } from "react-native-responsive-fontsize"
import { color } from "../../theme"
import { size, type } from "../../theme/fonts";

export const TITLE: TextStyle = {
    fontFamily: type.base,
    lineHeight: metric.ratioHeight(26),
    fontSize: RFValue(18),
    color: color.palette.white
}

export const TEXT_LARGE: TextStyle = {
    fontFamily: type.base,
    lineHeight: metric.ratioHeight(26),
    fontSize: RFValue(18),
    color: color.palette.white
}

export const TEXT_MEDIUM: TextStyle = {
    fontFamily: type.base,
    lineHeight: metric.ratioHeight(26),
    fontSize: RFValue(18),
    color: color.palette.white
}

export const TEXT_SMALL: TextStyle = {
    fontFamily: type.base,
    lineHeight: metric.ratioHeight(20),
    fontSize: RFValue(16),
    color: color.palette.white
}

export const MENU_ICON_STYLE: ImageStyle = {
    width: metric.ratioWidth(24),
    height: metric.ratioHeight(24)
}

/*****************************************/
export const SAFE_CONTAINER: ViewStyle = {
    backgroundColor: color.background,
    flex: 1
}

export const MAIN_CONTAINER: ViewStyle = {
    backgroundColor: color.background,
    flex : 1,  
    alignItems: 'center', 
    marginRight: 0,
    marginLeft : 0,
    marginTop : 0,
    marginBottom: 0
}

export const HEADER_PROPS: HeaderProps = {
    leftIconSource: images.hamMenu,
    leftIconStyle: MENU_ICON_STYLE,
    rightIconStyle: MENU_ICON_STYLE,
    rightIconSource: images.bell,
    isLeftIconAnimated: false,
    isRightIconAnimated: false
}
/*****************************************/

export const MESSAGE_FRAME: ViewStyle    = {
    flex: 1,
    alignItems: 'center', 
    justifyContent: 'center', 
    flexDirection: 'row', 
    marginBottom : 0, 
    backgroundColor : color.background
}

export const MESSAGE_FLAT_LIST: ViewStyle   = {
    flex: 1, 
    width : '100%',  
    height : '100%', 
    marginLeft : 0,
    marginRight : 0,
    paddingHorizontal : 0, 
    
}
export const MESSAGE_FLAT_LIST_SEPERATOR: ViewStyle   = {
    height: 1, 
    backgroundColor: color.messageHighlight
}

export const REFRESH_CONTROL: any = { tintColor: color.palette.lighterGrey }

export const NOTIFICATION_ICON: ImageStyle = {
    width: metric.ratioWidth(43.75),
    height: metric.ratioHeight(50)
}

export const TEXT_NO_NOTIFICATION: TextStyle = {
    fontFamily: type.base,
    fontSize: size.header,
    color: color.whiteText,
    marginTop: metric.ratioHeight(20),
    textAlign: 'center'
}

export const TEXT_SUB_NO_NOTIFICATION: TextStyle = {
    fontFamily: type.base,
    fontSize: size.large,
    color: color.whiteText,
    marginTop: metric.ratioHeight(9),
    textAlign: 'center'
}

export const EMPTY_NOTIFICATION: ViewStyle = {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
}
