import { types } from "mobx-state-tree"
import { StoreName } from "../../../constants/app.constant"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store/general-resources.store"
import { MessagesViews, MessagesActions, MessagesPropsModel, MessagesResourcesViews } from "../viewmodels"
import { MessagesServiceActions } from "../services/messages.services"

const MessagesModel = types.model(StoreName.Messages, MessagesPropsModel)

export const MessagesStoreModel = types.compose(
    MessagesModel,
    MessagesViews,
    MessagesActions, MessagesServiceActions)

export const MessagesResourcesStoreModel = types.compose(GeneralResourcesStoreModel, MessagesResourcesViews)

