import { Instance, SnapshotOut } from "mobx-state-tree"
import { MessagesResourcesStoreModel, MessagesStoreModel } from "./messages.store"

export type MessagesStore = Instance<typeof MessagesStoreModel>
export type MessagesStoreSnapshot = SnapshotOut<typeof MessagesStoreModel>

export type MessagesResourcesStore = Instance<typeof MessagesResourcesStoreModel>
export type MessagesResourcesStoreSnapshot = SnapshotOut<typeof MessagesResourcesStoreModel>