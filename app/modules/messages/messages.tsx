import React, { useState } from "react"
import { observer } from "mobx-react-lite"
import { useConfigurate } from "../../custom-hooks/use-configure-controller";
import { SafeAreaView, View, RefreshControl, FlatList, TouchableHighlight, Text, Image } from "react-native"
import * as Styles from "./messages.styles"
import { MessagesProps } from "./messages.props";
import MessagesController from "./messages.controller";
import { MessagesComponents } from './views/messages-card/messages-card'
import { useIsFocused } from "@react-navigation/native";
import { color } from "../../theme/color";
import { images } from "../../theme/images";

export const Messages: React.FunctionComponent<MessagesProps> = observer((props) => {

  const controller = useConfigurate(MessagesController, props) as MessagesController
  const disabled = MessagesController.viewModel?.getIsDisabled

  const [isRefreshing, setIsRefreshing] = useState(false)
  const [, updateState] = React.useState();
  const forceUpdate = React.useCallback(() => updateState({}), []);

  const locale = MessagesController.rootStore?.getLanguage

  const onPullToRefresh = () => {
    setIsRefreshing(true)
    controller.onRefresh()

    // const newList = MessagesController.viewModel.getMessageResults.slice()
    // MessagesController.viewModel.setMessagesResult(newList)
    setIsRefreshing(false)
    forceUpdate()
  }

  useIsFocused()

  const renderMessageListView = () => {
    return (
      <View shouldRasterizeIOS style={Styles.MESSAGE_FRAME} >
        <FlatList
          data={MessagesController.viewModel?.getMessageRead.concat()}
          extraData={MessagesController.viewModel?.getMessageRead?.slice()}
          style={Styles.MESSAGE_FLAT_LIST}
          initialNumToRender={5}
          onEndReachedThreshold={1.0}

          refreshControl={
            <RefreshControl
              style={Styles.REFRESH_CONTROL}
              // refreshing={MessagesController.viewModel?.getIsPullRefreshing}
              refreshing={isRefreshing}
              onRefresh={onPullToRefresh}
            />
          }
          showsVerticalScrollIndicator={false}

          renderItem={({ item }) =>
            <TouchableHighlight
              disabled={disabled}
              activeOpacity={0.8}
              underlayColor={color.messageHighlight}
              onPress={() => {
                // MessagesController.viewModel.getMessagesPress ? null :
                controller.onMessagePress(item)
              }}
            >
              {/* // onPress={() => { MessagesController.viewModel.getMessagesPress ? null :
                  //   controller.onMessagePress( item.uid, item.button_text, item.message_id, item.heading, item.message, item.sent_at, item.read_message_status) } }
                    onPress={() => {  controller.onMessagePress( item.uid, item.button_text, item.message_id, item.heading, item.message, item.sent_at, item.read_message_status) } }
                  > */}

              <MessagesComponents
                uid={item.uid}
                button_text={item?.button_text}
                message_id={item?.message_id}
                heading={item?.heading}
                message={controller.filterOutHTMLTags(item?.message)}
                sent_at={controller.convertDate(item?.sent_at)}
                read_message_status={item?.read_message_status}
                item={item}
              />
            </TouchableHighlight>

          }
          ItemSeparatorComponent={({ highlighted }) => (
            <View shouldRasterizeIOS style={Styles.MESSAGE_FLAT_LIST_SEPERATOR} />
          )}


          ListHeaderComponent={({ highlighted }) => (
            <View shouldRasterizeIOS style={Styles.MESSAGE_FLAT_LIST_SEPERATOR} />
          )}

          ListFooterComponent={({ highlighted }) => (
            <View shouldRasterizeIOS style={Styles.MESSAGE_FLAT_LIST_SEPERATOR} />
          )}


          keyExtractor={(item, index) => index.toString()}
          scrollEnabled={true}
        />
      </View>
    )
  }

  const renderEmptyMessage = () => {
    return (
      <View style={Styles.EMPTY_NOTIFICATION}>
        <Image source={images.defaultNotification} resizeMode={'contain'} style={Styles.NOTIFICATION_ICON} />
        <Text style={Styles.TEXT_NO_NOTIFICATION}>{MessagesController.resourcesViewModel?.getResourcesNoNotificationTitle(locale)}</Text>
        <Text style={Styles.TEXT_SUB_NO_NOTIFICATION}>{MessagesController.resourcesViewModel?.getResourcesNoNotificationSubTitle(locale)}</Text>
      </View>
    )
  }

  return (
    <SafeAreaView style={Styles.SAFE_CONTAINER}>
      <View shouldRasterizeIOS
        style={Styles.MAIN_CONTAINER}>
        <>
          {
            MessagesController.viewModel?.getMessageRead.length !== 0 ?
              renderMessageListView()
              :
              renderEmptyMessage()
          }
        </>
      </View>

    </SafeAreaView>

  )
})