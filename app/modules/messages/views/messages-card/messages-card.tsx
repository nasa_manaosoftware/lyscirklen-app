import React, { useEffect } from "react"
import { observer } from "mobx-react-lite"
import { Text } from "../../../../components"
import { View } from "react-native"
import { MessagesProps } from "../../messages.props"
import * as Styles from './messages-card.styles'
import * as metric from '../../../../theme'

export const MessagesComponents: React.FunctionComponent<MessagesProps> = observer((props) => {

    useEffect(() => {

    }, [])

    const renderSentAtView = () => {
        const isRead = props?.item?.read_message_status || props?.read_message_status
        return (

            <View shouldRasterizeIOS
                style={Styles.SENT_AT_VIEW}
            >
                {isRead ? null :
                    (<View
                        style={[Styles.CIRCLE, Styles.DOT]}>
                        <Text />
                    </View>)}


                <Text numberOfLines={1} style={Styles.SENT_AT_TEXT_VIEW} >
                    {props?.sent_at}
                </Text>
            </View>
        )
    }

    return (

        <View style={Styles.MESSAGE_CONTAINER}>
            <View style={Styles.TOUCHABLE_CONTAINER} >

                <Text numberOfLines={1} style={Styles.HEADER_TEXT_VIEW} >
                    {metric.isMicrolearn() ? props?.heading : props?.heading.toUpperCase()}
                </Text>

                <Text numberOfLines={2}
                    style={Styles.MESSAGE_TEXT_VIEW} >
                    {props?.message}
                </Text>

                {renderSentAtView()}
            </View>
        </View>
    )
})


