import { TextStyle, ViewStyle, ImageStyle, Platform } from "react-native"
import * as metric from "../../../../theme"
import { color } from "../../../../theme"
import { RFValue } from "react-native-responsive-fontsize"
import { size, type } from "../../../../theme/fonts";


export const MESSAGE_CONTAINER : ViewStyle & TextStyle   = {
    // backgroundColor:color.backgroundActive, 
    minHeight : metric.ratioHeightEx(140), 
    width : '100%', 
    margin : 0, 
    justifyContent: 'center', 
    paddingLeft : 0,
    paddingRight : 0,
}


export const TOUCHABLE_CONTAINER : ViewStyle & TextStyle   = {
    // backgroundColor:color.background, 
    // height : '100%', 
    // width : '100%', 
    flex: 1,
    marginBottom : 1,

    paddingTop :  metric.ratioHeightEx(22),
    paddingLeft :  metric.ratioHeightEx(22),
    paddingRight :  metric.ratioHeightEx(27),
}

export const CIRCLE: ViewStyle = {
    borderRadius: 10,
    width: metric.ratioWidth(20),
    height: metric.ratioWidth(20),
    justifyContent: 'center',
}

export const HEADER_TEXT_VIEW: ViewStyle & TextStyle   = {
    minHeight : metric.ratioHeightEx(20), 
    width : '100%',
    fontFamily: type.base,
    lineHeight: metric.ratioHeight(26),
    fontSize: size.extraLarge,
    color: color.palette.white,
 }

export const MESSAGE_TEXT_VIEW: ViewStyle & TextStyle   = {
    // height : metric.isTablet ? metric.ratioHeight(50) : metric.ratioHeight(48),
    minHeight : metric.ratioHeight(60),
    width : '100%', 
    marginTop : 5,
    fontFamily: type.base,
    // lineHeight: (metric.isTablet && (Platform.OS === 'ios')) ? metric.ratioHeight(24) : metric.ratioHeight(28),
    lineHeight: metric.ratioHeight(28),
    fontSize: size.large,
    color: color.palette.white
}
export const SENT_AT_VIEW: ViewStyle & TextStyle   = {
    height : metric.ratioHeightEx(30), 
    flexDirection: 'row', 
    marginTop : metric.ratioHeight(5), 
    justifyContent: 'flex-end', 
    alignItems: 'center'
}
export const DOT: ViewStyle & TextStyle   = {
    backgroundColor: color.redDot, 
    marginLeft: metric.ratioWidth(10), 
    height: metric.ratioWidth(8), 
    width: metric.ratioWidth(8)
}

export const SENT_AT_TEXT_VIEW: ViewStyle & TextStyle   = {
    flex : 0, 
    marginLeft : metric.ratioWidth(8), 
    justifyContent: 'center' ,
    fontFamily: type.base,
    lineHeight: metric.ratioHeight(26),
    fontSize: RFValue(12),
    color: color.whiteText
}

