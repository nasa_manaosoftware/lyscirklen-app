import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"
import { MessagesStoreModel } from '../../models/messages-store'
import { ExtractCSTWithSTN } from 'mobx-state-tree/dist/core/type/type'

export interface MessagesProps extends NavigationContainerProps<ParamListBase> {
    uid: string,
    button_text: string,
    message_id: string,
    heading?: string,
    message?: string,
    sent_at?: any,
    read_message_status?: boolean,
    disableMessage?: boolean,
    item?: ExtractCSTWithSTN<typeof MessagesStoreModel>,
    onPress?: (feed$?: ExtractCSTWithSTN<typeof MessagesStoreModel>) => void
}
