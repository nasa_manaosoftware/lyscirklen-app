import { DrawerActions, StackActions } from "@react-navigation/core";
import { onSnapshot } from "mobx-state-tree";
import { Alert, Keyboard, Platform } from "react-native";
import { NavigationKey } from "../../constants/app.constant";
import { INavigationRoute } from "../../models";
import { RootStore } from "../../models/root-store/root.types";
import { BaseController } from "../base.controller";
import { MessagesProps } from "./messages.props";
import { MessagesResourcesStore, MessagesResourcesStoreModel, MessagesStore, MessagesStoreModel } from "./storemodels";
import { IMessages } from "../../models/messages-store"
import FirebaseFireStore from "../../utils/firebase/fire-store/fire-store";
import moment, { Moment } from "moment"
import PushNotification from "react-native-push-notification";
import PushNotificationIOS from "@react-native-community/push-notification-ios";
import { MessagesStoreModel as MessagesModelChild } from "../../models/messages-store/messages-store.store"
import { ExtractCSTWithoutSTN } from "mobx-state-tree/dist/internal";
import { FirebaseFirestoreTypes } from "@react-native-firebase/firestore";
import { SnapshotChangeType } from "../../constants/firebase/fire-store.constant";
import { INotificationListListener } from "../../utils/firebase/fire-store/fire-store.types";

class MessagesController extends BaseController {
    /*
       Stateful Variable
   */
    public static viewModel: MessagesStore
    public static resourcesViewModel: MessagesResourcesStore
    private static _disposer: () => void


    /*
        Mark Constructor
    */

    constructor(rootStore?: RootStore, props?: MessagesProps & Partial<INavigationRoute>) {
        super(rootStore, props)
        if (this._myProps?.route?.params?.isInitialized) return
        this._setupResourcesViewModel()
        this._setupNavigation()
        this._setupProps(props)
        this._setupViewModel()
        this._setupSnapShot()
    }

    /*
       Mark Setup
    */

    private _setupResourcesViewModel = () => {
        MessagesController.resourcesViewModel = MessagesResourcesStoreModel.create({ ...this._rootStore?.getAllGeneralResourcesStore })
    }
    private _setupViewModel = () => {
        const localStore = this._rootStore?.getModuleStore(NavigationKey.Messages)
        MessagesController.viewModel = localStore && MessagesStoreModel.create({ ...localStore }) || MessagesStoreModel.create({})
        this._setInitializedToPropsParams()
    }

    private _setupSnapShot = () => {
        onSnapshot(MessagesController.viewModel, (snap: MessagesStore) => {
            this._rootStore?.setModuleStoreValue(NavigationKey.Messages, snap)
        })
    }

    public onSideMenuPress = () => {
        this._myProps?.navigation?.dispatch(DrawerActions.openDrawer() as any)
    }

    public async viewDidAppearOnce() {
        this.onFetchMessage()
        this._onNotificationListener()
    }

    private onFetchMessage = async (loading = true) => {

        if (this._rootStore?.getSharedStore?.getIsConnected) {
            // const organizationId = MessagesController.rootStore?.getOrganizationId
            // if (organizationId) {
            //     console.log('organizationId ', organizationId);
                
                this._isGlobalLoading(loading)

                const messageReadList: IMessages[] = await FirebaseFireStore.fetchReadMessage(this._rootStore?.getUserStore?.getUserId)
                if (messageReadList) {
                    const messages = []
                    const badgeNumber = []
                    messageReadList.map(item => {
                        if (item.heading !== undefined && item.message !== undefined && item.sent_at !== undefined) {
                            if (item.read_message_status === false) {
                                badgeNumber.push(item)
                            }
                            messages.push(item)
                        }
                    })

                    messages.sort((firstBook, secondBook) => firstBook.sent_at > secondBook.sent_at ? -1 : 1)

                    this._rootStore?.UserStore?.setBadgeNumber(badgeNumber.length)
                    if (Platform.OS === 'android') {
                        PushNotification.setApplicationIconBadgeNumber(badgeNumber.length)
                    } else {
                        PushNotificationIOS.setApplicationIconBadgeNumber(badgeNumber.length)
                    }

                    MessagesController.viewModel.setMessagesRead(messages)
                }


                this._isGlobalLoading(false)
            // } else {
            //     Alert.alert('Invalid Organization Id');
            // }
        }
    }

    public onRefresh = async () => {
        const isConnected = this._rootStore?.getSharedStore?.getIsConnected
        if (!isConnected) return
        MessagesController.viewModel.setIsRefreshing(true)
        this._isGlobalLoading(true)
        await this.onFetchMessage()

        MessagesController.viewModel.setIsRefreshing(false)
        this._isGlobalLoading(false)
    }

    // private updateBadge = async () => {
    //     if (Platform.OS === "android") {
    //         PushNotification.setApplicationIconBadgeNumber(this._rootStore.getUserStore.getBadgeNumber)
    //     } else {
    //         PushNotificationIOS.setApplicationIconBadgeNumber(this._rootStore.getUserStore.getBadgeNumber)
    //     }
    // }



    public onRefreshMessage = async () => {

        await this.onFetchMessage(false)
    }

    public onSetUnReadMessage = async (message_id: string) => {

        FirebaseFireStore.setReadMessage(this._rootStore?.getUserStore.getUserId, message_id)
        await this.onFetchMessage(false)
    }

    public isAlreadyRead = (message_id: string): boolean => {
        for (var i = 0; i < MessagesController.viewModel.getMessageRead.length; i++) {
            if (MessagesController.viewModel.getMessageRead[i].message_id == message_id) {
                return (MessagesController.viewModel.getMessageRead[i].read_message_status);
            }
        }
        return (false);
    }
    public filterOutHTMLTags = (message: string): string => {
        message = message?.replace(/<[^>]*>?/gm, '')
        message = message?.replace(/&nbsp;/g, ' ');
        message = message?.replace(/&amp;/g, '&');
        return (message);
    }


    public convertDate = (timeStamp) => {
        var newDate = moment(timeStamp).format('DD/MM/YYYY HH:mm');
        return (newDate)
    }

    public onSetReadMessage = async (message) => {
        await FirebaseFireStore.setReadMessage(this._rootStore?.getUserStore?.getUserId, message.message_id)
        // await this.onFetchMessage(true)
    }

    private _onNotificationListener = () => {
        const onNext: (snapshot: FirebaseFirestoreTypes.QuerySnapshot) => void = (snap => {
            if (snap.metadata.fromCache) return
            snap.docChanges().forEach(change => {
                let notification
                if (change.doc.data().heading !== undefined && change.doc.data().sent_at !== undefined && change.doc.data().message !== undefined) {
                    const message_id = change.doc.id
                    notification = change.doc.data()
                    notification = {
                        ...notification,
                        message_id: message_id
                    }
                }

                if (change.type === SnapshotChangeType.ADDED) {
                    if (notification) MessagesController.viewModel?.addNotificationIntoList(notification)
                } else if (change.type === SnapshotChangeType.MODIFIED) {
                    if (notification) MessagesController.viewModel?.updateNotificationIntoList(notification)
                }
            })
        })

        const onError: (e: Error) => void = (e => {
            console.log(e)
        })

        const onComplete: () => void = () => {
            console.log("onCompleteListener")
        }

        const params: INotificationListListener = {
            userId: this._rootStore?.getUserStore?.getUserId,
            onNext,
            onError,
            onComplete
        }

        MessagesController._disposer = FirebaseFireStore.onNotificationListener(params)
    }

    public onMessagePress = (item: ExtractCSTWithoutSTN<typeof MessagesModelChild>): void => {
        // if (item.message_id) {
        const navigation = this._myProps?.navigation
        const params = {
            uid: item.uid,
            button_text: item.button_text,
            message_id: item.message_id,
            heading: item.heading,
            message: item.message,
            sent_at: item.sent_at,
            read_message_status: item.read_message_status
        }



        this._isGlobalLoading(true)
        // MessagesController.viewModel?.setDisableMessagesPress(true)
        // this.onSetReadMessage(item)
        // item.setMarkRead()
        MessagesController.viewModel?.setIsDisabled(true)
        // this.updateBadge()
        navigation?.dispatch(StackActions.push(NavigationKey.MessageDetail, params) as any)
        this._delayExecutor(() => MessagesController.viewModel?.setIsDisabled(false), 1200)

        this._isGlobalLoading(false)


        // } else {
        //     Alert.alert('Invalid UID')
        // }
    }


}

export default MessagesController
