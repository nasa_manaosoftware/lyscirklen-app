import { types, flow } from "mobx-state-tree"
import { MessagesPropsModel } from "../viewmodels"
import { APIResponse, APISuccessResponse } from "../../../constants/service.types"
import { RootStore } from "../../../models/root-store/root.types"
import MessagesServices from "../../../services/api-domain/search-audio.services"
import { IMessages } from "../../../models/messages-store/messages-store.types"

export const MessagesServiceActions = types.model(MessagesPropsModel).actions(self => {

    const fetchSearchAudioBook = flow(function* (rootStore?: RootStore, searchText? : string ) {

        const params = {
            start_at: 0,
            page_size: 100,
            searchText : searchText
        }

        try {
            const result: APIResponse =  yield MessagesServices.searchAudioBook(params)
            let success = (result as APISuccessResponse)?.data

            let list: Partial<IMessages[]> = success?.data

            // if (list?.length > 0) {
            //     const existingList = self.userList
            //     self.userList = self.isLoadMore ? existingList?.concat(list) : list
            // }
            // self.isLoadDone = list?.length < self.page_size
            // return result
            return list
        } catch (e) {
            console.log(e)
            return e
        } finally {
            // self.isPullRefreshing = false
            // self.isLoadMore = false
            // self.isFirstLoadDone = true
        }

    })


    return {  fetchSearchAudioBook }
})