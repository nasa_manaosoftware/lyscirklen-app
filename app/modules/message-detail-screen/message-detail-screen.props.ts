import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"

export interface MessageDetailProps extends NavigationContainerProps<ParamListBase> {
    uid: string,
    button_text: string,
    message_id: string,
    heading: string,
    message: string,
    organization_id: string,
    description: string,
    sent_at: any,
    read_message_status: boolean

}