import { onSnapshot } from "mobx-state-tree";
import { NavigationKey } from "../../constants/app.constant";
import { INavigationRoute, RootStore } from "../../models";
import { BaseController } from "../base.controller";
import { MessageDetailProps } from "./message-detail-screen.props";
import { MessageDetailResourcesStoreModel, MessageDetailStoreModel } from "./storemodels/message-detail-screen.store";
import { MessageDetailResourcesStore, MessageDetailStore } from "./storemodels/message-detail-screen.types";
import { Alert, Platform } from "react-native";
import { StackActions } from "@react-navigation/core"
import FirebaseAnalytics from "../../utils/firebase/analytics/analytics";
import NotificationHandler from '../../custom-hooks/use-notification/notification.handler'
import FirebaseFireStore from "../../utils/firebase/fire-store/fire-store";
import PushNotification from "react-native-push-notification";
import ShortcutBadge from 'react-native-app-badge';
import PushNotificationIOS from "@react-native-community/push-notification-ios";
// import FirebaseFireStore from "../../utils/firebase/fire-store/fire-store";
// import { IMessages } from "../../models/messages-store";
// import { ExtractCSTWithoutSTN } from "mobx-state-tree/dist/internal";
// import { MessagesStoreModel as MessagesModelChild } from "../../models/messages-store/messages-store.store"

class MessageDetailController extends BaseController {

    /*
       Stateful Variable
   */

    public static viewModel: MessageDetailStore
    public static resourcesViewModel: MessageDetailResourcesStore
    public static myProps: MessageDetailProps & Partial<INavigationRoute>

    /*
        Mark Constructor
    */

    constructor(rootStore?: RootStore, props?: MessageDetailProps & Partial<INavigationRoute>) {
        super(rootStore, props)
        if (this._myProps?.route?.params?.isInitialized) return
        this._setupResourcesViewModel()
        this._setupNavigation()
        this._setupProps(props)
        this._setupViewModel()
        this._setupSnapShot()
    }

    /*
       Mark Setup
    */

    private _setupResourcesViewModel = () => {
        MessageDetailController.resourcesViewModel = MessageDetailResourcesStoreModel.create({ ...this._rootStore?.getAllGeneralResourcesStore })
    }
    private _setupViewModel = () => {
        const localStore = this._rootStore?.getModuleStore(NavigationKey.MessageDetail)
        MessageDetailController.viewModel = localStore && MessageDetailStoreModel.create({ ...localStore }) || MessageDetailStoreModel.create({})
        this._setInitializedToPropsParams()
    }

    private _setupSnapShot = () => {
        onSnapshot(MessageDetailController.viewModel, (snap: MessageDetailStore) => {
            this._rootStore?.setModuleStoreValue(NavigationKey.MessageDetail, snap)
        })
    }

    public queryAudioBookName = (audiobookId) => {
        const audioBooks = this._rootStore?.getAudioBooksStore.getAudiobooksList

        let audiobookName = ""
        audioBooks.map(item => {
            if (item.uid === audiobookId) {
                audiobookName = item.name
            }
        })

        return audiobookName
    }

    public queryAudioBookCategory = (audiobookId) => {
        const audioBooks = this._rootStore?.getAudioBooksStore.getAudiobooksList

        let audiobookCategory = ""
        audioBooks.map(item => {
            if (item.uid === audiobookId) {
                audiobookCategory = item.category_id
            }
        })

        return audiobookCategory
    }

    public goToAudioBook = async (book_id: string) => {
        if (book_id) {
            if (MessageDetailController.viewModel?.getIsDisabled) return
            MessageDetailController.viewModel?.setIsDisabled(true)
            
            const navigation = this._myProps?.navigation
            const params = {
                id: book_id
            }
            var audiobookName = this.queryAudioBookName(book_id)
            var audiobookCategory = this.queryAudioBookCategory(book_id)
            this._isGlobalLoading(true)

            //firebase analytic
            FirebaseAnalytics.openVideoDetail({
                user_id: this._rootStore?.getUserStore?.getUserId,
                book_name: audiobookName,
                category_name: audiobookCategory
            })

            navigation?.dispatch(StackActions.push(NavigationKey.AudioBookScreen, params) as any)
            this._isGlobalLoading(false)
        } else {
            Alert.alert('Invalid UID')
        }
        this._delayExecutor(() => MessageDetailController.viewModel?.setIsDisabled(false), 1200) 
    }

    public goToAudioBookNoDisable = async (book_id: string) => {
        if (book_id) {            
            const navigation = this._myProps?.navigation
            const params = {
                id: book_id
            }
            this._isGlobalLoading(true)
            navigation?.dispatch(StackActions.push(NavigationKey.AudioBookScreen, params) as any)
            this._isGlobalLoading(false)
        } else {
            Alert.alert('Invalid UID')
        }
        // this._delayExecutor(() => MessageDetailController.viewModel?.setIsDisabled(false), 1200) 
    }

    public logFirebaseAnalytics = async (book_id: string) => {
        if (book_id) {
            var audiobookName = this.queryAudioBookName(book_id)
            var audiobookCategory = this.queryAudioBookCategory(book_id)

            //firebase analytic
            FirebaseAnalytics.openVideoDetail({
                user_id: this._rootStore?.getUserStore?.getUserId,
                book_name: audiobookName,
                category_name: audiobookCategory
            })

         } else {
            Alert.alert('Invalid UID')
        }
    }



    
    public onSetReadMessage = async (message) => {
        await FirebaseFireStore.setReadMessage(this._rootStore?.getUserStore?.getUserId, message.message_id)
    }

    private updateBadge = async () => {
        if (Platform.OS === "android") {
            PushNotification.setApplicationIconBadgeNumber(this._rootStore.getUserStore.getBadgeNumber)
        } else {
            PushNotificationIOS.setApplicationIconBadgeNumber(this._rootStore.getUserStore.getBadgeNumber)
        }
    }

    viewDidAppearAfterFocus = async () => {
        this.onSetReadMessage(this._myProps?.route?.params)
        this._myProps?.route?.params.setMarkRead()
        this.updateBadge()
    }

}

export default MessageDetailController
