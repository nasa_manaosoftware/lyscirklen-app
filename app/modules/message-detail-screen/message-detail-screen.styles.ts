
import { Platform, TextStyle, ViewStyle } from "react-native";
import { RFValue } from "react-native-responsive-fontsize";
import { color } from "../../theme";
import { size, type } from "../../theme/fonts";
import * as metric from '../../theme/metric'

/*****************************************/
export const HEADER_VIEW: ViewStyle = {
    backgroundColor: color.background,
    paddingVertical: metric.ratioHeight(20)
}

export const TITLE_VIEW: ViewStyle = {
    flexDirection: 'row',
    marginLeft: metric.isTablet()? metric.ratioWidth(27) : metric.ratioWidth(41),
    alignItems: 'center'
}

export const VERTICAL_LINE: ViewStyle = {
    backgroundColor: color.redLine,
    width: metric.ratioWidth(2),
    height: metric.ratioHeight(30)
}

export const HEADING_RED_VIEW_LONG: ViewStyle = {
    backgroundColor : color.redLine, 
    width : metric.ratioWidth(2),  
    height : '100%'
}

export const TITLE_TEXT: TextStyle = {
    fontFamily: type.base,
    fontSize: size.header,
    color: color.white,
    marginLeft: metric.ratioWidth(18),
    marginBottom: metric.isMicrolearn() ? null : metric.ratioHeight(-5),
    paddingRight: metric.ratioWidth(12)
}

export const HTML_SCROLL_VIEW: ViewStyle = {
    backgroundColor : color.background, 
    height : '200%', 
    width : '100%'
}

/***********************************************************************/
export const HTML_VIEW: ViewStyle = {
    paddingTop : metric.ratioWidth(20),
    paddingBottom : metric.ratioWidth(20),
    marginLeft: metric.ratioHeight(45),
    marginRight: metric.ratioHeight(45)
}

export const TAG_STYLE: TextStyle = {
    p: {
        // paddingVertical: 0,
        lineHeight: metric.ratioHeight(28),
        fontFamily: type.base,
        fontSize: size.large,
        color: color.whiteText,
        paddingBottom: metric.ratioHeight(30)
    },
    h1: {
        // paddingVertical: 0,
        fontFamily: type.bold,
        color: color.whiteText,
        fontWeight: 'bold',
        fontSize: metric.isTablet() ? RFValue(29) : RFValue(29),
        paddingBottom: metric.ratioHeight(30)
    },
    h2: {
        // paddingVertical: 0,
        fontFamily: type.bold,
        color: color.whiteText,
        fontWeight: 'bold',
        fontSize: metric.isTablet() ? RFValue(22) : RFValue(22),
        paddingBottom: metric.ratioHeight(30)
    },
    li: {
        // paddingVertical: 0,
        lineHeight: metric.ratioHeight(28),
        fontFamily: type.base,
        fontSize: RFValue(16),
        color: color.whiteText
    },
    a: {
        // paddingVertical: 0,
        lineHeight: metric.ratioHeight(28),
        fontFamily: type.base,
        fontSize: RFValue(16),
        color: color.whiteText
    }
    // ,
    // strong: {
    //     // paddingVertical: 0,
    //     lineHeight: metric.ratioHeight(28),
    //     fontFamily: type.bold,
    //     fontSize: RFValue(16),
    //     color: color.whiteText,
    //     fontWeight: 'bold'
    // },
    // em: {
    //     // paddingVertical: 0,
    //     lineHeight: metric.ratioHeight(28),
    //     fontFamily: type.italic,
    //     fontSize: RFValue(16),
    //     fontStyle: 'italic',
    //     color: color.whiteText
    // },
    // u: {
    //     // paddingVertical: 0,
    //     lineHeight: metric.ratioHeight(28),
    //     fontFamily: type.base,
    //     color: color.whiteText
    // }
}

/***********************************************************************/
export const GOTO_AUDIOBOOK_VIEW: ViewStyle = {
    backgroundColor : color.background, 
    flex : 1,
    alignItems : 'center',   
    alignContent: 'center', 
    paddingLeft  : metric.ratioWidth(38), 
    paddingRight : metric.ratioWidth(38), 
    paddingBottom : metric.ratioHeight(50) 
}

export const GOTO_AUDIOBOOK_BUTTON: ViewStyle = {
    // backgroundColor : color.buttonGradient,  
    flex : 1,
    width : '100%',
    minHeight : metric.ratioHeight(50),
    borderRadius : metric.ratioHeight(50),
    justifyContent: 'center',
    alignItems: 'center',
}

export const GOTO_AUDIOBOOK_BUTTON_TEXT: TextStyle = {
    fontFamily: type.base, 
    fontSize: size.header, 
    textAlign:'center',
    color: color.white
}