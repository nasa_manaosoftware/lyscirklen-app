import { types } from "mobx-state-tree"
import { StoreName } from "../../../constants/app.constant"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store/general-resources.store"
import { MessageDetailScreenResourcesViews, MessageDetailViews } from "../viewmodels/message-detail-screen.views"
import { MessageDetailPropsModel, MessageDetailActions } from "../viewmodels"
import { MessageDetailService } from "../services/message-detail-screen.services"
import { DownloadedStoreModel } from "../../../models/downloaded-store/downloaded.store"

const MessageDetailModel = types.model(StoreName.Audiobook, MessageDetailPropsModel)

export const MessageDetailStoreModel = types.compose(
    MessageDetailModel,
    MessageDetailActions,
    MessageDetailViews,
    MessageDetailService,
    DownloadedStoreModel
)

export const MessageDetailResourcesStoreModel = types.compose(GeneralResourcesStoreModel, MessageDetailScreenResourcesViews)

