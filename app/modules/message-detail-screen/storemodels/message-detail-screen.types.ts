import { Instance, SnapshotOut } from "mobx-state-tree"
import { MessageDetailResourcesStoreModel, MessageDetailStoreModel } from "./message-detail-screen.store"

export type MessageDetailStore = Instance<typeof MessageDetailStoreModel>
export type MessageDetailStoreSnapshot = SnapshotOut<typeof MessageDetailStoreModel>

export type MessageDetailResourcesStore = Instance<typeof MessageDetailResourcesStoreModel>
export type MessageDetailResourcesStoreSnapshot = SnapshotOut<typeof MessageDetailResourcesStoreModel>