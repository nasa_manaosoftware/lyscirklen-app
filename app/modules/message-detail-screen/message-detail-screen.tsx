import React, { useEffect } from "react"
import { observer } from "mobx-react-lite"
import { useConfigurate } from "../../custom-hooks/use-configure-controller";
import { TouchableHighlight, ScrollView, Text, View, Linking } from "react-native"
import { color } from "../../theme";
import { MessageDetailProps } from "./message-detail-screen.props";
import MessageDetailController from "./message-detail-screen.controller";
import * as Styles from './message-detail-screen.styles'
import HTML from "react-native-render-html";
import { IGNORED_TAGS } from 'react-native-render-html/src/HTMLUtils';
import LinearGraient from 'react-native-linear-gradient'
import * as StringUtils from '../../utils/string.utils'
import * as metric from '../../theme/metric'

export const MessageDetailScreen: React.FunctionComponent<MessageDetailProps> = observer((props) => {

    const controller = useConfigurate(MessageDetailController, props) as MessageDetailController

    useEffect(() => {
        console.log('props', MessageDetailController.myProps);

    }, [])

    const splitString = (link) => {
        const url = link.split(':')
        console.log('url ', url);
        
        if(url[0] === 'https' || url[0] === 'http' || url[0] === 'mailto') {
            return url
        } else {
            const urlLink = link.split('.')
            console.log('urlLink ', urlLink);
            
            return urlLink
        }
    }

    const openUrl = (link) => {
        const url = splitString(link)
        console.log("url ==> ", url);
        let openLink
        if (url[0] === 'https') {
            openLink = link
        } else if (url[0] === 'mailto') {
            openLink = link
        } else if (url[0] === 'http') {
            openLink = 'https://' + url[1]
        } else {
            openLink = 'https://' + link
        }

        Linking.canOpenURL(openLink).then(supported => {
            if (!supported) {
                console.log('Can\'t handle url: ' + openLink);
            } else {
                Linking.openURL(openLink)
                    .catch(err => {
                        console.warn('openURL error', err);
                    });
            }
        }).catch(err => console.warn('An unexpected error happened', err))
    }

    const renderHeaderView = () => {
        return (
            <View shouldRasterizeIOS style={Styles.HEADER_VIEW}>
                <View style={Styles.TITLE_VIEW}>
                    {(MessageDetailController.myProps.heading.length > 26) ?
                        (<View style={Styles.HEADING_RED_VIEW_LONG} />)
                        :
                        (<View style={Styles.VERTICAL_LINE} />)
                    }
                    <Text style={Styles.TITLE_TEXT}>{MessageDetailController.myProps.heading}</Text>
                </View>
            </View>
        )
    }

    const renderGotoAudioBookView = (book_id: string) => {
        return (
            <View style={Styles.GOTO_AUDIOBOOK_VIEW} >
                <TouchableHighlight
                    disabled={MessageDetailController.viewModel?.getIsDisabled}
                    style={Styles.GOTO_AUDIOBOOK_BUTTON}
                    underlayColor={color.backgroundActive}
                    activeOpacity={0.8}
                    // onPress={() => controller.goToAudioBook(book_id)}
                    onPressIn={() => controller.goToAudioBookNoDisable(book_id)}
                    onPressOut={() => controller.logFirebaseAnalytics(book_id)}
                >
                    <LinearGraient style={Styles.GOTO_AUDIOBOOK_BUTTON} colors={color.buttonGradient}>
                        <Text style={Styles.GOTO_AUDIOBOOK_BUTTON_TEXT}>{MessageDetailController.myProps.button_text.toUpperCase()}</Text>
                    </LinearGraient>
                </TouchableHighlight>
            </View>
        )
    }

    return (
        <View style={{ backgroundColor: color.background, flex: 1 }}>
            {renderHeaderView()}
            <ScrollView style={Styles.HTML_SCROLL_VIEW}>
                <HTML
                    containerStyle={Styles.HTML_VIEW}
                    tagsStyles={Styles.TAG_STYLE}
                    ignoredStyles={["font-family", "letter-spacing", "font-size", "color"]}
                    html={StringUtils.brToNewLine(MessageDetailController.myProps.message)}
                    ignoredTags={[...IGNORED_TAGS]}
                    onLinkPress={(event, href) => openUrl(href)}
                    listsPrefixesRenderers={{
                        ul: (_htmlAttribs, _children, _convertedCSSStyles, passProps) => {
                            return <View style={{
                                marginRight: 10,
                                width: 10 / 2.8,
                                height: 10 / 2.8,
                                marginTop: metric.ratioHeight(12),
                                borderRadius: 10 / 2.8,
                                backgroundColor: color.whiteText,
                            }}/>
                        }
                      }}
                />

                {MessageDetailController.myProps.uid ? renderGotoAudioBookView(MessageDetailController.myProps.uid) : null}

            </ScrollView>

        </View>
    )
})