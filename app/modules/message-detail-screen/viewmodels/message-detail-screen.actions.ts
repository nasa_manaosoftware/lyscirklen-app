
import { types } from "mobx-state-tree"
import { MessageDetailPropsModel } from "./message-detail-screen.model"

export const MessageDetailActions = types.model(MessageDetailPropsModel).actions(self => {
    const setIsDisabled = (value: boolean) => {
        self.isDisabled = value
    }

    return {
        setIsDisabled
    }
})