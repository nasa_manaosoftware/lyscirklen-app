import { types } from "mobx-state-tree"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store/general-resources.store"
import { GeneralResources } from "../../../constants/firebase/remote-config"
import { NavigationKey } from "../../../constants/app.constant"
import { MessageDetailPropsModel } from "."

export const MessageDetailViews = types.model(MessageDetailPropsModel)
    .views(self => ({
        get getGoToAudioBookButton() {
            return "GO TO AUDIOBOOK"
        },
        get getIsDisabled(): boolean {
            return self.isDisabled
        }
    
       
    }))

export const MessageDetailScreenResourcesViews = GeneralResourcesStoreModel.views(self => {

    //MARK: Volatile State
    const { AudioBookScreen } = GeneralResources
    const {
        playButton,
        downloadButton,
        downloadedTitle
    } = AudioBookScreen

    //MARK: Views
    const getResources = (key: string, childKeyOrShareKey: string | boolean = false, isChildDotNotation: boolean = false) => self.getValues(childKeyOrShareKey ? key : NavigationKey.MessageDetail, childKeyOrShareKey ? true : key, isChildDotNotation)
    const getResourcePlayButtonTitle = () => getResources(playButton, true)
    const getResourceDownloadButtonTitle = () => getResources(downloadButton, true)
    const getResourceDownloadedTitle = () => getResources(downloadedTitle, true)

    return {
        getResourcePlayButtonTitle,
        getResourceDownloadButtonTitle,
        getResourceDownloadedTitle
    }
})
