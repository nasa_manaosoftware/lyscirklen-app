import { types } from "mobx-state-tree";

export const MessageDetailPropsModel = {
    isDisabled: types.optional(types.boolean, false)
} 