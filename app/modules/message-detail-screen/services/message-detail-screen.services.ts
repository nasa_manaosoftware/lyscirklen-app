import { types, flow } from "mobx-state-tree"
import DownloadServices from '../../../services/api-domain/download.service'
import { DownloadedPropsModel } from "../../../models/downloaded-store/downloaded.models"

export const MessageDetailService = types.model(DownloadedPropsModel).actions(self => {
    const downloadBook = flow(function* (book) {
        try {
            const result = DownloadServices.downloadedAudioBook(book)
            return result
        } catch (err) {
            console.log(err);
            return err
        }
    })

    return { downloadBook }
})