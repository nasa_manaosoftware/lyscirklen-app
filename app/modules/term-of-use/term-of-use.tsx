import React from "react"
import { observer } from "mobx-react-lite"
import { useConfigurate } from "../../custom-hooks/use-configure-controller";
import { Linking, SafeAreaView, ScrollView, Text, View } from "react-native"
import * as Styles from "./term-of-use.styles"
import { TermOfUseProps } from "./term-of-use.props";
import TermOfUseController from "./term-of-use.controller";
import HTML from "react-native-render-html";
import { IGNORED_TAGS } from 'react-native-render-html/src/HTMLUtils';
import * as StringUtils from '../../utils/string.utils'
import { color } from "../../theme";
import * as metric from '../../theme/metric'


export const TermOfUse: React.FunctionComponent<TermOfUseProps> = observer((props) => {

    const controller = useConfigurate(TermOfUseController, props) as TermOfUseController
    const title = TermOfUseController.resourcesViewModel?.getResourcesTermOfUseTitle(TermOfUseController.rootStore?.getLanguage)

    const splitString = (link) => {
        const url = link.split(':')
        console.log('url ', url);
        
        if(url[0] === 'https' || url[0] === 'http' || url[0] === 'mailto') {
            return url
        } else {
            const urlLink = link.split('.')
            console.log('urlLink ', urlLink);
            
            return urlLink
        }
    }

    const openUrl = (link) => {
        const url = splitString(link)
        console.log("url ==> ", url);
        let openLink
        if (url[0] === 'https') {
            openLink = link
        } else if (url[0] === 'mailto') {
            openLink = link
        } else if (url[0] === 'http') {
            openLink = 'https://' + url[1]
        } else {
            openLink = 'https://' + link
        }

        Linking.canOpenURL(openLink).then(supported => {
            if (!supported) {
                console.log('Can\'t handle url: ' + openLink);
            } else {
                Linking.openURL(openLink)
                    .catch(err => {
                        console.warn('openURL error', err);
                    });
            }
        }).catch(err => console.warn('An unexpected error happened', err))
    }

    return (
        <SafeAreaView style={{ flex: 1 }}>
            <View style={Styles.HEADER_VIEW}>
                <View style={Styles.TITLE_VIEW}>
                    <View style={Styles.VERTICAL_LINE} />
                    <Text style={Styles.TITLE_TEXT}>{title}</Text>
                </View>
            </View>
            <ScrollView style={Styles.DESCRIPTION_VIEW}>
                <HTML
                    containerStyle={Styles.HTML_VIEW}
                    tagsStyles={Styles.TAG_STYLE}
                    ignoredStyles={["font-family", "letter-spacing", "font-size", "color", "font-style", "background-color"]}
                    ignoredTags={[...IGNORED_TAGS]}
                    html={StringUtils.brToNewLine(TermOfUseController.rootStore?.getTermOfUse)}
                    onLinkPress={(event, href) => openUrl(href)}
                    listsPrefixesRenderers={{
                        ul: (_htmlAttribs, _children, _convertedCSSStyles, passProps) => {
                            return <View style={{
                                marginRight: 10,
                                width: 10 / 2.8,
                                height: 10 / 2.8,
                                marginTop: metric.ratioHeight(12),
                                borderRadius: 10 / 2.8,
                                backgroundColor: color.whiteText,
                            }}/>
                        }
                      }}
                />
            </ScrollView>
        </SafeAreaView>
    )
})