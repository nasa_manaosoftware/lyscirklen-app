import { DrawerActions } from "@react-navigation/core";
import { onSnapshot } from "mobx-state-tree";
import { Alert } from "react-native";
import { NavigationKey } from "../../constants/app.constant";
import { INavigationRoute } from "../../models";
import { RootStore } from "../../models/root-store/root.types";
import { TabController } from "../tab.controller";
import { TermOfUseResourcesStore, TermOfUseResourcesStoreModel, TermOfUseStore, TermOfUseStoreModel } from "./storemodel";
import { TermOfUseProps } from "./term-of-use.props";

class TermOfUseController extends TabController {

    /*
       Stateful Variable
   */

    public static viewModel: TermOfUseStore
    public static resourcesViewModel: TermOfUseResourcesStore

    /*
        Mark Constructor
    */

    constructor(rootStore?: RootStore, props?: TermOfUseProps & Partial<INavigationRoute>) {
        super(rootStore, props)
        if (this._myProps?.route?.params?.isInitialized) return
        this._setupResourcesViewModel()
        this._setupNavigation()
        this._setupProps(props)
        this._setupViewModel()
        this._setupSnapShot()
    }

    /*
       Mark Setup
    */

    private _setupResourcesViewModel = () => {
        TermOfUseController.resourcesViewModel = TermOfUseResourcesStoreModel.create({ ...this._rootStore?.getAllGeneralResourcesStore })
    }
    private _setupViewModel = () => {
        const localStore = this._rootStore?.getModuleStore(NavigationKey.TermOfUse)
        TermOfUseController.viewModel = localStore && TermOfUseStoreModel.create({ ...localStore }) || TermOfUseStoreModel.create({})
        this._setInitializedToPropsParams()
    }

    private _setupSnapShot = () => {
        onSnapshot(TermOfUseController.viewModel, (snap: TermOfUseStore) => {
            this._rootStore?.setModuleStoreValue(NavigationKey.FrontScreen, snap)
        })
    }

    public onSideMenuPress = () => {
        this._myProps?.navigation?.dispatch(DrawerActions.openDrawer() as any)
    }

    public onNotificationPress = () => {
        // this._myProps?.navigation?.dispatch(StackActions.push(NavigationKey.NotificationList) as any)
        Alert.alert('Notifications')
    }

}

export default TermOfUseController
