import { ImageStyle, Platform, TextStyle, ViewStyle } from "react-native";
import { RFValue } from "react-native-responsive-fontsize";
import { HeaderProps } from "../../components/header/header.props";
import { color } from "../../theme";
import { type } from "../../theme/fonts";
import { images } from "../../theme/images";
import * as metric from '../../theme/metric'

export const CONTAINER_VIEW: ViewStyle = {
    flex: 1,
    marginHorizontal: metric.ratioWidth(0),
    marginBottom: metric.ratioWidth(0),
    ...Platform.select({
        ios: {
            marginTop: metric.isNewIPhone ? metric.ratioHeight(40) : metric.ratioHeight(20)
        },
        android: {
            marginTop: metric.ratioHeight(0),
        }
    })
}

export const ICON_CLOSE: ImageStyle = {
    width: metric.ratioWidth(20),
    height: metric.ratioHeight(20),
    alignSelf: 'flex-end',
    marginHorizontal: metric.ratioWidth(20),
    marginVertical: metric.ratioHeight(20)
}

export const HEADER_VIEW: ViewStyle = {
    backgroundColor: color.background,
    paddingVertical: metric.ratioHeight(20)
}

export const TITLE_VIEW: ViewStyle = {
    flexDirection: 'row',
    marginLeft: metric.ratioWidth(48),
    alignItems: 'center'
}

export const VERTICAL_LINE: ViewStyle = {
    backgroundColor: color.redLine,
    width: metric.ratioWidth(2),
    minHeight: metric.ratioHeight(30)
}

export const TITLE_TEXT: TextStyle = {
    fontFamily: type.base,
    fontSize: RFValue(32),
    color: color.white,
    marginLeft: metric.ratioWidth(18),
    marginTop: metric.ratioHeight(-3)
    // marginBottom: metric.isMicrolearn() ? null : metric.ratioHeight(-5)
}

export const DESCRIPTION_VIEW: ViewStyle = {
    backgroundColor: color.background
}

export const HTML_VIEW: ViewStyle = {
    padding: metric.ratioWidth(25),
    marginLeft: metric.ratioWidth(23),
}

export const MENU_ICON_STYLE: ImageStyle = {
    width: metric.ratioWidth(24),
    height: metric.ratioHeight(24)
}

export const HEADER_PROPS: HeaderProps = {
    leftIconSource: images.hamMenu,
    leftIconStyle: MENU_ICON_STYLE,
    rightIconStyle: MENU_ICON_STYLE,
    rightIconSource: images.whiteBell,
    isLeftIconAnimated: false,
    isRightIconAnimated: false,
    containerStyle: { backgroundColor: color.background }
}

export const TAG_STYLE: TextStyle = {
    p: {
        // paddingVertical: 0,
        lineHeight: metric.ratioHeight(28),
        fontFamily: type.base,
        fontSize: RFValue(16),
        color: color.whiteText,
        paddingBottom: metric.ratioHeight(30)
    },
    h1: {
        // paddingVertical: 0,
        fontFamily: type.bold,
        color: color.whiteText,
        fontWeight: 'bold',
        fontSize: metric.isTablet() ? RFValue(29) : RFValue(29),
        paddingBottom: metric.ratioHeight(30)
    },
    h2: {
        // paddingVertical: 0,
        fontFamily: type.bold,
        fontWeight: 'bold',
        color: color.whiteText,
        fontSize: metric.isTablet() ? RFValue(22) : RFValue(22),
        paddingBottom: metric.ratioHeight(30)
    },
    li: {
        // paddingVertical: 0,
        lineHeight: metric.ratioHeight(28),
        fontFamily: type.base,
        fontSize: RFValue(16),
        color: color.whiteText
    },
    a: {
        // paddingVertical: 0,
        lineHeight: metric.ratioHeight(28),
        fontFamily: type.base,
        fontSize: RFValue(16),
        color: color.whiteText
    }
    // ,
    // strong: {
    //     // paddingVertical: 0,
    //     lineHeight: metric.ratioHeight(28),
    //     fontFamily: type.bold,
    //     fontSize: RFValue(16),
    //     color: color.textTerm,
    //     fontWeight: 'bold'
    // },
    // em: {
    //     // paddingVertical: 0,
    //     lineHeight: metric.ratioHeight(28),
    //     fontFamily: type.italic,
    //     fontSize: RFValue(16),
    //     fontStyle: 'italic',
    //     color: color.textTerm
    // },
    // u: {
    //     // paddingVertical: 0,
    //     lineHeight: metric.ratioHeight(28),
    //     fontFamily: type.base,
    //     color: color.textTerm
    // }
}