import { types } from "mobx-state-tree"
import { NavigationKey } from "../../../constants/app.constant"
import { GeneralResources } from "../../../constants/firebase/remote-config/remote-config.constant"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store/general-resources.store"
import { TermOfUse } from "../term-of-use"
import { TermOfUsePropsModel } from "./term-of-use.models"

export const TermOfUseViews = types.model(TermOfUsePropsModel)
    .views(self => ({
    }))

export const TermOfUseResourcesViews = GeneralResourcesStoreModel.views(self => {

    // //MARK: Volatile State
    const { TermOfUse } = GeneralResources

    const { termOfUse } = TermOfUse

    // //MARK: Views
    const getResources = (locale: string, key: string, childKeyOrShareKey: string | boolean = false, isChildDotNotation: boolean = false) => self.getValues(locale, childKeyOrShareKey ? key : NavigationKey.TermOfUse, childKeyOrShareKey ? true : key, isChildDotNotation)
    const getResourcesTermOfUseTitle = (locale: string) => getResources(locale, termOfUse, true)

    return {
        getResourcesTermOfUseTitle

    }
})
