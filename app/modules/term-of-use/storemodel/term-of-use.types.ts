import { Instance, SnapshotOut } from "mobx-state-tree"
import { TermOfUseResourcesStoreModel, TermOfUseStoreModel } from "./term-of-use.store"

export type TermOfUseStore = Instance<typeof TermOfUseStoreModel>
export type TermOfUseStoreSnapshot = SnapshotOut<typeof TermOfUseStoreModel>

export type TermOfUseResourcesStore = Instance<typeof TermOfUseResourcesStoreModel>
export type TermOfUseResourcesStoreSnapshot = SnapshotOut<typeof TermOfUseResourcesStoreModel>