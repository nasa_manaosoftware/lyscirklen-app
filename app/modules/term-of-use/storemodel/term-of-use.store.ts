import { types } from "mobx-state-tree"
import { TermOfUseActions, TermOfUseViews, TermOfUsePropsModel, TermOfUseResourcesViews } from "../viewmodels"
import { StoreName } from "../../../constants/app.constant"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store/general-resources.store"

const TermOfUseModel = types.model(StoreName.TermOfUse, TermOfUsePropsModel)

export const TermOfUseStoreModel = types.compose(
    TermOfUseModel,
    TermOfUseActions,
    TermOfUseViews
)

export const TermOfUseResourcesStoreModel = types.compose(GeneralResourcesStoreModel, TermOfUseResourcesViews)

