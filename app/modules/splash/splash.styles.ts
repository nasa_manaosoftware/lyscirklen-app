import { ViewStyle, ImageStyle } from "react-native"
import { color } from "../../theme"
import { FULL } from "../../styles/styles"
import { RFValue } from "react-native-responsive-fontsize"
import * as metric from '../../theme/metric'

export const CONTAINER: ViewStyle = { 
    ...FULL,
    backgroundColor: color.background
}

export const APPLOGO: ImageStyle = {
    width: metric.screenWidth,
    height: metric.screenHeight
}
export const APPLOGO_CONTAINER: ViewStyle = {
    ...FULL,
    justifyContent: "center",
    marginBottom: 14
}
