// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import SplashController from "./splash.controllers"
import SplashScreen from 'react-native-splash-screen'
import { View, StatusBar, Image, Platform, ActivityIndicator } from "react-native"
import { images } from "../../theme/images"
import { useConfigurate } from "../../custom-hooks/use-configure-controller"
import FastImage from "react-native-fast-image"

// MARK: Style Import
import * as Styles from "./splash.styles"

export const Splashscreen: React.FunctionComponent<{}> = observer((props) => {
    SplashScreen.hide()

    useConfigurate(SplashController, props) as SplashController

    return (


        <View shouldRasterizeIOS style={Styles.CONTAINER}>
            <View shouldRasterizeIOS style={Styles.APPLOGO_CONTAINER} >
                <ActivityIndicator size="large" color='#FFFFFF' />
                {/* <FastImage style={Styles.APPLOGO} source={images.splashScreen} resizeMode={FastImage.resizeMode.contain} /> */}
            </View>
        </View>
    )
})
