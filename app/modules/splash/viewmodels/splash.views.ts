import { types } from "mobx-state-tree"
import { SplashPropsModel } from "./splash.models"
export const SplashViews = types.model(SplashPropsModel)
    .views(self => ({
        get getContent(): string {
            return self.content
        },
        get getContentObject() {
            return self.contentObject
        },
        get getIsLoading(): boolean {
            return self.isLoading
        }
    }))