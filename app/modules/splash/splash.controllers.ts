// MARK: Import
import { GeneralResourcesStore, INavigationRoute } from "../../models"
import { RootStore } from "../../models/root-store/root.types"
import { onSnapshot } from "mobx-state-tree"
import { BaseController } from "../base.controller"
import { FirebaseUserInfo } from "../../utils/firebase/authentication/auth.types"
import deviceInfoModule from "react-native-device-info"
import FireBaseServices from "../../utils/firebase/authentication/auth"
import FirebaseFireStore from "../../utils/firebase/fire-store/fire-store";
import PushNotification from "react-native-push-notification";
import PushNotificationIOS from "@react-native-community/push-notification-ios";
import { Platform } from "react-native"
import auth from '@react-native-firebase/auth'
import NotificationServices from "../../utils/firebase/notification/notification"
import { NavigationKey } from "../../constants/app.constant"
import { SplashProps } from "./splash.props"
import FirebaseAnalytics from "../../utils/firebase/analytics/analytics";
import crashlytics from '@react-native-firebase/crashlytics';

// MARK: Vaiable Declaration

class SplashController extends BaseController {

    /*
        Mark Injectable Variable & Declaration
    */

    /*
         Vulnerable Variable -- always will be cleared when deinit.
     */

    /*
        Stateful Variable
    */

    public static viewModel: GeneralResourcesStore

    /*
        Mark Constructor
    */

    constructor(rootStore?: RootStore, props?: SplashProps & Partial<INavigationRoute>) {
        super(rootStore)
        this._setupResourcesViewModel()
        this._setupViewModel()
        this._setupProps(props)
        this._setupSnapShot()
    }

    /*
       Mark Setup
   */

    private _setupResourcesViewModel = () => {
    }
    private _setupViewModel = () => {
        SplashController.viewModel = this._rootStore?.getAllGeneralResourcesStore
    }

    private _setupSnapShot = () => {
        onSnapshot(SplashController.viewModel, (snap: GeneralResourcesStore) => {
        })
    }

    /*
       Mark Data
   */
    private _getLocalizedString = async () => {
        await SplashController.viewModel?.fetchLocalizedGeneralResources()
    }

    private _getGeneralResources = async () => {
        await SplashController.viewModel?.fetchRemoteConfig()
    }

    // private _initSharedInstanceProps = () => {
    //     SplashController.viewModel?.initSharedInstanceProps(this._rootStore)
    // }

    public _getAppId = () => {
        SplashController.rootStore?.setOrganizationId('lyscirklen')
    }

    public forceResetRootStore = () => {
        // this._rootStore?.setIsShowTermOfUse(false)
        // this._rootStore?.setIsShowPrivacy(false)
        // this._rootStore?.setAppName(deviceInfoModule.getBundleId())
        // this._rootStore?.setIsSelectedFront(true)
        // this._rootStore?.setIsSelectedSearch(false)
        // this._rootStore?.setIsSelectedMyLibrary(false)
        // this._rootStore?.setIsSelectedAboutUs(false)
        // this._rootStore?.setIsSelectedNotofication(false)
        // this._rootStore?.setIsSelectedTermOfUse(false)
        // this._rootStore?.setIsSelectedPrivacy(false)
        // this._rootStore?.setIndex(0)
        // this._rootStore?.setDeviceId(deviceInfoModule.getUniqueId())
        // this._rootStore?.resetPlaybackStore()
    }

    public updateDeviceToken = async () => {
        // const token: string = await NotificationServices.getToken()
        // FirebaseFireStore.setDeviceToken(this._rootStore?.getUserStore?.getUserId, deviceInfoModule.getUniqueId(), token)
    }

    public updateUserRole = async () => {
        // await FirebaseFireStore.addUserRole(this._rootStore?.getUserStore?.getUserId)
    }

    private loginAnonymous = async () => {
        const navigation = this._myProps?.navigation
        auth()
            .signInAnonymously()
            .then(() => {
                console.log('User signed in anonymously');
                FirebaseAnalytics.userLaunchApp({
                    user_id: this._rootStore?.getUserStore.getUserId
                })
                SplashController.rootStore?.getAuthStore?.setIsLoggedIn(true)
                this.updateUserRole()
                navigation?.navigate(NavigationKey.MainStack)
                this._isGlobalLoading(false)
                this.updateDeviceToken()
                // this.onAuthStateChanged()
            })
            .catch(error => {
                if (error.code === 'auth/operation-not-allowed') {
                    console.log('Enable anonymous in your firebase console.');
                }
                console.error(error);
            })
    }

    // private onAuthStateChanged = () => {
    //     auth().onAuthStateChanged((user) => {
    //         if (user) {
    //             this.updateDeviceToken(user.uid)
    //         }
    //     })
    // }

    // MARK: Event handling

    //@override
    backProcess() {
        return true
    }

    /*
       Mark Life cycle
    */

    viewWillAppearOnce = async () => {
        // this.forceResetRootStore()
        this._getAppId()
    }

    //@override
    async viewDidAppearOnce() {
        // console.log('viewDidAppearOnce splash');
        // crashlytics().crash();
        // await this.checkUninstall()
        // await this.loginAnonymous()
        await this._getLocalizedString()
        await this._getGeneralResources()
        this._rootStore.setLanguage('da')

        this._delayExecutor(() => {
            this._rootStore?.setIsFetchingResources(false)
        }, 3000)
    }

    public clearDownloaded = async () => {
        const deviceId = deviceInfoModule.getUniqueId()
        const result = await FirebaseFireStore.clearAudiobooksInDevice(deviceId, this._rootStore?.getOrganizationId)
    }

    checkUninstall = async () => {
        // if (this._rootStore?.getIsNewApp) {
        //     await this.clearDownloaded()
        //     if (Platform.OS === 'android') {
        //         PushNotification.setApplicationIconBadgeNumber(0)
        //     } else {
        //         PushNotificationIOS.setApplicationIconBadgeNumber(0)
        //     }
        //     const user: FirebaseUserInfo = SplashController.viewModel?.fetchCurrentUser()
        //     if (user && user?.emailVerified === false) {
        //         await FireBaseServices.signOut()
        //     }
        //     this._rootStore.setIsNewApp(false);
        // }
    }

    public deInit = () => {
        super.deInit && super.deInit()
    }
}

export default SplashController
