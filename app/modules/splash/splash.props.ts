import { ParamListBase } from "@react-navigation/native"
import { NavigationContainerProps } from "react-navigation";

export interface SplashProps extends NavigationContainerProps<ParamListBase> {
}