import { types } from "mobx-state-tree"
import { SplashViews, SplashActions } from "../viewmodels"
import { SplashPropsModel } from "../viewmodels"
import { SplashServiceActions } from "../services/splash.services"
import { StoreName } from "../../../constants/app.constant"


const SplashModel = types.model(StoreName.Splash, SplashPropsModel)
// .views(ฺBaseSplashViews)
// .actions(ฺBaseSplashActions)


export const SplashStoreModel = types.compose(
    SplashModel,
    SplashViews,
    SplashActions,
    SplashServiceActions)

