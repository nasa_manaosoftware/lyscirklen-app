import { ViewStyle, TextStyle, ImageStyle, Platform } from "react-native"
import { color } from "../../theme"
import * as metric from "../../theme"
import { RFValue } from "react-native-responsive-fontsize"
import { FULL, FONT_REGULAR, FONT_LIGHT } from "../../styles/styles"
import { size, type } from "../../theme/fonts"

export const SAFE_AREA_VIEW: ViewStyle = {
    ...FULL,
}
export const SCROLL_VIEW: ViewStyle = {
    paddingBottom: metric.ratioHeight(24),
    // backgroundColor: color.palette.pearl
}
export const TEXT_CONTAINER_VIEW: ViewStyle = {
    marginTop: metric.ratioHeight(34),
    marginHorizontal: metric.ratioWidth(37),
    paddingBottom: metric.ratioHeight(23)
}

export const BASE_TEXT: TextStyle = {
    textAlign: 'center',
}
export const MAIN_CONTAINER_VIEW: ViewStyle = {
    paddingHorizontal: metric.ratioWidth(47),
    width: '100%',
    height: '100%',
    flexDirection: 'column',
}

export const TITLE_CONTAINNER: ViewStyle = {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: metric.ratioHeight(80),
}

export const TITLE_SPACE: ViewStyle = {
    width: metric.ratioWidth(2),
    height: metric.ratioHeight(32),
    backgroundColor: color.redLine,
}

export const TITLE_VIEW: ViewStyle = {
    width: '100%',
}


export const BUTTON_CONTAINER_VIEW: ViewStyle = {
    backgroundColor: color.palette.pearl,
    alignItems: 'center',
    paddingTop: metric.ratioHeight(10),

}

export const CHECKBOX_CONTAINER_VIEW: ViewStyle = {
    backgroundColor: color.palette.pearl,
    flexDirection: "row",
    alignItems: 'center',
    paddingHorizontal: metric.ratioWidth(37),
}

export const PRIVACY_CONTAINER_VIEW: ViewStyle = {
    flexDirection: 'row',
}

export const BUTTON: ViewStyle = {
    marginBottom: metric.ratioHeight(39),
}
export const MEMBERSHIP_VIEW: ViewStyle = {
    alignItems: 'center',
    marginTop: metric.ratioHeight(10)
}

export const CHECK: ViewStyle = {
    flexDirection: 'row',
    alignItems: 'flex-start',
    marginLeft: metric.ratioWidth(-2.5),
    marginRight: metric.ratioWidth(5),
    marginTop: metric.ratioHeight(5)
}

export const ICON_CHECK: ImageStyle = {
    width: metric.ratioWidth(19.25),
    height: metric.ratioHeight(19.25)
}


export const PRIVACY_VIEW: ViewStyle = {
    flexWrap: 'wrap',
    flexDirection: 'row',
    ...Platform.select({
        ios: {
            marginTop: metric.isIPhone ? metric.ratioHeight(0) : metric.ratioHeight(2)
        },
        android: {
            marginTop: metric.isMicrolearn() ?
                null
                :
                metric.isTablet() ?
                    metric.ratioHeight(4)
                    :
                    metric.ratioHeight(3.5)
        }
    })
}

export const PRIVACY_TEXT: TextStyle = {
    ...FONT_LIGHT,
    color: color.palette.darkGrey,
    fontSize: RFValue(13),
    lineHeight: metric.ratioHeight(30),
    paddingHorizontal: metric.ratioWidth(10),
}

export const PRIVACY_LINK: TextStyle = {
    ...FONT_LIGHT,
    color: color.palette.pink,
    fontSize: RFValue(13),
    lineHeight: metric.ratioHeight(30),
    textDecorationLine: 'underline'
}

export const CHECK_VIEW: ViewStyle = {
    flexDirection: 'row'
}

export const SUBMIT_VIEW: ViewStyle = {
    paddingTop: 0,
    marginTop: 30,
    width: '100%',
}
export const TERMS_AND_AGREEMENT_VIEW: ViewStyle = {
    height: '20%',
    marginTop: metric.ratioHeight(30),
    width: '100%',
    alignItems: 'flex-start',
}

export const SUBMIT_TEXT: TextStyle = {
    ...FONT_LIGHT,
    fontSize: RFValue(20),
    fontFamily: type.title,
    color: color.white,
    textAlign: 'center',
    top: metric.isMicrolearn() ? null : metric.ratioHeight(2)
}

export const TITLE_TEXT: TextStyle = {
    fontSize: RFValue(30),
    fontFamily: type.title,
    color: 'white',
    marginLeft: metric.ratioWidth(18),
    marginTop: metric.isMicrolearn() ? null : metric.ratioHeight(6)
}
export const TERMS_AND_AGREEMENT: TextStyle = {
    ...FONT_LIGHT,
    fontSize: RFValue(16),
    fontFamily: type.base
}
export const PRIVACY_LINK_TEXT: TextStyle = {
    ...FONT_LIGHT,
    fontSize: RFValue(16),
    fontFamily: type.base,
    textDecorationLine: 'underline'
}

export const BUTTON_VIEW: ViewStyle = {
    borderRadius: 6,
    alignItems: 'center',
    justifyContent: 'center',
    height: metric.ratioHeight(50)
}

export const TEXT_LOGIN_DESCRIPTION: TextStyle = {
    color: '#8C8BB1',
    fontFamily: type.base,
    fontSize: RFValue(16),
    marginTop: metric.ratioHeight(18),

}
