// MARK: Import

import { NavigationKey } from "../../constants/app.constant"
import { SigninProps } from "./signin.props"
import { INavigationRoute } from "../../models"
import { RootStore } from "../../models/root-store/root.types"
import { SigninStore, SigninStoreModel, SigninResourcesStore, SigninResourcesStoreModel } from "./storemodels"
import { onSnapshot } from "mobx-state-tree"
import { StackActions } from "@react-navigation/native"
import { BaseController } from "../base.controller"
import { Alert, Keyboard } from "react-native"
import * as Validate from "../../utils/local-validate"
import auth from '@react-native-firebase/auth'
import FirebaseAnalytics from "../../utils/firebase/analytics/analytics";
import FirebaseFireStore from "../../utils/firebase/fire-store/fire-store";
import NotificationServices from "../../utils/firebase/notification/notification"
import deviceInfoModule from "react-native-device-info"
import { NativeModules } from 'react-native'

class SigninController extends BaseController {

    /*
        Mark Injectable Variable & Declaration
    */

    /*
        Vulnerable Variable -- always will be cleared when deinit.
    */

    /*
        Stateful Variable
    */

    public static viewModel: SigninStore
    public static resourcesViewModel: SigninResourcesStore

    /*
        Mark Constructor
    */

    constructor(rootStore?: RootStore, props?: SigninProps & Partial<INavigationRoute>) {
        super(rootStore, props)
        if (this._myProps?.route?.params?.isInitialized) return
        this._setupResourcesViewModel()
        this._setupProps(props)
        this._setupViewModel()
        this._setupSnapShot()
    }


    /*
       Mark Setup
   */

    private _setupResourcesViewModel = () => {
        SigninController.resourcesViewModel = SigninResourcesStoreModel.create({ ...this._rootStore?.getAllGeneralResourcesStore })
    }
    private _setupViewModel = () => {
        const localStore = this._rootStore?.getModuleStore(NavigationKey.Signin)
        SigninController.viewModel = localStore && SigninStoreModel.create({ ...localStore }) || SigninStoreModel.create({})
        this._setInitializedToPropsParams()
    }

    private _setupSnapShot = () => {
        onSnapshot(SigninController.viewModel, (snap: SigninStore) => {
        })
    }

    /*
       Mark Data
   */


    /*
       Mark Event
   */

    public onCheckboxDidTouch = (value: boolean) => {
        SigninController.viewModel?.setIsValidate(value)
        SigninController.viewModel?.setEnableSubmitButton(this.verifyCheckBoxAndMembershipNumber())
    }

    public verifyCheckBoxAndMembershipNumber = (): boolean => {

        if (SigninController.viewModel.getIsValidate) {
            const { getMembershipNumber } = SigninController.viewModel

            if (getMembershipNumber != null) {
                if ((getMembershipNumber.length > 4) && (getMembershipNumber.length < 11)) {
                    return (true)
                }
            }
        }
        return (false)
    }

    public openPrivacy = () => {
        SigninController.rootStore?.setIsShowPrivacy(true)
    }

    public openTermsAndConditions = () => {
        SigninController.rootStore?.setIsShowTermOfUse(true)
    }


    public onMembershipChangeText = (value: string): void => {
        value = this.normalizeMembership(value)
        SigninController.viewModel?.setErrorLogin(null)
        SigninController.viewModel?.setShowSigninErrorMessage(false)

        const params = {
            value: value,
            localViewModel: SigninController.viewModel as any,
            globalViewModel: this._rootStore?.getUserStore
        }
        Validate.onMembershipChangeText(params)
        SigninController.viewModel?.setEnableSubmitButton(this.verifyCheckBoxAndMembershipNumber())

    }

    public normalizeMembership = (value: string): string => {
        value = value.replace(/[^\d]/g, "");
        if (value.length > 10) {
            return (value.substring(0, 10));
        }
        return value;
    }

    private checkLoginAccount = () => {
        const { ReactNativeConfig } = NativeModules

        const env: string = ReactNativeConfig.buildEnvironment
        switch (env) {
            // Microlearn
            case "microlearnProduction":
            case "microlearnDevelopment":
            case "microlearnStaging":
                return "@lyscirklen.com"                
                // return "@microlearn.com"

            // Lyscirklen
            case "LyscirklenProduction":
            case "LyscirklenDevelopment":
            case "LyscirklenStaging":
                // return "@business-danmark.com"
                return "@lyscirklen.com"
                default:
                // return "@business-danmark.com"
                return "@lyscirklen.com"                
        }
    }

    private loginFirebase = (memberNo: string, password: string, success: (user) => void, failed: (err: string) => void) => {
        const email: string = this.checkLoginAccount()
        let memberNumber = memberNo + email

        auth().signInWithEmailAndPassword(memberNumber, password)
            .then(function (user) {
                success(user)
            }).catch(function (err) {
                console.log(err.code);
                failed(err.code)
            });
    }

   

    public onSubmitButtonDidTouch = async () => {
        // public onSubmitButtonDidTouch  = (): void => {
        Keyboard.dismiss()
        /*
            1) Validate Data
            2) Sign-In 
        */
        if (this._rootStore?.getSharedStore?.getIsConnected) {
            if (SigninController.viewModel?.getIsValidate == true) {
                if (this._checkValidData() == true) {
                    this._isGlobalLoading(true)
                    const { getMembershipNumber } = SigninController.viewModel

                    //this.loginFirebase(this._rootStore?.getUserStore?.getMembershipNumber, 'Manao100%', (user) => {
                    this.loginFirebase(getMembershipNumber, 'Manao100%', (user) => {
                        SigninController.rootStore?.getAuthStore?.setIsLoggedIn(true)
                        this._isGlobalLoading(false)
                        SigninController.viewModel?.setIsValidate(false)

                        FirebaseFireStore.setLoginDate(getMembershipNumber)
                        // FirebaseFireStore.setLoginDate(this._rootStore?.getUserStore?.getMembershipNumber)

                        this.updateDeviceToken()

                        //firebase analytic
                        const uid = user.user.uid
                        FirebaseAnalytics.userLogin({
                            userId: uid,
                            organization_id: this._rootStore?.getOrganizationId
                        })

                    }, (err: string) => {
                        SigninController.viewModel?.setEnableSubmitButton(true)
                        SigninController.viewModel?.setShowSigninErrorMessage(true)
                        SigninController.viewModel?.setErrorLogin(SigninController.resourcesViewModel?.getResourceSigninError())
                        this._isGlobalLoading(false)
                    })

                }
            }
        } else {
            SigninController.viewModel?.setEnableSubmitButton(true)
            SigninController.viewModel?.setShowSigninErrorMessage(true)
            SigninController.viewModel?.setErrorLogin(SigninController.resourcesViewModel?.getResourceNoInternetUnableToLogin())
            this._isGlobalLoading(false)
        }
    }

    public updateDeviceToken = async () => {
        const token: string = await NotificationServices.getToken()
        // FirebaseFireStore.setDeviceToken(this._rootStore?.getUserStore?.getMembershipNumber, deviceInfoModule.getUniqueId(), token)

        const { getMembershipNumber } = SigninController.viewModel
        FirebaseFireStore.setDeviceToken(getMembershipNumber, deviceInfoModule.getUniqueId(), token)

    }

    public goToTermsAndConditions = (): void => {
        const { navigation } = this._myProps
        navigation?.dispatch(StackActions.push(NavigationKey.TermsAndConditions) as any)
    }

    public _checkValidData(): boolean {

        const { getMembershipNumber } = SigninController.viewModel

        if (getMembershipNumber != null) {
            if ((getMembershipNumber.length > 4) && (getMembershipNumber.length < 11)) {
                return (true);
            } else if (getMembershipNumber.length == 0) {
                SigninController.viewModel?.setErrorLogin(SigninController.resourcesViewModel?.getResourceErrorEmptyMembership())
            } else {
                SigninController.viewModel?.setErrorLogin(SigninController.resourcesViewModel?.getResourceErrorInvalidMembership())
            }
        } else {
            SigninController.viewModel?.setErrorLogin(SigninController.resourcesViewModel?.getResourceErrorEmptyMembership())
        }

        return (false);
    }

    public getPrivacyPolicy = async () => {
        const privacyPolicy = await SigninController.viewModel?.getPrivacyPolicy(this._rootStore?.getOrganizationId)

        if ((privacyPolicy) && (privacyPolicy.content)) {
            this._rootStore?.setPrivacyPolicy(privacyPolicy.content)
        }
    }

    public getTermOfUse = async () => {
        const termOfUse = await SigninController.viewModel?.getTermOfUse(this._rootStore?.getOrganizationId)
        if ((termOfUse) && (termOfUse.content)) {
            this._rootStore?.setTermOfUse(termOfUse.content)
        }

    }

    public getAboutUS = async () => {
        const aboutUS = await SigninController.viewModel?.getAboutUS(this._rootStore?.getOrganizationId)
        if ((aboutUS) && (aboutUS.content)) {
            this._rootStore?.setAboutUS(aboutUS.content)
        }

    }

    /*
       Mark Helper
   */

    async viewDidAppearOnce() {
        this.getPrivacyPolicy()
        this.getTermOfUse()
        this.getAboutUS()
    }

    // viewWillAppearOnce = async () => {
    //     if (this._rootStore?.getSharedStore?.getIsConnected) {
    //         SigninController.viewModel.setEnableSubmitButton(this.verifyCheckBoxAndMembershipNumber())
    //     }
    // }
}

export default SigninController