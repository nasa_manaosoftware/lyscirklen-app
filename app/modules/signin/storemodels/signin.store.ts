import { types } from "mobx-state-tree"
import { SigninActions, SigninViews, SigninPropsModel, SigninResourcesViews } from "../viewmodels"
import { StoreName } from "../../../constants/app.constant"
import { SigninServiceActions } from "../services/signin.services"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store"
import { GeneralResourcesServiceActions } from "../../../models/general-resources-store/services/general-resources.services"

const SigninModel = types.model(StoreName.Signin, SigninPropsModel)
const SigninLocalStore = types.compose(SigninModel, SigninActions, SigninViews, SigninServiceActions, GeneralResourcesServiceActions)
export const SigninStoreModel = SigninLocalStore
export const SigninResourcesStoreModel = types.compose(GeneralResourcesStoreModel, SigninResourcesViews)