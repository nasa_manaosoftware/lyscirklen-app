import { Instance, SnapshotOut } from "mobx-state-tree"
import { SigninStoreModel, SigninResourcesStoreModel } from "./signin.store"

export type SigninStore = Instance<typeof SigninStoreModel>
export type SigninStoreSnapshot = SnapshotOut<typeof SigninStoreModel>

export type SigninResourcesStore = Instance<typeof SigninResourcesStoreModel>
export type SigninResourcesStoreSnapshot = SnapshotOut<typeof SigninResourcesStoreModel>