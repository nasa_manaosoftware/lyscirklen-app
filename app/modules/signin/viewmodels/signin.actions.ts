import { SigninPropsModel } from "./signin.models"
import { types } from "mobx-state-tree"

export const SigninActions = types.model(SigninPropsModel).actions(self => {
    const setPassword = (value: string) => self.password = value
    const setMembershipNumber = (value: string) => self.membership_number = value
    const setIsValidate = (value: boolean) => self.isValidate = value
    const setErrorLogin = (value: string) => self.errorLogin = value
    const setShowSigninErrorMessage = (value: boolean) => self.showSigninErrorMessage = value
    const setEnableSubmitButton = (value: boolean) => self.enableSubmitButton = value
    return {
        setPassword,
        setIsValidate,
        setErrorLogin,
        setMembershipNumber,
        setShowSigninErrorMessage,
        setEnableSubmitButton
    }
})