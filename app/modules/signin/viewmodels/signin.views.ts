import { types } from "mobx-state-tree"
import { SigninPropsModel } from "./signin.models"
import { GeneralResources } from "../../../constants/firebase/remote-config"
import { NavigationKey } from "../../../constants/app.constant"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store"

export const SigninViews = types.model(SigninPropsModel).views(self => ({
    get getPassword() {
        return self.password
    },
    get getMembershipNumber() {
        return self.membership_number
    },
    
    get getIsValidate() {
        return self.isValidate
    },

    get getEnableSubmitButton() {
        return self.enableSubmitButton
    },

    get getShowSigninErrorMessage() {
        return self.showSigninErrorMessage
    },

    get getErrorMembershipNumber() {
        return self.errorMembershipNumber
    },

    get getLoginErrorMessage() {
        return self.errorLogin
    },

}))

export const SigninResourcesViews = GeneralResourcesStoreModel

    .views(self => {
        //MARK: Volatile State

        const { SigninScreen } = GeneralResources
        const { title, 
            membership_placeholder, 
            login_button, 
            acceptance_placeholder, 
            error_empty_membership,
            error_invalid_membership,
            error_signin,
            no_internet_unable_to_login,
            privacyPolicy,
            termsOfUse,
            andLink,
            termOfUseTitle,
            privacyPolicyTitle,
            loginDescription
         } = SigninScreen
        const { Signin } = NavigationKey

        //MARK: Views
        const getResources = (locale: string, key: string, childKeyOrShareKey: string | boolean = false, isChildDotNotation: boolean = false) => self.getValues(locale, childKeyOrShareKey ? key : Signin, childKeyOrShareKey ? true : key, isChildDotNotation)
        const getResourceTitle = (locale: string) => getResources(locale, title)
        const getResourceSubTitle = (locale: string) => getResources(locale, membership_placeholder)
        const getResourceDescription = (locale: string) => getResources(locale, login_button)
        const getResourceButtonAcceptTitle = (locale: string) => getResources(locale, login_button, true)
        const getResourcePrivacyTitle = (locale: string) => getResources(locale, login_button)
        const getResourceSigninButton= (locale: string) => getResources(locale, login_button)
        const getResourceSigninError= (locale: string) => getResources(locale, error_signin)
        const getResourceAcceptance= (locale: string) => getResources(locale, acceptance_placeholder)
        const getResourceMembership= (locale: string) => getResources(locale, membership_placeholder)

        const getResourceTermsOfUseLinkTitle= (locale: string) => getResources(locale, termsOfUse)
        const getResourcePrivacyLinkTitle= (locale: string) => getResources(locale, privacyPolicy)
        const getResourceAndLinkTitle= (locale: string) => getResources(locale, andLink)

        const getResourceErrorEmptyMembership= (locale: string) => getResources(locale, error_empty_membership)
        const getResourceErrorInvalidMembership= (locale: string) => getResources(locale, error_invalid_membership)
        const getResourceErrorSignin= (locale: string) => getResources(locale, error_signin)
        const getResourceNoInternetUnableToLogin= (locale: string) => getResources(locale, no_internet_unable_to_login)

        const getResourcesTermOfUseTitle = (locale: string) => getResources(locale, termOfUseTitle, true)
        const getResourcesPrivacyPolicyTitle = (locale: string) => getResources(locale, privacyPolicyTitle, true)

        const getResourcesLoginDecription = (locale: string) => getResources(locale, loginDescription)

        return {
            getResourceTitle,
            getResourceSubTitle,
            getResourceDescription,
            getResourceButtonAcceptTitle,
            getResourcePrivacyTitle,
            getResourcePrivacyLinkTitle,
            getResourceTermsOfUseLinkTitle,
            getResourceAndLinkTitle,

            getResourceSigninButton,
            getResourceSigninError,
            getResourceAcceptance,
            getResourceMembership,
            getResourceErrorEmptyMembership,
            getResourceErrorInvalidMembership,
            getResourceErrorSignin,
            getResourceNoInternetUnableToLogin,
            getResourcesTermOfUseTitle,
            getResourcesPrivacyPolicyTitle,
            getResourcesLoginDecription
        }
    })