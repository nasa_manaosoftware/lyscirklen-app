import { types } from "mobx-state-tree"

export const SigninPropsModel = {
    userId: types.maybeNull(types.string),
    errorMessageUserId: types.maybeNull(types.string),
    password: types.maybeNull(types.string),
    errorMessagePassword: types.maybeNull(types.string),
    isValidate: types.optional(types.boolean,false),
    modalVisible: types.optional(types.boolean,false),
    errorLogin: types.maybeNull(types.string),
    membership_number: types.maybeNull(types.string),
    errorMembershipNumber: types.maybeNull(types.string),
    showSigninErrorMessage: types.optional(types.boolean,false),

    enableSubmitButton: types.optional(types.boolean,false),

} 