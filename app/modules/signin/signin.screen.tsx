// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import { SigninProps } from "./signin.props"
import SigninController from "./signin.controllers"
import { Text, Wallpaper, Icon, Button } from "../../components"
import { SafeAreaView } from "react-navigation"
import { View, Keyboard, TouchableOpacity } from "react-native"
import { useConfigurate } from "../../custom-hooks/use-configure-controller"
import * as metric from "../../theme"
import MemberLabelTextField from '../../libs/member-label-textfield/member-label-text-field'
import * as Styles from "./signin.styles"
import { images } from "../../theme/images"
import { ModalTermComponents } from "../../components/modal-term/modal-term";
import { color } from "../../theme"

export const SigninScreen: React.FunctionComponent<SigninProps> = observer((props) => {

    const controller = useConfigurate(SigninController, props) as SigninController
    // MARK: Render
    const yesIconSource: string = SigninController.viewModel?.getIsValidate ? images.selected : images.unSelected

    const isConnected = SigninController.rootStore?.getSharedStore?.getIsConnected

    const MembershipView = () => {
        return (
            <View style={Styles.MEMBERSHIP_VIEW}>
                <MemberLabelTextField
                    placeholder={SigninController.resourcesViewModel?.getResourceMembership()}
                    onChangeTextValue={controller.onMembershipChangeText}
                    // noBorder
                    returnKeyType="next"
                    keyboardType={'phone-pad'}
                    value={SigninController.viewModel.getMembershipNumber}
                    errorMessage={SigninController.viewModel?.getLoginErrorMessage}

                />
                {
                    SigninController.rootStore?.getOrganizationId === 'lyscirklen' ?
                        <View style={{width: '100%'}}>
                            {
                                SigninController.viewModel?.getShowSigninErrorMessage ? null :

                                    <Text style={Styles.TEXT_LOGIN_DESCRIPTION}>
                                        {SigninController.resourcesViewModel?.getResourcesLoginDecription()}
                                    </Text>
                            }
                        </View>
                        :
                        null
                }

            </View>
        )
    }

    const TermsAndConditionView = () => {
        return (
            <View style={Styles.TERMS_AND_AGREEMENT_VIEW}>
                <View shouldRasterizeIOS style={Styles.CHECK_VIEW}>
                    <Button preset="none" isAnimated={false} isSolid style={Styles.CHECK} onPress={() => controller.onCheckboxDidTouch(!SigninController.viewModel.getIsValidate)} >
                        <Icon source={yesIconSource} style={Styles.ICON_CHECK} />
                    </Button>
                    <View shouldRasterizeIOS style={Styles.PRIVACY_VIEW}>
                        <Text style={Styles.TERMS_AND_AGREEMENT}>
                            {SigninController.resourcesViewModel?.getResourceAcceptance()}
                        </Text>

                        <Text text={SigninController.resourcesViewModel?.getResourceTermsOfUseLinkTitle()}
                            style={Styles.PRIVACY_LINK_TEXT} onPress={() => controller.openTermsAndConditions()} />

                        <Text text={SigninController.resourcesViewModel?.getResourceAndLinkTitle()}
                            style={Styles.TERMS_AND_AGREEMENT} />

                        <Text text={SigninController.resourcesViewModel?.getResourcePrivacyLinkTitle()}
                            style={Styles.PRIVACY_LINK_TEXT} onPress={() => controller.openPrivacy()} />

                    </View>

                </View>

                <View style={Styles.SUBMIT_VIEW}>
                    <Button preset="none"
                        // style={[Styles.BUTTON_VIEW, { backgroundColor: SigninController.viewModel.getEnableSubmitButton ? '#FF5634' : '#949494' }]}
                        style={[Styles.BUTTON_VIEW, { backgroundColor: SigninController.viewModel.getEnableSubmitButton ? color.loginBackground : color.loginDisableBackground }]}
                        textStyle={Styles.SUBMIT_TEXT}
                        text={SigninController.resourcesViewModel?.getResourceSigninButton().toUpperCase()}
                        onPress={SigninController.viewModel.getEnableSubmitButton ? controller.onSubmitButtonDidTouch : null}
                    />
                </View>

            </View>

        )

    }

    const renderPrivacyPolicy = () => {
        return (
            <ModalTermComponents
                title={SigninController.resourcesViewModel?.getResourcesPrivacyPolicyTitle()}
                visible={SigninController.rootStore?.getIsShowPrivacy}
                description={SigninController.rootStore?.getPrivacyPolicy}
                onPressed={() => SigninController.rootStore?.setIsShowPrivacy(false)}
                isConnected={isConnected} />
        )
    }

    const renderModalTerm = () => {
        return (
            <ModalTermComponents
                title={SigninController.resourcesViewModel?.getResourcesTermOfUseTitle()}
                visible={SigninController.rootStore?.getIsShowTermOfUse}
                description={SigninController.rootStore?.getTermOfUse}
                onPressed={() => SigninController.rootStore?.setIsShowTermOfUse(false)}
                isConnected={isConnected} />
        )
    }

    return (
        <Wallpaper showBubble>
            <SafeAreaView
                {...metric.safeAreaViewProps}
                shouldRasterizeIOS
                style={[Styles.SAFE_AREA_VIEW, { marginTop: isConnected ? metric.ratioHeight(0) : metric.ratioHeight(metric.isIPhone ? -80 : -50) }]}
            >
                <TouchableOpacity
                    onPress={() => { Keyboard.dismiss() }}
                    activeOpacity={1}
                >


                    <View style={Styles.MAIN_CONTAINER_VIEW}>

                        <View shouldRasterizeIOS style={Styles.TITLE_CONTAINNER}>
                            <View shouldRasterizeIOS style={Styles.TITLE_SPACE} />
                            <Text
                                style={Styles.TITLE_TEXT}
                                text={metric.isMicrolearn() ? SigninController.resourcesViewModel?.getResourceTitle() : SigninController.resourcesViewModel?.getResourceTitle().toUpperCase()} />

                        </View>


                        {MembershipView()}
                        {TermsAndConditionView()}
                        {renderPrivacyPolicy()}
                        {renderModalTerm()}

                    </View>

                </TouchableOpacity>

            </SafeAreaView>
        </Wallpaper>


    )
})
