import { RootStore } from "../models/root-store/root.types";
import { INavigationState } from "../models/navigation-store/navigation.types";
import { BaseController, BaseProps, IAlertParams } from "./base.controller";
import { MaterialTopTabBarProps } from "@react-navigation/material-top-tabs";
import { MaterialTopTabDescriptorMap } from "@react-navigation/material-top-tabs/lib/typescript/src/types";
import { TextStyle, Alert } from "react-native";
import { tabNavigationOptions } from "../theme";

export class TabController extends BaseController {

    /*
       Mark Inherited Variable & Declaration
   */

    protected _tabState: INavigationState
    protected _tabProps: MaterialTopTabBarProps

    /*
       Mark Setup
   */

    constructor(rootStore?: RootStore, props?: BaseProps) {
        super(rootStore, props)
    }
    protected _setupTabPageState = (state?: INavigationState) => {
        this._tabState = state
        this._onTabPageChange()
    }

    public setupChildProps = (props: MaterialTopTabBarProps) => {
        this._tabProps = props
        this._setupTabPageState((props as any)?.state)
    }

    /*
       Mark Data
   */

    public tapTitleStyle: TextStyle = {

    }

    public getTapTitle = (descriptors: MaterialTopTabDescriptorMap, routeKey: string, route: any) => {
        const { options } = descriptors[routeKey]
        const title =
            options.tabBarLabel !== undefined
                ? options.tabBarLabel
                : options.title !== undefined
                    ? options.title
                    : route.name
        return title
    }

    /*
       Mark Event
   */

    protected _onTabPageChange = () => {
        const index = this._tabState?.index
        const routeNames = this._tabState?.routeNames
    }

    /**
     * 
     * IMPORTANT: Don't use => function Both Parent and Child Module. if you want to override base methods and call `super`. Otherwise the super will be undefined.
     * 
     */

    public onPress(route: any, index: number, params: object = { isInitialized: false }) {
        const isFocused = this._tabProps?.state.index === index
        const event = this._tabProps?.navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true
        })

        if (!isFocused && !event.defaultPrevented) {
            this._tabProps?.navigation.navigate(route.name, params)
        }

        switch (index) {
            case 0:
                TabController.rootStore?.setIsSelectedFront(true),
                    TabController.rootStore?.setIsSelectedSearch(false),
                    TabController.rootStore?.setIsSelectedMyLibrary(false),
                    TabController.rootStore?.setIsSelectedNotofication(false),
                    TabController.rootStore?.setIsSelectedAboutUs(false),
                    TabController.rootStore?.setIsSelectedTermOfUse(false),
                    TabController.rootStore?.setIsSelectedPrivacy(false)
                break;
            case 1:
                TabController.rootStore?.setIsSelectedFront(false),
                    TabController.rootStore?.setIsSelectedSearch(false),
                    TabController.rootStore?.setIsSelectedMyLibrary(false),
                    TabController.rootStore?.setIsSelectedNotofication(false),
                    TabController.rootStore?.setIsSelectedAboutUs(true),
                    TabController.rootStore?.setIsSelectedTermOfUse(false),
                    TabController.rootStore?.setIsSelectedPrivacy(false)

                break;
            default:
                break;
        }
    }

    public onLongPress(route: any) {
        this._tabProps?.navigation.emit({
            type: 'tabLongPress',
            target: route.key,
        })
    }

    /*
        Mark Utils
    */

    protected static _generalAlertOS = (params: IAlertParams) => {
        TabController._delayExecutor(() => Alert.alert(params.title, params.message), params?.delay || 1000)
    }
}