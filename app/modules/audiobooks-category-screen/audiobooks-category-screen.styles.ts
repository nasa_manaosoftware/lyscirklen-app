import { TextStyle, ViewStyle } from "react-native"
import { color } from "../../theme"
import * as metric from "../../theme"
import { size, type } from "../../theme/fonts"
import { RFValue } from "react-native-responsive-fontsize"

export const CONTAINER: ViewStyle = {
    flex: 1,
    backgroundColor: color.background
}

export const TOPIC_VIEW_CATEGORY: ViewStyle = {
    flex: 0,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingHorizontal: metric.ratioWidth(16),
    marginTop: metric.ratioHeight(21),
    marginBottom: metric.ratioHeight(15)
}

export const URGENT_TITLE_BORDER: ViewStyle = {
    backgroundColor: color.redLine,
    width: metric.ratioWidth(2),
    height: metric.isMicrolearn() ? metric.ratioHeight(22) : metric.ratioHeight(18),
    marginRight: metric.ratioWidth(11)
}

export const TOPIC_TITLE_TEXT: TextStyle = {
    marginTop: metric.isMicrolearn() ? null : metric.ratioHeight(5),
    marginBottom: metric.isMicrolearn() ? metric.ratioHeight(2) : null,
    fontFamily: type.base,
    fontSize: size.header,
    color: color.whiteText
}

export const AUDIOBOOKS_LIST_CONTAINER: ViewStyle = {
    flex: 1
}

export const FLATLIST_CONTAINER: ViewStyle = {
    marginLeft: metric.isTablet() ? metric.ratioWidth(13) : metric.ratioWidth(14),
    paddingTop: metric.ratioHeight(10),
    paddingBottom: metric.ratioHeight(20),
    paddingHorizontal: metric.ratioWidth(17)
}

export const ITEMSEPERATOR: ViewStyle = {
    marginBottom: metric.ratioHeight(30)
}