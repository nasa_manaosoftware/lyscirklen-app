import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"

export interface AudiobooksCategoryScreenProps extends NavigationContainerProps<ParamListBase> {
    category_id: string,
    categoryName: string
}