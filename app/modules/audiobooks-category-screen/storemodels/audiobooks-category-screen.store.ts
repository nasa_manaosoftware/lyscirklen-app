import { types } from "mobx-state-tree"
import { AudiobooksCategoryScreenViews, AudiobooksCategoryScreenActions, AudiobooksCategoryScreenResourcesViews } from "../viewmodels"
import { AudiobooksCategoryScreenPropsModel } from "../viewmodels"
import { StoreName } from "../../../constants/app.constant"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store/general-resources.store"
import { AudiobookStoreModel } from "../../../models/audiobook-store"
import { AudiobookCategoriesStoreModel } from "../../../models/audiobook-category-store"
import { UserStoreModel } from "../../../models/user-store/user.store"
import { DownloadedStoreModel } from "../../../models/downloaded-store/downloaded.store"
import { AudioBookCategoryService } from "../services/audiobooks-category-screen.services"

const AudiobooksCategoryScreenModel = types.model(StoreName.AudiobooksCategory, AudiobooksCategoryScreenPropsModel)

export const AudiobooksCategoryScreenStoreModel = types.compose(
    AudiobooksCategoryScreenModel,
    AudiobooksCategoryScreenViews,
    AudiobooksCategoryScreenActions,
    AudiobookStoreModel,
    AudiobookCategoriesStoreModel,
    UserStoreModel,
    DownloadedStoreModel,
    AudioBookCategoryService
)

export const AudiobooksCategoryScreenResourcesStoreModel = types.compose(GeneralResourcesStoreModel, AudiobooksCategoryScreenResourcesViews)

