import { Instance, SnapshotOut } from "mobx-state-tree"
import { AudiobooksCategoryScreenResourcesStoreModel, AudiobooksCategoryScreenStoreModel } from "./audiobooks-category-screen.store"

export type AudiobooksCategoryScreenStore = Instance<typeof AudiobooksCategoryScreenStoreModel>
export type AudiobooksCategoryScreenStoreSnapshot = SnapshotOut<typeof AudiobooksCategoryScreenStoreModel>

export type AudiobooksCategoryScreenResourcesStore = Instance<typeof AudiobooksCategoryScreenResourcesStoreModel>
export type AudiobooksCategoryScreenResourcesStoreSnapshot = SnapshotOut<typeof AudiobooksCategoryScreenResourcesStoreModel>