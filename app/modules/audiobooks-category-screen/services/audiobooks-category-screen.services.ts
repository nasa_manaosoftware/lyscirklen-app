import { types, flow } from "mobx-state-tree"
import DownloadServices from '../../../services/api-domain/download.service'
import { DownloadedPropsModel } from "../../../models/downloaded-store/downloaded.models"
import { RootStore } from "../../../models/root-store/root.types"

export const AudioBookCategoryService = types.model(DownloadedPropsModel).actions(self => {
    const downloadBook = flow(function* (rootStore: RootStore, book) {
        try {
            const result = DownloadServices.downloadedAudioBook(rootStore, book)
            return result
        } catch (err) {
            return err
        }
    })
    return { 
        downloadBook
    }
})