import { DrawerActions, StackActions } from "@react-navigation/core";
import { onSnapshot } from "mobx-state-tree";
import { DownloadStatus, NavigationKey } from "../../constants/app.constant";
import { INavigationRoute } from "../../models";
import { RootStore } from "../../models/root-store/root.types";
import { BaseController } from "../base.controller";
import { AudiobooksCategoryScreenProps } from "./audiobooks-category-screen.props";
import { AudiobooksCategoryScreenResourcesStoreModel, AudiobooksCategoryScreenStoreModel } from "./storemodels/audiobooks-category-screen.store";
import { AudiobooksCategoryScreenResourcesStore, AudiobooksCategoryScreenStore } from "./storemodels/audiobooks-category-screen.types";
import FirebaseFireStore from "../../utils/firebase/fire-store/fire-store";
import { DOWNLOADEDBOOK } from "../../models/downloaded-store/downloaded.types";
import { Alert, PermissionsAndroid, Platform } from "react-native";
import FirebaseAnalytics from "../../utils/firebase/analytics/analytics";
import analytics from '@react-native-firebase/analytics'

class AudiobooksCategoryScreenController extends BaseController {
    onClickAudiobook(item: unknown): void {
        throw new Error("Method not implemented.");
    }

    /*
       Stateful Variable
   */

    public static viewModel: AudiobooksCategoryScreenStore
    public static resourcesViewModel: AudiobooksCategoryScreenResourcesStore
    public static myProps: AudiobooksCategoryScreenProps & Partial<INavigationRoute>

    public static navigateTo: string
    public static category_name: string
    /*
        Mark Constructor
    */

    constructor(rootStore?: RootStore, props?: AudiobooksCategoryScreenProps & Partial<INavigationRoute>) {
        super(rootStore, props)
        if (this._myProps?.route?.params?.isInitialized) return
        this._setupResourcesViewModel()
        this._setupNavigation()
        this._setupProps(props)
        this._setupViewModel()
        this._setupSnapShot()
    }

    /*
       Mark Setup
    */

    private _setupResourcesViewModel = () => {
        AudiobooksCategoryScreenController.resourcesViewModel = AudiobooksCategoryScreenResourcesStoreModel.create({ ...this._rootStore?.getAllGeneralResourcesStore })
    }
    private _setupViewModel = () => {
        const localStore = this._rootStore?.getModuleStore(NavigationKey.AudiobooksCategory)
        AudiobooksCategoryScreenController.viewModel = localStore && AudiobooksCategoryScreenStoreModel.create({ ...localStore }) || AudiobooksCategoryScreenStoreModel.create({})
        this._setInitializedToPropsParams()
    }

    private _setupSnapShot = () => {
        onSnapshot(AudiobooksCategoryScreenController.viewModel, (snap: AudiobooksCategoryScreenStore) => {
            this._rootStore?.setModuleStoreValue(NavigationKey.AudiobooksCategory, snap)
        })
    }

    _setupProps = (myProps: AudiobooksCategoryScreenProps & Partial<INavigationRoute>) => {
        super._setupProps && super._setupProps(myProps)
        AudiobooksCategoryScreenController.myProps = {
            ...myProps as object,
            ...myProps?.route?.params
        }
    }

    public onSideMenuPress = () => {
        this._myProps?.navigation?.dispatch(DrawerActions.openDrawer() as any)
    }

    public queryCategoryName = (book) => {
        const categoryStore = AudiobooksCategoryScreenController.rootStore?.getAudioBookCategoriesStore?.getAudiobooksCetegories

        categoryStore.map(item => {
            if (book instanceof Array) {
                if (item.uid === book[0].category_id) {
                    AudiobooksCategoryScreenController.category_name = item.name
                }
            } else {
                if (item.uid === book.category_id) {
                    AudiobooksCategoryScreenController.category_name = item.name
                }
            }
        })
    }


    public onPressAudiobookCoverAndNameAndInfoIcon = (item: any) => {
        if (AudiobooksCategoryScreenController.viewModel?.getIsDisabled) return
        AudiobooksCategoryScreenController.viewModel?.setIsDisabled(true)
        AudiobooksCategoryScreenController.navigateTo = 'audiobook detail'

        this.queryCategoryName(item)

        //firebase analytic
        FirebaseAnalytics.openVideoDetail({
            user_id: this._rootStore?.getUserStore?.getUserId,
            book_name: item?.name,
            category_name: AudiobooksCategoryScreenController.category_name
        })

        if (item.video_url === '') {
            //firebase analytic
            FirebaseAnalytics.amountVideoAttached({
                userId: this._rootStore?.getUserStore.getUserId,
                book_name: item.name,
                category_name: AudiobooksCategoryScreenController.category_name,
                video_url: item.video_url
            })
        }

        const navigation = this._myProps?.navigation
        const params = {
            id: item.uid
        }
        navigation?.dispatch(StackActions.push(NavigationKey.AudioBookScreen, params) as any)
        this._delayExecutor(() => AudiobooksCategoryScreenController.viewModel?.setIsDisabled(false), 1200)
    }

    public checkAudiobookDownloaded = (audiobooks: any, downloadedList: any) => {
        return audiobooks.map(item1 => {
            downloadedList.map(item2 => {
                if (item1.uid === item2.uid) {
                    item1.download_status = DownloadStatus.SUCCESS
                    item1.file_path = item2.file_path
                }
            })
            return item1
        })
    }

    public fetchAudiobooksList = async () => {
        const categoryId = AudiobooksCategoryScreenController.myProps?.category_id
        const organizationId = AudiobooksCategoryScreenController.rootStore?.getOrganizationId

        const audiobooksList = AudiobooksCategoryScreenController.rootStore?.getAudioBooksStore?.getAudiobooksList
        const audioList = []

        audiobooksList.map(item1 => {
            if (item1.category_id === categoryId) {
                if (item1.organization_id === organizationId) {
                    audioList.push(item1)
                }
            }
        })
        // const downloadedList = AudiobooksCategoryScreenController.rootStore?.getDownloadedStore?.getDownloadedList
        // const audiobooksResult = this.checkAudiobookDownloaded(audioList, downloadedList)
        this.sortingBookName(audioList)
    }

    public sortingBookName = (audiobooksResult) => {
        const audiobooks = audiobooksResult.sort((firstBook, secondBook) => firstBook.created_date > secondBook.created_date ? -1 : 1)
        AudiobooksCategoryScreenController.rootStore?.getAudioBookCategoriesStore.setAudiobooks(audiobooks)
    }

    public onRefresh = () => {
        AudiobooksCategoryScreenController.viewModel?.setIsRefresh(true)
        this.fetchAudiobooksList()
        this._delayExecutor(() => AudiobooksCategoryScreenController.viewModel?.setIsRefresh(false), 1000)
    }

    public onHandleLoadmore = async () => {
        if (AudiobooksCategoryScreenController.viewModel?.getIsLoadMore === true) return
        AudiobooksCategoryScreenController.viewModel?.setIsLoadMore(true)

        const lastId = AudiobooksCategoryScreenController.viewModel?.getLastAudiobookId
        const categoryId = AudiobooksCategoryScreenController.myProps.category_id
        const organizationId = AudiobooksCategoryScreenController.rootStore?.getOrganizationId
        const newAudiobooks = await FirebaseFireStore.fetchAudiobooksWhenLoadMore(categoryId, lastId, organizationId)
        const downloadedList = AudiobooksCategoryScreenController.rootStore?.getDownloadedStore?.getDownloadedList
        const audiobooksResult = this.checkAudiobookDownloaded(newAudiobooks.resultAudiobooksList, downloadedList)

        if (AudiobooksCategoryScreenController.rootStore?.getAudioBookCategoriesStore.getAudiobooks.length === 0) {
            AudiobooksCategoryScreenController.rootStore?.getAudioBookCategoriesStore.setAudiobooks(audiobooksResult)
        } else {
            AudiobooksCategoryScreenController.rootStore?.getAudioBookCategoriesStore.setAudiobooks([
                ...AudiobooksCategoryScreenController.rootStore?.getAudioBookCategoriesStore.getAudiobooks,
                ...audiobooksResult
            ])
        }
        AudiobooksCategoryScreenController.viewModel?.setLastAudiobookId(newAudiobooks.lastAudiobook_id)
        AudiobooksCategoryScreenController.viewModel?.setIsLoadMore(false)
    }


    public onDownloaded = async (book, result) => {
        const params = {
            deviceId: AudiobooksCategoryScreenController.rootStore?.getDeviceId,
            file_path: result.file_path,
            download_status: result.download_status,
            job_id: result.job_id,
            audiobook_id: book.uid,
            category_id: book.category_id,
            description: book.description,
            forfatter: book.forfatter,
            image: {
                file_name: book.image.file_name,
                storage_path: book.image.storage_path,
                thumbnail_url: book.image.thumbnail_url,
                url: book.image.url
            },
            name: book.name,
            organization_id: book.organization_id,
            video: {
                name: book.video?.name,
                url: book.video?.url,
                download_url: book.video?.download_url,
                streaming_url: book.video?.streaming_url
            }
        }
        const resultData = await FirebaseFireStore.updateDownloadedAudioBook(params)
        await this.onFetchDownloaded()
    }

    public onFetchDownloaded = async () => {
        const organizationId = AudiobooksCategoryScreenController.rootStore?.getOrganizationId

        //update audiobook downloaded in download store
        const downloaded = await FirebaseFireStore.fetchMyDownloaded(AudiobooksCategoryScreenController.rootStore?.getDeviceId, organizationId)
        AudiobooksCategoryScreenController.rootStore?.getDownloadedStore?.setDownloadedList(downloaded)

        //update audiobook downloaded in category store
        const updateDataDownloaded = this.checkAudiobookDownloaded(AudiobooksCategoryScreenController.rootStore?.getAudioBookCategoriesStore.getAudiobooks, downloaded)
        AudiobooksCategoryScreenController.rootStore?.getAudioBookCategoriesStore.setAudiobooks(updateDataDownloaded)
    }

    public updateDownloadingOnCategory = (audiobooks, audiobooksList) => {

        return audiobooks.map(item1 => {
            audiobooksList.map(item2 => {
                if (item1.uid === item2.uid) {
                    if (item2.is_downloading) {
                        item1.is_downloading = item2.is_downloading
                    } else {
                        item1.download_status = item2.download_status
                        item1.is_downloading = item2.is_downloading
                    }
                }
            })
            return item1
        })
    }


    public onUpdateStatusDownloading = (book) => {
        const audioBooks = AudiobooksCategoryScreenController.rootStore?.getAudioBooksStore.getAudiobooksList
        let data = []
        audioBooks.map(item => {
            if (item.uid === book.uid) {
                data.push({
                    ...item,
                    is_downloading: true
                })
            }
            else {
                data.push({
                    ...item
                })
            }
        })
        //update is_download on root store
        AudiobooksCategoryScreenController.rootStore?.getAudioBooksStore.setAudiobooksList(data)

        //update is_download on this screen
        const result = this.updateDownloadingOnCategory(AudiobooksCategoryScreenController.rootStore?.getAudioBookCategoriesStore.getAudiobooks, AudiobooksCategoryScreenController.rootStore?.getAudioBooksStore?.getAudiobooksList)
        AudiobooksCategoryScreenController.rootStore?.getAudioBookCategoriesStore.setAudiobooks(result)

        //download service
        this.downloadProgress(book)

    }

    public onUpdateDownloadFailed = (book) => {
        const audioBooks = AudiobooksCategoryScreenController.rootStore?.getAudioBookCategoriesStore.getAudiobooks
        let data = []
        audioBooks.map(item => {
            if (item.uid === book.uid) {
                data.push({
                    ...item,
                    is_downloading: false
                })
            }
            else {
                data.push({
                    ...item
                })
            }
        })

        AudiobooksCategoryScreenController.rootStore?.getAudioBookCategoriesStore.setAudiobooks(data)
    }


    public downloadProgress = async (book) => {
        const result = await AudiobooksCategoryScreenController.viewModel?.downloadBook(this._rootStore, book)
        if (result.download_status === DownloadStatus.SUCCESS) {
            this.onDownloaded(book, result)
        } else if (result.download_status === DownloadStatus.FAILED) {
            // Alert.alert('Download failed')
            Alert.alert("", AudiobooksCategoryScreenController.resourcesViewModel?.getResourceNoInternetUnableToDownload(AudiobooksCategoryScreenController.rootStore?.getLanguage))
            this.onUpdateDownloadFailed(book)
        }
        this._delayExecutor(() => AudiobooksCategoryScreenController.viewModel?.setIsDisabled(false), 1200)
    }

    public onPressDownload = async (book) => {

        if (this._rootStore?.getSharedStore?.getIsConnected == false) {
            Alert.alert("", AudiobooksCategoryScreenController.resourcesViewModel?.getResourceNoInternetUnableToDownload(AudiobooksCategoryScreenController.rootStore?.getLanguage))
            return
        }

        if (Platform.OS === 'ios') {
            this.onUpdateStatusDownloading(book)
        } else {

            const result = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE)

            if (result === PermissionsAndroid.RESULTS.GRANTED) {
                this.onUpdateStatusDownloading(book)
            } else {
                Alert.alert('The permission is denied but requestable')
            }
        }
    }

    public onFetchAudioList = () => {

        const audiobooksList = AudiobooksCategoryScreenController.rootStore?.getAudioBooksStore?.getAudiobooksList
        const audiobooks = AudiobooksCategoryScreenController.rootStore?.getAudioBookCategoriesStore.getAudiobooks
        
        audiobooksList.map(item1 => {
            audiobooks.map(item2 => {
                if (item1.is_downloading) {
                    if (item1.uid === item2.uid) {
                        item2.is_downloading = item1.is_downloading
                    }
                } else {
                    if (item1.uid === item2.uid) {
                        item2.download_status = item1.download_status,
                            item2.file_path = item1.file_path
                    }
                }
                return item2
            })
        })
        AudiobooksCategoryScreenController.rootStore?.getAudioBookCategoriesStore.setAudiobooks(audiobooks)
    }

    // public queryCategoryName = (book) => {
    //     const categoryStore = AudiobooksCategoryScreenController.rootStore?.getAudioBookCategoriesStore?.getAudiobooksCetegories

    //     categoryStore.map(item => {
    //         if (item.uid === book.category_id) {
    //             AudiobooksCategoryScreenController.category_name = item.name
    //         }
    //     })
    // }

    public onPressPlayback = (audiobook: any) => {

        if (this._rootStore?.getSharedStore?.getIsConnected) {

            if (((audiobook as DOWNLOADEDBOOK).download_status == DownloadStatus.DO_NOT_DOWNLOAD) && (this._rootStore?.getSharedStore?.getIsConnected == false)) {
                Alert.alert("", AudiobooksCategoryScreenController.resourcesViewModel?.getResourceNoInternetUnableToPlay(AudiobooksCategoryScreenController.rootStore?.getLanguage))
                return
            }

            this.queryCategoryName(audiobook)
            this._rootStore?.resetPlaybackStore()
            setTimeout(() => {
                this._rootStore?.getPlaybackStore?.play(({ ...audiobook }) as DOWNLOADEDBOOK)
            }, 300)

            AudiobooksCategoryScreenController.viewModel?.setIsDisabled(true)
            this._delayExecutor(() => AudiobooksCategoryScreenController.viewModel?.setIsDisabled(false), 1200)

            //firebase analytic
            FirebaseAnalytics.videoInitialPlayed({
                book_name: audiobook.name,
                category_name: AudiobooksCategoryScreenController.category_name
            })

            FirebaseAnalytics.amountVideoPlayedPerUser({
                user_number: this._rootStore?.getUserStore?.getUserId,
                book_name: audiobook.name,
                category_name: AudiobooksCategoryScreenController.category_name
            })

        } else {
            const locale = AudiobooksCategoryScreenController.rootStore?.getLanguage
            Alert.alert("", AudiobooksCategoryScreenController.resourcesViewModel?.getResourceNoInternetUnableToPlay(locale))
        }

    }

    viewDidAppearAfterFocus = async () => {
        console.log("getCurrentPageName", this._rootStore?.getNavigationStore?.getCurrentPageName)
        if (this._rootStore?.getNavigationStore?.getCurrentPageName === NavigationKey.FrontScreen) {
            AudiobooksCategoryScreenController.viewModel?.setIsLoading(true)
            AudiobooksCategoryScreenController.viewModel?.setIsLoadMore(false)
            this.fetchAudiobooksList()
            this._delayExecutor(() => AudiobooksCategoryScreenController.viewModel?.setIsLoading(false), 1500)
        }
    }

    viewWillDisappear = async () => {

        if (AudiobooksCategoryScreenController.rootStore?.getAudioBookCategoriesStore.getAudiobooks.length > 0 &&
            AudiobooksCategoryScreenController.navigateTo !== 'audiobook detail') {
            AudiobooksCategoryScreenController.rootStore?.getAudioBookCategoriesStore.clearAudiobooksCategoryResult();
            AudiobooksCategoryScreenController.viewModel?.setLastAudiobookId("")
        }
        AudiobooksCategoryScreenController.navigateTo = ''
        AudiobooksCategoryScreenController.viewModel?.setIsLoadMore(false)
        AudiobooksCategoryScreenController.viewModel?.setIsRefresh(false)
    }

}

export default AudiobooksCategoryScreenController