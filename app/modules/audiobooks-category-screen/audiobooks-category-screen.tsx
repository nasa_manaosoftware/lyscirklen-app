import React, { useEffect } from "react"
import { AudiobooksCategoryScreenProps } from "./audiobooks-category-screen.props"
import { observer } from "mobx-react-lite"
import { useConfigurate } from "../../custom-hooks/use-configure-controller"
import AudiobooksCategoryScreenController from "./audiobooks-category-screen.controller"
import { View, Text, RefreshControl, ActivityIndicator, Alert, FlatList } from "react-native"
import * as Styles from "./audiobooks-category-screen.styles"
import { AudioBook } from "../../components/audio-book/audio-book"
import { color } from "../../theme/color"
import { Platform } from "react-native";
import { images } from "../../theme/images"

export const AudiobooksCategoryScreen: React.FunctionComponent<AudiobooksCategoryScreenProps> = observer((props) => {
    const controller = useConfigurate(AudiobooksCategoryScreenController, props) as AudiobooksCategoryScreenController
    const disabled = AudiobooksCategoryScreenController.viewModel?.getIsDisabled
    const isLoading = AudiobooksCategoryScreenController.viewModel?.getIsLoading
    const audiobooksList = AudiobooksCategoryScreenController.rootStore?.getAudioBookCategoriesStore.getAudiobooks

    useEffect(() => {
        console.log("isLoading", isLoading)
    }, [])

    useEffect(() => {

        controller.onFetchAudioList()

    }, [AudiobooksCategoryScreenController.rootStore?.getAudioBooksStore.getAudiobooksList])

    return (
        <View style={Styles.CONTAINER}>
            <View shouldRasterizeIOS style={Styles.TOPIC_VIEW_CATEGORY}>
                <View style={Styles.URGENT_TITLE_BORDER} />
                <Text style={Styles.TOPIC_TITLE_TEXT}>{AudiobooksCategoryScreenController.myProps.categoryName} </Text>
            </View>

            {AudiobooksCategoryScreenController.viewModel?.getIsRefresh && Platform.OS === 'ios' || isLoading ?
                <View style={isLoading ? { flex: 1 } : null}>
                    <ActivityIndicator style={isLoading ? { flex: 1 } : null} size="large" color={color.white} />
                </View>
                :
                <View shouldRasterizeIOS style={Styles.AUDIOBOOKS_LIST_CONTAINER}>
                    <FlatList
                        refreshControl={
                            <RefreshControl
                                refreshing={AudiobooksCategoryScreenController.viewModel?.getIsRefresh}
                                onRefresh={() => controller.onRefresh()}
                            />
                        }
                        contentContainerStyle={Styles.FLATLIST_CONTAINER}
                        extraData={audiobooksList?.slice()}
                        data={audiobooksList}
                        keyExtractor={(item, index) => index.toString()}
                        initialNumToRender={10}
                        onEndReachedThreshold={0.1}
                        onEndReached={controller.onHandleLoadmore}
                        ItemSeparatorComponent={() =>
                            (<View shouldRasterizeIOS style={Styles.ITEMSEPERATOR} />)
                        }
                        renderItem={({ item, index }) => 
                            <AudioBook
                                key={index}
                                disabled={disabled}
                                title={item.name}
                                author={item.forfatter}
                                bookCover={item.image.url ? item.image.url : null}
                                // downloadStatus={item.download_status}
                                isMyLibrary={false}
                                hasVideoUrl={item.video_url ? true : false}
                                onPressPlayBackButton={() => controller.onPressPlayback(item)}
                                onPressBookCover={() => controller.onPressAudiobookCoverAndNameAndInfoIcon(item)}
                                onPressBookName={() => controller.onPressAudiobookCoverAndNameAndInfoIcon(item)}
                                onPressInfoIcon={() => controller.onPressAudiobookCoverAndNameAndInfoIcon(item)}
                                onPressAuthorName={() => controller.onPressAudiobookCoverAndNameAndInfoIcon(item)}
                                // onPressDownload={() => controller.onPressDownload(item)}
                                // isDownloading={item.is_downloading}
                            />
                        }
                    />
                </View>
            }
        </View>
    )
})