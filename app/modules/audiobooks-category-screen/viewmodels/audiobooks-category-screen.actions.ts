import { types } from "mobx-state-tree"
import { AudiobooksCategoryScreenPropsModel } from "./audiobooks-category-screen.models"

export const AudiobooksCategoryScreenActions = types.model(AudiobooksCategoryScreenPropsModel).actions(self => {
    const setIsDisabled = (value: boolean) => {
        self.isDisabled = value
    }

    const setIsRefresh = (value: boolean) => {   
        self.isRefresh = value     
    }

    const setOnEndReachedCalledDuringMomentum = (value: boolean) => {   
        self.onEndReachedCalledDuringMomentum = value     
    }

    const setLastAudiobookId = (value: string) => {
        self.lastAudiobookId = value      
    }

    const setIsLoadMore = (value: boolean) => {   
        self.isLoadMore = value     
    }

    const setIsLoading = (value: boolean) => { 
        self.isLoading = value  
    }

    const setCategoryId = (value: string) => {
        self.categoryId = value
    }


    return {
        setIsDisabled,
        setIsRefresh,
        setOnEndReachedCalledDuringMomentum,
        setLastAudiobookId,
        setIsLoadMore,
        setIsLoading,
        setCategoryId
    }
})