import { types } from "mobx-state-tree"
import { AudiobooksCategoryScreenPropsModel } from "./audiobooks-category-screen.models"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store/general-resources.store"
import { GeneralResources } from "../../../constants/firebase/remote-config"
import { NavigationKey } from "../../../constants/app.constant"

export const AudiobooksCategoryScreenViews = types.model(AudiobooksCategoryScreenPropsModel)
    .views(self => ({
        get getIsDisabled(): boolean {
            return self.isDisabled
        },
        get getIsRefresh(): boolean {
            return self.isRefresh
        },
        get getOnEndReachedCalledDuringMomentum(): boolean {
            return self.onEndReachedCalledDuringMomentum
        },
        get getLastAudiobookId(): string {
            return self.lastAudiobookId
        },
        get getIsLoadMore(): boolean {
            return self.isLoadMore
        },
        get getIsLoading(): boolean {
            return self.isLoading
        },
        get getCategoryId(): string {
            return self.categoryId
        }

    }))

export const AudiobooksCategoryScreenResourcesViews = GeneralResourcesStoreModel.views(self => {

    //MARK: Volatile State
    const { AudioBookScreen } = GeneralResources
    const {
        playButton,
        downloadButton,
        downloadedTitle,
        no_internet_unable_to_download,
        no_internet_unable_to_play
    } = AudioBookScreen

    //MARK: Views
    const getResources = (locale: string, key: string, childKeyOrShareKey: string | boolean = false, isChildDotNotation: boolean = false) => self.getValues(locale, childKeyOrShareKey ? key : NavigationKey.AudioBookScreen, childKeyOrShareKey ? true : key, isChildDotNotation)
    const getResourcePlayButtonTitle = (locale: string) => getResources(locale, playButton, true)
    const getResourceDownloadButtonTitle = (locale: string) => getResources(locale, downloadButton, true)
    const getResourceDownloadedTitle = (locale: string) => getResources(locale, downloadedTitle, true)
    const getResourceNoInternetUnableToDownload = (locale: string) => getResources(locale, no_internet_unable_to_download, true)
    const getResourceNoInternetUnableToPlay = (locale: string) => getResources(locale, no_internet_unable_to_play, true)

    return {
        getResourcePlayButtonTitle,
        getResourceDownloadButtonTitle,
        getResourceDownloadedTitle,
        getResourceNoInternetUnableToDownload,
        getResourceNoInternetUnableToPlay
    }
})
