import { types } from "mobx-state-tree";

export const AudiobooksCategoryScreenPropsModel = {
    isDisabled: types.optional(types.boolean, false),
    isRefresh: types.optional(types.boolean, false),
    isLoadMore: types.optional(types.boolean, false),
    onEndReachedCalledDuringMomentum: types.optional(types.boolean, false),
    lastAudiobookId: types.optional(types.string, ""),
    isLoading: types.optional(types.boolean, false),
    categoryId: types.optional(types.string, '')
}