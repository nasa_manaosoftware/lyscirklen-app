import { DrawerActions } from "@react-navigation/core";
import { onSnapshot } from "mobx-state-tree";
import { Alert } from "react-native";
import { NavigationKey } from "../../constants/app.constant";
import { INavigationRoute } from "../../models";
import { RootStore } from "../../models/root-store/root.types";
import { TabController } from "../tab.controller";
import { PrivacyPolicyProps } from "./privacy-policy.props";
import { PrivacyPolicyResourcesStoreModel, PrivacyPolicyStoreModel } from "./storemodel/privacy-policy.store";
import { PrivacyPolicyResourcesStore, PrivacyPolicyStore } from "./storemodel/privacy-policy.types";

class PrivacyPolicyController extends TabController {

    /*
       Stateful Variable
   */

    public static viewModel: PrivacyPolicyStore
    public static resourcesViewModel: PrivacyPolicyResourcesStore

    /*
        Mark Constructor
    */

    constructor(rootStore?: RootStore, props?: PrivacyPolicyProps & Partial<INavigationRoute>) {
        super(rootStore, props)
        if (this._myProps?.route?.params?.isInitialized) return
        this._setupResourcesViewModel()
        this._setupNavigation()
        this._setupProps(props)
        this._setupViewModel()
        this._setupSnapShot()
    }

    /*
       Mark Setup
    */

    private _setupResourcesViewModel = () => {
        PrivacyPolicyController.resourcesViewModel = PrivacyPolicyResourcesStoreModel.create({ ...this._rootStore?.getAllGeneralResourcesStore })
    }
    private _setupViewModel = () => {
        const localStore = this._rootStore?.getModuleStore(NavigationKey.FrontScreen)
        PrivacyPolicyController.viewModel = localStore && PrivacyPolicyStoreModel.create({ ...localStore }) || PrivacyPolicyStoreModel.create({})
        this._setInitializedToPropsParams()
    }

    private _setupSnapShot = () => {
        onSnapshot(PrivacyPolicyController.viewModel, (snap: PrivacyPolicyProps) => {
            this._rootStore?.setModuleStoreValue(NavigationKey.FrontScreen, snap)
        })
    }

    public onSideMenuPress = () => {
        this._myProps?.navigation?.dispatch(DrawerActions.openDrawer() as any)
    }

    public onNotificationPress = () => {
        Alert.alert('Notifications')
    }

}

export default PrivacyPolicyController
