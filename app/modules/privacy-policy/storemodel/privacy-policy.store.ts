import { types } from "mobx-state-tree"
import { PrivacyPolicyActions, PrivacyPolicyViews, PrivacyPolicyResourcesViews, PrivacyPolicyPropsModel } from "../viewmodels"
import { StoreName } from "../../../constants/app.constant"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store/general-resources.store"

const PrivacyPolicyModel = types.model(StoreName.PrivacyPolicy, PrivacyPolicyPropsModel)

export const PrivacyPolicyStoreModel = types.compose(
    PrivacyPolicyModel,
    PrivacyPolicyActions,
    PrivacyPolicyViews
)

export const PrivacyPolicyResourcesStoreModel = types.compose(GeneralResourcesStoreModel, PrivacyPolicyResourcesViews)

