import { Instance, SnapshotOut } from "mobx-state-tree"
import { PrivacyPolicyResourcesStoreModel, PrivacyPolicyStoreModel } from "./privacy-policy.store"

export type PrivacyPolicyStore = Instance<typeof PrivacyPolicyStoreModel>
export type PrivacyPolicyStoreSnapshot = SnapshotOut<typeof PrivacyPolicyStoreModel>

export type PrivacyPolicyResourcesStore = Instance<typeof PrivacyPolicyResourcesStoreModel>
export type PrivacyPolicyResourcesStoreSnapshot = SnapshotOut<typeof PrivacyPolicyResourcesStoreModel>