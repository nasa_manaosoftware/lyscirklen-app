import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"

export interface PrivacyPolicyProps extends NavigationContainerProps<ParamListBase> {
}