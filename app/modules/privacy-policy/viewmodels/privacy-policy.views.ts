import { types } from "mobx-state-tree"
import { NavigationKey } from "../../../constants/app.constant"
import { GeneralResources } from "../../../constants/firebase/remote-config/remote-config.constant"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store/general-resources.store"
import { PrivacyPolicyPropsModel } from "./privacy-policy.models"

export const PrivacyPolicyViews = types.model(PrivacyPolicyPropsModel)
    .views(self => ({
    }))

export const PrivacyPolicyResourcesViews = GeneralResourcesStoreModel.views(self => {

    // //MARK: Volatile State
    const { PrivacyPolicy } = GeneralResources

    const { privacyPolicy } = PrivacyPolicy

    // //MARK: Views
    const getResources = (locale: string, key: string, childKeyOrShareKey: string | boolean = false, isChildDotNotation: boolean = false) => self.getValues(locale, childKeyOrShareKey ? key : NavigationKey.PrivacyPolicy, childKeyOrShareKey ? true : key, isChildDotNotation)
    const getResourcesPrivacyPolicyTitle = (locale: string) => getResources(locale, privacyPolicy, true)

    return {
        getResourcesPrivacyPolicyTitle
    }
})
