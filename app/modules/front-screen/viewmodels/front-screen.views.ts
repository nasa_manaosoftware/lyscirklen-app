import { types } from "mobx-state-tree"
import { FrontScreenPropsModel } from "./front-screen.models"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store/general-resources.store"
import { GeneralResources } from "../../../constants/firebase/remote-config"
import { NavigationKey } from "../../../constants/app.constant"

export const FrontScreenViews = types.model(FrontScreenPropsModel)
    .views(self => ({
        get getIsDisabled(): boolean {
           return self.isDisabled
        }
    }))

export const FrontScreenResourcesViews = GeneralResourcesStoreModel.views(self => {

    //MARK: Volatile State
    const { FrontScreen } = GeneralResources
    const {
        audiobooksTitle,
        categoriesTitle
    } = FrontScreen

    //MARK: Views
    const getResources = (locale: string, key: string, childKeyOrShareKey: string | boolean = false, isChildDotNotation: boolean = false) => self.getValues(locale, childKeyOrShareKey ? key : NavigationKey.FrontScreen, childKeyOrShareKey ? true : key, isChildDotNotation)
    const getResourceAudiobooksTitle = (locale?: string) => getResources(locale, audiobooksTitle)
    const getResourceCategoriesTitle = (locale?: string) => getResources(locale, categoriesTitle)

    return {
        getResourceAudiobooksTitle,
        getResourceCategoriesTitle
    }
})
