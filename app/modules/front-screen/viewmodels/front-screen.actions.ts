import { ExecFileSyncOptionsWithStringEncoding } from "child_process"
import { types } from "mobx-state-tree"
import { FrontScreenPropsModel } from "./front-screen.models"

export const FrontScreenActions = types.model(FrontScreenPropsModel).actions(self => {
    const setIsDisabled = (value: boolean) => {
        self.isDisabled = value
    }
    
    return {
        setIsDisabled
    }
})