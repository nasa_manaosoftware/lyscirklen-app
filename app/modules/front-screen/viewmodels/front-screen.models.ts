import { types } from "mobx-state-tree";

export const FrontScreenPropsModel = {
    isDisabled: types.optional(types.boolean, false)
} 