import { ImageStyle, TextStyle, ViewStyle } from "react-native";
import { HeaderProps } from "../../components/header/header.props";
import { images } from "../../theme/images";
import * as metric from "../../theme"
import { color } from "../../theme";
import { RFValue } from "react-native-responsive-fontsize";
import { size, type } from "../../theme/fonts";

export const MENU_ICON_STYLE: ImageStyle = {
    width: metric.ratioWidth(21),
    height: metric.ratioHeight(45)
}
export const MENU_LEFT_ICON_STYLE: ImageStyle = {
    width: metric.ratioWidth(23),
    height: metric.ratioHeight(45)
}

export const HEADER_PROPS: HeaderProps = {
    leftIconSource: images.hamMenu,
    leftIconStyle: MENU_LEFT_ICON_STYLE,
    rightIconStyle: MENU_ICON_STYLE,
    rightIconSource: images.whiteBell,
    isLeftIconAnimated: false,
    isRightIconAnimated: false
}

export const SCROLL_VIEW: ViewStyle = {
    flexGrow: 1,
    paddingBottom: metric.ratioHeight(50) 
}

export const MAINVIEW: ViewStyle = {
    flex: 1,
    marginTop: metric.ratioHeight(20),
    marginHorizontal: metric.ratioWidth(15),
}


export const TITLE_VIEW: ViewStyle = {
    flex: 0
}

export const TOPIC_TITLE_TEXT: TextStyle = {
    fontFamily: type.base,
    fontSize: size.header,
    color: color.whiteText
}

export const LIST_VIEW: ViewStyle = {
    paddingVertical: metric.isIPhone? metric.ratioHeight(8) :  metric.ratioHeight(11)
}

export const IMAGE_BOOK: ViewStyle = {
    width: metric.ratioWidth(120),
    height: metric.isTablet() ?
        (metric.ratioHeight(295))
        :
        metric.isIPhone ?
            metric.ratioHeight(240)
            :
            metric.ratioHeight(200),
    marginRight: metric.ratioWidth(12)
}

export const FLATLIST_CONTAINER: ViewStyle = {
    width: '100%'
}

export const MESSAGE_BOX_VIEW: ViewStyle = {
    marginVertical: metric.ratioHeight(11),
    backgroundColor: color.background,
    paddingHorizontal: metric.ratioWidth(15),
    paddingVertical: metric.ratioHeight(17)
}

export const TOPIC_VIEW: ViewStyle = {
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    paddingRight: metric.ratioWidth(10)
}

export const TOPIC_VIEW_CATEGORY: ViewStyle = {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingRight: metric.ratioWidth(10)
}

export const URGENT_TITLE_BORDER: ViewStyle = {
    backgroundColor: color.redLine,
    width: metric.ratioWidth(2),
    height: metric.ratioHeight(22),
    marginRight: metric.ratioWidth(11),
    marginTop: metric.ratioHeight(8)
}

export const URGENT_TITLE_BORDER_CATEGORY: ViewStyle = {
    backgroundColor: color.redLine,
    width: metric.ratioWidth(2),
    height: metric.ratioHeight(22),
    marginRight: metric.ratioWidth(11)
}

export const TEXT_TITLE: TextStyle = {
    fontFamily: type.base,
    fontSize: size.header,
    color: color.whiteText
}

export const MESSAGE_VIEW: ViewStyle = {
    marginTop: metric.ratioHeight(11),
    paddingLeft: metric.ratioWidth(15),
    minHeight: metric.ratioHeight(80)
}

export const MESSAGE_TEXT: TextStyle = {
    fontFamily: type.base,
    fontSize: size.large,
    color: color.white,
    // textAlign: 'justify'
}

export const CATEGORY_VIEW: ViewStyle = {
    marginTop: metric.ratioHeight(10),
    paddingHorizontal: metric.ratioWidth(15)
}

export const CATEGORY_LIST_VIEW: ViewStyle = {
    marginTop: metric.ratioHeight(10)
}

export const TOUCHABLE_STYLE: ViewStyle = {
    borderRadius: RFValue(40),
    marginRight: RFValue(10),
    marginTop: metric.ratioHeight(4),
    marginBottom: RFValue(8)
}

export const CATEGORY_BUTTON: ViewStyle = {
    borderRadius: RFValue(40),
    width: metric.ratioWidth(170),
    height: metric.ratioHeight(50),
    // backgroundColor: color.categoryButton,
    // backgroundColor: color.buttonGradient,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: metric.ratioWidth(8)
}

export const CATEGORY_BUTTON_TEXT: TextStyle = {
    textAlign: 'center',
    fontFamily: type.base,
    fontSize: size.large,
    color: color.whiteText
}

export const EMPTY_VIEW: ViewStyle = {
    width: '100%',
    height: metric.ratioHeight(180)
}
