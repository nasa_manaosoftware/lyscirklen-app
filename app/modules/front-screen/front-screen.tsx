import React from "react"
import { observer } from "mobx-react-lite"
import { FrontScreenProps } from "./front-screen.props";
import FrontScreenController from "./front-screen.controller";
import { useConfigurate } from "../../custom-hooks/use-configure-controller";
import { SafeAreaView, Text, View, FlatList, TouchableHighlight, Image } from "react-native"
import * as Styles from "./front-screen.styles"
import FastImage from "react-native-fast-image";
import { color } from "../../theme";
import { AnimatedScrollView } from "../../components";
import * as metric from "../../theme"
import { images } from "../../theme/images";
import LinearGraient from 'react-native-linear-gradient'

export const FrontScreen: React.FunctionComponent<FrontScreenProps> = observer((props) => {

    const controller = useConfigurate(FrontScreenController, props) as FrontScreenController
    const disabled = FrontScreenController.viewModel?.getIsDisabled

    const locale = FrontScreenController.rootStore?.getLanguage

    const bookList = () => {
        // console.log('bookList', FrontScreenController.rootStore?.getAudioBooksStore?.getAudiobooksRandom.length);

        return (
            <>
                <View style={Styles.TITLE_VIEW}>
                    <Text style={Styles.TOPIC_TITLE_TEXT}>{FrontScreenController.resourcesViewModel?.getResourceAudiobooksTitle(locale)}</Text>
                </View>
                {FrontScreenController.rootStore?.getAudioBooksStore?.getAudiobooksRandom.length !== 0 ?
                    <>
                        <View style={Styles.LIST_VIEW}>
                            <FlatList
                                data={FrontScreenController.rootStore?.getAudioBooksStore?.getAudiobooksRandom}
                                renderItem={({ item }) =>
                                    <>
                                        {item ?
                                            <TouchableHighlight
                                                disabled={disabled}
                                                activeOpacity={0.8}
                                                underlayColor={color.background}
                                                onPress={() => controller.onClickAudiobook(item)}
                                            >
                                                <FastImage resizeMode="contain" source={item?.image?.url ? { uri: item?.image?.url } : images.defaultImage} style={Styles.IMAGE_BOOK} />
                                            </TouchableHighlight>
                                            :
                                            null
                                        }
                                    </>
                                }
                                keyExtractor={(item, index) => index.toString()}
                                horizontal={true}
                                contentContainerStyle={Styles.FLATLIST_CONTAINER}
                                scrollEnabled={false}
                            />
                        </View>
                    </>
                    :
                    <View style={Styles.EMPTY_VIEW} />
                }
            </>
        )
    }

    const audioBookMessage = () => {
        return (
            <>
                {FrontScreenController.rootStore?.getAudioBooksStore?.getMessagesTitle === "" && FrontScreenController.rootStore?.getAudioBooksStore?.getMessagesDetail === "" ||
                    FrontScreenController.rootStore?.getAudioBooksStore?.getMessagesTitle === null && FrontScreenController.rootStore?.getAudioBooksStore?.getMessagesDetail === null ?
                    (null)
                    :
                    (
                        <View style={Styles.MESSAGE_BOX_VIEW}>
                            <View style={Styles.TOPIC_VIEW}>
                                <View style={Styles.URGENT_TITLE_BORDER} />
                                <Text style={Styles.TEXT_TITLE}>{FrontScreenController.rootStore?.getAudioBooksStore?.getMessagesTitle}</Text>
                            </View>
                            <View style={Styles.MESSAGE_VIEW}>
                                <Text style={Styles.MESSAGE_TEXT}>{FrontScreenController.rootStore?.getAudioBooksStore?.getMessagesDetail}</Text>
                            </View>
                        </View>
                    )
                }
            </>
        )
    }

    const bookCategory = () => {
        return (
            <>
                {FrontScreenController.rootStore?.getAudioBookCategoriesStore?.getAudiobooksCetegories.length !== 0 ?
                    <View style={Styles.CATEGORY_VIEW}>
                        <View style={Styles.TOPIC_VIEW_CATEGORY}>
                            <View style={Styles.URGENT_TITLE_BORDER_CATEGORY} />
                            <Text style={Styles.TOPIC_TITLE_TEXT}>{FrontScreenController.resourcesViewModel?.getResourceCategoriesTitle(locale)}</Text>
                        </View>
                        <View style={Styles.CATEGORY_LIST_VIEW}>
                            <FlatList
                                data={FrontScreenController.rootStore?.getAudioBookCategoriesStore?.getAudiobooksCetegories}
                                renderItem={({ item }) =>
                                    <TouchableHighlight
                                        disabled={disabled}
                                        activeOpacity={0.8}
                                        underlayColor={color.background}
                                        style={Styles.TOUCHABLE_STYLE}
                                        onPress={() => controller.onClickAudiobookCategory(item)}
                                    >
                                        <LinearGraient style={Styles.CATEGORY_BUTTON} colors={color.buttonGradient}>
                                            <Text style={Styles.CATEGORY_BUTTON_TEXT} numberOfLines={1} ellipsizeMode={'tail'}>{item.name.toUpperCase()}</Text>
                                        </LinearGraient>
                                    </TouchableHighlight>
                                }
                                keyExtractor={(item, index) => index.toString()}
                                numColumns={2}
                                scrollEnabled={false}
                            />
                        </View>
                    </View>
                    :
                    null
                }
            </>
        )
    }

    return (
        <SafeAreaView style={{ backgroundColor: color.background, flex: 1 }}>
            <AnimatedScrollView
                contentContainerStyle={Styles.SCROLL_VIEW}
                {...metric.solidScrollView}
            >
                <View style={Styles.MAINVIEW}>
                    {bookList()}
                    {audioBookMessage()}
                    {bookCategory()}
                </View>
            </AnimatedScrollView>
        </SafeAreaView>
    )
})