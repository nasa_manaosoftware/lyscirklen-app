import { Instance, SnapshotOut } from "mobx-state-tree"
import { FrontScreenStoreModel, FrontScreenResourcesStoreModel } from "./front-screen.store"

export type FrontScreenStore = Instance<typeof FrontScreenStoreModel>
export type FrontScreenStoreSnapshot = SnapshotOut<typeof FrontScreenStoreModel>

export type FrontScreenResourcesStore = Instance<typeof FrontScreenResourcesStoreModel>
export type FrontScreenResourcesStoreSnapshot = SnapshotOut<typeof FrontScreenResourcesStoreModel>