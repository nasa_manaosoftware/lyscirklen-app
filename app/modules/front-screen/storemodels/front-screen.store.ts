import { types } from "mobx-state-tree"
import { FrontScreenViews, FrontScreenActions, FrontScreenResourcesViews } from "../viewmodels"
import { FrontScreenPropsModel } from "../viewmodels"
import { StoreName } from "../../../constants/app.constant"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store/general-resources.store"
import { AudiobookStoreModel } from "../../../models/audiobook-store"
import { AudiobookCategoriesStoreModel } from "../../../models/audiobook-category-store"
import { UserStoreModel } from "../../../models/user-store/user.store"

const FrontScreenModel = types.model(StoreName.FrontScreen, FrontScreenPropsModel)

export const FrontScreenStoreModel = types.compose(
    FrontScreenModel,
    FrontScreenViews,
    FrontScreenActions,
    AudiobookStoreModel,
    AudiobookCategoriesStoreModel,
    UserStoreModel
)

export const FrontScreenResourcesStoreModel = types.compose(GeneralResourcesStoreModel, FrontScreenResourcesViews)

