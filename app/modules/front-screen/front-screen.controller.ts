import { DrawerActions, StackActions } from "@react-navigation/core";
import { onSnapshot } from "mobx-state-tree";
import { Alert } from "react-native";
import { DownloadStatus, NavigationKey } from "../../constants/app.constant";
import { INavigationRoute } from "../../models";
import { RootStore } from "../../models/root-store/root.types";
import { BaseController } from "../base.controller";
import { FrontScreenProps } from "./front-screen.props";
import { FrontScreenResourcesStoreModel, FrontScreenStoreModel } from "./storemodels/front-screen.store";
import { FrontScreenResourcesStore, FrontScreenStore } from "./storemodels/front-screen.types";
import FirebaseFireStore from "../../utils/firebase/fire-store/fire-store";
import FirebaseAnalytics from "../../utils/firebase/analytics/analytics";
import { newLineToBr } from "../../utils/string.utils";

class FrontScreenController extends BaseController {

    /*
       Stateful Variable
   */

    public static viewModel: FrontScreenStore
    public static resourcesViewModel: FrontScreenResourcesStore
    public static category_name: string

    /*
        Mark Constructor
    */

    constructor(rootStore?: RootStore, props?: FrontScreenProps & Partial<INavigationRoute>) {
        super(rootStore, props)
        if (this._myProps?.route?.params?.isInitialized) return
        this._setupResourcesViewModel()
        this._setupNavigation()
        this._setupProps(props)
        this._setupViewModel()
        this._setupSnapShot()
    }

    /*
       Mark Setup
    */

    private _setupResourcesViewModel = () => {
        FrontScreenController.resourcesViewModel = FrontScreenResourcesStoreModel.create({ ...this._rootStore?.getAllGeneralResourcesStore })
    }
    private _setupViewModel = () => {
        const localStore = this._rootStore?.getModuleStore(NavigationKey.FrontScreen)
        FrontScreenController.viewModel = localStore && FrontScreenStoreModel.create({ ...localStore }) || FrontScreenStoreModel.create({})
        this._setInitializedToPropsParams()
    }

    private _setupSnapShot = () => {
        onSnapshot(FrontScreenController.viewModel, (snap: FrontScreenStore) => {
            this._rootStore?.setModuleStoreValue(NavigationKey.FrontScreen, snap)
        })
    }

    public onSideMenuPress = () => {
        this._myProps?.navigation?.dispatch(DrawerActions.openDrawer() as any)
    }

    public onNotificationPress = () => {
        Alert.alert('Notifications front')
    }

    public onClickAudiobookCategory = (category: any) => {
        FrontScreenController.viewModel?.setIsDisabled(true)
        const navigation = this._myProps?.navigation
        navigation?.dispatch(StackActions.push(NavigationKey.AudiobooksCategory, {
            category_id: category.uid,
            categoryName: category.name
        }) as any)
        this._delayExecutor(() => FrontScreenController.viewModel?.setIsDisabled(false), 1200)
    }

    public queryCategoryName = (book) => {
        const categoryStore = FrontScreenController.rootStore?.getAudioBookCategoriesStore?.getAudiobooksCetegories

        categoryStore.map(item => {
            if (item.uid === book.category_id) {
                FrontScreenController.category_name = item.name
            }
        })
    }

    public onClickAudiobook = (item: any) => {
        FrontScreenController.viewModel?.setIsDisabled(true)
        this.queryCategoryName(item)

         //firebase analytic
         FirebaseAnalytics.openVideoDetail({
            user_id: this._rootStore?.getUserStore?.getUserId,
            book_name: item?.name,
            category_name:  FrontScreenController.category_name
        })
        
        if (item.video_url === '') {
            //firebase analytic
            FirebaseAnalytics.amountVideoAttached({
                userId: this._rootStore?.getUserStore.getUserId,
                book_name: item?.name,
                category_name:  FrontScreenController.category_name,
                video_url: item?.video_url
            })
        }

        const params = {
            id: item.uid,
            isInitialized: false
        }
        const navigation = this._myProps?.navigation
        navigation?.dispatch(StackActions.push(NavigationKey.AudioBookScreen, params) as any)
        this._delayExecutor(() => FrontScreenController.viewModel?.setIsDisabled(false), 1200)
    }

    private displayNewestVideo = () => {
        const video = FrontScreenController.rootStore?.getAudioBooksStore?.getAudiobooksList
        const newestVideo = []
        // console.log('video ', video);
        
        newestVideo.push(video[0], video[1], video[2])
        FrontScreenController.rootStore?.getAudioBooksStore?.setAudiobooksRandom(newestVideo)
    }

    private fectAudioBookList = async () => {
        //fetch audiobook list
        const organizationId = FrontScreenController.rootStore?.getOrganizationId
        const audiobooksList = await FirebaseFireStore.fetchAudiobooks(organizationId)

        audiobooksList.sort((firstBook, secondBook) => firstBook.created_date > secondBook.created_date ? -1 : 1)
        FrontScreenController.rootStore?.getAudioBooksStore?.setAudiobooksList(audiobooksList)

        if (audiobooksList.length !== 0) {
            this.displayNewestVideo()
        } else {
            FrontScreenController.rootStore?.getAudioBooksStore?.setAudiobooksRandom([])
        }
    }

    private resetSideMenuStore = async () => {
        FrontScreenController.rootStore?.setIsSelectedFront(true),
            FrontScreenController.rootStore?.setIsSelectedNotofication(false),
            FrontScreenController.rootStore?.setIsSelectedAboutUs(false),
            FrontScreenController.rootStore?.setIsSelectedTermOfUse(false),
            FrontScreenController.rootStore?.setIsSelectedPrivacy(false),
            FrontScreenController.rootStore?.setIndex(0)
    }




    viewDidAppearAfterFocus = async () => {

        if (this._rootStore?.getNavigationStore?.getCurrentPageName === NavigationKey.SearchAudioBook ||
            this._rootStore?.getNavigationStore?.getCurrentPageName === NavigationKey.MyLibrary ||
            this._rootStore?.getNavigationStore?.getCurrentPageName === NavigationKey.AboutUs ||
            this._rootStore?.getNavigationStore?.getCurrentPageName === NavigationKey.AudioBookScreen ||
            this._rootStore?.getNavigationStore?.getCurrentPageName === NavigationKey.MessageDetail ||
            this._rootStore?.getNavigationStore?.getCurrentPageName === NavigationKey.Messages ||
            this._rootStore?.getNavigationStore?.getCurrentPageName === NavigationKey.TermOfUse ||
            this._rootStore?.getNavigationStore?.getCurrentPageName === NavigationKey.PrivacyPolicy
        ) {
            this._isGlobalLoading(false)
        }

        this.resetSideMenuStore()

        this.fectAudioBookList()
    }
}

export default FrontScreenController
