import { types } from "mobx-state-tree"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store/general-resources.store"
import { GeneralResources } from "../../../constants/firebase/remote-config"
import { NavigationKey } from "../../../constants/app.constant"
import { PlayBackPropsModel } from "./playback-screen.models"

export const PlayBackScreenViews = types.model(PlayBackPropsModel)
    .views(self => ({

       
    }))

export const PlayBackScreenResourcesViews = GeneralResourcesStoreModel.views(self => {

    //MARK: Volatile State
    const {  } = GeneralResources

    //MARK: Views
    const getResources = (key: string, childKeyOrShareKey: string | boolean = false, isChildDotNotation: boolean = false) => self.getValues(childKeyOrShareKey ? key : NavigationKey.PlayBackScreen, childKeyOrShareKey ? true : key, isChildDotNotation)
  

    return {
    }
})
