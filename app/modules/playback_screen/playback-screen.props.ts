import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"

export interface PlayBackScreenProps extends NavigationContainerProps<ParamListBase> {
}