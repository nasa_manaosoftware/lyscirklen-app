import React from "react"
import { observer } from "mobx-react-lite"
import { useConfigurate } from "../../custom-hooks/use-configure-controller";
import { Alert, Platform, SafeAreaView, ScrollView, Text, View } from "react-native"
import { color } from "../../theme";
import { PlayBackScreenProps } from "./playback-screen.props";
import PlayBackScreenController from "./playback-screen.controller";

export const PlayBackScreen: React.FunctionComponent<PlayBackScreenProps> = observer((props) => {

    const controller = useConfigurate(PlayBackScreenController, props) as PlayBackScreenController
    
    return (
        <SafeAreaView style={{ backgroundColor: color.background, flex: 1 }}>
            <Text style={{color: 'white', fontSize: 20}}>Play Back Screen</Text>
        </SafeAreaView>
    )
})