import { Instance, SnapshotOut } from "mobx-state-tree"
import { PlayBackScreenResourcesStoreModel, PlayBackScreenStoreModel } from "./playback-screen.store"

export type PlayBackScreenStore = Instance<typeof PlayBackScreenStoreModel>
export type PlayBackScreenStoreSnapshot = SnapshotOut<typeof PlayBackScreenStoreModel>

export type PlayBackScreenResourcesStore = Instance<typeof PlayBackScreenResourcesStoreModel>
export type PlayBackScreenResourcesStoreSnapshot = SnapshotOut<typeof PlayBackScreenResourcesStoreModel>