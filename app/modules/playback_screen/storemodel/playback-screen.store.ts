import { types } from "mobx-state-tree"
import { StoreName } from "../../../constants/app.constant"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store/general-resources.store"
import { PlayBackPropsModel, PlayBackScreenActions, PlayBackScreenResourcesViews, PlayBackScreenViews } from "../viewmodel"

const PlayBackScreenModel = types.model(StoreName.PlayBack, PlayBackPropsModel)

export const PlayBackScreenStoreModel = types.compose(
    PlayBackScreenModel,
    PlayBackScreenActions,
    PlayBackScreenViews,
)

export const PlayBackScreenResourcesStoreModel = types.compose(GeneralResourcesStoreModel, PlayBackScreenResourcesViews)

