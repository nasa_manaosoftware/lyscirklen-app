import { onSnapshot } from "mobx-state-tree";
import { NavigationKey } from "../../constants/app.constant";
import { INavigationRoute } from "../../models";
import { RootStore } from "../../models/root-store/root.types";
import { BaseController } from "../base.controller";
import { PlayBackScreenProps } from "./playback-screen.props";
import { PlayBackScreenResourcesStore, PlayBackScreenResourcesStoreModel, PlayBackScreenStore, PlayBackScreenStoreModel } from "./storemodel";

class PlayBackScreenController extends BaseController {

    /*
       Stateful Variable
   */

    public static viewModel: PlayBackScreenStore
    public static resourcesViewModel: PlayBackScreenResourcesStore
    public static myProps: PlayBackScreenProps & Partial<INavigationRoute>

    /*
        Mark Constructor
    */

    constructor(rootStore?: RootStore, props?: PlayBackScreenProps & Partial<INavigationRoute>) {
        super(rootStore, props)
        if (this._myProps?.route?.params?.isInitialized) return
        this._setupResourcesViewModel()
        this._setupNavigation()
        this._setupProps(props)
        this._setupViewModel()
        this._setupSnapShot()
    }

    /*
       Mark Setup
    */

    private _setupResourcesViewModel = () => {
        PlayBackScreenController.resourcesViewModel = PlayBackScreenResourcesStoreModel.create({ ...this._rootStore?.getAllGeneralResourcesStore })
    }
    private _setupViewModel = () => {
        const localStore = this._rootStore?.getModuleStore(NavigationKey.AudioBookScreen)
        PlayBackScreenController.viewModel = localStore && PlayBackScreenStoreModel.create({ ...localStore }) || PlayBackScreenStoreModel.create({})
        this._setInitializedToPropsParams()
    }

    private _setupSnapShot = () => {
        onSnapshot(PlayBackScreenController.viewModel, (snap: PlayBackScreenProps) => {
            this._rootStore?.setModuleStoreValue(NavigationKey.AudioBookScreen, snap)
        })
    }
   
}

export default PlayBackScreenController
