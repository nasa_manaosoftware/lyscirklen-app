import { RootStore } from "../models/root-store/root.types";
import { BaseNavigationProps } from "../custom-hooks/use-configure-controller";
import { NativeEventSubscription, BackHandler, Keyboard, Alert, Platform } from "react-native";
import { ListenerKey } from "../constants/app.constant";
import { INavigationRoute } from "../models/navigation-store/navigation.types";
import { GeneralResources } from "../constants/firebase/remote-config/remote-config.constant";
import NetInfo from "@react-native-community/netinfo";

export type IAlertParams = {
    title?: string,
    message?: string,
    onOKPress?: () => void,
    onCancelPress?: () => void,
    delay?: number
}

export type BaseProps =
    BaseNavigationProps &
    Partial<INavigationRoute>

export class BaseController {

    /*
       Mark Inherited Variable & Declaration
    */

    /*
        Vulnerable Variable -- always will be cleared when deinit.
    */
    protected _viewModel: any
    protected _rootStore: RootStore
    protected _myProps: BaseProps
    private _backHandlerDiposer: NativeEventSubscription

    /*
        Stateful Variable
    */

    public static rootStore: RootStore //for Sharing
    public static myProps: BaseProps //for Sharing

    /*
       Mark Setup
   */

    constructor(rootStore?: RootStore, props?: BaseProps) {
        this._setupRootStore(rootStore)
        this._setupProps(props)
    }

    private _setupRootStore = (rootStore?: RootStore) => {
        BaseController.rootStore = rootStore
        this._rootStore = rootStore
    }

    protected _setupProps = (props: BaseProps) => {
        const comingProps = props?.route?.params
        this._myProps = {
            ...props as any,
            ...comingProps
        }
        BaseController.myProps = this._myProps
    }

    protected _setInitializedToPropsParams = () => {
        if (!this._myProps?.route) return
        this._myProps.route.params = {
            ...this._myProps.route.params,
            isInitialized: true
        }
    }

    /*
       Mark Data
   */

    protected _setupNavigation = () => {
        if (!this._rootStore?.getNavigationStore?.getNavigation) {
            this._rootStore?.getNavigationStore?.setNavigation(this._myProps?.navigation)
        }
    }

    /*
       Mark Event
   */

    protected _isConnected = () => this._rootStore?.getSharedStore?.getIsConnected

    protected _isGlobalLoading = (value: boolean) => this._rootStore?.SharedStore?.setIsLoading(value)

    private registerBackButtonHandler = () => {
        if (!this._viewModel)
            this._backHandlerDiposer = BackHandler.addEventListener(ListenerKey.HardwareBackPress, this.backProcess)
    }
    private _unRegisterBackButtonHandler = () => this._backHandlerDiposer?.remove()

    public backProcess() {
        Keyboard.dismiss()
        this._myProps?.navigation?.goBack()
        BaseController?.rootStore?.getPlaybackStore?.onHidePlayer()
        return true
    }

    /*
       Mark Life cycle
   */

    /**
     * 
     * IMPORTANT: Don't use => function if you want to override base methods and call `super`. Otherwise the super will be undefined.
     * 
     */

    public viewWillAppearOnce() {
    }

    public async viewDidAppearOnce() {
    }

    public async viewDidAppearAfterFocus(newProps?: BaseProps) {
        // this._setupProps(newProps) -- should set in each module instead. Example in `SearchList Screen`
        this.registerBackButtonHandler()
    }

    public viewWillDisappear() {
        this._unRegisterBackButtonHandler()
    }

    public deInit() {
        this._unRegisterBackButtonHandler()
    }


    /*
       Mark Utils
    */

    protected static _delayExecutor = (task: any, delay: number = 1000) => {
        setTimeout(() => { task && task() }, delay)
    }

    protected _delayExecutor = (task: any, delay: number = 1000) => {
        setTimeout(() => { task && task() }, delay)
    }

    protected _generalAlertOS = (params: IAlertParams) => {
        this._delayExecutor(() => Alert.alert(params.title, params.message), params?.delay || 1000)
    }

    protected _confirmationAlertOS = (params: IAlertParams) => {
        const buttonOKTitle = this._rootStore.getGeneralResourcesStore(GeneralResources.SharedKeys.buttonOKTitle, '', true)
        const buttonCancelTitle = this._rootStore.getGeneralResourcesStore(GeneralResources.SharedKeys.buttonCancelTitle, '', true)
        this._delayExecutor(() => Alert.alert(params?.title, params?.message, [
            { text: buttonOKTitle, onPress: params?.onOKPress },
            { text: buttonCancelTitle, onPress: params?.onCancelPress }
        ]), params?.delay || 1000)
    }
}