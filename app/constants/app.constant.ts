export enum DesignDimension {
    Width = 414,
    Height = 896
}

export enum SearchListType {}

export enum GeneralResourcesJSONPath {
    Path = "generalresource.json"
}

export enum Locale {
    Denmark = "da",
    Default = "en"
}

export enum FileUploadType {
    GalleryImage = "gallery_image",
    ProfileImage = "profile_image",
}

export enum DownloadStatus {
    SUCCESS = "success",
    FAILED = "failed",
    DO_NOT_DOWNLOAD = "do_not_download"
}

export enum NavigationKey {
    /** BD **/
    FrontScreen = "front_screen",
    Signin = "signin_screen",
    SearchAudioBook = "search_screen",
    MyLibrary = "my_library_screen",
    Notification = "notification_screen",
    AboutUs = "about_us_screen",
    TermOfUse = "term_of_use_screen",
    TermOfUseStack = "term_of_use_screen_stack",
    PrivacyPolicy = "privacy_policy_screen",
    PrivacyPolicyStack = "privacy_policy_screen_stack",
    MainScreen = "main_screen",
    MainMenu = "main_menu",
    AudiobooksCategory = "audiobooks_category_screen",
    AudioBookScreen = "audio_book_screen",
    MainStack = "main_stack",
    FrontScreenStack = 'front_screen_stack',
    SearchStack = "search_stack",
    LibraryStack = 'library_stack',
    AboutUsStack = 'about_us_stack',
    ShardedKeys = "share_keys",
    PlayBackScreen = "play_back_screen",
    Messages = "messages_screen",
    MessagesStack = 'messages_screen_stack',
    MessageDetail = "message_detail_screen",
    Splash = "splash_screen",
    Home = "home_screen",
    TermsAndConditions = "terms_and_conditions_screen",
    NotificationList = "notification_list_screen",
    VerifyUser = "verify_user"
}

export enum StoreName {
    /** BD **/
    FrontScreen = "FrontScreenStore",
    Signin = "SigninStore",
    SearchAudioBook = "SearchAudioBookStore",
    MyLibrary = "MyLibraryStore",
    AboutUs = "AboutUsStore",
    Audiobook = 'AudiobookStore',
    AudiobookCategories = 'AudiobookCetegoriesStore',
    Main = "MainStore",
    AudiobooksCategory = 'AudiobooksCategoryStore',
    Downloaded = 'DownloadedStore',
    PlayBack = 'PlayBackStore',
    TermOfUse = 'TermOfUseStore',
    PrivacyPolicy = 'PrivacyPolicyStore',
    Messages = 'MessagesStore',
    Downloading = "DownloadingStore",
    Root = "RootStore",
    Navigation = "NavigationStore",
    Splash = "SplashStore",
    ValidationStore = "ValidationStore",
    // Playback
    PlaybackController = "PlaybackController",
    FetchingResourcesPropertyStore = "isFetchingResourcesPropertyStore",

    /** DMD **/
    Login = "LoginStore",
    User = "UserStore",
    Auth = "AuthStore",
    Shared = "SharedStore",
    GeneralResources = "GeneralResourcesStore",
    Module = "ModuleStore",

    NotificationList = "NotificationListStore",
    Tab = "TabStore",
    Search = "Search",
    CustomDrawer = "CustomDrawer"
}

export enum EventName {
    NavigationEvents = "NavigationEvents"
}

export enum ListenerKey {
    HardwareBackPress = "hardwareBackPress"
}


export const newIphoneDeviceIds: string[] = ['iPhone10,3', 'iPhone10,6', 'iPhone11,2', 'iPhone11,4', 'iPhone11,6', 'iPhone11,8', 'iPhone12,1', 'iPhone12,3', 'iPhone12,5']
//https://gist.github.com/adamawolf/3048717