export enum FirebaseAuthErrorResponse {
    UserNotFound = "auth/user-not-found",
    WrongPassword = "auth/wrong-password",
    EmailAlreadyInUse = "auth/email-already-in-use",
    EmailInvalid = "auth/invalid-email",
    NetworkRequestFailed = "auth/network-request-failed",
    NoAuthorization = "auth/requires-recent-login"
}

export enum AXIOSErrorResponse {
    ECONNABORTED = "ECONNABORTED"
}

export enum FirebaseStorageErrorResponse {
    FileNotFound = "storage/file-not-found"
}