import { RemoteConfigDictionary } from "./remote-config.dictionaries"

export enum RemoteConfigRootKey {
    GeneralResources = "general_resources",
    Tags = "dmd_rmcf_tags",
    Notifications = "notification",
    Devices = "devices",
}

export enum RemoteConfigGroupedKey {
    SharedKeys = "share_keys"
}

export class GeneralResources {
    static SharedKeys = {
        play: RemoteConfigDictionary.PlayButtonTitle,
        download: RemoteConfigDictionary.DownloadButtonTitle,
        noInternetConnectionTitle: RemoteConfigDictionary.NoInternetConnectionTitle,
        backHandleMessage: RemoteConfigDictionary.BackHandleMessage,
        backHandleOK: RemoteConfigDictionary.BackHandleOK,
        backHandleCancel: RemoteConfigDictionary.BackHandleCancel,
        buttonOKTitle: RemoteConfigDictionary.BackHandleOK,
        buttonCancelTitle: RemoteConfigDictionary.BackHandleCancel
    }

    static Notification = {
        newComingMessage: RemoteConfigDictionary.NewComingMessage,
        requestForEditing: RemoteConfigDictionary.RequestForEditing,
    }


    static ErrorMessage = {
    }

    static Modal = {
    }

    static Alert = {
    }


    static SigninScreen = {
        title: RemoteConfigDictionary.Title,
        membership_placeholder: RemoteConfigDictionary.Membership,
        login_button: RemoteConfigDictionary.Login,
        acceptance_placeholder: RemoteConfigDictionary.Acceptance,
        privacyPolicy: RemoteConfigDictionary.privacyPolicy,

        error_empty_membership: RemoteConfigDictionary.ErrorEmptyMembership,
        error_invalid_membership: RemoteConfigDictionary.ErrorInvalidMembership,
        error_signin: RemoteConfigDictionary.ErrorSignin,
        no_internet_unable_to_login : RemoteConfigDictionary.NoInternetUnableToLogin,
        termsOfUse: RemoteConfigDictionary.TermsOfUse,
        andLink: RemoteConfigDictionary.AndLink,
        termOfUseTitle: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigDictionary.TermOfUseMenu,
        privacyPolicyTitle: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigDictionary.PrivacyPolicyMenu,
        loginDescription: RemoteConfigDictionary.LoginDescription

    }

    static SearchAudioBookScreen = {
        enter_search_word: RemoteConfigDictionary.EnterSearchWord,
        search_results: RemoteConfigDictionary.SearchResults,
        search_items: RemoteConfigDictionary.SearchItems,
        search_place_holder: RemoteConfigDictionary.SearchPlaceHolder,
        no_internet_unable_to_search: RemoteConfigDictionary.SearchNoInternet,
        no_internet_unable_to_download : RemoteConfigDictionary.NoInternetUnableToDownload,
        no_internet_unable_to_play: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigDictionary.no_internet_unable_to_play
    }

    static TermsAndConditionScreen = {
    }


    static SideMenuBar = {
        homeSideMenuBarTitle: RemoteConfigDictionary.HomeSideMenuBarTitle,
 
        inboxSideMenuBarTitle: RemoteConfigDictionary.InboxSideMenuBarTitle,
        faqSideMenuBarTitle: RemoteConfigDictionary.FaqSideMenuBarTitle,
        contactSideMenuBarTitle: RemoteConfigDictionary.ContactSideMenuBarTitle,
        logoutSideMenuBarTitle: RemoteConfigDictionary.LogoutSideMenuBarTitle,
    }

    static NotificationScreen = {
        screenTitle: RemoteConfigDictionary.Title
    }

    static SearchList = {
    }

    static Filters = {
    }

    /** BD **/

    static MainMenu = {
        frontScreenMenu: RemoteConfigDictionary.FrontScreenMenu,
        searchMenu: RemoteConfigDictionary.SearchMenu,
        myLibraryMenu: RemoteConfigDictionary.MyLibraryMenu,
        notification: RemoteConfigDictionary.Notification,
        aboutUsMenu: RemoteConfigDictionary.AboutUsMenu,
        termOfUseTitle: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigDictionary.TermOfUseMenu,
        privacyPolicyTitle: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigDictionary.PrivacyPolicyMenu,
        logoutMenu: RemoteConfigDictionary.LogoutMenu,
        logoutTitle: RemoteConfigDictionary.LogoutTitle,
        logoutMessage: RemoteConfigDictionary.LogoutMessage,
        confirmButton: RemoteConfigDictionary.LogoutButton,
        cancelButton: RemoteConfigDictionary.CancelButton
    }

    static FrontScreen = {
        audiobooksTitle: RemoteConfigDictionary.AudiobooksTitle,
        categoriesTitle: RemoteConfigDictionary.CategoriesTitle
    }

    static AudioBookScreen = {
        playButton: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigDictionary.PlayButtonTitle,
        downloadButton: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigDictionary.DownloadButtonTitle ,
        downloadedTitle: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigDictionary.DownloadedTitle,
        no_internet_unable_to_download : RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigDictionary.no_internet_unable_to_download,
        no_internet_unable_to_play : RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigDictionary.no_internet_unable_to_play
    }

    static MyLibrary = {
        myDownloadTitle: RemoteConfigDictionary.MyDownloadTitle,
        noDataTitle: RemoteConfigDictionary.NoDataTitle,
        deleteButtonTitle: RemoteConfigDictionary.DeleteButtonTitle,
        cancelButtonTitle: RemoteConfigDictionary.CancelButtonTitle,
        modalMessage: RemoteConfigDictionary.ModalMessage
    }

    static TermOfUse = {
        termOfUse: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigDictionary.TermOfUseMenu
    }

    static PrivacyPolicy = {
        privacyPolicy: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigDictionary.PrivacyPolicyMenu
    }

    static AboutUs = {
        callUs: RemoteConfigDictionary.CallUs,
        readMore: RemoteConfigDictionary.ReadMore,
        telephoneNumber: RemoteConfigDictionary.TelephoneNumber,
        readMoreLink: RemoteConfigDictionary.ReadMoreLink,
        aboutUsTitle: RemoteConfigDictionary.AboutUsTitle
    }

    static Messages = {
        noNotificationTitle: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigDictionary.NoNotificationTitle,
        noNotificationSubTitle: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigDictionary.NoNotificationSubTitle
    }
}