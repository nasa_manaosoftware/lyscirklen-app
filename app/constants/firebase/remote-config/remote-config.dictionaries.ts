export class RemoteConfigDictionary {
    static AppName = "app_name"
    static slogan = "slogan"
    

    static Image = "image"

    static Title = "title"
    static SubTitle = "sub_title"
    static Detail = "detail"
    static Description = "description"


    static HomeSideMenuBarTitle = "home_side_menu_bar_title"

    static InboxSideMenuBarTitle = "inbox_side_menu_bar_title"
    static FaqSideMenuBarTitle = "faq_side_menu_bar_title"
    static ContactSideMenuBarTitle = "contact_side_menu_bar_title"
    static LogoutSideMenuBarTitle = "logout_side_menu_bar_title"

    static ButtonActivateTitle = "button_activate_title"
    static ButtonDeactivateTitle = "button_deactivate_title"


    static ErrorMessage = "error_message"
    static NoInternetConnectionTitle = "no_internet_connection_title"

    static AlertEmailSentTitle = "email_sent_title"
    static AlertDeleteProileImageTitle = "delete_profile_image_title"
    static AlertDeleteProileImageMessage = "delete_profile_image_message"
    static AlertDeleteImageInGalleryTitle = "delete_image_in_gallery_title"
    static AlertDeleteImageInGalleryMessage = "delete_image_in_gallery_message"

    static NewComingMessage = "new_coming_message"

    static RequestForEditing = "request_for_editing"

    static AlertValidateUserTitle = "validate_user_title"
    static AlertValidateUserMessage = "validate_user_message"

    static PrivacyTitle = "privacy_title"
    static PrivacyLinkTitle = "privacy_link_title"

    static ShouldRestorePurchasesTitle = "shouldRestorePurchases"

    static RegionListEmptyTitle = "region_list_empty_title"

    /** BD **/
    static FrontScreenMenu = "front_screen_menu"
    static SearchMenu = "search_menu"
    static MyLibraryMenu = "my_library_menu"
    static Notification = "notification_menu"
    static AboutUsMenu = "about_us_menu"
    static LogoutMenu = "log_out_menu"
    static LogoutTitle = "confirm_log_out_title"
    static LogoutMessage = "confirm_log_out_message"
    static LogoutButton = "confirm_log_out_button"
    static CancelButton = "cancel_log_out_button"

    //signin


    static Membership = "membership_placeholder"
    static Login = "login_button"
    static Acceptance = "acceptance_placeholder"
    static privacyPolicy = "privacyPolicy"
    static LoginError = "error_signin"

    static TermsOfUse = "termsOfUse"
    static AndLink = "and_placeholder"

    static ErrorEmptyMembership = "error_empty_membership"
    static ErrorInvalidMembership = "error_invalid_membership"
    static ErrorSignin = "error_signin"
    static NoInternetUnableToLogin = "no_internet_unable_to_login"
    static no_internet_unable_to_play = "no_internet_unable_to_play"
    static LoginDescription = "login_description"
    
    //front screen
    static AudiobooksTitle = "audiobooks_title"
    static CategoriesTitle = "categories_title"

    //share keys
    static PlayButtonTitle = 'play_button'
    static DownloadButtonTitle = "download_button"
    static DownloadedTitle = "downloaded_title"
    static TermOfUseMenu = "term_of_use_title"
    static PrivacyPolicyMenu = "privacy_policy_title"
    static no_internet_unable_to_download = "no_internet_unable_to_download"
    static BackHandleMessage = "back_handle_message"
    static BackHandleOK = "back_handle_ok_title"
    static BackHandleCancel = "bavk_handle_cancel_title"
    static NoNotificationTitle = "no_notification_title"
    static NoNotificationSubTitle = "no_notification_sub_title"

    //Search screen
    static EnterSearchWord = "enter_search_word"
    static SearchResults = "search_results"
    static SearchItems = "search_items"
    static SearchPlaceHolder = "search_place_holder"
    static SearchNoInternet = "no_internet_unable_to_search"
    static NoInternetUnableToDownload = "no_internet_unable_to_download"
    
    
    //my library screen
    static MyDownloadTitle = "title"
    static NoDataTitle = "no_data_title"
    static DeleteButtonTitle = "delete_button"
    static CancelButtonTitle = "cancel_button"
    static ModalMessage = "modal_message"

    //about us
    static CallUs = "call_us_title"
    static ReadMore = "read_more_title"
    static TelephoneNumber = "telephone_number"
    static ReadMoreLink = "read_more_link"
    static AboutUsTitle = "about_us_title"
}