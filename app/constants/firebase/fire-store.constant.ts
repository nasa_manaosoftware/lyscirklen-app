export enum Collection {
    NOTIFICATIONS = "notifications",

    USERS = "users",

    MESSAGES = "messages",
    COMMENTS = "comments",
    ERROR_LOGS = "error_logs",
    DEVICE_TOKENS = "device_tokens",

    /** BD **/
    PRIVACY_POLICY = "privacy_policy",
    TERM_OF_USE = "term_of_use",
    AUDIOBOOKS = 'audiobooks',
    CATEGORIES = 'categories',
    HOMESCREEN = 'home_screen',
    DEVICES = 'devices',
    AUDIOBOOK = 'audiobook',
    ABOUT_US = "about_us",
    PUSH_NOTIFICATIONS = 'push_notifications'
}

export enum MobileProperty {

    PASSWORD = "password",
    CONFIRM_PASSWORD = "confirmPassword",
    NOTIFICATION_LIST = "notificationList",
    COMPARE_PROFILE_IMAGE = "compare_profile_image",
    COMPARE_GALLERY_IMAGE_LIST = "compare_gallery_image_list",
    PROFILE_IMAGE_TO_REMOVE = "profile_image_to_remove",
    TYPE = "type",
    FILE_PATH = "file_path",

    MEMBERSHIP_NUM  = "membershipNumber",
    



    //Exists in Timer object.
    IS_TIMER_SET = "isTimerSet",
    EXPIRY_TIMER = "expiry_timer",
    TIMER = "timer",
    DATE_TIME_AGO = "dateTimeAgo",
    DATE_TIME = "dateTime",
    EXPIRY_DATE_COUNT = "expiryDateCount",
    SERVER_TIME = "serverTime",

    //Exists in ISelect object.
    KEY = "key",
    LABEL = "label",

    //Module Store
    IS_INITIALIZED = "isInitialized",

    //Exists in Message object.
    UNIQUE_MOBILE_ID = "unique_mobile_id",

}

export enum MediaProperty {
    STORAGE_PATH = "storage_path",
    FILE_NAME = "file_name",
    URL = "url"
}

export enum Property {
    UID = "uid",
    AGE = "age",
    IS_HIGHLIGHT = "is_highlight",
    FULL_NAME = "full_name",
    EMAIL = "email",
    MEMBERSHIP = "membership",
    MEMBERSHIP_NUM = "membership_number", 
    STATUS = "status",

    CREATED_DATE = "created_date",
    UPDATED_DATE = "updated_date",

    PROFILE_IMAGE = "profile_image",
    // GALLERY_IMAGE_LIST = "gallery_image_list",
    IS_MANAO_TESTER = "is_manao_tester",
    //Exists in User role.
    IS_VERIFIED = "is_verified",
    ROLE = "role",
    DEVICE_TOKEN = "device_token",

    //Exists in Dude role.
    USER_ID = "user_id",
    //Exists for payment.

    EXPIRY_DATE = "expiry_date",

    
    //Exists in Favourite object.
    //Exists in Page object.
    PAGE_SIZE = "page_size",
    //Exists in Page object.
    SERVER_TIME = "server_time",
    IS_LAST_PAGE = "is_last_page",
    LAST_ID = "last_id",
    IS_LOAD_DONE = "is_load_done",
    PAGE = "page",
    //Exists in My dude list object.
    //Exists in Notification object.
    NOTIFICATION_ID = "notification_id",
    TITLE = "title",
    BODY = "body",
    DATA = "data",
    NAVIGATION = "navigation",
    MISC = "misc",
    TOPIC = "topic",
    IS_READ = "is_read",
    //Exists in Comment object,
    COMMENT_ID = "comment_id",

    //Exists in FavouriteList object.
    //Exists in ChatList object.
    SENDER = "sender",
    USER = "user",

    LAST_MESSAGE = "last_message",

    TEXT = "text",

    //Exists in Message object.
    MESSAGE_ID = "message_id",
    VIDEO = "video_url",
    AUDIO = "audio",
    IS_SENT = "sent",
    IS_RECEIVED = "received",
    IS_PENDING = "pending",
    SYSTEM = "system",

    
        
    // audiobooks object.
    CATEGORY_ID = "category_id",
    DESCRIPTION = "description",
    FORFATTER = "forfatter",
    IMAGE = "image",
    FILE_NAME = "name",
    STORAGE_PATH = "storage_path",
    THUMBNAIL_URL = "thumbnail_url",
    URL = "url",
    NAME = "name",
    ORGANIZATION_ID = "organization_id",
    VIDEO_URL = "video_url",
    DOWNLOAD_STATUS = "download_status",
    FILE_PATH = "file_path",
    JOB_ID = "job_id",
    IS_DOWNLOADING = "is_downloading",
    VIDEO_DURATION = "video_duration",
    DOWNLOAD_URL = "download_url",
    STREAMING_URL = "streaming_url",

    //categoty
    AUDIO_BOOK_COUNT = "audio_book_count",
    ORDER = "order",
    PUSH_NOTIFICATION_ID = "push_notification_id",
    HEADING = "heading",
    MESSAGE = "message",
    AUDIOBOOK_ID = "audiobook_id",
    BUTTON_TEXT = "button_text",

    // Playback
    IS_PAUSE = "is_pause",
    CURRENT_DURATION = "current_duration",
    IS_CLOSE_PLAYER = "is_close_player",
    IS_COLLAPSE_PLAYER = "is_collapse_player",
    IS_CHANGE_COLLAPSE_PLAYER = "is_change_collapse_player",
    IS_SHOW_MINI_PLAYER = "is_show_mini_player",
    AUDIO_BOOK = "audio_book",
    IS_DONE = "is_done",
    ON_BUFFERING = "on_buffering",
    PLAY_PAUSE = "play_pause",
    IS_SEEKING = "is_seeking",
    IS_ENDED = "is_ended",
}

export enum SnapshotChangeType {
    ADDED = "added",
    MODIFIED = "modified",
    REMOVED = "removed"
}