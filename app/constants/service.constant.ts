export enum ServiceType {
    //FirebaseSDK
    //API
    UpdateDeviceToken,
    SearchAudioBook,

    SignOut,
    GetServerDateTime,
    GetNotificationList,
    MarkReadNotificationById,
    MarkReadNotificationByTopic
}

export enum HeaderType {
    ContentType = "Content-Type",
    Authorization = "authorization",
    Bearer = "Bearer",
    Language = "Language"
}

