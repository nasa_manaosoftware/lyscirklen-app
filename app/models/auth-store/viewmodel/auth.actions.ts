import { types } from "mobx-state-tree"
import { AuthPropsModel } from "../auth.model"
import { FirebaseUserInfo } from "../../../utils/firebase/authentication/auth.types"

export const AuthActions = types.model(AuthPropsModel).actions(self => {
    const setFirebaseUserInfo = (value: FirebaseUserInfo) => {
        self.firebaseUserInfo = value
    }
    const setIsLoggedIn = (value: boolean | undefined) => {
        self.isLoggedIn = value
    }
    return { setFirebaseUserInfo, setIsLoggedIn }
})