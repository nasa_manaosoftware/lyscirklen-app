import { types } from "mobx-state-tree"
import { FirebaseUserInfo } from "../../utils/firebase/authentication/auth.types"

export const AuthPropsModel = {
    firebaseUserInfo: types.maybeNull(types.frozen<FirebaseUserInfo>()),
    isLoggedIn: types.maybeNull(types.boolean)
}