import { types } from "mobx-state-tree"
import { AuthViews, AuthActions } from "./viewmodel"
import { AuthPropsModel } from "./auth.model"
import { StoreName } from "../../constants/app.constant"


const AuthModel = types.model(StoreName.Auth, AuthPropsModel)


export const AuthStoreModel = types.compose(
    AuthModel,
    AuthViews,
    AuthActions)

