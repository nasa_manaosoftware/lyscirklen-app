import { Instance, SnapshotOut } from "mobx-state-tree"
import { AuthStoreModel } from "./auth.store"

export type AuthStore = Instance<typeof AuthStoreModel>
export type AuthStoreSnapshot = SnapshotOut<typeof AuthStoreModel>

export type IAuthResponse = {
    token?: string
    isLoggedOut?: boolean
}