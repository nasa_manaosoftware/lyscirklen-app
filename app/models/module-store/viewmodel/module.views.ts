
import { types } from "mobx-state-tree"
import { ModulePropsModel } from "../module.model"

export const ModuleViews = types.model(ModulePropsModel)
    .views(self => ({
        getModule(key: string) {
            return { ...self.allStoreData[key] }
        },
        get allModule() {
            return self.allStoreData
        }
    }))