import { types } from "mobx-state-tree"
import { ModulePropsModel } from "../module.model"
import { toJS } from "mobx"

export const ModuleActions = types.model(ModulePropsModel).actions(self => {
    const setModuleValues = (key: string, value: any) => {
        let allStoreData = toJS(self.allStoreData)
        if (!value) {
            delete allStoreData[key]
        } else {
            allStoreData = {
                ...allStoreData,
                ...{
                    [key]: value
                }
            }
        }
        self.allStoreData = allStoreData
    }

    const clearAllModules = () => {
        self.allStoreData = {}
    }
    return { setModuleValues, clearAllModules }
})