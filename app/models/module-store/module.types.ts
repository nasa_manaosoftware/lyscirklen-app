import { Instance, SnapshotOut } from "mobx-state-tree"
import { ModuleStoreModel } from "./module.store"
import { Property, MobileProperty } from "../../constants/firebase/fire-store.constant"

export type ModuleStore = Instance<typeof ModuleStoreModel>
export type ModuleStoreSnapshot = SnapshotOut<typeof ModuleStoreModel>

export type IModuleStore = {
    [Property.DATA]?: object,
    [MobileProperty.IS_INITIALIZED]?: boolean
}