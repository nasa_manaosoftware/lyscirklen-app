import { types } from "mobx-state-tree"
import { ModuleViews, ModuleActions } from "./viewmodel"
import { ModulePropsModel } from "./module.model"
import { StoreName } from "../../constants/app.constant"

const ModuleModel = types.model(StoreName.Module, ModulePropsModel)

export const ModuleStoreModel = types.compose(
    ModuleModel,
    ModuleViews,
    ModuleActions)

