import { types } from "mobx-state-tree"

export const ModulePropsModel = {
    allStoreData: types.optional(types.frozen(), {})
}