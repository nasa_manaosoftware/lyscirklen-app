import { Instance, SnapshotOut } from "mobx-state-tree"
import { PlaybackStoreModel } from "./playback-store.store"
export type PlaybackStore = Instance<typeof PlaybackStoreModel>
export type PlaybackStoreSnapshot = SnapshotOut<typeof PlaybackStoreModel>