import { types } from "mobx-state-tree"
import { Property } from "../../constants/firebase/fire-store.constant"
import { DOWNLOADEDBOOK } from "../downloaded-store/downloaded.types";

export const PlaybackPropsModel = {
    [Property.CURRENT_DURATION]: types.optional(types.number, 0),
    [Property.UID]: types.maybeNull(types.string),
    [Property.AUDIO_BOOK]: types.maybeNull(types.frozen<DOWNLOADEDBOOK>()),
    [Property.IS_PAUSE]: types.optional(types.boolean, true),
    [Property.IS_CLOSE_PLAYER]: types.optional(types.boolean, true),
    [Property.IS_COLLAPSE_PLAYER]: types.optional(types.boolean, true),
    [Property.IS_SHOW_MINI_PLAYER]: types.optional(types.boolean, false),
    [Property.VIDEO_DURATION]: types.optional(types.number, 0),
    [Property.IS_DONE]: types.optional(types.boolean, false),
    [Property.IS_SEEKING]: types.optional(types.boolean, false),
    [Property.ON_BUFFERING]: types.optional(types.boolean, false),
    [Property.PLAY_PAUSE]: types.optional(types.boolean, true),
    [Property.IS_ENDED]: types.optional(types.boolean, false),
    category_name: types.optional(types.string, '')
}