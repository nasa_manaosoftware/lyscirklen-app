import { types } from "mobx-state-tree"
import { PlaybackViews, PlaybackActions } from "./viewmodel"
import { PlaybackPropsModel } from "./playback-store.model"
import { StoreName } from "../../constants/app.constant"

export const PlaybackModel = types.model(StoreName.PlayBack, PlaybackPropsModel)

export const PlaybackStoreModel = types.compose(
    PlaybackModel,
    PlaybackViews,
    PlaybackActions)

