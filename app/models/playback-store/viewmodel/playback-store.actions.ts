import { types } from "mobx-state-tree"
import { PlaybackPropsModel } from "../playback-store.model"

export const PlaybackActions = types.model(PlaybackPropsModel).actions(self => {
    //MARK: Volatile State

    //MARK: Actions
    const setCurrentDuration = (value: number) => {       
        self.current_duration = value
        if(self.video_duration - self.current_duration <= 1) {
            self.is_done = false
        }
        // self.is_ended = false
    }

    const setUid = (value: string) => {
        self.uid = value
    }

    const setAudiobook = (value: any) => {
        self.audio_book = value
    }

    const setIsPause = (value: boolean) => {
        self.is_pause = value
        // self.play_pause = value
    }

    const setPlayPause = (value: boolean) => {
        self.play_pause = value
    }

    const setIsClosePlayer = (value: boolean) => {
        self.is_close_player = value
    }

    const setIsCollapse = (value: boolean) => {
        self.is_collapse_player = value
    }

    const setIsShowMiniPlayer = (value: boolean) => {
        self.is_show_mini_player = value
    }

    const setVideoDuration = (value: number) => {
        self.video_duration = value
    }

    const setIsDone = (value: boolean) => {
        self.is_done = value
    }

    const onBuffering = (value: boolean) => {
        self.on_buffering = value
    }

    const play = (value: any) => {
        self.is_close_player = false
        self.is_collapse_player = false
        self.is_pause = true
        self.play_pause = true
        self.audio_book = null
        if (self?.audio_book?.uid !== value?.uid) {
            self.is_pause = false
            self.play_pause = false
            self.video_duration = 0
            self.current_duration = 0
            self.audio_book = value
        }
    }

    const playAfterEnd = () => {
        self.is_pause = false
        self.is_done = false
        self.play_pause = self.is_pause
        self.current_duration = 0
    }

    const onShowPlayer = () => {
        self.is_close_player = false
        self.is_close_player = false
        self.is_collapse_player = false
    }

    const onHidePlayer = () => {
        self.is_collapse_player = true
        if (self.is_close_player) {
            self.is_show_mini_player = false
        } else if (self.audio_book && self.is_collapse_player) {
            self.is_show_mini_player = true
        }
        self.is_close_player = false
    }

    const onClosePlayer = () => {
        self.is_close_player = true
        self.is_pause = true
        self.play_pause = true
        self.is_show_mini_player = false
        self.is_collapse_player = true
    }

    const onLoaded = (duration: number) => {
        self.video_duration = duration
        self.is_done = false
        self.current_duration = 0
        self.is_pause = false
        self.play_pause = false
    }

    const onEnded = () => {
        self.is_done = true
        self.is_pause = true
        self.play_pause = true
        self.is_seeking = false
        self.is_ended = true
    }

    const setCategoryName = (value: string) => {
        self.category_name = value
    }

    const setIsSeeking = (value: boolean) => {
        self.is_seeking = value
    }

    const setIsEnded = (value: boolean) => {
        self.is_ended = value
    }

    return {
        setCurrentDuration,
        setUid,
        setAudiobook,
        setIsPause,
        setIsClosePlayer,
        setIsCollapse,
        setIsShowMiniPlayer,
        setVideoDuration,
        setCategoryName,
        setIsDone,
        onShowPlayer,
        onHidePlayer,
        onClosePlayer,
        onLoaded,
        onEnded,
        play,
        playAfterEnd,
        onBuffering,
        setPlayPause,
        setIsSeeking,
        setIsEnded
    }
})