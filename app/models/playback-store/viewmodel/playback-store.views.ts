import { types } from "mobx-state-tree"
import { DOWNLOADEDBOOK } from "../../downloaded-store/downloaded.types"
import { PlaybackPropsModel } from "../playback-store.model"
export const PlaybackViews = types.model(PlaybackPropsModel)
    .views(self => ({
        get getCurrentDuration(): number {
            return self.current_duration
        },
        get getUid(): string {
            return self.uid
        },
        get getAudiobook(): DOWNLOADEDBOOK {
            return self.audio_book
        },
        get getIsPause(): boolean {
            return self.is_pause
        },
        get getPlayPause(): boolean {
            return self.play_pause
        },
        get getIsClosePlayer(): boolean {
            return self.is_close_player
        },
        get getIsCollapse(): boolean {
            return self.is_collapse_player
        },
        get getIsShowMiniPlayer(): boolean {
            return self.is_show_mini_player
        },
        get getVideoDuration(): number {
            return self.video_duration
        },
        get getIsDone(): boolean {
            return self.is_done || (self.video_duration - self.current_duration <= 1)
        },
        get getCategoryName(): string {
            return self.category_name
        },
        get getOnBuffering(): boolean {
            return self.on_buffering
        },
        get getIsSeeking(): boolean {
            return self.is_seeking
        },
        get getIsEnded(): boolean {
            return self.is_ended
        }
    }))