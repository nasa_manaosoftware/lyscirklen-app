import { castToSnapshot, types, unprotect } from "mobx-state-tree"
import { AudiobookPropsModel } from "../audiobook.model"
import { AUDIOBOOK } from "../audiobook.types"

export const AudiobookActions = types.model(AudiobookPropsModel).actions(self => {
    const setAudiobooksList = (value: (Partial<AUDIOBOOK>)[]) => {
        self.audiobooksList = castToSnapshot(value)      
    }
    const setAudiobooksRandom = (value: (Partial<AUDIOBOOK>)[]) => {
        self.audiobooksRandom = value
    }
    const setMessagesTitle = (value: string) => {
        self.messagesTitle = value
    }
    const setMessagesDetail = (value: string) => {
        self.messagesDetail = value
    }

    return {
        setAudiobooksList,
        setAudiobooksRandom,
        setMessagesTitle,
        setMessagesDetail
    }
})