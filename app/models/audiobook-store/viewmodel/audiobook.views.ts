import { types } from "mobx-state-tree";
import { AudiobookPropsModel } from "../audiobook.model";
import { AUDIOBOOK } from "../audiobook.types";

export const AudiobookViews = types.model(AudiobookPropsModel).views(self => ({
    get getAudiobooksList(): (Partial<AUDIOBOOK>)[] {
        return self.audiobooksList
    },
    get getAudiobooksRandom(): (Partial<AUDIOBOOK>)[] {
        return self.audiobooksRandom
    },
    get getMessagesTitle(): string {
        return self.messagesTitle
    },
    get getMessagesDetail(): string {
        return self.messagesDetail
    },
}))