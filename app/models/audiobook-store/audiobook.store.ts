import { types } from "mobx-state-tree"
import { StoreName } from "../../constants/app.constant"
import { AudiobookPropsModel } from "./audiobook.model"
import { AudiobookActions, AudiobookViews } from "./viewmodel"

const AudiobookModel = types.model(StoreName.Audiobook, AudiobookPropsModel)

export const AudiobookStoreModel = types.compose(
    AudiobookModel,
    AudiobookViews,
    AudiobookActions
)