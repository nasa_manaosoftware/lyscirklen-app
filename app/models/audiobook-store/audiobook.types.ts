import { Instance, SnapshotOut } from "mobx-state-tree"
import { Property } from "../../constants/firebase/fire-store.constant"
import { AudiobookStoreModel } from "./audiobook.store"

export type AudiobookStore = Instance<typeof AudiobookStoreModel>
export type AudiobookStoreSnapshot = SnapshotOut<typeof AudiobookStoreModel>

export type AUDIOBOOK_IMAGE = {
    [Property.FILE_NAME]?: string,
    // [Property.STORAGE_PATH]?: string,
    // [Property.THUMBNAIL_URL]?: string,
    [Property.URL]?: string
}

export type AUDIOBOOK_VIDEO = {
    [Property.NAME]?: string,
    [Property.URL]?: string,
    [Property.DOWNLOAD_URL]?: string,
    [Property.STREAMING_URL]?: string
}

export type AUDIOBOOK = {
    [Property.UID]?: string,
    [Property.CATEGORY_ID]?: string,
    [Property.DESCRIPTION]?: string,
    [Property.FORFATTER]?: string,
    [Property.IMAGE]?: AUDIOBOOK_IMAGE,
    [Property.NAME]?: string,
    [Property.ORGANIZATION_ID]?: string,
    // [Property.VIDEO_DURATION]?: number,
    [Property.VIDEO]?: string,
    // [Property.DOWNLOAD_STATUS]?: string,
    // [Property.IS_DOWNLOADING]?: boolean,
    // [Property.FILE_PATH]?: string
}