import { types } from "mobx-state-tree";
import { AUDIOBOOK} from "./audiobook.types";

export const AudiobookPropsModel = {
    audiobooksList:  types.optional(types.frozen<(Partial<AUDIOBOOK>)[]>(), []),
    audiobooksRandom: types.optional(types.frozen<(Partial<AUDIOBOOK>)[]>(), []),
    messagesTitle: types.optional(types.string, ""),
    messagesDetail: types.optional(types.string, "")
}

