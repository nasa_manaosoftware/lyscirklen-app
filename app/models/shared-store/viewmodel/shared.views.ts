import { types } from "mobx-state-tree"
import { SharedPropsModel } from "../shared.model"

export const SharedViews = types.model(SharedPropsModel)
    .views(self => ({
        get getIsEditing() {
            return self.isEditing
        },
        get getIsConnected() {
            return self.isConnected
        },

        get getIsLoading() {
            return self.isLoading
        },
        // get getServerTime() {
        //     return self.serverTime
        // },
        get getTimer() {
            return self.timer
        }
    }))