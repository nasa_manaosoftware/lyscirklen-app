import { types } from "mobx-state-tree"
import { SharedPropsModel } from "../shared.model"
import { RootStore } from "../../root-store/root.types"
import BackgroundTimer from 'react-native-background-timer'
import moment from "moment"
import * as LocaleUtils from "../../../utils/locale"
import 'moment/locale/da'
export const SharedActions = types.model(SharedPropsModel).actions(self => {
    const setIsEditing = (value: boolean) => {
        self.isEditing = value
    }
    const setIsConnected = (value: boolean) => {
        self.isConnected = value
    }

    const setIsLoading = (value: boolean) => {
        if (value === self.isLoading) return
        self.isLoading = value
    }

    const setIsTimerSet = (rootStore?: RootStore, value?: boolean) => {
        self.isTimerSet = value
        if (value) {
            runInterval(rootStore)
        } else {
            invalidateTimer()
        }
    }

    const runInterval = (rootStore?: RootStore) => {
        const timeTickingProcess = async () => {
            const serverDate = moment(rootStore?.getSharedStore?.getServerTime)
            const updatedDate = moment(self.dateTime)
            self.dateTimeAgo = moment.duration(updatedDate.diff(serverDate)).locale(LocaleUtils.getLocaleDevice().toLowerCase()).humanize(true)
        }
        timeTickingProcess()
        let timer = BackgroundTimer.setInterval(() => timeTickingProcess(), 1000)
        self.timer = timer
    }

    const invalidateTimer = () => {
        BackgroundTimer.clearInterval(self.timer)
    }

    return {
        setIsEditing,
        setIsConnected,
        setIsLoading,
        setIsTimerSet
    }
})