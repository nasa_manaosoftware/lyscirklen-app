import { types } from "mobx-state-tree"
import { IFlatlistSelection } from "./shared.types"
import { MobileProperty, Property } from "../../constants/firebase/fire-store.constant"

export const SharedPropsModel = {
    isEditing: types.optional(types.boolean, false),
    isConnected: types.maybeNull(types.boolean),
    isLoading: types.optional(types.boolean, false),

    /*
        Instance of constant value from once initialize on launchscreen to increase performance.
    */
    /*
        Time tricking handling
    */
    [MobileProperty.DATE_TIME]: types.maybeNull(types.frozen()),
    [MobileProperty.IS_TIMER_SET]: types.optional(types.boolean, false),
    [MobileProperty.TIMER]: types.maybeNull(types.frozen()),
    [MobileProperty.DATE_TIME_AGO]: types.maybeNull(types.string),
}