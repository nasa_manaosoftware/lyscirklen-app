import { types } from "mobx-state-tree"
import { NavigationStoreModel } from "../navigation-store"
import { UserStoreModel } from "../user-store"
import { AuthStoreModel } from "../auth-store"
import { ModuleStoreModel } from "../module-store"
import { SharedStoreModel } from "../shared-store"
import { GeneralResourcesStoreModel } from "../general-resources-store"
import { NotificationStoreModel } from "../notification-store"
import { MobileProperty, Property } from "../../constants/firebase/fire-store.constant"
import { AudiobookStoreModel } from "../audiobook-store"
import { AudiobookCategoriesStoreModel } from "../audiobook-category-store"
import { DownloadedStoreModel } from "../downloaded-store/downloaded.store"
import { PlaybackStoreModel } from "../playback-store/playback-store.store"

export const RootPropsModel = {
    NavigationStore: types.optional(NavigationStoreModel, {}),
    NotificationStore: types.optional(NotificationStoreModel, {}),
    UserStore: types.optional(UserStoreModel, {}),
    AuthStore: types.optional(AuthStoreModel, {}),
    SharedStore: types.optional(SharedStoreModel, {}),
    ModuleStore: types.optional(ModuleStoreModel, {}),
    GeneralResourcesStore: types.optional(GeneralResourcesStoreModel, {}),
    isFetchingResourcesPropertyStore: types.optional(types.boolean, true),
    isNewApp: types.optional(types.boolean, true),

    //BD stores
    AudioBooksStore: types.optional(AudiobookStoreModel, {}),
    AudioBookCategoriesStore: types.optional(AudiobookCategoriesStoreModel, {}),

    //Manao_Tester
    [MobileProperty.UNIQUE_MOBILE_ID]: types.maybeNull(types.string),
    [Property.IS_MANAO_TESTER]: types.optional(types.boolean, false),

    /** BD **/
    isSelectedFront: types.optional(types.boolean, true),
    isSelectedSearch: types.optional(types.boolean, false),
    isSelectedMyLibrary: types.optional(types.boolean, false),
    isSelectedNotification: types.optional(types.boolean, false),
    isSelectedAboutUs: types.optional(types.boolean, false),
    isSelectedTermOfUse: types.optional(types.boolean, false),
    isSelectedPrivacy: types.optional(types.boolean, false),
    AudiobookStore: types.optional(AudiobookStoreModel, {}),
    AudiobookCetegoriesStore: types.optional(AudiobookCategoriesStoreModel, {}),
    index: types.optional(types.number, 0),
    termOfUseDescription: types.optional(types.string, ''),
    aboutUSDescription: types.optional(types.string, ''),
    privacyPolicyDescription: types.optional(types.string, ''),
    isShowTermOfUse: types.optional(types.boolean, false),
    isShowPrivacy: types.optional(types.boolean, false),
    appName: types.optional(types.string, ''),
    organization_id: types.optional(types.string, ''),
    deviceId: types.optional(types.string, ''),
    DownloadedStore: types.optional(DownloadedStoreModel, {}),
    appState: types.optional(types.string, ''),

    // Playback
    PlaybackStore: types.optional(PlaybackStoreModel, {}),

    language: types.optional(types.string, 'da'),
}