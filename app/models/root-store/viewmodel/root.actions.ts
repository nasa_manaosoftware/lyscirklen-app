import { types, unprotect } from "mobx-state-tree"
import { RootPropsModel } from "../root-store-model"
import { NavigationStoreModel } from "../../navigation-store"
import { UserStoreModel } from "../../user-store"
import { AuthStoreModel } from "../../auth-store"
import { DownloadedStoreModel } from "../../downloaded-store/downloaded.store"
// import { DownloadingStoreModel } from "../../downloading-store/downloading.store"
import { PlaybackStoreModel } from "../../playback-store"


export const RootActions = types.model(RootPropsModel).actions(self => {

    const setNavigationStore = (value: any) => {
        self.NavigationStore = value
    }
    // const setFilterStore = (value: any, rootStore?: RootStore) => {
    //     self.FilterStore = value
    //     rootStore && self.FilterStore.setDefaultValue(rootStore)
    // }
    const setUserStore = (value: any) => {
        self.UserStore = value
    }

    const setAuthStore = (value: any) => {
        self.AuthStore = value
    }
    const setSharedStore = (value: any) => {
        self.SharedStore = value
    }
    const setNotificationStore = (value: any) => {
        self.NotificationStore = value
    }
    const setModuleStoreValue = (key: string, value: any) => {
        self.ModuleStore.setModuleValues(key, value)
        self.ModuleStore.allStoreData = {
            ...self.ModuleStore.allStoreData,
            ...value
        }
    }
    const removeModuleStore = () => {
        self.ModuleStore.clearAllModules()
    }
    const setGeneralResourcesStore = (value: any) => {
        self.GeneralResourcesStore = value
    }
    const setIsFetchingResources = (value: boolean) => {
        self.isFetchingResourcesPropertyStore = value
    }
    const setIsNewApp = (value: boolean) => {
        self.isNewApp = value
    }
    const resetRootStore = () => {
        setNavigationStore(NavigationStoreModel.create({}))
        setUserStore(UserStoreModel.create({}))
        setAuthStore(AuthStoreModel.create({}))
        setUserStore(UserStoreModel.create({}))
        removeModuleStore()
    }
    const resetPlaybackStore = () => {
        setPlaybackStore(PlaybackStoreModel.create({}))
    }
    const setIsManaoTester = (value: boolean) => {
        self.is_manao_tester = value
    }
    const setUniqueMobileId = (value: string) => {
        self.unique_mobile_id = value
    }
    const setIsSelectedFront = (value: boolean) => {
        self.isSelectedFront = value
    }
    const setIsSelectedSearch = (value: boolean) => {
        self.isSelectedSearch = value
    }
    const setIsSelectedMyLibrary = (value: boolean) => {
        self.isSelectedMyLibrary = value
    }
    const setIsSelectedNotofication = (value: boolean) => {
        self.isSelectedNotification = value
    }
    const setIsSelectedAboutUs = (value: boolean) => {
        self.isSelectedAboutUs = value
    }
    const setIsSelectedTermOfUse = (value: boolean) => {
        self.isSelectedTermOfUse = value
    }
    const setIsSelectedPrivacy = (value: boolean) => {
        self.isSelectedPrivacy = value
    }
    const setIndex = (value: number) => {
        self.index = value
    }
    const setTermOfUse = (value: string) => {
        self.termOfUseDescription = value
    }
    const setAboutUS = (value: string) => {
        self.aboutUSDescription = value
    }
    const setPrivacyPolicy = (value: string) => {
        self.privacyPolicyDescription = value
    }
    const setIsShowTermOfUse = (value: boolean) => {
        self.isShowTermOfUse = value
    }
    const setIsShowPrivacy = (value: boolean) => {
        self.isShowPrivacy = value
    }
    const setAudioBooksStore = (value: any) => {
        self.AudioBooksStore = value
    }
    const setAudioBookCategoriesStore = (value: any) => {
        self.AudioBookCategoriesStore = value
    }
    const setAppName = (value: string) => {
        self.appName = value
    }
    const setOrganizationId = (value: string) => {
        self.organization_id = value
    }
    const setDeviceId = (value: string) => {
        self.deviceId = value
    }
    const setDownloadedStore = (value: any) => {
        self.DownloadedStore = value
    }

    const setPlaybackStore = (value: any) => {
        self.PlaybackStore = value
    }

    const setAppState = (value: string) => {
        self.appState = value
    }

    const setLanguage = (value: string) => {
        self.language = value
    }

    return {
        setNavigationStore,
        setUserStore,
        setAuthStore,
        setSharedStore,
        setModuleStoreValue,
        setGeneralResourcesStore,
        setIsFetchingResources,
        setIsNewApp,
        resetRootStore,
        setNotificationStore,
        // setFilterStore,
        setIsManaoTester,
        setUniqueMobileId,
        setIsSelectedFront,
        setIsSelectedSearch,
        setIsSelectedMyLibrary,
        setIsSelectedAboutUs,
        setIsSelectedTermOfUse,
        setIsSelectedPrivacy,
        setIndex,
        setTermOfUse,
        setPrivacyPolicy,
        setAboutUS,
        setIsShowTermOfUse,
        setIsShowPrivacy,
        setAudioBooksStore,
        setAudioBookCategoriesStore,
        setAppName,
        setOrganizationId,
        setIsSelectedNotofication,
        setDeviceId,
        setDownloadedStore,
        setPlaybackStore,
        resetPlaybackStore,
        setAppState,
        setLanguage
    }
})

