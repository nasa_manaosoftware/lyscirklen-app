import { types } from "mobx-state-tree"
import { RootPropsModel } from "../root-store-model"
import * as Utils from "../../../utils"
import * as DataUtils from "../../../utils/data.utils"

export const RootViews = types.model(RootPropsModel)
    .views(self => ({
        get getNavigationStore() {
            return self.NavigationStore
        },
        get getUserStore() {
            return self.UserStore
        },
        // get getChatStore() {
        //     return self.ChatStore
        // },
        get getAuthStore() {
            return self.AuthStore
        },
        get getSharedStore() {
            return self.SharedStore
        },
        get getAllModuleStore(): any {
            return self.ModuleStore
        },
        get getNotificationStore() {
            return self.NotificationStore
        },
        getModuleStore(key: string) {
            const object = self.ModuleStore.getModule(key)
            return object && !Utils.isEmptyObject(object) && object || null
        },
        get getAllGeneralResourcesStore() {
            return self.GeneralResourcesStore
        },
        getGeneralResourcesStore(locale?: string, rootKey?: string, childKey?: string | boolean): any {
            return self.GeneralResourcesStore.getValues(locale, rootKey, childKey)
        },

        get getIsNewApp() {
            return self.isNewApp
        },

        get getIsFetchingResources() {
            return self.isFetchingResourcesPropertyStore
        },
        // get getFilterStore() {
        //     return self.FilterStore
        // },
        get getIsManaoTester() {
            const devices: any[] = DataUtils.getSelectObject(self?.GeneralResourcesStore.getDevices)
            console.log(devices)
            return devices.some(e => e.key === self.unique_mobile_id)
        },

        /** BD **/
        get getIsSelectedFront() {
            return self.isSelectedFront
        },
        get getIsSelectedSearch() {
            return self.isSelectedSearch
        },
        get getIsSelectedMyLibrary() {
            return self.isSelectedMyLibrary
        },
        get getIsSelectedNotification() {
            return self.isSelectedNotification
        },
        get getIsSelectedAboutUs() {
            return self.isSelectedAboutUs
        },
        get getIsSelectedTermOfUse() {
            return self.isSelectedTermOfUse
        },
        get getIsSelectedPrivacy() {
            return self.isSelectedPrivacy
        },
        get getIndex() {
            return self.index
        },
        get getTermOfUse() {
            return self.termOfUseDescription
        },
        get getAboutUS() {
            return self.aboutUSDescription
        },
        get getPrivacyPolicy() {
            return self.privacyPolicyDescription
        },
        get getIsShowTermOfUse() {
            return self.isShowTermOfUse
        },
        get getIsShowPrivacy() {
            return self.isShowPrivacy
        },
        get getAudioBooksStore() {
            return self.AudioBooksStore
        },
        get getAudioBookCategoriesStore() {
            return self.AudioBookCategoriesStore
        },
        get getAppName(): string {
            return self.appName
        },
        get getOrganizationId(): string {
            return self.organization_id
        },
        get getDeviceId(): string {
            return self.deviceId
        },
        get getDownloadedStore() {
            return self.DownloadedStore
        },
        // get getDownloadingList() {
        //     return self.DownloadingList
        // },

        // Playback
        get getPlaybackStore(): any {
            return self.PlaybackStore
        },

        get getAppState(): string {
            return self.appState
        },
        get getLanguage() {
            return self.language
        }
    }))