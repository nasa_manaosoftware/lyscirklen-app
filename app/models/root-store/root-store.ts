import { types } from "mobx-state-tree"
import { StoreName } from "../../constants/app.constant"
import { RootViews, RootActions } from "./viewmodel"
import { RootPropsModel } from "./root-store-model"

const RootModel = types.model(StoreName.Root).props(RootPropsModel)

export const RootStoreModel = types.compose(RootModel, RootViews, RootActions)