import { Instance, SnapshotOut } from "mobx-state-tree"
import { RootStoreModel } from "./root-store"

//@ts-ignore
export interface RootStore extends Instance<typeof RootStoreModel> { }

export interface RootStoreSnapshot extends SnapshotOut<typeof RootStoreModel> { }