import { types } from "mobx-state-tree"

export const GeneralResourcesPropsModel = {
    version: types.maybeNull(types.string),
    generalResources: types.maybeNull(types.frozen()),
    notifications: types.maybeNull(types.frozen()),
    devices: types.maybeNull(types.frozen()),
}