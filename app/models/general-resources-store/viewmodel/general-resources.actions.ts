import { types } from "mobx-state-tree"
import { GeneralResourcesPropsModel } from "../general-resources.model"

export const GeneralResourcesActions = types.model(GeneralResourcesPropsModel).actions(self => {
    const setGeneralResources = (value: any) => {
        self.generalResources = value
    }

    const setNotification = (value: any) => {
        self.notifications = value
    }
    const setDevices = (value: any) => {
        self.devices = value
    }
    return {
        setGeneralResources,
        setNotification,
        setDevices
    }
})

