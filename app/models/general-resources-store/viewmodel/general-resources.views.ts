import { types } from "mobx-state-tree"
import { GeneralResourcesPropsModel } from "../general-resources.model"
import * as Utils from "../../../utils"

export const GeneralResourcesViews = types.model(GeneralResourcesPropsModel)
    .views(self => ({
        getValues(locale?: string, rootKey?: string, childKeyORShareKey?: string | boolean, isChildDotNotation: boolean = false): any {
            const notExistingKey = "NOT_EXISTING_KEY"
            if (this.getGeneralResources(locale)) {
                if (childKeyORShareKey) {
                    if (childKeyORShareKey == true) {
                        return Utils.refDotNotation(this.getGeneralResources(locale), rootKey) || notExistingKey
                    } else if (this.getGeneralResources(locale)[rootKey]) {
                        const resources = this.getGeneralResources(locale)[rootKey]
                        if (isChildDotNotation) return Utils.refDotNotation(resources, childKeyORShareKey) || notExistingKey
                        return resources[childKeyORShareKey]
                    }
                }
            }

            return notExistingKey
        },
        getGeneralResources(locale: string): any {
            return self.generalResources ? self.generalResources[locale] : {}
        },

        getNotification(rootKey: string): string {
            return self.notifications && Utils.refDotNotation(self.notifications, rootKey)
        },
        get getDevices(): any {
            return self.devices
        },
    }))