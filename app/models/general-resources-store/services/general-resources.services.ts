import { types, flow } from "mobx-state-tree"
import { GeneralResourcesPropsModel } from "../general-resources.model"
import * as FirebaseUtils from "../../../utils/firebase/remote-config"
import * as AppConstant from "../../../constants/app.constant"
import * as RMCFConstant from "../../../constants/firebase/remote-config"
import * as Locale from "../../../utils/locale"
import * as FileSystem from "../../../utils/file-system"
import FireBaseAuthServices from "../../../utils/firebase/authentication/auth"
import fireStore from "../../../utils/firebase/fire-store/fire-store"

export const GeneralResourcesServiceActions = types.model(GeneralResourcesPropsModel).actions(self => {
    //MARK: Volatile State


    //MARK: Helper

    //MARK: Service Actions

    const setGeneralResources = (object: any) => {

        const { GeneralResources } = RMCFConstant.RemoteConfigRootKey
        self.generalResources = {
            [AppConstant.Locale.Denmark]: object[GeneralResources],
        }
    }

    const fetchRemoteConfig = flow(function* () {
        try {
            const object = yield FirebaseUtils.getRemoteConfigValues()
            const path = '/'.concat(AppConstant.GeneralResourcesJSONPath.Path)

            // console.log('object ', object);
            
            
            if (object) {
                yield FileSystem.writeFile(path, object)
                setGeneralResources(object)
            }
        } catch (e) {
            console.log(e)
        }
    })

    const fetchLocalizedGeneralResources = flow(function* () {
        try {
            const path = '/'.concat(AppConstant.GeneralResourcesJSONPath.Path)
            let object = yield FileSystem.readFile(path)
            
            if (!object) {
                object = yield Locale.getLocalizedGeneralResources()
            }
            if (object) {
                yield FileSystem.writeFile(path, object)
                setGeneralResources(object)
            }
        } catch (e) {
            console.log(e)
        }
    })

    const fetchCurrentUser = () => {
        try {
            return FireBaseAuthServices.fetchCurrentUser()
        } catch (e) {
            console.log(e)
            return e
        }
    }

    const getPrivacyPolicy = (value: string) => {
        try {
            return fireStore.getPrivacyPolicy(value)

        } catch (e) {
            console.log(e)
            return e
        }
    }

    const getTermOfUse = (value: string) => {
        try {
            return fireStore.getTermOfUse(value)

        } catch (e) {
            console.log(e)
            return e
        }
    }

    const getAboutUS = (value: string) => {
        try {
            return fireStore.getAboutUS(value)

        } catch (e) {
            console.log(e)
            return e
        }
    }

    return {
        fetchRemoteConfig,
        fetchLocalizedGeneralResources,
        fetchCurrentUser,
        getPrivacyPolicy,
        getTermOfUse,
        getAboutUS
    }
})

