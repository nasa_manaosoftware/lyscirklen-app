import { Instance, SnapshotOut } from "mobx-state-tree"
import { GeneralResourcesStoreModel } from "./general-resources.store"

export type GeneralResourcesStore = Instance<typeof GeneralResourcesStoreModel>
export type GeneralResourcesStoreSnapshot = SnapshotOut<typeof GeneralResourcesStoreModel>