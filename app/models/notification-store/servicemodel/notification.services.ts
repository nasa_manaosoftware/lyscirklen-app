import { types, flow } from "mobx-state-tree"
import NotificationServices from "../../../services/api-domain/notification.services"
import { NotificationPropsModel } from "../notification-store.model"
import { INotification } from "../notification-store.types"

export const NotificationServiceActions = types.model(NotificationPropsModel).actions(self => {
    const markReadNotificationById = (notificationId: string) => {
        const params: Partial<INotification> = {
            notification_id: notificationId
        }

        NotificationServices.markReadNotificationById(params)
    }

    const markReadNotificationByTopic = (topic: string) => {
        const params: Partial<INotification> = {
            topic: topic
        }

        NotificationServices.markReadCommentNotificationByTopic(params)
    }

    return {
        markReadNotificationById,
        markReadNotificationByTopic
    }
})