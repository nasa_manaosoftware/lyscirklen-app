import { types } from "mobx-state-tree";
import { DOWNLOADEDBOOK } from "./downloaded.types";

export const DownloadedPropsModel = {
    downloadedList: types.optional(types.frozen<(Partial<DOWNLOADEDBOOK>)[]>(), [])
}