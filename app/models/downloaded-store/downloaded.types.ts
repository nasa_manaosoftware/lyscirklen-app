import { Instance, SnapshotOut } from "mobx-state-tree"
import { Property } from "../../constants/firebase/fire-store.constant"
import { AUDIOBOOK_IMAGE } from "../audiobook-store/audiobook.types"
import { DownloadedStoreModel } from "./downloaded.store"

export type DownloadedStore = Instance<typeof DownloadedStoreModel>
export type DownloadedStoreSnapshot = SnapshotOut<typeof DownloadedStoreModel>

export type AUDIOBOOK_VIDEO = {
    [Property.NAME]?: string,
    [Property.URL]?: string,
    [Property.DOWNLOAD_URL]?: string
}

export type DOWNLOADEDBOOK = {
    [Property.UID]?: string,
    [Property.CATEGORY_ID]?: string,
    [Property.DESCRIPTION]?: string,
    [Property.FORFATTER]?: string,
    [Property.IMAGE]?: AUDIOBOOK_IMAGE,
    [Property.NAME]?: string,
    [Property.ORGANIZATION_ID]?: string,
    [Property.VIDEO]?: AUDIOBOOK_VIDEO,
    [Property.DOWNLOAD_STATUS]?: string,
    [Property.FILE_PATH]?: string,
    [Property.JOB_ID]?: number
}
