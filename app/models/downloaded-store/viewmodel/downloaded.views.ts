import { types } from "mobx-state-tree";
import { DownloadedPropsModel } from "../downloaded.models";
import { DOWNLOADEDBOOK } from "../downloaded.types";

export const DownloadedViews = types.model(DownloadedPropsModel).views(self => ({

    get getDownloadedList(): (Partial<DOWNLOADEDBOOK>)[]  {
        return self.downloadedList
    },

}))