import { types } from "mobx-state-tree"
import { DownloadedPropsModel } from "../downloaded.models"
import { DOWNLOADEDBOOK } from "../downloaded.types"

export const DownloadedActions = types.model(DownloadedPropsModel).actions(self => {
    const setDownloadedList = (value: (Partial<DOWNLOADEDBOOK>)[]) => {
        self.downloadedList = value
    }
    return {
        setDownloadedList
    }
})