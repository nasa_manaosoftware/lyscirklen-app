import { types } from "mobx-state-tree"
import { StoreName } from "../../constants/app.constant"
import { DownloadedPropsModel } from "./downloaded.models"
import { DownloadedActions, DownloadedViews } from "./viewmodel"

const DownloadedModel = types.model(StoreName.Downloaded, DownloadedPropsModel)

export const DownloadedStoreModel = types.compose(
    DownloadedModel,
    DownloadedViews,
    DownloadedActions
)