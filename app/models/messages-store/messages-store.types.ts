import { Instance, SnapshotOut } from "mobx-state-tree"
import { MessagesStoreModel } from "./messages-store.store"

export type SearchStore = Instance<typeof MessagesStoreModel>
export type MessagesStoreSnapshot = SnapshotOut<typeof MessagesStoreModel>

export interface IMessages  {
    uid: string,
    button_text: string,
    message_id: string,
    heading: string,
    message: string,
    organization_id: string,
    description: string,
    sent_at: any,
    read_message_status: boolean

}

