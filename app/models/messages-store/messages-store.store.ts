import { types } from "mobx-state-tree"
import { MessagesViews, MessagesActions } from "./viewmodel"
import { MessagesPropsModel } from "./messages-store.model"
import { StoreName } from "../../constants/app.constant"
import { MessagesServiceActions } from "./servicemodel"


export const MessagesModel = types.model(StoreName.Messages, MessagesPropsModel)

export const MessagesStoreModel = types.compose(
    MessagesModel,
    MessagesViews,
    MessagesActions,
    MessagesServiceActions)

