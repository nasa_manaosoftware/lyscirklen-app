import { types } from "mobx-state-tree"
import { MessagesPropsModel } from "../messages-store.model"
export const MessagesActions = types.model(MessagesPropsModel).actions(self => {
  //MARK: Volatile State

  //MARK: Actions
  const setMarkRead = () => {
      self.read_message_status = true
  }
  return {
    setMarkRead,
  }
})
