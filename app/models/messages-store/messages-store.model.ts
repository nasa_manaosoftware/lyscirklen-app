import { types } from "mobx-state-tree"

export const MessagesPropsModel = {
    uid: types.maybeNull( types.string ),
    button_text: types.maybeNull( types.string ),
    message_id: types.maybeNull( types.string ),
    heading: types.maybeNull( types.string ),
    message: types.maybeNull( types.string ),
    organization_id: types.maybeNull( types.string ),
    description: types.maybeNull( types.string ),
    sent_at: types.optional(types.frozen(), null ),
    read_message_status: types.maybeNull( types.boolean )
}

