import { types, flow } from "mobx-state-tree"
import { MessagesPropsModel } from "../messages-store.model"

export const MessagesServiceActions = types.model(MessagesPropsModel).actions(self => {

    /*
         No need to use `flow` and `yield` because we don't care about any responses.
    */

    // const markReadNotificationByTopic = (topic: string) => {
    //     const params: Partial<IMessages> = {
    //         topic: topic
    //     }
    //     SearchServices.markReadCommentNotificationByTopic(params)
    // }

    return {
    }
})