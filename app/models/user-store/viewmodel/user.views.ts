import { types } from "mobx-state-tree"
import { UserPropsModel } from "../user.model"
import { ISelect, IUser, IFireStorage } from "../user.types"
import { IUploadFileData } from "../../../utils/firebase/fire-storage/fire-storage.types"
import { Image } from "react-native-image-crop-picker"
import * as DataUtils from "../../../utils/data.utils"
import { INotification } from "../../notification-store/notification-store.types"
import { RootStore } from "../../root-store/root.types"
import { GeneralResources } from "../../../constants/firebase/remote-config/remote-config.constant"

export const UserViews = types.model(UserPropsModel).views(self => ({
    get getUserId(): string {
        return self.uid
    },
    get getCreatorId(): string {
        return self.user_id
    },
    get getFullName(): string {
        return self.full_name
    },
    get getEmail(): string {
        return self.email
    },

    get getPassword(): string {
        return self.password
    },
    get getDescription(): string {
        return self.description
    },
    get getConfirmPassword(): string {
        return self.confirmPassword
    },
    get getIsVerified(): boolean {
        return self.is_verified
    },
    get getDeviceToken(): string {
        return self.device_token
    },
    get getIsHighlight(): boolean {
        return self.is_highlight
    },

    get getMembershipNumber(): string {
        return self.membership_number
    },

    get getShouldUploadProfileImage(): (Partial<IUploadFileData & Image>)[] {
        const isSame = DataUtils.isEqual(self.profile_image, self.compare_profile_image)
        return !isSame ? self.profile_image?.filter(x => !self.compare_profile_image?.includes(x)) : []
    },

    get getProfileImageToUpdateDetails(): IFireStorage {
        return self.profile_image?.length > 0 && {
            file_name: self.profile_image[0].file_name,
            storage_path: self.profile_image[0].storage_path,
            url: self.profile_image[0].url
        }
    },

    get getProfileImage(): (Partial<IUploadFileData & Image>)[] {
        return self.profile_image
    },

    get getProfileImagesToRemove(): (Partial<IUploadFileData & Image>)[] {
        return self.profile_image_to_remove
    },
    get getStatus(): string {
        return self.status
    },

    get getExpiryDate(): any {
        return self.expiry_date
    },
    get getIsTimerSet(): boolean {
        return self.isTimerSet
    },
    getNotificationById(id: string): INotification {
        const filtered = self.notificationList?.filter(e => e.notification_id === id)
        return filtered?.length > 0 && filtered[0]
    },

    get getIsManaoTester(): boolean {
        return self.is_manao_tester
    },

    get getOrganizationId(): string {
        return self.organization_id
    },

    get getBadgeNumber(): number {
        return self.badge_number
    }
}))