import { types, isAlive } from "mobx-state-tree"
import { UserPropsModel } from "../user.model"
import { IUser, ISelect, UserStore } from "../user.types"
import { Image } from "react-native-image-crop-picker"
import { IUploadFileData } from "../../../utils/firebase/fire-storage/fire-storage.types"
import { RootStore } from "../../root-store/root.types"
import BackgroundTimer from 'react-native-background-timer'
import moment from "moment"
import { INotification } from "../../notification-store"

export const UserActions = types.model(UserPropsModel).actions(self => {
    const setUser = (value: Partial<IUser>) => {
        self.uid = value.uid
        self.full_name = value.full_name
    }
    const setDescription = (value: string) => {
        self.description = value
    }
    const setPassword = (value: string) => {
        self.password = value
    }
    const setMembershipNumber = (value: string) => {
        self.membership_number = value
    }
    const setUserId = (value: string) => {
        self.uid = value
    }
    const setDeviceToken = (value: string) => {
        self.device_token = value
    }
    const setStatus = (value: string) => {
        self.status = value
    }
    const setExpiryDate = (value?: any) => {
        self.expiry_date = value
    }

    const setIsTimerSet = (rootStore?: RootStore, value?: boolean) => {
        self.isTimerSet = value
        if (value && self.expiry_date) {
            runInterval(rootStore)
        } else {
            invalidateTimer()
        }
    }

    const runInterval = async (rootStore?: RootStore) => {
        if (self.expiry_timer) return 
        const timeTickingProcess = async () => {
            const serverDate = moment(rootStore?.getSharedStore?.getServerTime)
            const expiryDate = moment(self.expiry_date)
            if (!expiryDate) return setIsTimerSet(null, false)
            const isTimeUp = serverDate.isSameOrAfter(expiryDate)
            if (isTimeUp) {
                self.is_highlight = !isTimeUp
                setIsTimerSet(null, !isTimeUp)
                await rootStore?.getUserStore?.invalidateDudeHighlight(self.uid)
                return 
            }
            isAlive(self) ? self.expiryDateCount = moment.duration(expiryDate.diff(serverDate)).asMilliseconds() : invalidateTimer()
        }
        await timeTickingProcess()
        if (!self.expiry_timer) {
            self.expiry_timer = BackgroundTimer.setInterval(async () => await timeTickingProcess(), 1000)
        }
    }

    const invalidateTimer = () => {
        if (self.expiry_timer)
            BackgroundTimer.clearInterval(self.expiry_timer)
    }

    const _setReadToNotificationByIndex = (existingList: INotification[], index: number) => {
        const targetItem = existingList[index]
        targetItem.misc.is_read = true
        if (self.notificationList?.length > 0) {
            existingList[index] = targetItem
            self.notificationList = existingList
        }
    }

    const setNotificationList = (notificationList?: INotification[]) => self.notificationList = notificationList
    const addNotificationToList = (notification?: INotification) => {
        const list = self.notificationList?.slice() || []
        const isExist = list && list.findIndex(e => e.notification_id === notification.notification_id) > -1
        if (!isExist) list.splice(0, 0, notification)
        self.notificationList = list
    }
    const setReadNotificationById = (id?: string) => {
        if (!id) return
        const tempList = self.notificationList
        const existingList = tempList?.slice()
        const foundItemIndex = existingList?.findIndex(v => v.notification_id === id)
        if (foundItemIndex === -1) return 
        _setReadToNotificationByIndex(existingList, foundItemIndex)
    }
    const setReadNotificationByTopic = (topic?: string) => {
        if (!topic) return 
        const tempList = self.notificationList
        const existingList: INotification[] = tempList?.slice()
        existingList?.forEach((v, i) => {
            if (v.topic === topic) _setReadToNotificationByIndex(existingList, i)
        })
    }

    const setIsManaoTester = (value: boolean) => {
        self.is_manao_tester = value
    }

    const setOrganizationId = (value: string) => {
        self.organization_id = value
    }

    const setBadgeNumber = (value: number) => {
        self.badge_number = value
    }

    const bindingDataFromObject = (dudeProfileObject?: any) => {
        if (!dudeProfileObject) return 

        const profile: UserStore = { ...dudeProfileObject }
        setUserId(profile?.uid)
        setDescription(profile?.description)

        setStatus(profile?.status)

        setExpiryDate(profile?.expiry_date)
        setIsManaoTester(profile?.is_manao_tester)
    }

    return {
        setUser,
        setPassword,
        setMembershipNumber,
        setUserId,

        setDeviceToken,
        setDescription,

        setStatus,
        bindingDataFromObject,

        setIsTimerSet,
        runInterval,

        setExpiryDate,
        setNotificationList,
        addNotificationToList,
        setReadNotificationById,
        setReadNotificationByTopic,

        setIsManaoTester,

        setOrganizationId,
        setBadgeNumber,
    }
})