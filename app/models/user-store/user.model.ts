import { types } from "mobx-state-tree"
import { ISelect, IUser } from './user.types'
import { IUploadFileData } from '../../utils/firebase/fire-storage/fire-storage.types'
import { Image } from 'react-native-image-crop-picker'
import { Property, MobileProperty } from "../../constants/firebase/fire-store.constant"
import { INotification } from "../notification-store/notification-store.types"

export const UserPropsModel = {
    [Property.UID]: types.maybeNull(types.string),

    //As a general object can get this variable.

    [Property.USER_ID]: types.maybeNull(types.string),
    [Property.IS_HIGHLIGHT]: types.optional(types.boolean, true),

    [Property.FULL_NAME]: types.maybeNull(types.string),
    [Property.EMAIL]: types.maybeNull(types.string),
    [Property.MEMBERSHIP]: types.maybeNull(types.string),
    [MobileProperty.PASSWORD]: types.maybeNull(types.string),

    [Property.MEMBERSHIP_NUM]: types.maybeNull(types.string),
    
    [MobileProperty.CONFIRM_PASSWORD]: types.maybeNull(types.string),
    [Property.DESCRIPTION]: types.maybeNull(types.string),
    [Property.STATUS]: types.maybeNull(types.string),

    //As a user object can get this variable.
    // [Property.COUNTRY]: types.maybeNull(types.frozen<ISelect>()),
    [Property.IS_VERIFIED]: types.maybeNull(types.boolean),    

    [Property.AGE]: types.maybeNull(types.frozen<ISelect>()),
    [Property.ROLE]: types.maybeNull(types.string),
    [Property.CREATED_DATE]: types.maybeNull(types.frozen()),
    [Property.UPDATED_DATE]: types.maybeNull(types.frozen()),
    [Property.DEVICE_TOKEN]: types.maybeNull(types.string),


    

    

    [Property.PROFILE_IMAGE]: types.optional(types.frozen<(Partial<IUploadFileData & Image>)[]>(), []),

    
    [MobileProperty.NOTIFICATION_LIST]: types.optional(types.frozen<Partial<INotification>[]>(), []),


    
    //Payment

    [Property.EXPIRY_DATE]: types.maybeNull(types.frozen()),

    //Time tricking handling
    [MobileProperty.EXPIRY_DATE_COUNT]: types.optional(types.number, 0),
    [MobileProperty.IS_TIMER_SET]: types.optional(types.boolean, false),
    [MobileProperty.EXPIRY_TIMER]: types.maybeNull(types.frozen()),

    //images handling
    [MobileProperty.COMPARE_PROFILE_IMAGE]: types.optional(types.frozen<(Partial<IUploadFileData & Image>)[]>(), []),
    [MobileProperty.COMPARE_GALLERY_IMAGE_LIST]: types.optional(types.frozen<(Partial<IUploadFileData & Image>)[]>(), []),
    [MobileProperty.PROFILE_IMAGE_TO_REMOVE]: types.optional(types.frozen<(Partial<IUploadFileData & Image>)[]>(), []),
    [Property.IS_MANAO_TESTER]: types.optional(types.boolean, false),

    //BD
    organization_id: types.optional(types.string, ""),

    badge_number: types.optional(types.number, 0 ),

}