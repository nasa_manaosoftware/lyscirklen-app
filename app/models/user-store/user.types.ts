import { Instance, SnapshotOut } from "mobx-state-tree"
import { UserStoreModel } from "./user.store"
import { Property, MobileProperty, MediaProperty } from "../../constants/firebase/fire-store.constant"
export type UserStore = Instance<typeof UserStoreModel>
export type UserStoreSnapshot = SnapshotOut<typeof UserStoreModel>

export type ISelect = {
    key?: string,
    value?: string
}

export type IFireStorage = {
    [MediaProperty.STORAGE_PATH]?: string,
    [MediaProperty.FILE_NAME]?: string,
    [MediaProperty.URL]?: string
}

export type IUser = {
    [Property.UID]?: string,

    [Property.USER_ID]?: string,
    [Property.FULL_NAME]?: string,
    [Property.EMAIL]?: string,

    [Property.DESCRIPTION]?: string,
    [MobileProperty.PASSWORD]?: string,
    [MobileProperty.CONFIRM_PASSWORD]?: string,

    [Property.ROLE]?: string,
    [Property.PROFILE_IMAGE]?: IFireStorage,
    // [Property.GALLERY_IMAGE_LIST]?: IFireStorage[],
    [Property.IS_HIGHLIGHT]?: boolean,
    [Property.CREATED_DATE]?: any,
    [Property.UPDATED_DATE]?: any,
    [Property.IS_VERIFIED]?: boolean,

    [Property.STATUS]?: string,
    [MobileProperty.PROFILE_IMAGE_TO_REMOVE]?: IFireStorage[],
    [MobileProperty.COMPARE_PROFILE_IMAGE]?: IFireStorage[],
    [MobileProperty.COMPARE_GALLERY_IMAGE_LIST]?: IFireStorage[],
    [MobileProperty.EXPIRY_DATE_COUNT]?: number,
    [MobileProperty.IS_TIMER_SET]?: boolean,
    [Property.EXPIRY_DATE]?: any,
    [Property.IS_MANAO_TESTER]?: boolean,
}

export type ISEARCH_AUDIO_BOOK = {
    name: string,
    description?: string,
    forfatter?: string,
    uid?: string,

    image : {
        url?: string,
    }
}

export type IListPage = {
    [Property.PAGE_SIZE]?: number,
    [Property.IS_LAST_PAGE]?: boolean,
    [Property.LAST_ID]?: string,
}

