import { types } from "mobx-state-tree"
import { UserServiceAuthActions } from "./user.authentication.services"
import { UserRegisterServiceActions } from "./user.register.services"
import { UserDetailsServiceActions } from "./user.user-details.services"

export const UserServiceActions = types.compose(
    UserServiceAuthActions,
    UserRegisterServiceActions,
    UserDetailsServiceActions
)