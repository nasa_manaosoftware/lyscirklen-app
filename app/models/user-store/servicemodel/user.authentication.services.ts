import { types, flow } from "mobx-state-tree"
import { UserPropsModel } from "../user.model"
import AuthServices from "../../../services/api-domain/auth.services"
import { RootStore } from "../../root-store/root.types"
import PushNotificationIOS from "@react-native-community/push-notification-ios"
import PushNotification from "react-native-push-notification"
import * as metric from "../../../theme"
import ShortcutBadge from "react-native-app-badge"

export const UserServiceAuthActions = types.model(UserPropsModel).actions(self => {
    const signInWithEmailAndPassword = flow(function* () {
        const params = { email: self.email, password: self.password }
        // return yield AuthServices.signInWithEmailAndPassword(params)
        return true
    })

    const sendPasswordResetEmail = flow(function* () {
        // return yield AuthServices.sendPasswordResetEmail({email: self.email})
        return true
    })
    const forceLogoutWithoutAuth = (rootStore: RootStore) => {
        if (metric.isIPhone) PushNotificationIOS.setApplicationIconBadgeNumber(0)
        PushNotification.setApplicationIconBadgeNumber(0)

        rootStore?.getSharedStore?.setIsTimerSet(null, false)
        rootStore?.resetRootStore()
    }

    return {
        signInWithEmailAndPassword,
        sendPasswordResetEmail,
        forceLogoutWithoutAuth
    }
})