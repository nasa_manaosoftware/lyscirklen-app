import { types, flow } from "mobx-state-tree"
import { UserPropsModel } from "../user.model"
import UserServices from "../../../services/api-domain/user.services"

export const UserDetailsServiceActions = types.model(UserPropsModel).actions(self => {
    const updateDeviceToken = flow(function* (token: string) {
        return yield UserServices.updateDeviceToken(token)
    })

    const signOut = flow(function* () {
        if (!self.device_token) return true
        return yield UserServices.signOut(self.device_token)
        return true
    })

    return {
        updateDeviceToken,
        signOut
    }
})