import { types } from "mobx-state-tree"
import { ValidationPropsModel } from "../validation-store.model"

export const ValidationActions = types.model(ValidationPropsModel).actions(self => {
    const setFullNameErrorMessage = (value: string) => {
        self.fullNameErrorMessage = value
    }
    const setEmailErrorMessage = (value: string) => {
        self.emailErrorMessage = value
    }
    const setCountryErrorMessage = (value: string) => {
        self.countryErrorMessage = value
    }
    const setCityErrorMessage = (value: string) => {
        self.cityErrorMessage = value
    }
    const setAgeErrorMessage = (value: string) => {
        self.ageErrorMessage = value
    }
    const setPasswordErrorMessage = (value: string) => {
        self.passwordErrorMessage = value
    }
    const setConfirmPasswordErrorMessage = (value: string) => {
        self.confirmPasswordErrorMessage = value
    }
    const setIsValid = (value: boolean) => {
        self.isValid = value
    }
    const setIsCreateDudeValid = (value: boolean) => {
        self.isCreateDudeValid = value
    }
    const setTagListErrorMessage = (value: string) => {
        self.tagListErrorMessage = value
    }
    const setHavdSogerHanListErrorMessage = (value: string) => {
        self.havdSongerHanListErrorMessage = value
    }
    const setInterestListErrorMessage = (value: string) => {
        self.interestListErrorMessage = value
    }
    const setDescriptionErrorMessage = (value: string) => {
        self.descriptionErrorMessage = value
    }
    const setProfileImageErrorMessage = (value: string) => {
        self.profileImageErrorMessage = value
    }
    const setGalleryImageErrorMessage = (value: string) => {
        self.galleryImageErrorMessage = value
    }
    const setLoginErrorMessage = (value: string) => {
        self.loginErrorMessage = value
    }
    const setSigninErrorMessage = (value: string) => {
        self.signinErrorMessage = value
    }
    const setMembershipErrorMessage = (value: string) => {
        self.membershipErrorMessage = value
    }

    return {
        setFullNameErrorMessage,
        setEmailErrorMessage,
        setCountryErrorMessage,
        setCityErrorMessage,
        setAgeErrorMessage,
        setPasswordErrorMessage,
        setConfirmPasswordErrorMessage,
        setMembershipErrorMessage,
        setIsValid,
        setIsCreateDudeValid,
        setTagListErrorMessage,
        setHavdSogerHanListErrorMessage,
        setInterestListErrorMessage,
        setDescriptionErrorMessage,
        setProfileImageErrorMessage,
        setGalleryImageErrorMessage,
        setLoginErrorMessage,
        setSigninErrorMessage
    }
})