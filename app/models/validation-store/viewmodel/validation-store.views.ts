import { types } from "mobx-state-tree"
import { ValidationPropsModel } from "../validation-store.model"

export const ValidationViews = types.model(ValidationPropsModel).views(self => ({
    get getIsValid(): boolean {
        return false
    },
    get getAllUserCredentialsValid(): boolean {
        return true
    },
    get getFullNameErrorMessage(): string {
        return self.fullNameErrorMessage
    },
    get getEmailErrorMessage(): string {
        return self.emailErrorMessage
    },
    get getCountryErrorMessage(): string {
        return self.countryErrorMessage
    },
    get getCityErrorMessage(): string {
        return self.cityErrorMessage
    },
    get getAgeErrorMessage(): string {
        return self.ageErrorMessage
    },
    get getPasswordErrorMessage(): string {
        return self.passwordErrorMessage
    },
    get getConfirmPasswordErrorMessage(): string {
        return self.confirmPasswordErrorMessage
    },
    get getTagListErrorMessage(): string {
        return self.tagListErrorMessage
    },
    get getHavdSogerHanListErrorMessage(): string {
        return self.havdSongerHanListErrorMessage
    },
    get getInterestListErrorMessage(): string {
        return self.interestListErrorMessage
    },
    get getDescriptionErrorMessage(): string {
        return self.descriptionErrorMessage
    },
    get getProfileImageErrorMessage(): string {
        return self.profileImageErrorMessage
    },
    get getGalleryImageErrorMessage(): string {
        return self.galleryImageErrorMessage
    },
    get getLoginErrorMessage(): string {
        return self.loginErrorMessage
    }
}))