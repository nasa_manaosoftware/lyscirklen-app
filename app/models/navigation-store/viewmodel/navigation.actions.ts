import { types } from "mobx-state-tree"
import { NavigationPropsModel } from "../navigation.model"

export const NavigationActions = types.model(NavigationPropsModel).actions(self => {
    const setPreviousState = (value: any) => self.previousState = value
    const setCurrentState = (value: any) => self.currentState = value
    const setNavigation = (value?: any) => self.navigation = value
    const setNavigationTo = (value?: any) => self.navigationTo = value
    return { setPreviousState, setCurrentState, setNavigation, setNavigationTo }
})