import { types } from "mobx-state-tree"
import { NavigationViews, NavigationActions } from "./viewmodel"
import { NavigationPropsModel } from "./navigation.model"
import { StoreName } from "../../constants/app.constant"

const NavigationModel = types.model(StoreName.Navigation, NavigationPropsModel)

export const NavigationStoreModel = types.compose(
    NavigationModel,
    NavigationViews,
    NavigationActions)