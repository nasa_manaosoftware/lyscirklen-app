import { Instance, SnapshotOut } from "mobx-state-tree"
import { Property } from "../../constants/firebase/fire-store.constant"
import { AudiobookCategoriesStoreModel} from "./audiobook-category.store"

export type AudiobookCategoriesStore = Instance<typeof AudiobookCategoriesStoreModel>
export type AudiobookCategoriesSnapshot = SnapshotOut<typeof AudiobookCategoriesStoreModel>

export type CATEGORY = {
    [Property.UID]?: string,
    [Property.AUDIO_BOOK_COUNT]?: number,
    [Property.NAME]?: string,
    [Property.ORDER]?: number,
    [Property.ORGANIZATION_ID]?: string,
}