import { types } from "mobx-state-tree";
import { CATEGORY } from "./audiobook-category.types";
import { AUDIOBOOK } from "../../models/audiobook-store/audiobook.types"

export const AudiobookCetegoriesPropsModel = {
    categoryList:  types.optional(types.frozen<(Partial<CATEGORY>)[]>(), []),
    audiobooks:  types.optional(types.frozen<(Partial<AUDIOBOOK>)[]>(), []),
}

