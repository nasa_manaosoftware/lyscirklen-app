import { types } from "mobx-state-tree"
import { AudiobookCetegoriesPropsModel } from "../audiobook-category.model"
import { CATEGORY } from "../audiobook-category.types"
import { AUDIOBOOK } from "../../../models/audiobook-store/audiobook.types"

export const AudiobooksCetegoriesActions = types.model(AudiobookCetegoriesPropsModel).actions(self => {
    const setAudiobooksCetegories = (value: (Partial<CATEGORY>)[]) => {
        self.categoryList = value
    }

    const clearAudiobooksCategoryResult = () => {
        self.audiobooks = []
    }

    const setAudiobooks= (value: (Partial<AUDIOBOOK>)[]) => {
        self.audiobooks = value      
    }

    return {
        setAudiobooksCetegories,
        clearAudiobooksCategoryResult,
        setAudiobooks
    }
})