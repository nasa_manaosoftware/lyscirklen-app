import { types } from "mobx-state-tree";
import { AudiobookCetegoriesPropsModel } from "../audiobook-category.model";
import { CATEGORY } from "../audiobook-category.types";

import { AUDIOBOOK } from "../../../models/audiobook-store/audiobook.types"

export const AudiobookCetegoriesViews = types.model(AudiobookCetegoriesPropsModel).views(self => ({
    get getAudiobooksCetegories(): (Partial<CATEGORY>)[] {
        return self.categoryList
    },

    get getAudiobooks(): (Partial<AUDIOBOOK>)[] {
        return self.audiobooks
    },

}))