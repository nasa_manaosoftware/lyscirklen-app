import { types } from "mobx-state-tree"
import { StoreName } from "../../constants/app.constant"
import { AudiobookCetegoriesPropsModel } from "./audiobook-category.model"
import { AudiobooksCetegoriesActions, AudiobookCetegoriesViews, } from "./viewmodel"

const AudiobookCategoriesStore = types.model(StoreName.AudiobookCategories, AudiobookCetegoriesPropsModel)

export const AudiobookCategoriesStoreModel = types.compose(
    AudiobookCategoriesStore,
    AudiobookCetegoriesViews,
    AudiobooksCetegoriesActions
)