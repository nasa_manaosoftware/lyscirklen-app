import { types } from "mobx-state-tree"

export const SearchPropsModel = {
    author: types.maybeNull( types.string ),
    title: types.maybeNull( types.string ),
    bookCover: types.maybeNull( types.string ),
    description: types.maybeNull( types.string ),
    uid: types.maybeNull( types.string ),
    download_status:  types.maybeNull( types.string ),
}