import { Instance, SnapshotOut } from "mobx-state-tree"
import { SearchStoreModel } from "./search-store.store"
export type SearchStore = Instance<typeof SearchStoreModel>
export type SearchStoreSnapshot = SnapshotOut<typeof SearchStoreModel>



export interface ISearch  {
    author: string,
    title: string,
    bookCover: string,
    description: string,
    uid: string,
    download_status: string
}

