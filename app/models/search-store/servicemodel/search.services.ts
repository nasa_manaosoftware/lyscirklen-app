import { types, flow } from "mobx-state-tree"
import { SearchPropsModel } from "../search-store.model"

export const SearchServiceActions = types.model(SearchPropsModel).actions(self => {

    /*
         No need to use `flow` and `yield` because we don't care about any responses.
    */

    // const markReadNotificationByTopic = (topic: string) => {
    //     const params: Partial<ISearch> = {
    //         topic: topic
    //     }
    //     SearchServices.markReadCommentNotificationByTopic(params)
    // }

    return {
    }
})