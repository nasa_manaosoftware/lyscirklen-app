import { types } from "mobx-state-tree"
import { SearchViews, SearchActions } from "./viewmodel"
import { SearchPropsModel } from "./search-store.model"
import { StoreName } from "../../constants/app.constant"
import { SearchServiceActions } from "./servicemodel"


export const SearchModel = types.model(StoreName.SearchAudioBook, SearchPropsModel)

export const SearchStoreModel = types.compose(
    SearchModel,
    SearchViews,
    SearchActions,
    SearchServiceActions)

