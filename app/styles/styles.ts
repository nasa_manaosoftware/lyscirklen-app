
import {
    ViewStyle,
    TextStyle
} from 'react-native';
import { type } from '../theme/fonts';

export const FULL: ViewStyle = {
    flex: 1
}

export const FONT_REGULAR: TextStyle = {
    fontWeight: '100',
    fontSize: 32,
    fontFamily: type.base,
    // fontFamily: "Teko-Regular",
}

export const FONT_LIGHT: TextStyle = {
    // fontWeight: '300',
    // fontFamily: "pt-sans.narrow",
    // fontFamily: "Teko-Regular",
}

export const FONT_BOLD: TextStyle = {
    // fontWeight: '600',
    // fontFamily: "PTSans-Bold",
    // fontFamily: "Teko-Regular",
}

export const FONT_MEDIUM: TextStyle = {
    fontWeight: '500',
    // fontFamily: "PTSans-Regular",
    // fontFamily: "Teko-Regular",
}