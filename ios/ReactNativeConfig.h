//
//  ReactNativeConfig.h
//  Lyscirklen
//
//  Created by Kasidit Kaweevoraluk on 2/3/2563 BE.
//  Copyright © 2563 Facebook. All rights reserved.
//

#ifndef ReactNativeConfig_h
#define ReactNativeConfig_h

#import <React/RCTBridgeModule.h>

@interface ReactNativeConfig : NSObject <RCTBridgeModule>
@end

#endif /* ReactNativeConfig_h */
