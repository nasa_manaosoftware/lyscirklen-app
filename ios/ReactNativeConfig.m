//
//  ReactNativeConfig.m
//  Lyscirklen
//
//  Created by Kasidit Kaweevoraluk on 2/3/2563 BE.
//  Copyright © 2563 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ReactNativeConfig.h"

@implementation ReactNativeConfig

RCT_EXPORT_MODULE();

- (NSDictionary *)constantsToExport
{
  NSString* buildEnvironment = [[[NSBundle mainBundle] infoDictionary] valueForKey:@"BuildEnvironment"];
  NSString *bundleIdentifier = [[NSBundle mainBundle] bundleIdentifier];

    if ([bundleIdentifier containsString:@"com.businessdanmark"]) {
      return @{ @"buildEnvironment": buildEnvironment,
                @"backgroundColor": @"#040236",
                @"playBackgroundColor": @"#0049E4",
                @"infoBackgroundColor": @"#393861",
                @"categoryBackgroundColor": @"#0049E4",
                @"loginBackgroundColor": @"#FF5634",
                @"loginDisableBackgroundColor": @"#949494",
                @"trashBackgroundColor": @"#852318",
                @"inputBackgroundColor": @"#393861",
                @"inputTextColor": @"#FFFFFF",
                
                @"aboutBackgroundColor": @"#FF5634",
                @"loginBackgroundColor": @"#FF5634",
                @"drawBackgroundColor": @"#393861",
                @"messageDetailBackgroundColor": @"#156D99",
                @"networkBackgroundColor": @"#852318",                
                @"pushNotificationBackgroundColor": @"#FF190C",
                @"headerBackgroundColor": @"#FF5634",
                @"secondaryBackgroundColor": @"#F1F1FF",
                @"messagesText": @"#040236",
                @"DOWNLOAD_BACKGROUND_COLOR": @"#FF5634",
                @"VIDEO_SEEK_BAR_COLOR": @"#FF5634",
                @"CANCEL_TEXT_COLOR": @"#01010E",
                @"NETWORK_AWARENESS": @"#852318",
                @"DELETE_BUTTON_COLOR": @"#852318",
                
                /* icon */
                @"SELECTED_ICON": @"BD_SELECTED_ICON",
                @"UN_SELECTED_ICON": @"BD_UN_SELECTED_ICON",
                @"CLOSE_MODAL_ICON": @"BD_CLOSE_MODAL_ICON",
                @"HOME_ACTIVE_ICON": @"BD_HOME_ACTIVE_ICON",
                @"SEARCH_ACTIVE_ICON": @"BD_SEARCH_ACTIVE_ICON",
                @"MY_LIBRARY_ACTIVE_ICON": @"BD_MY_LIBRARY_ACTIVE_ICON",
                @"ABOUT_US_ACTIVE_ICON": @"BD_ABOUT_US_ACTIVE_ICON",
                @"SEARCH_ICON": @"BD_SEARCH_ICON",

                /* logo image */
                @"HEADER_LOGO": @"BD_LOGO"
              };
    } else {
      return @{ @"buildEnvironment": buildEnvironment,
//                @"backgroundColor": @"#536B77",
                @"backgroundColor": @"#121C21",
                @"playBackgroundColor": @"#0096DB",
                @"infoBackgroundColor": @"#156D99",
                @"categoryBackgroundColor": @"#156D99",
                @"loginBackgroundColor": @"#156D99",
                @"loginDisableBackgroundColor": @"#939393",
                @"trashBackgroundColor": @"#FF190C",
                @"inputBackgroundColor": @"#FFFFFF",
                @"inputTextColor": @"#272727",
                
                @"aboutBackgroundColor": @"#156D99",
                @"drawBackgroundColor": @"#156D99",
                @"messageDetailBackgroundColor": @"#FF5634",
                @"networkBackgroundColor": @"#FF190C",
                @"pushNotificationBackgroundColor": @"#FF190C",
                @"headerBackgroundColor": @"#156D99",
                @"secondaryBackgroundColor": @"#F7F8F9",
                @"messagesText": @"#272727",
                @"DOWNLOAD_BACKGROUND_COLOR": @"#156D99",
                @"VIDEO_SEEK_BAR_COLOR": @"#156D99",
                @"CANCEL_TEXT_COLOR": @"#121C21",
                @"NETWORK_AWARENESS": @"#FF190C",
                @"DELETE_BUTTON_COLOR": @"#D2271E",
                
                /* logo image */
                @"SELECTED_ICON": @"ML_SELECTED_ICON",
                @"UN_SELECTED_ICON": @"ML_UN_SELECTED_ICON",
                @"CLOSE_MODAL_ICON": @"ML_CLOSE_MODAL_ICON",
                @"HOME_ACTIVE_ICON": @"ML_HOME_ACTIVE_ICON",
                @"SEARCH_ACTIVE_ICON": @"ML_SEARCH_ACTIVE_ICON",
                @"MY_LIBRARY_ACTIVE_ICON": @"ML_MY_LIBRARY_ACTIVE_ICON",
                @"ABOUT_US_ACTIVE_ICON": @"ML_ABOUT_US_ACTIVE_ICON",
                @"SEARCH_ICON": @"ML_SEARCH_ICON",
                
                /* icon */
                @"HEADER_LOGO": @"ML_LOGO"
            };
    }
  
}

+ (BOOL)requiresMainQueueSetup
{
    return YES;
}

@end
