//
//  AppDelegate+ForceUpdate.m
//  Lyscirklen
//
//  Created by Wiphu Jong Blaine on 1/11/21.
//  Copyright © 2021 Facebook. All rights reserved.
//

#import "AppDelegate+ForceUpdate.h"

@implementation AppDelegate (ForceUpdate)


- (void)checkUpdates:(NSString*)appID updateRequire:(void (^)(bool))updateRequireCB  updateNotRequire:(void (^)(bool))updateNotRequireCB{
  NSString *urlApp = @"http://itunes.apple.com/lookup?id=";
  urlApp = [urlApp stringByAppendingString:appID];
  NSURL *url = [NSURL URLWithString:urlApp];

  NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
  NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration];

  NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithURL: url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
      NSLog(@">Response: %@",response);
      NSLog(@">Data: %@",data);
      NSLog(@">Error: %@",error);
    
    id dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
    if (dict != nil) {

        NSArray *r = [dict valueForKey:@"results"];
        if ( [r count] > 0 ){
          NSString *sversion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
          NSDictionary *o = [r objectAtIndex:0];
          NSString *version = [o valueForKey:@"version"];

          
          if ( [sversion compare:version] == NSOrderedAscending ){
            updateRequireCB( true );
          } else {
            updateNotRequireCB( true ); /* App is up to date. */
          }

        } else {
          updateNotRequireCB( true ); /* App does not exist in the apple store */
        }

      
    } else {
        updateNotRequireCB( true ); /* Something went wrong */
    }
  }];
  [dataTask resume];

  
}

@end
