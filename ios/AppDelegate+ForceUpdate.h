//
//  AppDelegate+ForceUpdate.h
//  Lyscirklen
//
//  Created by Wiphu Jong Blaine on 1/11/21.
//  Copyright © 2021 Facebook. All rights reserved.
//

#import "AppDelegate.h"

NS_ASSUME_NONNULL_BEGIN

@interface AppDelegate (ForceUpdate)

- (void)checkUpdates:(NSString*)appID updateRequire:(void (^)(bool))updateRequireCB  updateNotRequire:(void (^)(bool))updateNotRequireCB;

@end

NS_ASSUME_NONNULL_END
